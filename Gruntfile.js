// Generated on 2014-07-10 using generator-angular 0.7.1
'use strict';

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  grunt.loadNpmTasks('grunt-remove-logging');

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    yeoman: {
      // configurable paths
      app: require('./bower.json').appPath || 'app',
      dist: 'dist'
    },
    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        'Gruntfile.js',
        '<%= yeoman.app %>/**/{,*/}*.js',
        '!<%= yeoman.app %>/bower_components/**/{,*/}*.js',
        '!<%= yeoman.app %>/components/e2etools/pageobject.js',
        '!<%= yeoman.app %>/**/{,*/}*.e2e.js',
        '!<%= yeoman.app %>/**/{,*/}*.spec.js',
        '!<%= yeoman.app %>/**/{,*/}*.pageobject.js'
      ]
    },
/*
    karma: {
      unit: {
        configFile: 'karma.conf.js',
        singleRun: true
      }
    },
    */
    removelogging: {
      dist: {
        src: '<%= yeoman.app %>/modules/**/*.js'
      },
      options: {
        replaceWith : '//console.log removed.'
      }
    }
  });

  grunt.registerTask('noBuild', 'Notifies the user that no dist folder is created', function (arg) {
    var exclamationMarks = '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!';
    var text = exclamationMarks + ' Note: No dist folder is created, Grunt JUST run jsHint and unit tests ' + exclamationMarks;
    //grunt.log.ok(text);
    grunt.log.subhead(text);
  });

  grunt.registerTask('default', [
    'removelogging',
    'newer:jshint',
    //'karma',
    'noBuild'
  ]);
};
