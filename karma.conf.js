// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function(config) {
  config.set({
    // base path, that will be used to resolve files and exclude
    basePath: 'app/',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [
      'bower_components/jquery/dist/jquery.js',
      'bower_components/angular/angular.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'bower_components/angular-resource/angular-resource.js',
      'bower_components/angular-cookies/angular-cookies.js',
      'bower_components/angular-sanitize/angular-sanitize.js',
      'bower_components/lodash/dist/lodash.compat.js',
      'bower_components/restangular/dist/restangular.js',
      'bower_components/less/dist/less.js',
      'bower_components/sass.js/dist/sass.js',
      'bower_components/x2js/xml2json.js',
      'bower_components/angular-x2js/src/x2js.js',
      'bower_components/blueimp-load-image/js/load-image.js',
      'bower_components/blueimp-tmpl/js/tmpl.js',
      'bower_components/blueimp-load-image/js/load-image-ios.js',
      'bower_components/blueimp-load-image/js/load-image-orientation.js',
      'bower_components/blueimp-load-image/js/load-image-meta.js',
      'bower_components/blueimp-load-image/js/load-image-exif.js',
      'bower_components/blueimp-load-image/js/load-image-exif-map.js',
      'bower_components/blueimp-canvas-to-blob/js/canvas-to-blob.js',
      'bower_components/blueimp-file-upload/js/vendor/jquery.ui.widget.js',
      'bower_components/blueimp-file-upload/js/jquery.fileupload.js',
      'bower_components/blueimp-file-upload/js/jquery.fileupload-process.js',
      'bower_components/blueimp-file-upload/js/jquery.fileupload-validate.js',
      'bower_components/blueimp-file-upload/js/jquery.fileupload-image.js',
      'bower_components/blueimp-file-upload/js/jquery.fileupload-audio.js',
      'bower_components/blueimp-file-upload/js/jquery.fileupload-video.js',
      'bower_components/blueimp-file-upload/js/jquery.fileupload-angular.js',
      'bower_components/blueimp-file-upload/js/jquery.iframe-transport.js',
      'modules/msContentItemContactForm/contactForm.html',
      'modules/**/*.js',
      'config.js'
    ],


    preprocessors: {
      'modules/msContentItemContactForm/contactForm.html': 'ng-html2js'
    },

    // list of files / patterns to exclude
    exclude: [
      '**/{,*/}*pageobject.js',
      '**/{,*/}*.e2e.js',
      '**/{,*/}*.min.js',
      'bower_components/es5-shim/**',
      'bower_components/angular-scenario/**',
      'bower_components/json3/**'
    ],

    // web server port
    port: 8080,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['Chrome'],

    reporters: ['progress', 'teamcity'],


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  });
};
