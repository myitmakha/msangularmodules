'use strict';

angular.module('msLogin')
  .directive('msLoginButton', ['$auth', '$rootScope', function ($auth, $rootScope) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        settings: '='
      },
      template:'<div><button ng-if="!logged&&!internalSettings.hideLogin" ng-click="goIdServer()">Login</button><span ng-if="logged&&!internalSettings.hideLogout">Welcome {{userInfo.email}}! <button ng-click="logout()">Logout</button></span></div>',
      link: function (scope, element) {
        scope.internalSettings=scope.settings||{};

        $rootScope.$watch('userInfo',function(userInfo){
          scope.userInfo = userInfo;
        });

        var checkIfLoggedIn = function(){
          scope.logged = $auth.isLoggedIn();
        };
        $rootScope.$on('auth:tokenChange',checkIfLoggedIn);
        checkIfLoggedIn();
        scope.logout = function(){
          $auth.logout();
        };
        scope.goIdServer = function(){
          $auth.redirectToIdServerFromClientSite(scope.internalSettings.provider);
        };

      }
    };
  }]);
