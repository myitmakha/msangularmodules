'use strict';

angular.module('msLogin')
  .directive('msLoginPopup', ['$auth', '$rootScope','$window', 'ngDialog', 'Config', function ($auth, $rootScope,$window,ngDialog,Config) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        settings: '='
      },
      link: function (scope, element) {

        scope.internalSettings=scope.settings||{};
        scope.showPopup=false;
        var showLoginPopup = function(details){
          scope.showPopup = true;
        };
        var provider = scope.internalSettings.provider;
        scope.showCancel = false;

        scope.goIdServer = function(){
          if(scope.redirectUrl){
            $window.location.href=scope.redirectUrl;
          }else if(scope.internalSettings.idServer){
            scope.internalSettings.idServer.provider=provider;
            $auth.redirectToIdServer(scope.internalSettings.idServer);
          }else{
            $auth.redirectToIdServerFromClientSite(provider);
          }
        };
        scope.cancel = function(){
          scope.showPopup = false;
          $auth.logout();
        };

        scope.$on('login:openPopup',function(e,data){
          data=data||{};
          provider = data.provider||scope.internalSettings.provider;
          scope.showCancel = data.showCancel;
          scope.redirectUrl = data.redirectUrl;
          ngDialog.open({ template: Config.GetSessionStoredValue('modulesDirectory') + '/msLogin/loginPopup.tpl.html', scope: scope, closeByDocument: false });
        });
      }
    };
  }]);

