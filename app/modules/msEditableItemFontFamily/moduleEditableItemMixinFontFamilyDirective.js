'use strict';

angular.module('msEditableItemFontFamily')
  .directive('editableItemMixinFontFamilyDirective', ['fontLoader', '$http', 'Config', function (fontLoader, $http, Config) {
    return {
      scope: {
        editable: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msEditableItemFontFamily/editableItemMixinFontFamily.tpl.html',
      restrict: 'A',
      link: function (scope, element, attrs) {
        var panel = element.find('.editable-dialog');

        $(document).on('click', function(e){


          if ( $(event.target).parents().add(event.target).is(element.find('span')) ){
            var elePosition = {
                left: element.offset().left + element.outerWidth(),
                top: element.offset().top - $(window).scrollTop()
              },
              bottomEdge = panel.outerHeight() + elePosition.top,
              screenHeight = $(window).height();

            if (bottomEdge > screenHeight) {
              elePosition.top = elePosition.top - ( (bottomEdge - screenHeight) + 20 );
            }

            panel.addClass('open');

            panel.attr({
              'style': 'display: block; left: ' + elePosition.left + 'px !important; top: ' + elePosition.top + 'px !important;'
            });

          } else if( !$(event.target).parents().add(event.target).hasClass('editable-dialog') ){
            panel.fadeOut(200);
          }

        });

      }
    };
  }]);
