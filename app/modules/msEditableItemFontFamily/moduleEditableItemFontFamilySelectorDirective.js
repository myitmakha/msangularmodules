'use strict';

angular.module('msEditableItemFontFamily')
  .directive('editableItemFontFamilySelectorDirective', ['fontLoader', '$http', function (fontLoader, $http) {
    return {
      scope: {
        ngModel: '=',
        visible:'='
      },
      template: '<div class="font-panel">'+
        '<div class="close"><i class="icon-delete"></i></div>' +
        '<div ms-scrollbar-plus>'+
        '<div class="fonts">'+
        '<div ng-if="visible" style="font-family: {{fontFamily}};"'+
        ' class="font {{escapeFontFamily(fontFamily)}}" ng-class="{active: fontFamily===ngModel}" ng-click="changed(fontFamily)" ng-repeat="fontFamily in fontFamilies">'+
        '{{fontFamily}}'+
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="font-panel-overlay"></div>',
      restrict: 'A',
      link: function (scope, element, attrs) {
        scope.escapeFontFamily = fontLoader.escapeFontFamily;

        //This timeout is to allow the switching to perform quick and load the fonts after the animation happend
        setTimeout(function(){
          scope.fontFamilies = fontLoader.getAll();

          scope.$watch('ngModel',function(name){
            if(!name){
              return;
            }
            var index = fontLoader.getIndexByFont(name);
            preLoadFontsAroundIndexFont(index);
          });
        },200);

        element.find('.fonts').scrollTop(100);

        scope.$watch('visible',function(visible){
          if(!scope.ngModel||!visible){
            return;
          }
          setTimeout(function(){scrollToFont(scope.ngModel);},200);
        });


        scope.changed = function(fontFamily){
          scope.ngModel=fontFamily;
          scope.$parent.$root.$broadcast('markChanges', 'font changed');
        };

        var preLoadFontsAroundIndexFont = function(index){
          var buffer = 20;
          var min = Math.max(0,index-buffer);
          var max = Math.min(scope.fontFamilies.length,index+10+buffer);
          fontLoader.loadFontsByRange(min,max);
        };


        var scrollToFont = function(name){
          var index = fontLoader.getIndexByFont(name);
          var $fonts = element.find('.scroll-content');
          var $font = $fonts.find('.font').eq(index);
          var offsetFont = $font.offset().top;
          var heightFont = $font.outerHeight();
          var offsetFonts = $fonts.offset().top;
          var needToScroll = offsetFont-offsetFonts;
          var currentScroll = $fonts.scrollTop();
          var heightScrollDiv = $fonts.outerHeight();
          var scrollTo = currentScroll+needToScroll-heightScrollDiv/2+heightFont;
          $fonts.scrollTop(scrollTo);
        };

        element.find('.scroll-content').on('scroll',function(e){
          var heightElement = element.find('.font:first').outerHeight()+1;
          var scroll = e.target.scrollTop;
          var currentTop = Math.floor(scroll/heightElement);
          preLoadFontsAroundIndexFont(currentTop);
        });

      }
    };
  }]);