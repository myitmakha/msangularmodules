'use strict';

describe('msEditableItemFontFamily directive', function () {
  // load the controller's module
  beforeEach(module('msEditableItemFontFamily'));

  var pageHtml = '<div editable-item-font-family-directive editable="editable"></div>';
  var scope, elm, $compile;

  //Load scope
  beforeEach(inject(function ($rootScope, _$compile_) {
    scope = $rootScope.$new();
    $compile = _$compile_;
  }));

  var compileDirective = function (editable, tpl) {
    scope.editable = editable || {};
    if (!tpl) tpl = pageHtml;
    tpl = '<form name="form">' + tpl + '</tpl>';
    var form = $compile(tpl)(scope);
    elm = form.children().eq(0);
    scope.$digest();
  };

});
