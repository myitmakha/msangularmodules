'use strict';

angular.module('msEditableItemFontFamily')
  .directive('editableItemFontFamilyDirective', ['fontLoader', '$http', function (fontLoader, $http) {
    return {
      scope: {
        editable: '='
      },
      template: '<div>'+
      '<div ng-if="editable.SettingType==\'variable\'" editable-item-single-font-family-directive editable="editable"></div>'+
      '<div ng-if="editable.SettingType==\'mixin\'" editable-item-mixin-font-family-directive editable="editable"></div>'+
      '</div>',
      restrict: 'A',
      replace:true,
      link: function (scope, element, attrs) {
        
      }
    };
  }]);