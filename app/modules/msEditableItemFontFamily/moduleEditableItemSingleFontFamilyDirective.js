'use strict';

angular.module('msEditableItemFontFamily')
  .directive('editableItemSingleFontFamilyDirective', ['fontLoader', '$http', function (fontLoader, $http) {
    return {
      scope: {
        editable: '='
      },
      template: '<div><span class="label">{{editable.name}}</span>'+
        '<div font-family-picker-directive ng-model="editable.value"></div>'+
        '</div>',
      restrict: 'A',
      link: function (scope, element, attrs) {

      }
    };
  }]);

angular.module('msEditableItemFontFamily')
  .directive('fontFamilyPickerDirective', ['fontLoader', '$http', function (fontLoader, $http) {
    return {
      require:'ngModel',
      scope: {
        ngModel: '='
      },
      template: '<div>'+
        '<div class="active-font" style="font-family: {{fontFamilyName}};">{{fontFamilyName}}</div>'+
        '<div class="font-wrapper" editable-item-font-family-selector-directive ng-model="fontFamilyName" visible="listVisible"></div>'+
        '</div>',
      restrict: 'A',
      link: function (scope, element, attrs, ngModelCtrl) {
        scope.listVisible=false;

        scope.$watch('fontFamilyName',function(fontFamilyName){
          ngModelCtrl.$setViewValue(fontFamilyName);
        });

        var panel = $('.font-panel', element),
            panelClose = $('.close', panel);

        $('.active-font', element).on('click', function () {

          panel.addClass('open');

          scope.listVisible=true;

          panelClose.on('click', function () {
            panel.removeClass('open');
            scope.listVisible=false;
            scope.$apply();
          });

          scope.$apply();
        });

        var postfix='';
        var prefix='';

        ngModelCtrl.$formatters.push(function(modelValue) {
          if(typeof modelValue === 'undefined'){
            return modelValue;
          }
          postfix='';
          prefix='';
          //console.log removed.
          if(modelValue[0]==='"' && modelValue[modelValue.length-1]==='"'){
            postfix='"';
            prefix='"';
            return modelValue.substr(1,modelValue.length-2);
          }
          return modelValue;
        });

        ngModelCtrl.$render = function() {
          scope.fontFamilyName = ngModelCtrl.$viewValue;
          fontLoader.loadFontByName(scope.fontFamilyName);
        };

        ngModelCtrl.$parsers.push(function(viewValue) {
          return prefix + viewValue + postfix;
        });

      }
    };
  }]);