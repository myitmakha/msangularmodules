'use strict';

angular.module('msLayoutManager')
  .factory('LayoutService', ['PageBuilderManager', 'LayoutServiceArrayHelper', 'LayoutServiceFactoryHelper', '$q', '$rootScope', function (PageBuilderManager, LayoutServiceArrayHelper, LayoutServiceFactoryHelper, $q, $rootScope) {
    var recalculateWidths = function (id) {
      searchForElementAndParentsById(id).then(function (element) {
        //console.log removed.
        if (element) {
          var element2BeResized = element.element;
          if (element.parents.length > 1) {
            element2BeResized = element.parents[element.parents.length - 2];
          }
          LayoutServiceArrayHelper.recalculateWidths(element2BeResized, 12);
        }
        $rootScope.$broadcast('recalculateWidth');
      });
    };

    var searchForElementAndParentsById = function (id) {
      var deferred = $q.defer();
      PageBuilderManager.getCurrentPage().then(function (page) {
        var result = LayoutServiceArrayHelper.searchOnPageForElementAndParents(page, 'id', id);
        deferred.resolve(result);
      });
      return deferred.promise;
    };

    var addNestedRow = function (id, isAfter, contentItemData, exceed) {
      var deferred = $q.defer();
      var fakePage = {type: 'page'};
      searchForElementAndParentsById(id).then(function (element) {
        var elementToAdd = LayoutServiceFactoryHelper.getNewElement(fakePage, contentItemData);
        LayoutServiceArrayHelper.addElementOnArray(element.parents[0].content, element.index, elementToAdd);
        moveElement(id, elementToAdd.content[0].id, !isAfter);
        deferred.resolve({success: true});
      });
      $rootScope.$broadcast('markChanges', contentItemData.type + ' content item has been added.');
      return deferred.promise;
    };

    var resizeElement = function (id, size) {
      var deferred = $q.defer();
      searchForElementAndParentsById(id).then(function (element) {
        //console.log removed.
        var nextSibling = element.parents[0].content[element.index + 1];
        if (nextSibling) {
          var resizedSize = LayoutServiceArrayHelper.resizeElement(element.element, nextSibling, size);
          deferred.resolve({size: resizedSize});
          return;
        }

        deferred.resolve({size: 0});
      });
      $rootScope.$broadcast('markChanges', 'content item has been re-sized.');
      return deferred.promise;
    };

    var addElement = function (id, isAfter, contentItemData, exceed) {
      var deferred = $q.defer();
      exceed = exceed || 0;
      searchForElementAndParentsById(id).then(function (element) {
        if (element.parents[0].content.length >= 12 + exceed && element.element.type==='col') {
          deferred.resolve({success: false});
          return;
        }
        var elementToAdd = LayoutServiceFactoryHelper.getNewElement(element.parents[0], contentItemData);
        LayoutServiceArrayHelper.addElementOnArray(element.parents[0].content, element.index + (isAfter ? 1 : 0), elementToAdd);
        if (elementToAdd.type === 'col' || elementToAdd.type === 'row') {
          recalculateWidths(element.parents[0].id);
        }
        deferred.resolve({success: true});
      });
      $rootScope.$broadcast('markChanges', contentItemData.type + ' content item has been added.');
      return deferred.promise;
    };

    var deleteElement = function (id) {
      var deferred = $q.defer();
      searchForElementAndParentsById(id).then(function (element) {
        if (element.parents[0].type!=='page' && element.parents[0].id && element.parents[0].content.length === 1) {
          deleteElement(element.parents[0].id).then(function () {
            deferred.resolve();
          });
        } else {
          LayoutServiceArrayHelper.deleteElementOnArray(element.parents[0].content, element.index);
          //If i only had one sibling and they are on a nested row we have to move out the element from the nested row
          var parentRow = LayoutServiceArrayHelper.getClosestParent(element, 'row');
          if (LayoutServiceArrayHelper.levelOfNestedRows(element) > 1 && parentRow.content.length === 1 && parentRow.content[0].content.length === 1) {
            var siblingThatIsAloneInANestedRow = element.parents[0].content[0].content[0];
            moveElement(siblingThatIsAloneInANestedRow.id, parentRow.id, false);
          }
          recalculateWidths(element.parents[0].id);
          $rootScope.$broadcast('markChanges', 'content item has been deleted.');
          deferred.resolve();
        }
      });
      return deferred.promise;
    };

    var moveElement = function (idFrom, idTo, isAfter) {
      var deferred = $q.defer();
      //console.log removed.
      searchForElementAndParentsById(idFrom).then(function (elementFrom) {
        searchForElementAndParentsById(idTo).then(function (elementTo) {
          var isSameRow = false;
          if (elementTo.parents.length >= 2 && elementFrom.parents.length >= 2) {
            var rowFrom = elementFrom.parents[elementFrom.parents.length - 2];
            var rowTo = elementTo.parents[elementTo.parents.length - 2];
            isSameRow = (rowFrom === rowTo) && elementFrom.parents[0].content.length === 1 && elementFrom.parents.length === 3 && elementTo.parents.length === 2;
          }

          addElement(idTo, isAfter, elementFrom.element, (isSameRow ? 1 : 0)).then(function (result) {
            if (result.success) {
              deleteElement(idFrom);
            }
            deferred.resolve();
          });
        });
      });
      $rootScope.$broadcast('markChanges', 'content item has been moved.');
      return deferred.promise;
    };

    var moveToNestedRow = function (idFrom, idTo, isAfter) {
      var deferred = $q.defer();
      //console.log removed.
      searchForElementAndParentsById(idFrom).then(function (elementFrom) {
        searchForElementAndParentsById(idTo).then(function (elementTo) {
          var isSameRow = false;
          if (elementTo.parents.length >= 2 && elementFrom.parents.length >= 2) {
            var rowFrom = elementFrom.parents[elementFrom.parents.length - 2];
            var rowTo = elementTo.parents[elementTo.parents.length - 2];
            isSameRow = (rowFrom === rowTo) && elementFrom.parents[0].content.length === 1 && elementFrom.parents.length === 3 && elementTo.parents.length === 2;
          }

          addNestedRow(idTo, isAfter, elementFrom.element, (isSameRow ? 1 : 0)).then(function (result) {
            if (result.success) {
              deleteElement(idFrom);
            }
            deferred.resolve();
          });
        });
      });
      $rootScope.$broadcast('markChanges', 'content item has been moved to nested row.');
      return deferred.promise;
    };

    var layoutService = {
      getPageData: PageBuilderManager.getCurrentPage,
      getAvailableWidgets: LayoutServiceFactoryHelper.getAvailableWidgets,
      moveElement: moveElement,
      addElement: addElement,
      deleteElement: deleteElement,
      addNestedRow: addNestedRow,
      moveToNestedRow: moveToNestedRow,
      resizeElement: resizeElement
    };
    return layoutService;
  }]);