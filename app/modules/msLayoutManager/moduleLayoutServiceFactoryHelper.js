'use strict';

angular.module('msLayoutManager')
  .factory('LayoutServiceFactoryHelper', [function () {

    var getWidgetByType = function( contentItemType ) {

      var contentItems = getAvailableWidgets();

      for (var i=0; i < contentItems.length; i++) {
        if (contentItems[i].type === contentItemType) {
          return contentItems[i].name;
        }
      }
    };

    var getAvailableWidgets = function () {
      return [
        {
          name: 'Text',
          icon: '',
          type: 'text'
        },
        {
          name: 'Image',
          icon: '',
          type: 'single-image'
        },
        {
          name: 'Gallery',
          icon: '',
          type: 'slide-show'
        },
        {
          name: 'Social links',
          icon: '',
          type: 'social-link'
        },
        {
          name: 'Twitter',
          icon: '',
          type: 'twitter'
        },
        {
          name: 'Contact Form',
          icon: '',
          type: 'contact-form'
        },
        {
          name: 'Map',
          icon: '',
          type: 'map'
        },
        {
          name: 'Embed',
          icon: '',
          type: 'html'
        },
        {
          name: 'Spacer',
          icon: '',
          type: 'spacer'
        },
        {
          name: 'Video',
          icon: '',
          type: 'video'
        },
        {
          name: 'Divider',
          icon: '',
          type: 'divider'
        },
        {
          name: 'File',
          icon: '',
          type: 'downloadable-file'
        }/*,
        {
          name: 'Mini basket',
          icon: '',
          type: 'shop-mini-basket'
        },
        {
          name: 'Product',
          icon: '',
          type: 'shop-product'
        }*//*,
        {
          name: 'Shop',
          icon: '',
          type: 'shop-category'
        }*/
      ];
    };

    var getNewColumn = function (contentItemData) {
      return {
        id: getRandomId(),
        type: 'col',
        size: 12,
        content: [
          contentItemData
        ]
      };
    };

    var getNewRow = function (contentItemData) {
      return {
        id: getRandomId(),
        type: 'row',
        size: 12,
        content: [
          getNewColumn(contentItemData)
        ]
      };
    };

    var getRandomId = function () {
      return 'id-' + (new Date()).getTime() + '' + Math.floor(Math.random() * 100000);
    };

    var getNewElement = function (element, contentItemData) {
      contentItemData = angular.copy(contentItemData) || {};
      contentItemData.id = getRandomId();

      if (element.type === 'page') {
        return getNewRow(contentItemData);
      }
      if (element.type === 'row') {
        return getNewColumn(contentItemData);
      }
      return contentItemData;
    };

    var LayoutServiceFactoryHelper = {
      getNewElement: getNewElement,
      getAvailableWidgets: getAvailableWidgets,
      getWidgetByType: getWidgetByType
    };

    return LayoutServiceFactoryHelper;

  }]);