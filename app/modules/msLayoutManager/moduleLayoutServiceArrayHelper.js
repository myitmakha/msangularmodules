'use strict';

angular.module('msLayoutManager')

  .factory('LayoutServiceArrayHelper', [function () {

    var getNumberOfSideBySideColumnsOnElement = function (element) {
      var numberOfColumns = 0;
      if (element.content) {
        var maximunNumberOfColumns = 0;
        if (element.type === 'col') {
          maximunNumberOfColumns = 1;
        }
        for (var i = 0; element.content[i]; i++) {
          var columns = getNumberOfSideBySideColumnsOnElement(element.content[i]);
          if (element.type === 'col') {
            maximunNumberOfColumns = Math.max(maximunNumberOfColumns, columns);
          } else {
            maximunNumberOfColumns += columns;
          }
        }
        numberOfColumns = maximunNumberOfColumns;
      }
      return numberOfColumns;
    };
    var resizeElement = function (elementLeft, elementRight, sizeElementLeft) {
      var currentSizeElementLeft = elementLeft.size;
      var currentSizeElementRight = elementRight.size;
      var minElementsLeft = getNumberOfSideBySideColumnsOnElement(elementLeft);
      var minElementsRight = getNumberOfSideBySideColumnsOnElement(elementRight);
      var totalSize = (currentSizeElementRight + currentSizeElementLeft);
      var maxElementsLeft = totalSize - minElementsRight;
      sizeElementLeft = Math.max(sizeElementLeft, minElementsLeft);
      sizeElementLeft = Math.min(sizeElementLeft, maxElementsLeft);
      elementLeft.size = sizeElementLeft;
      elementRight.size = totalSize - sizeElementLeft;
      recalculateWidths(elementLeft, elementLeft.size, minElementsLeft);
      recalculateWidths(elementRight, elementRight.size, minElementsRight);
      return sizeElementLeft;
    };

    var recalculateWidths = function (element, maxColumns, currentColumns) {
      if(!element){
        return;
      }
      var elements = element.content;
      if(!elements){
        return;
      }
      if (!currentColumns) {
        currentColumns = getNumberOfSideBySideColumnsOnElement(element);
      }

      if (element.type === 'page') {
        for (var iRow = 0; elements[iRow]; iRow++) {
          recalculateWidths(elements[iRow], maxColumns);
        }
      } else if (element.type === 'row') {
        var num = Math.floor(maxColumns / currentColumns);
        var elementsPlus1 = maxColumns - num * currentColumns;
        for (var iCol = 0; elements[iCol]; iCol++) {
          var numColsInCol = getNumberOfSideBySideColumnsOnElement(elements[iCol]);
          var maxColsInCol = numColsInCol * num + Math.min(numColsInCol, elementsPlus1);
          elementsPlus1 -= Math.min(numColsInCol, elementsPlus1);
          elements[iCol].size = maxColsInCol;
          recalculateWidths(elements[iCol], maxColsInCol, currentColumns);
        }
      } else if (element.type === 'col') {
        for (var iNext = 0; elements[iNext]; iNext++) {
          if (elements[iNext].type === 'row') {
            recalculateWidths(elements[iNext], maxColumns);
          }
        }
      }
    };

    var recurseElementsAndReturnTargetAndParents = function (elements, field, value) {
      for (var i = 0; elements[i]; i++) {
        var element = elements[i];
        if (element[field] === value) {
          return {index: i, element: element, parents: []};
        }
        if (element.content) {
          var searchRecursive = recurseElementsAndReturnTargetAndParents(element.content, field, value);
          if (searchRecursive) {
            searchRecursive.parents.push(element);
            return searchRecursive;
          }
        }
      }
      return;
    };
    var searchOnPageForElementAndParents = function (page, field, value) {
      page.content = page.pagecontentblocks;
      page.type = 'page';
      var result;
      if (!page.content[0]) { //Means that there is no data on page lets simulate a imaginary row to append the content item to
        result = {index: 0, element: {type: 'row'}, parents: []};
      } else {
        result = recurseElementsAndReturnTargetAndParents(page.content, field, value);
      }
      if (result) {
        result.parents.push(page);
      }
      return result;
    };

    var addElementOnArray = function (elements, i, element) {
      elements.splice(i, 0, element);
    };

    var deleteElementOnArray = function (elements, i) {
      elements.splice(i, 1);
    };

    var levelOfNestedRows = function (element) {
      var rows = 0;
      for (var i = 0; element && element.parents && element.parents[i]; i++) {
        if (element.parents[i].type === 'row') {
          rows++;
        }
      }
      return rows;
    };

    var getClosestParent = function (element, type) {
      for (var i = 0; element && element.parents && element.parents[i]; i++) {
        if (element.parents[i].type === type || !type) {
          return element.parents[i];
        }
      }
      return;
    };

    var LayoutServiceArrayHelper = {
      searchOnPageForElementAndParents: searchOnPageForElementAndParents,
      addElementOnArray: addElementOnArray,
      deleteElementOnArray: deleteElementOnArray,
      recalculateWidths: recalculateWidths,
      resizeElement: resizeElement,
      levelOfNestedRows: levelOfNestedRows,
      getClosestParent: getClosestParent
    };
    return LayoutServiceArrayHelper;
  }]);