/**
 * Created by Albert on 15/07/2014.
 */
'use strict';
angular.module('msContentItemSpacer')
  .directive('msContentItemSpacer', [function () {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      template:'<div style="height:{{contentItemData.settings.height}}px" class="ms-spacer"></div>',
      link: function (scope) {
        //console.log removed.

        scope.contentItemData.settings = scope.contentItemData.settings||{};
        scope.contentItemData.settings.height = scope.contentItemData.settings.height||20;

        //console.log removed.
      }
    };
  }]);