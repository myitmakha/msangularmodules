/**
 * Created by Albert on 15/07/2014.
 */
'use strict';
angular.module('msContentItemSpacer')
  .directive('msContentItemSpacerSettings', ['$rootScope', function ($rootScope) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        settings: '='
      },
      template: '<div>' +
        '<div class="row-fix">' +
        '<div class="col-fix14">' +
        '<label>Height</label>' +
        '<div class="slider"></div>' +
        '</div>' +
        '</div>' +
        '</div>',

      link: function (scope, element) {
        scope.settings = scope.settings||{height:100};
        $('.slider', element).slider({
          min: 5,
          max: 800,
          range: 'min',
          value: scope.settings.height,
          slide: function (event, ui) {
            scope.settings.height = ui.value;
            scope.$apply();
          }
        });

        scope.$watch('settings', function (oldValue, newValue) {
          if (oldValue !== newValue) {
            $rootScope.$broadcast('markChanges', 'spacer settings has been changed.');
          }
        }, true);
      }
    };
  }]);
