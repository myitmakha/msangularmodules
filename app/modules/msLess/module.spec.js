'use strict';

describe('LessManager Factory', function () {

  var scope, LessManager;

  var editables = [
    {name: 'bgColor', type: 'color', variable: 'bg-color', value: 'red'},
    {name: 'fontFamily', type: 'fontFamily', variable: 'font-family', value: 'Commic Sans'},
    {name: 'fontSize', type: 'slider', variable: 'font-size', value: '16'},
    {name: 'logo', type: 'image', variable: 'logo-image', value: 'http://logo.com/logo.png'}
  ];

  var lessSettings = '@bg-color:red;@font-family:Commic Sans;@font-size:16;@logo-image:\'http://logo.com/logo.png\';';

  var lessCss = 'body{background-color:@bg-color;}';

  beforeEach(module('msLess'));

  beforeEach(inject(function ($rootScope, _$compile_, _$q_, _LessManager_) {
    scope = $rootScope.$new();
    LessManager = _LessManager_;
  }));

  it('should create the less from editables', function () {
    LessManager.editablesToSettings(editables).then(function (lessConverted) {
      expect(lessConverted).toEqual(lessSettings);
    });
    scope.$apply();
  });

  it('should compile less to css', function () {
    LessManager.compile2Css(lessSettings + lessCss).then(function (lessSettings) {
      expect(lessSettings.css).toContain('background-color: red;');
    });
    scope.$apply();
  });
});
