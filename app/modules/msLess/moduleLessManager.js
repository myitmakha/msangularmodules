'use strict';

angular.module('msLess')
  .factory('LessManager', ['$q', function ($q) {
    var editablesToSettings = function (editables) {
      var deferred = $q.defer();
      var editablesLess = '';
      for (var i = 0; i < editables.length; i++) {
        var editable = editables[i];
        if(editable.SettingType==='mixin'){
          var mixinEditableLess='.'+editable.Variable+'{';
          var variablesMixinEditableLess='';
          if(editable.Variables){
            for (var variableName in editable.Variables) {
              if (editable.Variables.hasOwnProperty(variableName)) {
                var variable = editable.Variables[variableName];
                variablesMixinEditableLess += '@' + editable.Variable + '-' + variableName+':'+variable.Value+';';
                mixinEditableLess+=variableName+':'+variable.Value+';';
              }
            }
          }
          mixinEditableLess+='}';
          editablesLess+=variablesMixinEditableLess+mixinEditableLess;
        }else if (editable.type === 'image' && !(editable.value[0]==='\''||editable.value[0]==='"')) {
          editablesLess += '@' + editable.variable + ':\'' + editable.value + '\';';
        } else {
          editablesLess += '@' + editable.variable + ':' + editable.value + ';';
        }
      }
      //console.log removed.
      deferred.resolve(editablesLess);
      return deferred.promise;
    };

    var compile2Css = function (css) {
      var deferred = $q.defer();

      //var parser = new less.Parser({},{contents:[]},{filename:'a'});
      less.render(css, function (err, tree) {
        if (err) {
          deferred.resolve({err:err});
        }else{
          deferred.resolve(tree);
        }
      });
      return deferred.promise;
    };

    return {
      editablesToSettings: editablesToSettings,
      compile2Css: compile2Css
    };
  }]);