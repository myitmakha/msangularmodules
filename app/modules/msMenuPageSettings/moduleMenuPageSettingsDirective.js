'use strict';

angular.module('msMenuPageSettings')
  .directive('msMenuPageSettings', ['$timeout','MenuManager', '$rootScope', function ($timeout,MenuManager,$rootScope) {
    return{
      restrict: 'A',
      template:'<div>&nbsp;<div ng-class="{hide:!hasChildren||hideSubmenuOptions}">Not possible to move under another menu item because has submenus</div><select ng-model="pageParentId" ng-class="{hide:hasChildren||hideSubmenuOptions}" ng-change="parentSelected()" ng-options="rootPage.id as rootPage.name for rootPage in rootPages | filter:{ id: \'!\'+page.id }"><option value="" selected>Top level menu item</option></select><div class="slide"></div>{{pageOrder}} - {{maxPageOrdering}}</div>',
      scope:{
        page:'='
      },
      link: function (scope, element, attrs) {
        scope.hideSubmenuOptions = false;
        var $slide;
        var initSlide = function(){
          $slide = element.find('.slide').slider({
            min: 0,
            max: 100,
            value: 0,
            slide: function (event, ui) {
              scope.pageOrder = ui.value;
              scope.$apply();
            }
          });
        };

        var updateSlide = function(value, max){
          if(max||max===0) {
            $slide.slider('option', 'max', max); // left handle should be at the left end, but it doesn't move
            if(max===0){
              $slide.fadeOut();
            }else{
              $slide.fadeIn();
            }
          }
          $slide.slider('value', value); //force the view refresh, re-setting the current value
        };
        initSlide();
        var refreshMenuSettings = function(){
          if(scope.page) {
            var menuItemSearch = MenuManager.getParentAndPosition(scope.page.id);
            var pageId = '';
            if(menuItemSearch.parentPage){
              pageId = menuItemSearch.parentPage.id;
            }
            scope.pageParentId = pageId;
            scope.maxPageOrdering = menuItemSearch.menuList.length-1;
            scope.pageOrder = menuItemSearch.position;
            scope.hasChildren = menuItemSearch.menu.subMenuItems.length;

            updateSlide(scope.pageOrder, scope.maxPageOrdering);

            ////console.log removed.
          }
        };
        scope.$watch('page',function(){
          refreshMenuSettings();
        });

        scope.$watch('pageOrder',function(newPageOrder, oldPageOrder){
          //console.log removed.
          if((newPageOrder||newPageOrder===0)&&(oldPageOrder||oldPageOrder===0)&&newPageOrder!==oldPageOrder) {
            //console.log removed.
            MenuManager.changePosition(scope.page.id, newPageOrder);
          }
        });

        $rootScope.$watch('siteData.menu',function(siteData){
          if(siteData){
            scope.rootPages = MenuManager.getRootPages();
            ////console.log removed.
          }
        });
        scope.parentSelected = function(){
          MenuManager.changeParent(scope.page.id, scope.pageParentId);
          refreshMenuSettings();
          ////console.log removed.
        };

      }
    };
  }]);