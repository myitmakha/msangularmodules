'use strict';

angular.module('msContentItemBlogPost')
  .directive('msContentItemBlogPost', ['$compile','BlogRepository','Config', 'DiscussionRepository', function ($compile,BlogRepository,Config, DiscussionRepository) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '=',
        contentItemMetaData: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemBlogPost/msBlogPost.tpl.html',

      link: function (scope, element) {

        scope.svgUrl = function( link ){
          return Config.GetEndPoint('modulesDirectory') + '/msContentItemBlog/sprites.svg#'+link;
        };

        scope.back = function(){
          scope.contentItemMetaData.setUrlParamsByPageType('blog');
        };

        var loadBlogPost = function(blogPostId){
          if(!blogPostId){
            scope.error='Blog post not defined.';
            return;
          }
          scope.contentItemMetaData.loading=true;

          BlogRepository.getPost(blogPostId).then(function(blogPost){
            scope.contentItemMetaData.loading=false;
            delete scope.error;
            delete scope.product;
            if(blogPost.status){
              scope.error='Error blog post not found.';
            }else{
              //console.log removed.
              scope.blogPost=blogPost;

              DiscussionRepository.getAllComments(blogPost.DiscussionId).then(
                function (comments) {
                  scope.blogPost.totalComments = comments.length;
                }
              );

              BlogRepository.getBlog(blogPost.BlogId).then(function(blog){
                delete scope.error;
                delete scope.product;
                if(blogPost.status){
                  scope.error='Error blog post not found.';
                }else{
                  //console.log removed.
                  scope.blog=blog;


                }
              });



            }
          });

        };
        var isStandaloneContentItem=scope.contentItemData.type==='blog-post';

        var blogPostId;
        scope.showLeftBar = true;
        scope.showBackToShopButton = true;
        if(isStandaloneContentItem){
          scope.$watch('contentItemData.settings.blogPostId',function(blogPostId){
            loadBlogPost(blogPostId);
          });
          scope.showLeftBar = false;
          scope.showBackToShopButton = false;
        }else if(scope.contentItemMetaData&&scope.contentItemMetaData.urlParams&&scope.contentItemMetaData.urlParams.param2){
          blogPostId=scope.contentItemMetaData.urlParams.param2;
          loadBlogPost(blogPostId);
        }else{
          scope.error='Blog post not found.';
        }
      }
    };
  }]);