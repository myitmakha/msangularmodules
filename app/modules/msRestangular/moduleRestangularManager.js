'use strict';

angular.module('msRestangular')
  .factory('RestangularManager', ['$rootScope','$q',function ($rootScope,$q) {

    var init = function(RestangularConfigurer){

      RestangularConfigurer.setFullResponse(true);
      /*RestangularConfigurer.addResponseInterceptor(function (data, operation, what, url, response, deferred) {
        //console.log removed.
        //data, put, resource requested, absolute url, data+status+headers, deferred
        if(response.status === 401){
          
        }
        return data;
      });*/
      RestangularConfigurer.setErrorInterceptor(function(response, deferred, responseHandler) {
        if(response.status === 401) {
          /*refreshAccesstoken().then(function() {
            // Repeat the request and then call the handlers the usual way.
            $http(response.config).then(responseHandler, deferred.reject);
            // Be aware that no request interceptors are called this way.
          });*/
          
          $rootScope.$broadcast('restangular:needLogin',{
            response:response
          });
          //return false; // error handled
        }

        return true; // error not handled
      });
    };

    var restangularManager = {
      init:init
    };
    return restangularManager;
  }]);


