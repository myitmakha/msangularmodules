'use strict';

angular.module('msRestangular')
  .factory('AppRestangular', ['Restangular', function (Restangular) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer.setBaseUrl('/');
    });
  }]);