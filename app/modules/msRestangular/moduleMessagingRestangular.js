'use strict';

angular.module('msRestangular')
  .factory('MessagingRestangular', ['Restangular', 'Config', '$location','RestangularManager', function (Restangular, Config, $location,RestangularManager) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularManager.init(RestangularConfigurer);
      RestangularConfigurer.setBaseUrl(Config.GetEndPoint('messagingEndpoint') + 'mail/');
    });
  }]);


