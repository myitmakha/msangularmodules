'use strict';

angular.module('msRestangular', ['restangular', 'msConfig'])
  .config(['RestangularProvider', function (RestangularProvider) {
    // setting up the base url for all api requests
    RestangularProvider.setDefaultHttpFields({cache: false});
  }]);