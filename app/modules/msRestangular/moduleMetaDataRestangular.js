'use strict';

angular.module('msRestangular')
  .factory('MetaDataRestangular', ['Restangular', 'Config', '$location','$rootScope','RestangularManager', function (Restangular, Config, $location, $rootScope,RestangularManager) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularManager.init(RestangularConfigurer);

      $rootScope.$on('auth:tokenChange',function(e,data){
        RestangularConfigurer.setDefaultHeaders(data.headers);
      });

      RestangularConfigurer.setBaseUrl(Config.GetEndPoint('metaDataApiEndpoint') + 'metadatas/');
    });
  }]);


