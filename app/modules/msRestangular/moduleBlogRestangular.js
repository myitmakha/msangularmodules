'use strict';

angular.module('msRestangular')
  .factory('BlogRestangular', ['Restangular', 'Config', '$window','$rootScope','$q','RestangularManager', function (Restangular, Config, $window,$rootScope,$q,RestangularManager) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      var deferedIsReady = $q.defer();
      var hasBlogId = false;
      
      RestangularManager.init(RestangularConfigurer);

      RestangularConfigurer.addFullRequestInterceptor(function (element, operation, route, url, headers, params, httpConfig) {
        return deferedIsReady.promise.then(function () {return {}; });
      });

      var somethingSet = function(){
        if(hasBlogId){
          deferedIsReady.resolve();
        }
      };

      $rootScope.$on('auth:tokenChange',function(e,data){
        RestangularConfigurer.setDefaultHeaders(data.headers);
      });

      $rootScope.$on('auth:blogIdChange',function(e,data){
        RestangularConfigurer.setBaseUrl(Config.GetEndPoint('agoraApiEndpoint') + 'blogs/' + data.blogId);
        hasBlogId = data&&data.blogId;
        somethingSet();
      });

    });
  }]);

angular.module('msRestangular')
  .factory('BaseBlogRestangular', ['Restangular', 'Config', '$window', function (Restangular, Config) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer.setFullResponse(true);
      RestangularConfigurer.setBaseUrl(Config.GetEndPoint('agoraApiEndpoint') + 'blogs/');
    });
  }]);
