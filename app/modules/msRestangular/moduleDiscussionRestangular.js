'use strict';

angular.module('msRestangular')
  .factory('DiscussionRestangular', ['Restangular', 'Config','$rootScope','RestangularManager', function (Restangular, Config,$rootScope,RestangularManager) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularManager.init(RestangularConfigurer);

      $rootScope.$on('auth:tokenChange',function(e,data){
        RestangularConfigurer.setDefaultHeaders(data.headers);
      });

      RestangularConfigurer.setBaseUrl(Config.GetEndPoint('agoraApiEndpoint') + 'discussions/');
    });
  }]);
