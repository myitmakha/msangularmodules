'use strict';

angular.module('msRestangular')
  .factory('IdServerRestangular', ['Restangular', 'Config','RestangularManager', function (Restangular, Config,RestangularManager) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularManager.init(RestangularConfigurer);
      RestangularConfigurer.setBaseUrl(Config.GetEndPoint('idServerEndpoint'));
    });
  }]);