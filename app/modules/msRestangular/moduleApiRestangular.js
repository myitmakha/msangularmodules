'use strict';

angular.module('msRestangular')
  .factory('ApiRestangular', ['Restangular', 'Config', function (Restangular, Config) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer.setBaseUrl(Config.GetEndPoint('apiEndpoint') + 'shops/');
    });
  }]);

angular.module('msRestangular')
  .factory('BaseApiRestangular', ['Restangular', 'Config', function (Restangular, Config) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer.setBaseUrl(Config.GetEndPoint('apiEndpoint') + 'shops/');
    });
  }]);
