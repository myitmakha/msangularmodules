//'use strict';
//
//describe('Restangular', function () {
//  // load the controller's module
//  beforeEach(module('msRestangular'));
//
//  beforeEach(function () {
//  });
//
//  var scope, SiteRestangular;
//
//  var mock = function (siteApiEndpoint, host) {
//    module('msConfig', function ($provide) {
//      $provide.constant('ENV', {siteApiEndpoint: siteApiEndpoint});
//    });
//    inject(function ($location) {
//      spyOn($location, 'host').andReturn(host);
//    });
//  };
//
//  var loadObjects = function () {
//    inject(function ($rootScope, _SiteRestangular_) {
//      scope = $rootScope.$new();
//      SiteRestangular = _SiteRestangular_;
//    })
//  };
//
//  describe('siteRestangular', function () {
//    it('check it grabs to correct setting', function () {
//      mock('http://domainOfTheTestSiteApiEndpoint.com/', 'domainOfTheCurrentHostWhereThisAppIsRunning.com');
//      loadObjects();
//      scope.$apply();
//      expect(SiteRestangular.configuration.baseUrl).toBe('http://domainOfTheTestSiteApiEndpoint.com/sites/domainOfTheCurrentHostWhereThisAppIsRunning.com');
//    });
//  });
//});
