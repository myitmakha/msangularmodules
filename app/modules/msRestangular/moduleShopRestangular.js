'use strict';

angular.module('msRestangular')
  .service('ShopRestangular', ['Restangular', 'Config', '$window','$rootScope','$q','RestangularManager', function (Restangular, Config, $window,$rootScope,$q,RestangularManager) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      var deferedIsReady = $q.defer();
      var hasShopId = false;

      RestangularManager.init(RestangularConfigurer);

      RestangularConfigurer.addFullRequestInterceptor(function (element, operation, route, url, headers, params, httpConfig) {
        return deferedIsReady.promise.then(function () {return {}; });
      });

      var somethingSet = function(){
        if(hasShopId){
          deferedIsReady.resolve();
        }
      };

      $rootScope.$on('auth:tokenChange',function(e,data){
        //console.log removed.
        RestangularConfigurer.setDefaultHeaders(data.headers);
      });

      $rootScope.$on('auth:shopIdChange',function(e,data){
        RestangularConfigurer.setBaseUrl(Config.GetEndPoint('shopApiEndpoint') + 'shops/' + data.shopId);
        hasShopId = data&&data.shopId;
        somethingSet();
      });

    });
  }]);