'use strict';

angular.module('msRestangular')
  .factory('SiteRestangular', ['Restangular', 'Config', '$location','$rootScope','RestangularManager', function (Restangular, Config, $location, $rootScope,RestangularManager) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularManager.init(RestangularConfigurer);

      $rootScope.$on('auth:tokenChange',function(e,data){
        RestangularConfigurer.setDefaultHeaders(data.headers);
      });

      $rootScope.$on('auth:siteIdChange',function(e,data){
        if(data.siteId){
          RestangularConfigurer.setBaseUrl(Config.GetEndPoint('siteApiEndpoint') + 'sites/' + data.siteId);
        }
      });

      var host = $location.host();

      //TODO: This is just for testing purposes
      if (host === '127.0.0.1' || host === 'localhost') {
        host = 'evianevian.dev.clienthost.mrsite.web';
      }

      var subDomains = Config.GetEndPoint('subDomains');
      if(subDomains) {
        var firstSubDomain = host.split('.')[0];
        if (subDomains.indexOf(firstSubDomain) !== -1) {
          host = host.replace(firstSubDomain + '.', '');
        }
      }

      RestangularConfigurer.setBaseUrl(Config.GetEndPoint('siteApiEndpoint') + 'sites/' + host);
    });
  }]);

angular.module('msRestangular')
  .factory('BaseSiteRestangular', ['Restangular', 'Config','RestangularManager', function (Restangular, Config, RestangularManager) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularManager.init(RestangularConfigurer);
      RestangularConfigurer.setBaseUrl(Config.GetEndPoint('siteApiEndpoint') + 'sites/');
    });
  }]);


