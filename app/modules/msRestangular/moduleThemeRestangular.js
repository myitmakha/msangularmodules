'use strict';

angular.module('msRestangular')
  .factory('ThemeRestangular', ['Restangular', 'Config','RestangularManager', function (Restangular, Config,RestangularManager) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularManager.init(RestangularConfigurer);
      RestangularConfigurer.setBaseUrl(Config.GetEndPoint('siteApiEndpoint') + 'themes/');
    });
  }]);