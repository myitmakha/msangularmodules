//'use strict';
//
//describe('msContentItemsLayout directive', function () {
//  // load the controller's module
//  beforeEach(module('msContentItemsLayout'));
//
//  //Test Data
//  var pages =
//    [
//      {
//        name: 'Home',
//        url: '/home',
//        ishomepage: true,
//        seo: {
//          'meta-title': 'Home Page Yo',
//          'meta-description': 'This is the homepage of phils ace site'
//        },
//        pagecontentblocks: [
//          {
//            id: '0',
//            type: 'row',
//            content: [
//              {
//                id: '0-0',
//                type: 'col',
//                size: 12,
//                content: [
//                  {
//                    id: '0-0-0',
//                    type: 'text',
//                    value: 'Firstelement'
//                  }
//                ]
//              }
//            ]
//          }
//        ]
//      },
//      {
//        name: 'About',
//        url: '/about',
//        ishomepage: true,
//        seo: {
//          'meta-title': 'Aboot  Page Yo',
//          'meta-description': 'This is the aboot of phils ace site'
//        },
//        pagecontentblocks: [
//          {
//            id: '0',
//            type: 'row',
//            content: [
//              {
//                id: '0-0',
//                type: 'col',
//                size: 12,
//                content: [
//                  {
//                    id: '0-0-0',
//                    type: 'text',
//                    value: 'First'
//                  },
//                  {
//                    id: '0-0-1',
//                    type: 'text',
//                    value: 'Second'
//                  },
//                  {
//                    id: '0-0-2',
//                    type: 'text',
//                    value: 'Third'
//                  }
//                ]
//              }
//            ]
//          },
//          {
//            id: '1',
//            type: 'row',
//            content: [
//              {
//                id: '1-0',
//                type: 'col',
//                size: 6,
//                content: [
//                  {
//                    id: '1-0-0',
//                    type: 'text',
//                    value: 'Forth'
//                  }
//                ]
//              },
//              {
//                id: '1-1',
//                type: 'col',
//                size: 6,
//                content: [
//                  {
//                    id: '1-1-0',
//                    type: 'text',
//                    value: 'Fifth'
//                  },
//                  {
//                    id: '1-1-1',
//                    type: 'text',
//                    value: 'Sixth'
//                  }
//                ]
//              }
//            ]
//          }
//        ]
//      }
//    ];
//  var pageHtml = '<div class="ms-content-items-layout-directive"></div>';
//  var scope, elm, $compile;
//
//  //Load scope
//  beforeEach(inject(function ($rootScope, _$compile_) {
//    scope = $rootScope.$new();
//    $compile = _$compile_;
//  }));
//
//  //Mock page repository
//  var mockPageRepository = function (page) {
//    inject(function (PageRepository, $q) {
//      var defered = $q.defer();
//      defered.resolve(page);
//      spyOn(PageRepository, 'getCurrentPage').andReturn(defered.promise);
//    });
//  };
//
//  var compileDirective = function (pageId, tpl) {
//    var page = pages[pageId];
//    mockPageRepository(page);
//    if (!tpl) tpl = pageHtml;
//    tpl = '<form name="form">' + tpl + '</tpl>';
//    var form = $compile(tpl)(scope);
//    elm = form.children().eq(0);
//    scope.$digest();
//  };
//
//  describe('home page', function () {
//
//    //Load page
//    beforeEach(function () {
//      compileDirective(0);
//    });
//
//    it('should create the proper amount of rows, columns and text content items', function () {
//      var rows = elm.find('.ms-row');
//      var columns = elm.find('.ms-col-12');
//      var textContentItems = elm.find('.text-block');
//
//      expect(rows.length).toBe(1);
//      expect(columns.length).toBe(1);
//      expect(textContentItems.length).toBe(1);
//    });
//
//    it('the text content items must have the correct text', function () {
//      var textContentItems = elm.find('.text-block');
//
//      expect(textContentItems.text()).toBe('Firstelement');
//    });
//
//  });
//
//  describe('about page', function () {
//
//    //Load page
//    beforeEach(function () {
//      compileDirective(1);
//    });
//
//    it('should create the proper amount of rows, columns and text content items', function () {
//      var rows = elm.find('.ms-row');
//      var columns = elm.find('.ms-col-12');
//      var smallColumns = elm.find('.ms-col-6');
//      var textContentItems = elm.find('.text-block');
//
//      expect(rows.length).toBe(2);
//      expect(columns.length).toBe(1);
//      expect(smallColumns.length).toBe(2);
//      expect(textContentItems.length).toBe(6);
//    });
//
//    it('the text content items must have the correct text', function () {
//      var textContentItems = elm.find('.text-block');
//
//      expect(textContentItems.eq(0).text()).toBe('First');
//      expect(textContentItems.eq(1).text()).toBe('Second');
//      expect(textContentItems.eq(2).text()).toBe('Third');
//      expect(textContentItems.eq(3).text()).toBe('Forth');
//      expect(textContentItems.eq(4).text()).toBe('Fifth');
//      expect(textContentItems.eq(5).text()).toBe('Sixth');
//    });
//
//  });
//});
