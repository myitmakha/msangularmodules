/**
 * Created by Albert on 15/07/2014.
 */

'use strict';
angular.module('msContentItemsLayout')
  .filter('getRowStyles', function () {
    return function (settings) {
      ////console.log removed.
      var styles = '';
      if (settings.color && settings.useColor) {
        styles += 'background-color:' + settings.color + ';';
      }
      return styles;
    };
  });
