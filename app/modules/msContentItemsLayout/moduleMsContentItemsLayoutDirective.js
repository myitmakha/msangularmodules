/**
 * Created by Albert on 15/07/2014.
 */

'use strict';
angular.module('msContentItemsLayout')
  .directive('msContentItemsLayoutDirective', ['$compile', 'PageBuilderManager', function ($compile, PageBuilderManager) {
    return {
      restrict: 'C',
      replace: true,
      link: function (scope, element, attrs) {
        PageBuilderManager.getCurrentPage().then(function (page) {
          PageBuilderManager.getEditable().then(function (editable) {
            scope.pagedata = page.pagecontentblocks;
            scope.editable = editable;

            scope.getStyles = function (row) {
              var settings = (row || {}).settings || {};
            };

            var noElement = angular.element('<div class="no-element">Drag &amp; Drop elements here</div>');

            var htmlEleLevel1 = '<div ng-if="next.type===\'row\'" data-id="{{next.id}}" class="ms-row ms-level-2">' +
              '<div class="ms-row-inner">' +
              '<div ng-repeat="col1 in next.content" data-id="{{col1.id}}" data-size="{{col1.size}}" class="ms-col ms-level-2" ng-class="\'ms-col-\' + col1.size">' +
              '<div ng-repeat="next1 in col1.content" level="2" ms-content-item content-item-data="next1" editable="editable"></div>' +
              '</div>' +
              '</div></div>';

            var htmlEleLevel2 = '{{next}}';

            var htmlEle = '<div ng-repeat="row in pagedata" data-id="{{row.id}}" ng-init="row.settings.color=row.settings.color||\'#ffffff\'" class="ms-row ms-level-1" style="{{row.settings | getRowStyles}}">' +
              //'<div ng-if="editable" class="rowSettings" style="margin-left:50px;position:absolute;">' +
              //'<div style="{{row.settings.useColor?\'\':\'display:none;\'}}"><input ng-model="row.settings.color" class="colorRow" /></div>' +
              //'<div>Use color:<input ng-model="row.settings.useColor" type="checkbox" /></div></div>' +

              '<div class="ms-row-inner">' +
              '<div ng-repeat="col in row.content" data-id="{{col.id}}" data-size="{{col.size}}" class="ms-col ms-level-1" ng-class="\'ms-col-\' + col.size">' +
              '<div ng-repeat="next in col.content">' +
              '<div ng-if="next.type!==\'row\'" level="1" ms-content-item content-item-data="next" editable="editable"></div>' +
              //'<div ng-if="next.type===\'row\'" ng-repeat="next in col.content">' +
              htmlEleLevel1 +
              '</div>' +
              '</div>' +
              '</div></div>';



            var ele = angular.element(htmlEle);
            $compile(ele)(scope, function (cloned) {
              element.append(cloned);

              if(editable) {
                scope.$watchCollection('pagedata', function () {

                  setTimeout(function () {

                    if(scope.pagedata === undefined || scope.pagedata.length === 0){
                      element.before(noElement);

                      var parentPad = $('.no-element').parent().css('padding');

                      $('.no-element').parent().css({'position': 'relative', 'height': $('.no-element').outerHeight() + (parentPad * 2)});
                      element.css({'top': '50%', 'position': 'absolute', 'width': element.parent().width() , 'box-sizing': 'border-box'});
                    } else {
                      $('.no-element').parent().removeAttr('style');
                      element.removeAttr('style');
                      $('.no-element').remove();
                    }

                    element.find('input.colorRow').minicolors({
                      change: function (hex, opacity) {
                        //scope.editable.value=hex;
                        ////console.log removed.
                        scope.$apply();
                      },
                      position: 'top left',
                      opacity: false,
                      control: 'brightness'
                    });
                  });
                });
              }
            });
          });
        });
      }
    };
  }]);