'use strict';

angular.module('msGeneralSettingsRepository').factory('generalSettingsRepository', ['ShopRestangular', function (ShopRestangular) {

  var generalSettingsRepository = {};

  generalSettingsRepository.getBasicInfo = function () {
    return ShopRestangular.one('settings/general/basicInfo').get().then(
      function (response) {
        return response.data;
      },
      function (error) {
        //console.log removed.
        return error;
      }
    );
  };

  generalSettingsRepository.updateBasicInfo = function (postData) {
    return ShopRestangular.all('settings/general/basicInfo').customPUT(postData).then(
      function (response) {
        return response.data;
      },
      function (error) {
        //console.log removed.
        return error;
      }
    );
  };

  generalSettingsRepository.getShopAddress = function () {
    return ShopRestangular.one('settings/general/shopaddress').get().then(
      function (response) {
        return response.data;
      },
      function (error) {
        //console.log removed.
        return error;
      }
    );
  };

  generalSettingsRepository.updateShopAddress = function (postData) {
    //console.log removed.
    return ShopRestangular.all('settings/general/shopaddress').customPUT(postData).then(
      function (response) {
        return response.data;
      },
      function (error) {
        //console.log removed.
        return error;
      }
    );
  };

  generalSettingsRepository.getSocialLinks = function () {
    return ShopRestangular.one('settings/general/socialLinks').get().then(
      function (response) {
        return response.data;
      },
      function (error) {
        //console.log removed.
        return error;
      }
    );
  };

  generalSettingsRepository.updateSocialLinks = function (postData) {
    return ShopRestangular.all('settings/general/socialLinks').customPUT(postData).then(
      function (response) {
        return response.data;
      },
      function (error) {
        //console.log removed.
        return error;
      }
    );
  };

  generalSettingsRepository.getSEO = function () {
    return ShopRestangular.one('settings/general/seo').get().then(
      function (response) {
        return response.data;
      },
      function (error) {
        //console.log removed.
        return error;
      }
    );
  };

  generalSettingsRepository.updateSEO = function (postData) {
    return ShopRestangular.all('settings/general/seo').customPUT(postData).then(
      function (response) {
        return response.data;
      },
      function (error) {
        //console.log removed.
        return error;
      }
    );
  };

  return generalSettingsRepository;
}]);
