'use strict';

angular.module('msEditableItemImage')
  .directive('editableItemMixinImageDirective', ['$compile', 'MediaLibraryManager', 'Config', function ($compile, MediaLibraryManager, Config) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        editable: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msEditableItemImage/editableItemMixinImageDirective.tpl.html',
      link: function (scope, element) {

        var panel = element.find('.editable-dialog');

        $(document).on('click', function(e){

          if ( $(event.target).parents().add(event.target).is(element.find('span')) ){
            var elePosition = {
                left: element.offset().left + element.outerWidth(),
                top: element.offset().top - $(window).scrollTop()
              },
              bottomEdge = panel.outerHeight() + elePosition.top,
              screenHeight = $(window).height();

            if (bottomEdge > screenHeight) {
              elePosition.top = elePosition.top - ( (bottomEdge - screenHeight) + 20 );
            }

            panel.addClass('open');

            panel.attr({
              'style': 'display: block; left: ' + elePosition.left + 'px !important; top: ' + elePosition.top + 'px !important;'
            });

          } else if( !$(event.target).parents().add(event.target).hasClass('editable-dialog') ){
            panel.fadeOut(200);
          }

        });

      }
    };
  }]);