'use strict';

angular.module('msEditableItemImage')
  .directive('editableItemImageDirective', ['$compile', 'MediaLibraryManager', function ($compile, MediaLibraryManager) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        editable: '='
      },
      template:'<div>'+
      '<div ng-if="editable.SettingType==\'variable\'" editable-item-single-image-directive editable="editable"></div>'+
      '<div ng-if="editable.SettingType==\'mixin\'" editable-item-mixin-image-directive editable="editable"></div>'+
      '</div>',
      link: function (scope, element) {
      }
    };
  }]);