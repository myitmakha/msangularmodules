'use strict';

angular.module('msEditableItemImage')
  .directive('editableItemSingleImageDirective', [ function () {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        editable: '='
      },
      template:'<div>' +
        '<span class="label">{{editable.name}}</span>' +
        '<div single-image-for-editable-directive ng-model="editable.value"></div>' +
        '</div>',
      link: function (scope, element) {
      }
    };
  }]);


angular.module('msEditableItemImage')
  .directive('singleImageForEditableDirective', ['$compile', 'MediaLibraryManager','$rootScope','Config', function ($compile, MediaLibraryManager,$rootScope, Config) {
    return {
      restrict: 'A',
      require:'ngModel',
      scope: {
        ngModel: '='
      },
      link: function (scope, element, attrs, ngModelCtrl) {

        var prefix='';
        var postfix='';

        ngModelCtrl.$formatters.push(function(modelValue) {
          prefix='';
          postfix='';

          if(typeof modelValue === 'undefined'){
            return modelValue;
          }
          if(modelValue.indexOf('url(')===0 && modelValue[modelValue.length-1]===')'){
            prefix='url(';
            postfix=')';
            modelValue=modelValue.substr(4,modelValue.length-5);
          }

          var themeUrl = Config.GetEndPoint('themesEndpoint') + $rootScope.siteData.siteId + '/' + $rootScope.siteData.defaultTheme + '/';

          modelValue = modelValue.replace('$theme_url+\'','\''+themeUrl);
          modelValue = modelValue.replace('@{theme_url}',themeUrl);

          return modelValue;
        });

        ngModelCtrl.$render = function() {
          scope.images = [{url: ngModelCtrl.$viewValue, default: 0}];
        };

        ngModelCtrl.$parsers.push(function(viewValue) {
          return prefix + viewValue + postfix;
        });

        scope.editableId = MediaLibraryManager.randomString(); // This is use by the media library manager for unique hook to each ms-upload directive.
        scope.modalTitle = 'Header image';

        var compileToView = function () {
          var html = '<div>' +
            '<div class="editable-image-wrap">' +
            '<div class="image-preview" ng-show="images[0].url.length > 2">' +
            '<a class="delete icon-delete ms-button danger" ng-click="removeImage()"></a>' +
            '<img src=' + scope.images[0].url + ' />' +
            '</div>' +
            '<ms-media-library mode="\'button\'" editable-id="editableId" uploaded-files="images" title=modalTitle multiple-upload=false></ms-media-library>' +
            '</div>' +
            '</div>';
          $compile(html)(scope, function (html) {
            element.html(html);
          });
        };

        scope.removeImage = function () {
          //console.log removed.
          scope.images = [];
          scope.images.push({url: '', default: 0});
          element.find('img').attr('src', scope.images[0].url);
          scope.$parent.$root.$broadcast('markChanges', scope.modalTitle + ' has been removed.');

          compileToView();
        };

        scope.$watch('images', function () {
          //console.log removed.
          if (scope.images[0].url === undefined || scope.images[0].url.indexOf("'") !== -1) { // jshint ignore:line
            //console.log removed.
            if (scope.images[0].url !== null) {
              scope.images[0].url = scope.images[0].url;
            }
          }
          else {
            //console.log removed.
            scope.images[0].url = "'" + scope.images[0].url + "'"; // jshint ignore:line
            scope.$parent.$root.$broadcast('markChanges', scope.modalTitle + ' changed');
          }

          ngModelCtrl.$setViewValue(scope.images[0].url);

          compileToView();
        });
      }
    };
  }]);