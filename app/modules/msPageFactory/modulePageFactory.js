/**
 * Created by Albert on 15/07/2014.
 */
'use strict';

angular.module('msPageFactory')
  .factory('PageFactory', ['$q', function ($q) {


    /* This functionality should be moved to the API, the page endpoint should handle all of this */

    var newPage = function (name, url, isHomePage, defaultContentItem, pageType, nonEditableOnPageBuilder) {
      var page = {};
      page.seo = {'meta-title': name, 'meta-description': ''};
      page.name = name;
      page.url = url;
      page.ishomepage = !!isHomePage;
      page.type = pageType||'custom';
      defaultContentItem=defaultContentItem||{type: 'text',value: ''};
      defaultContentItem.id='0-0-0';
      page.editableOnPageBuilder = !nonEditableOnPageBuilder;
      page.pagecontentblocks = [
        {
          id: '0',
          type: 'row',
          content: [
            {
              id: '0-0',
              type: 'col',
              size: 12,
              content: [
                defaultContentItem
              ]
            }
          ]
        }
      ];

      var deferred = $q.defer();
      deferred.resolve(page);
      return deferred.promise;
    };



    var pageFactory = {
      newPage: newPage
    };


    return pageFactory;
  }]);