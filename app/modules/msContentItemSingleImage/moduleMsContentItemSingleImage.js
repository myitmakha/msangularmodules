'use strict';

angular.module('msContentItemSingleImage')
  .directive('msContentItemSingleImage',  [ '$compile', function ($compile) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {

        var html = '';

        if( scope.contentItemData.settings.imageUrl ){

          if( scope.contentItemData.settings.linkUrl ){

            var regexp = new RegExp('https*:\/\/');
            
            if( scope.contentItemData.settings.linkUrl.replace(regexp, '').length > 0 ){
              html = '<div><a ng-href="{{contentItemData.settings.linkUrl}}" target="_blank"><img title="{{contentItemData.settings.title}}" alt="{{contentItemData.settings.alt}}" ng-src="{{contentItemData.settings.imageUrl}}" /><div class="caption" ng-if="contentItemData.settings.caption">{{contentItemData.settings.caption}}</div></a></div>';
            }

          } else {
            html = '<div><img title="{{contentItemData.settings.title}}" alt="{{contentItemData.settings.alt}}" ng-src="{{contentItemData.settings.imageUrl}}" /><div class="caption" ng-if="contentItemData.settings.caption">{{contentItemData.settings.caption}}</div></div>';
          }
        }



        $compile(html)(scope, function (html) {
          element.html(html);
        });

      }
    };
  }]);
