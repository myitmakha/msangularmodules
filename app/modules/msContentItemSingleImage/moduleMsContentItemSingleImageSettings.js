'use strict';
angular.module('msContentItemSingleImage')
  .directive('msContentItemSingleImageSettings', ['$compile', '$sce', 'MediaLibraryManager', '$rootScope', function ($compile, $sce, MediaLibraryManager, $rootScope) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        settings: '='
      },
      template: '<div>' +
      '<div class="row-fix">' +
      '<div class="col-fix14">' +
      '<div class="image-wrap">' +
      '<img ng-if="settings.imageUrl" title="{{settings.title}}" alt="{{settings.alt}}" ng-src="{{settings.imageUrl}}"/>' +
      '<ms-media-library mode="\'dialog\'" editable-id="editableId" uploaded-files="images" title=modalTitle multiple-upload=false></ms-media-library>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '<div class="row-fix">' +
      '<div class="col-fix7">' +
      '<label>Alt</label>' +
      '<input type="text" ng-model="settings.alt" />' +
      '</div>' +
      '<div class="col-fix7">' +
      '<label>Title</label>' +
      '<input type="text" ng-model="settings.title" />' +
      '</div>' +
      '</div>' +
      '<div class="row-fix">' +
      '<div class="col-fix14">' +
      '<label>Link URL</label>' +
      '<input type="url" class="link-url" ng-model="settings.linkUrl" />' +
      '</div>' +
      '</div>' +
      '<div class="row-fix">' +
      '<div class="col-fix14">' +
      '<label>Caption</label>' +
      '<textarea rows="4" ng-model="settings.caption"></textarea>' +
      '</div>' +
      '</div>' +
      '</div>',
      link: function (scope, element) {
        scope.settings.alt = scope.settings.alt || '';
        scope.settings.title = scope.settings.title || '';
        scope.settings.imageUrl = scope.settings.imageUrl || '';
        scope.settings.linkUrl = scope.settings.linkUrl || '';
        scope.settings.caption = scope.settings.caption || '';

        scope.images = [];
        scope.images.push({url: scope.settings.imageUrl, default: 0});
        scope.editableId = MediaLibraryManager.randomString(); // This is use by the media library manager for unique hook to each ms-upload directive.
        scope.modalTitle = 'Single image';

        scope.$watch('images', function (oldValue, newValue) {
          scope.settings.imageUrl = scope.images[0].url;
        });

        scope.$watch('settings', function (oldValue, newValue) {
          if (oldValue !== newValue) {
            $rootScope.$broadcast('markChanges', 'single image settings has been changed.');
          }
        }, true);


        var linkField = element.find('input.link-url');

        linkField.on('focus', function () {
          $(this).select();
        });

        linkField.on('blur', function () {
          if ((linkField.val().indexOf('http://') !== 0) && (linkField.val().indexOf('https://') !== 0)) {
            scope.settings.linkUrl = 'http://' + linkField.val();
          }
        });

      }
    };
  }]);
