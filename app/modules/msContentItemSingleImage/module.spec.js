'use strict';

describe('msContentItemSingleImageEdit directive', function () {

  // load the controller's module
  beforeEach(module('msContentItemSingleImage'));


  var html = '<div>' +
    '<img title="{{contentItemData.settings.title}}" alt="{{contentItemData.settings.alt}}" ng-src="{{contentItemData.settings.imageUrl}}" />' +
    '</div>';

  //Load scope
  beforeEach(inject(function ($rootScope, _$compile_) {
    scope = $rootScope.$new();
    $compile = _$compile_;
  }));

  var scope, elm, $compile;

  var compileDirective = function (title, alt, imageUrl, tpl) {
    scope.contentItemData = scope.contentItemData || { 'settings': {} };
    scope.contentItemData.settings.title = title;
    scope.contentItemData.settings.alt = alt;
    scope.contentItemData.settings.imageUrl = imageUrl;

    if (!tpl) tpl = html;
    tpl = '<form name="form">' + tpl + '</tpl>';
    var form = $compile(tpl)(scope);
    elm = form.children().eq(0);
    scope.$digest();
  };

  it('Loading a single image content item has correct alt, title and imageUrl', function () {
    compileDirective('my title', 'my alt text', 'https://omnibuilder.s3.amazonaws.com/1f13be68-8e80-41f2-8d5e-958e0a620ebf/560a72c7-110b-4c06-936e-f493ac4756c0.jpg');

    var img = angular.element(elm).find("img");

    expect(img.attr('title')).toBe('my title');
    expect(img.attr('alt')).toBe('my alt text');
    expect(img.attr('ng-src')).toBe('https://omnibuilder.s3.amazonaws.com/1f13be68-8e80-41f2-8d5e-958e0a620ebf/560a72c7-110b-4c06-936e-f493ac4756c0.jpg');
  });
});

describe('msContentItemSingleImageSettings directive', function () {

  beforeEach(module('msContentItemSingleImage'));
  beforeEach(module('msMediaLibrary'));
  beforeEach(module('msConfig'));

  var settingsHtml = '<div>' +
    '<div class="row-fix">' +
    '<div class="col-fix14">' +
    '<div class="image-wrap">' +
    '<img ng-if="settings.imageUrl" title="{{settings.title}}" alt="{{settings.alt}}" ng-src="{{settings.imageUrl}}"/>' +
    '<ms-media-library mode="\'dialog\'" editable-id="editableId" uploaded-files="images" title=modalTitle multiple-upload=false></ms-media-library>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '<div class="row-fix">' +
    '<div class="col-fix7">' +
    '<label>Alt</label>' +
    '<input type="text" ng-model="settings.alt" />' +
    '</div>' +
    '<div class="col-fix7">' +
    '<label>Title</label>' +
    '<input type="text" ng-model="settings.title" />' +
    '</div>' +
    '</div>' +
    '</div>';

  var scope, elm, $compile, MediaLibraryManager, Config, $httpBackend;

  //Load scope
  beforeEach(inject(function ($rootScope, _$compile_, _MediaLibraryManager_, _Config_, $injector) {
    $httpBackend = $injector.get('$httpBackend');
    Config - _Config_;
    MediaLibraryManager = _MediaLibraryManager_;
    scope = $rootScope.$new();
    $compile = _$compile_;
  }));

  var compileDirective = function (title, alt, imageUrl, tpl) {
    scope.settings = scope.settings || {};
    scope.settings.title = title;
    scope.settings.alt = alt;
    scope.settings.imageUrl = imageUrl;

    scope.images = [];
    scope.images.push({url: scope.settings.imageUrl, default: 0});
    scope.editableId = MediaLibraryManager.randomString(); // This is use by the media library manager for unique hook to each ms-upload directive.
    scope.modalTitle = 'Single image';

    if (!tpl) tpl = settingsHtml;
    tpl = '<form name="form">' + tpl + '</tpl>';
    var form = $compile(tpl)(scope);
    elm = form.children();
    scope.$digest();
  };

//  it('creates a single image settings form', function () {
//    $httpBackend.expectGET('webConfig.xml').respond(200, '');
//    $httpBackend.expectGET('bower_components/msUpload/moduleUpload.html').respond(200, '');
//
//    compileDirective('my title', 'my alt text', 'https://omnibuilder.s3.amazonaws.com/1f13be68-8e80-41f2-8d5e-958e0a620ebf/560a72c7-110b-4c06-936e-f493ac4756c0.jpg');
//
//    var img = angular.element(elm).find("img");
//
//    expect(img.attr('title')).toBe('my title');
//    expect(img.attr('alt')).toBe('my alt text');
//    expect(img.attr('ng-src')).toBe('https://omnibuilder.s3.amazonaws.com/1f13be68-8e80-41f2-8d5e-958e0a620ebf/560a72c7-110b-4c06-936e-f493ac4756c0.jpg');
//
//    //expect(elm.children().eq(0).val()).toContain('from@me.com');
//    //console.log removed.
//  });

});