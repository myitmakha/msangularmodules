'use strict';
angular.module('msContentItemSingleImage')
  .directive('msContentItemSingleImageEdit', ['$compile', 'MediaLibraryManager', function ($compile, MediaLibraryManager) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {
        var compileToView = function () {
          //var html= '<div class="ms-no-settings">To upload an image, click the blue cog to the top right of this box. To move your image, click the green circle to the top right of this box and drag it, to delete it, click the red cross.</div>';
          var html = '<ms-media-library mode="\'dialog\'" editable-id="editableId" uploaded-files="images" title=modalTitle multiple-upload=false></ms-media-library>';

          if( scope.contentItemData.settings.imageUrl ){

            if(scope.contentItemData.settings.linkUrl){

              var regexp = new RegExp('https*:\/\/');

              if( scope.contentItemData.settings.linkUrl.replace(regexp, '').length > 0 ){
                html = '<div><a ng-href="{{contentItemData.settings.linkUrl}}" target="_blank"><img title="{{contentItemData.settings.title}}" alt="{{contentItemData.settings.alt}}" ng-src="{{contentItemData.settings.imageUrl}}" /><div class="caption" ng-if="contentItemData.settings.caption">{{contentItemData.settings.caption}}</div></a></div>';
              }

            } else {
              html = '<div><img title="{{contentItemData.settings.title}}" alt="{{contentItemData.settings.alt}}" ng-src="{{contentItemData.settings.imageUrl}}" /><div class="caption" ng-if="contentItemData.settings.caption">{{contentItemData.settings.caption}}</div></div>';
            }

          }else{
            scope.images = [];
            scope.images.push({url: scope.contentItemData.settings.imageUrl, default: 0});
            scope.editableId = MediaLibraryManager.randomString(); // This is use by the media library manager for unique hook to each ms-upload directive.
            scope.modalTitle = 'Single image';

            scope.$watch('images', function () {
              scope.contentItemData.settings.imageUrl = scope.images[0].url;
            });
          }

          $compile(html)(scope, function (html) {
            element.html(html);
          });
        };

        scope.$watch('contentItemData', function () {
          //console.log removed.
          compileToView();
        }, true);
      }
    };
  }]);
