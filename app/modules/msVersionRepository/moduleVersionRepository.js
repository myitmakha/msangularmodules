'use strict';

angular.module('msVersionRepository').factory('VersionRepository', ['AppRestangular', 'ApiRestangular', 'BaseApiRestangular', '$q', function (AppRestangular, ApiRestangular, BaseApiRestangular, $q) {

  var getAppVersion = function () {
    var deferred = $q.defer();
    AppRestangular.one('version.txt').get().then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getApiVersion = function () {
    var deferred = $q.defer();
    ApiRestangular.one('version').get().then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getCountries = function(){
    var deferred = $q.defer();
    BaseApiRestangular.all('sets/countries').customGET().then(
      function (response) {
        var countries = response;
        deferred.resolve(countries);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getCurrencies = function(){
    var deferred = $q.defer();
    BaseApiRestangular.all('sets/currencies').customGET().then(
      function (response) {
        var currencies = response;
        deferred.resolve(currencies);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var versionRepository = {
    getAppVersion: getAppVersion,
    getApiVersion: getApiVersion,
    GetCountries: getCountries,
    getCurrencies: getCurrencies
  };

  return versionRepository;
}]);
