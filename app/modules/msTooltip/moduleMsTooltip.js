'use strict';
angular.module('msTooltip')

  .directive('msTooltip', ['$parse', function ($parse) {

    return {
      restrict: 'A',
      scope: {
        tooltipContent: '@'
      },
      link: function (scope, element, attrs) {

        element.tooltipster({
          content: $( '<span>'+scope.tooltipContent+'</span>' ),
          delay: 800
        });

      }
    };
  }]);
