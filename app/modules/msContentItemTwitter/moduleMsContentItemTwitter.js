/**
 * Created by Albert on 15/07/2014.
 */
'use strict';
angular.module('msContentItemTwitter')
  .directive('msContentItemTwitter', ['$compile', '$timeout', '$interval', function ($compile, $timeout, $interval) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      //template: '<a class="twitter-timeline" ng-href="contentItemData.settings.url" data-widget-id="widgetId">Tweets by @{{username}}</a>',
      link: function (scope, element) {

        var twitterFunction = function (d, s, id) {
          if (d.getElementById(id)) {
            var script = d.getElementById(id);
            script.parentElement.removeChild(script);
          }
          $timeout(function () {
            var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
            if (!d.getElementById(id)) {
              js = d.createElement(s);
              js.id = id;
              js.src = p + '://platform.twitter.com/widgets.js';
              fjs.parentNode.insertBefore(js, fjs);
            }
          });
        };
//var html = '<a class="twitter-timeline" ng-href="{{contentItemData.settings.url}}" data-widget-id="497711044584697856">Tweets by @{{contentItemData.settings.username}}</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?"http":"https";if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';

        var reloadTwitter = function (widgetId) {
          var html = '';
          if (widgetId) {
            html = '<a class="twitter-timeline" data-widget-id="' + widgetId + '"></a>';
          }else{
            html = '';
          }
          $compile(html)(scope, function (compiled) {
            element.html(compiled);
            twitterFunction(document, 'script', 'twitter-wjs');
          });

          $interval(
            function () {
              element.find('iframe').css({
                'max-width': '100%',
                'width': '100%'
              });
            }, 100, 20
          );

        };

        var debounceFunctionOfRefreshIframe = _.debounce(reloadTwitter, 500);

        var initializing = true;

        var firstLoadTriggerInstantlyOtherwiseDebounce = function (widgetId) {
          if (initializing) {
            initializing = false;
            reloadTwitter(widgetId);
          } else {
            debounceFunctionOfRefreshIframe(widgetId);
          }
        };

        scope.$watch('contentItemData.settings.widgetId', firstLoadTriggerInstantlyOtherwiseDebounce);
      }
    };
  }]);