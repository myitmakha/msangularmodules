//'use strict';
//
//describe('msContentItemText directive', function () {
//  // load the controller's module
//  beforeEach(module('msContentItemTwitter'));
//
//  var pageHtml = '<div ms-content-item-text text-content="textContent"></div>';
//  var scope, elm, $compile;
//
//  //Load scope
//  beforeEach(inject(function ($rootScope, _$compile_) {
//    scope = $rootScope.$new();
//    $compile = _$compile_;
//  }));
//
//  var compileDirective = function (textContent, tpl) {
//    scope.textContent = {value: textContent};
//    if (!tpl) tpl = pageHtml;
//    tpl = '<form name="form">' + tpl + '</tpl>';
//    var form = $compile(tpl)(scope);
//    elm = form.children().eq(0);
//    scope.$digest();
//  };
//
//  it('creating text-block with text has correct text', function () {
//    compileDirective('This is the content of the content item text');
//
//    expect(elm.text()).toBe('This is the content of the content item text');
//  });
//
//  it('creating text-block with no text is empty', function () {
//    compileDirective('');
//
//    expect(elm.text()).toBe('');
//  });
//
//});
