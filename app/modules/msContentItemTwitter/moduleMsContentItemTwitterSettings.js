/**
 * Created by Albert on 15/07/2014.
 */
'use strict';
angular.module('msContentItemTwitter')
  .directive('msContentItemTwitterSettings', ['$compile', '$sce', '$rootScope', function ($compile, $sce, $rootScope) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        settings: '='
      },
      template: '<div>' +
        '<div class="row-fix">'+
        '<div class="col-fix14">' +
        '<p>Click on the button below to go to Twitter. In the \'Username\' box, type in your Twitter username, then click on \'Create widget\'. Copy the long piece of code below the ‘Preview’ box  and paste it into the ‘Twitter code’ box below.</p>' +
        '</div>' +
        '<div class="col-fix14">' +
        '<a target="_blank" href="https://twitter.com/settings/widgets/new" class="ms-button info">Click here to get your twitter code</a>' +
        '</div>' +

        '<div class="col-fix14">' +
        '<label>Twitter code</label>' +
        '<input type="text" ng-model="settings.embeded"/>' +
        '</div>' +
        '</div>',
      link: function (scope) {
        scope.settings = scope.settings || {};
        scope.settings.embeded = scope.settings.embeded || '';
        scope.settings.widgetId = scope.settings.widgetId || '';

        scope.$watch('settings.embeded', function () {
          var parts = scope.settings.embeded.split('widget-id="');
          scope.settings.widgetId = scope.settings.embeded;
          if (parts[1]) {
            var moreParts = parts[1].split('"');
            if (moreParts[0]) {
              scope.settings.widgetId = moreParts[0];
            }
          }
        });

        scope.$watch('settings', function (oldValue, newValue) {
          if (oldValue !== newValue) {
            $rootScope.$broadcast('markChanges', 'twitter settings has been changed.');
          }
        }, true);
      }
    };
  }]);
