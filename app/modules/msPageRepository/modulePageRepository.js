/**
 * Created by Albert on 15/07/2014.
 */
'use strict';

angular.module('msPageRepository')
  .factory('PageRepository', ['SiteRepository', '$q', function (SiteRepository, $q) {

    var currentPage;

    var getSite = SiteRepository.getSite;

    var getPages = function () {
      var deferred = $q.defer();
      getSite().then(function (siteData) {
        deferred.resolve(siteData.pages);
      });
      return deferred.promise;
    };

    var getCurrentPage = function () {
      var deferred = $q.defer();
      deferred.resolve(currentPage);
      return deferred.promise;
    };

    var setCurrentPage = function (currentPageId) {
      var deferred = $q.defer();
      getSite().then(function (websiteData) {
        currentPage = websiteData.pages.filter(function (page) {
          return String(page.url).substring(1).toLowerCase() === currentPageId;
        })[0];
        if (!currentPage) {
          currentPage = {name: '404', url: '/404'};
          if (currentPageId === 'admin') {
            currentPage = {name: '403', url: '/403'};
          }
        }
        deferred.resolve(currentPage);
      });
      return deferred.promise;
    };


    var pageRepository = {
      getCurrentPage: getCurrentPage,
      setCurrentPage: setCurrentPage,
      getPages: getPages
    };
    return pageRepository;
  }]);
