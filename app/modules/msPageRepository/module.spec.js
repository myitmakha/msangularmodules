//'use strict';
//
//describe('Page repository', function () {
//  // load the controller's module
//  beforeEach(module('msPageRepository'));
//
//  var scope, PageRepository, $compile;
//  var siteData = {pages:[{name:'Home',url:'/home'},{name:'About',url:'/about'},{name:'contactUs',url:'/contactus'}]};
//
//
//  //Load scope
//  beforeEach(inject(function ($rootScope) {
//    scope = $rootScope.$new();
//  }));
//
//  var mockSiteRepository = function(siteData){
//    inject(function (SiteRepository, $q) {
//      var deferred = $q.defer();
//      deferred.resolve(siteData);
//      spyOn(SiteRepository, 'getSite').andReturn(deferred.promise);
//    });
//    inject(function (_PageRepository_) {
//      PageRepository=_PageRepository_;
//    });
//  };
//
//
//  describe('getCurrentPage and setCurrentPage functions', function () {
//
//    it('getCurrentPage before being set should return undefined', function () {
//      mockSiteRepository(siteData);
//      var pagePromise = PageRepository.getCurrentPage();
//      pagePromise.then(function (page) {
//        expect(page).toBeUndefined();
//      });
//      scope.$apply();
//    });
//
//    it('getCurrentPage after being set it should return set page', function () {
//      mockSiteRepository(siteData);
//      var setPagePromise = PageRepository.setCurrentPage('home');
//      var pagePromise = setPagePromise.then(function () {
//        return PageRepository.getCurrentPage();
//      });
//      pagePromise.then(function (page) {
//        expect(page.name).toBe('Home');
//      });
//      scope.$apply();
//    });
//
//    it('getCurrentPage after being set to an non-existing page it should return 404 page', function () {
//      mockSiteRepository(siteData);
//      var setPagePromise = PageRepository.setCurrentPage('nonExisting');
//      var pagePromise = setPagePromise.then(function () {
//        return PageRepository.getCurrentPage();
//      });
//      pagePromise.then(function (page) {
//        expect(page.name).toBe('404');
//      });
//      scope.$apply();
//    });
//
//  });
//
//  describe('getPages function', function () {
//
//    it('getPages returns all the pages', function () {
//      mockSiteRepository(siteData);
//      PageRepository.getPages().then(function (pages) {
//        expect(pages).toBe(siteData.pages);
//      });
//      scope.$apply();
//    });
//
//  });
//
//});
