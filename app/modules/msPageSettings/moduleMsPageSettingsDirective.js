'use strict';

angular.module('msPageSettings')
  .directive('msPageSettings', ['$filter', 'Config', function ($filter, Config) {
    return{
      restrict: 'A',
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msPageSettings/pageSettings.tpl.html',
      scope:{
        page:'='
      },
      link: function (scope, element, attrs) {

        scope.updateUrl = function (page) {
          page.url = '/' + $filter('cleanUrl')(page.name);
        };
      }
    };
  }]);