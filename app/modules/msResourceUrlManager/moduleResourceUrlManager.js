'use strict';

angular.module('msResourceUrlManager')
  .factory('ResourceUrlManager', ['$location', function ($location) {
    var $routeParams;
    var baseUrl;
    var setRouteParams = function(routeParams,baseUrlParam){
      $routeParams=routeParams;
      baseUrl=baseUrlParam||'';
    };

    var getAllParameters = function(){
      return $routeParams;
    };



    var getUrlFromRouteParameters = function(routeParams){
      var url = baseUrl+routeParams.pageId;
      if(!routeParams.param1){
        return url;
      }
      url+='/'+routeParams.param1;
      if(!routeParams.param2){
        return url;
      }
      url+='/'+routeParams.param2;
      if(!routeParams.param3){
        return url;
      }
      url+='/'+routeParams.param3;
      if(!routeParams.param4){
        return url;
      }
      url+='/'+routeParams.param4;
      return url;
    };

    var setUrlParameters = function(routeParams){
      var url = getUrlFromRouteParameters(routeParams);
      $location.path(url);
    };

    var setSearchParameters = function(){
      
    };

    var resourceUrlManager = {
      setRouteParams:setRouteParams,
      getAllParameters:getAllParameters,
      setUrlParameters:setUrlParameters,
      setSearchParameters:setSearchParameters
    };
    return resourceUrlManager;
  }]);