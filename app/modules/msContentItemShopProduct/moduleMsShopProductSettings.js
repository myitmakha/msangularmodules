'use strict';

angular.module('msContentItemShopProduct')
  .directive('msContentItemShopProductSettings', ['$compile','ProductService', function ($compile,ProductService) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        settings: '='
      },
      template: '<div>' +

        '<div class="row-fix">' +
        '<div class="col-fix14">' +
        '<label>Product Id</label>' +
        '<select ng-model="settings.productId" ng-options="product.ProductId as product.Name for product in products">' +
        '<option>--Choose a product--</option>' +
        '</select>' +
        '</div>' +
        '</div>' +

        '</div>',

      link: function (scope, element) {
        ProductService.getAll().then(function(products){
          scope.products=products;
        });
      }
    };
  }]);