'use strict';

angular.module('msContentItemShopProduct')
  .directive('msContentItemShopProduct', ['$compile','ProductService','Config', function ($compile,ProductService,Config) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '=',
        contentItemMetaData: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemShopProduct/msShopProduct.tpl.html',

      link: function (scope, element) {

        scope.back = function(){
          scope.contentItemMetaData.setUrlParamsByPageType('shop');
        };

        var selectedAttributes = [];
        scope.productVariant={};

        scope.checkValues = function(attribute,value){
          if(!scope.product){
            return;
          }
          if(attribute){
            selectedAttributes[attribute] = value;
          }
          //console.log removed.
          scope.productVariant = findVariantFromAttributes(scope.product.Variants, selectedAttributes);
          //console.log removed.
        };

        var findVariantFromAttributes = function(variants, attributes){
          if(!variants||!attributes){
            return;
          }
          for(var i=0;variants[i];i++){
            var variant=variants[i];
            var isThisVariant = true;
            for(var j=0;variant.AttributeOptions[j];j++){
              var vAttribute = variant.AttributeOptions[j];
              if(attributes[vAttribute.Key]!==vAttribute.Value){
                isThisVariant=false;
                break;
              }
            }
            if(isThisVariant){
              return variant;
            }
          }
        };

        var loadProduct = function(productId){
          if(!productId){
            scope.error='Product not defined';
            return;
          }
          scope.contentItemMetaData.loading=true;

          ProductService.getAll().then(function(products){
            ProductService.get(productId).then(function(product){
              selectedAttributes = [];
              scope.contentItemMetaData.loading=false;
              delete scope.error;
              delete scope.product;
              if(product.status){
                scope.error='Error product not found';
              }else{
                scope.product=product;
                scope.checkValues();
              }
            });
          });
        };
        var isStandaloneContentItem=scope.contentItemData.type==='shop-product';

        var productId;
        scope.showLeftBar = true;
        scope.showBackToShopButton = true;
        if(isStandaloneContentItem){
          scope.$watch('contentItemData.settings.productId',function(productId){
            loadProduct(productId);
          });
          scope.showLeftBar = false;
          scope.showBackToShopButton = false;
        }else if(scope.contentItemMetaData&&scope.contentItemMetaData.urlParams&&scope.contentItemMetaData.urlParams.param2){
          productId=scope.contentItemMetaData.urlParams.param2;
          loadProduct(productId);
        }
      }
    };
  }]);