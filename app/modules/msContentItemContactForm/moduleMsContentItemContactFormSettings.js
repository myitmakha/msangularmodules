'use strict';

angular.module('msContentItemContactForm')
  .directive('msContentItemContactFormSettings', ['$compile', '$sce', '$rootScope', 'Config', '$timeout', 'ngDialog', 'Helper', function ($compile, $sce, $rootScope, Config, $timeout, ngDialog, Helper) {
    return {
      restrict: 'A,E',
      replace: true,
      scope: {
        settings: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemContactForm/msContactFormSettings.tpl.html',
      link: function (scope, element) {

        scope.formFieldTypes = [
          {
            type: 'text',
            label: 'Text',
            placeholder: '',
            required: false
          },
          {
            type: 'textarea',
            label: 'Text Area',
            placeholder: '',
            required: false
          },
          {
            type: 'checkbox',
            label: 'Checkbox',
            placeholder: undefined,
            required: false,
            value: false
          },
          {
            type: 'radio',
            label: 'Radio',
            placeholder: undefined,
            required: false,
            options: ['Option 1', 'Option 2']
          },
          {
            type: 'dropdown',
            label: 'Drop Down',
            placeholder: undefined,
            required: false,
            options: ['Option 1', 'Option 2']
          }
        ];

        scope.settings = scope.settings || {};
        scope.settings.buttonText = scope.settings.buttonText || 'Send';
        scope.settings.toEmail = scope.settings.toEmail || 'mail@yourwebsite.com';
        scope.settings.subject = scope.settings.subject || 'You have a new contact form message';
        scope.settings.formItems = scope.settings.formItems;


        var reorderFormItems = function (newOrderList) {
          var newFormArray = [];
          angular.forEach(newOrderList, function (newOrder) {
            angular.forEach(scope.settings.formItems, function (item) {
              if (item.id === newOrder) {
                newFormArray.push(item);
              }
            });
          });

          scope.settings.formItems = [];
          scope.settings.formItems = newFormArray;
          scope.$apply();
        };

        $timeout( function(){
          $('ul.field-items', element).sortable({
            items: '> li',
            handle: '.basic-info .actions .icon-reorder',
            axis: 'y',
            containment: 'parent',
            stop: function (event, ui) {
              var newOrder = $(this).sortable('toArray', 'id');
              //console.log removed.
              reorderFormItems(newOrder);
            }
          });
        }, 100);

        scope.showNewItems = function(){
          ngDialog.open({ template: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemContactForm/newFieldPopUp.tpl.html', scope: scope, className: 'add-form-item'});
        };

        scope.addFormItem = function(formType){
          var newField = angular.copy(formType);
          newField.id = Helper.guid();
          scope.settings.formItems.push(newField);
          $rootScope.$broadcast('newFieldAdded');
          $rootScope.$broadcast('repositionSettings');

          //console.log removed.
        };

        scope.deleteField = function (index){
          scope.settings.formItems.splice(index, 1);
        };

        scope.showSettings = function(e) {
          $(e.target).parents('.field-item').siblings().children('.field-settings').slideUp();
          $(e.target).parents().siblings('.field-settings').slideToggle();
        };
      }
    };
  }]);
