'use strict';

angular.module('msContentItemContactForm')
  .directive('msContentItemContactForm', ['MessagingManager', 'Config', 'Helper', '$timeout', function (MessagingManager, Config, Helper, $timeout) {
    return {
      restrict: 'A,E',
      replace: true,
      scope: {
        contentItemData: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemContactForm/contactForm.html',
      link: function (scope, element) {

        scope.defaultFormItems = [
          {
            id: Helper.guid(),
            type: 'text',
            label: 'Name',
            placeholder: 'Enter your name',
            required: false
          },
          {
            id: Helper.guid(),
            type: 'text',
            label: 'Email',
            placeholder: 'Enter your email address',
            required: false
          },
          {
            id: Helper.guid(),
            type: 'textarea',
            label: 'Message',
            placeholder: 'Enter your message',
            required: false
          }
        ];

        scope.contentItemData.settings.toEmail =  scope.contentItemData.settings.toEmail || 'mail@yourwebsite.com';
        scope.contentItemData.settings.subject =  scope.contentItemData.settings.subject || 'You have a new contact form message';
        scope.contentItemData.settings.buttonText = scope.contentItemData.settings.buttonText || 'Send';
        scope.contentItemData.settings.formItems = scope.contentItemData.settings.formItems || scope.defaultFormItems;

        scope.formData = [];


        angular.forEach( scope.contentItemData.settings.formItems, function(item){

          if(item.id === undefined){
            item.id = Helper.guid();
          }
          var emptyObject = {};
          scope.formData.push(emptyObject);
        });

        var submitButton = element.find('.submit-button');

        scope.submitForm = function () {

          submitButton.addClass('sending');

          MessagingManager.sendMessage(
            {
              'to' : scope.contentItemData.settings.toEmail,
              'subject' : scope.contentItemData.settings.subject,
              'messageFields' : scope.formData
            }
          ).then(function (data) {
              if(data.status === 201){
                submitButton.removeClass('sending');
                submitButton.addClass('sent');

                submitButton.find('span').text('Your message has been sent');

                $timeout(function(){
                  submitButton.removeClass('sent');
                  submitButton.find('span').text('Send');
                }, 2000);

              } else {
                submitButton.addClass('error');

                submitButton.find('span').text('There was an error sending your message');

                $timeout(function(){
                  submitButton.find('span').text('Send');
                  submitButton.removeClass('error');
                }, 2000);

              }
            });
        };
      }
    };
  }]);
