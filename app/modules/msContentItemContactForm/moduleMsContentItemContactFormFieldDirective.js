'use strict';

angular.module('msContentItemContactForm')
  .directive('fieldDirective', ['Config', function (Config){

    return {
      restrict: 'E',
      replace: true,
      scope: {
        field: '=',
        formData: '='
      },
      link: function(scope){

        scope.formData = scope.formData;

        scope.setData = function(){

          scope.formData.label = scope.field.label;

          if( scope.field.type === 'dropdown'){
            scope.formData.value = scope.field.options[0];
          } else if( scope.field.type === 'checkbox'){
            scope.formData.value = scope.field.value;
          } else {
            scope.formData.value = '';
          }

        };

        scope.$watch('field', function(){
          scope.setData();
        }, true);

        scope.getTemplateUrl = function() {
          var type = scope.field.type;
          var templateUrl = Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemContactForm/fields/';
          return templateUrl += type + '.tpl.html';
        };
      },
      template: '<div ng-include="getTemplateUrl()"></div>'
    };


  }]);
