// 'use strict';

// describe("msContentItemContactForm tests", function () {
//   var elm, scope, $compile, $httpBackend, $window;

//   beforeEach(module('msConfig'));
//   beforeEach(module('msContentItemContactForm'));
//   beforeEach(module('modules/msContentItemContactForm/contactForm.html'));

//   beforeEach(inject(function (_$compile_, $rootScope, $injector, _$window_) {
//     $httpBackend = $injector.get('$httpBackend');
//     $window = _$window_;
//     scope = $rootScope;
//     $compile = _$compile_;

//     $window.sessionStorage = { // mocking sessionStorage
//       getItem: function (key) {
//         return this[key];
//       }
//     };

//     $window.sessionStorage.modulesDirectory = 'modules'; // comment: this is hre because other constants are accessed this way. Not sure this work across all browsers though, needs review

//     //$httpBackend.expectGET('webConfig.xml').respond(200, '');
//     $httpBackend.expectGET('config.json').respond(200, '');
//     $httpBackend.expectGET('/snapshots/server/jsredirects.json').respond(200, '');


//     scope.contentItemData = scope.contentItemData || { 'settings': {} };
//     scope.contentItemData.settings.nameValue = 'Darren Schwarz';
//     scope.contentItemData.settings.emailValue = 'darrenschwarz@gmail.com';
//     scope.contentItemData.settings.commentValue = 'No Comment.';
//     scope.contentItemData.settings.buttonText = 'send';
//     scope.contentItemData.settings.fromEmail = 'mrsitesmtp@gmail.com';
//     scope.contentItemData.settings.toEmail = 'mrsitesmtp@gmail.com';
//     scope.contentItemData.settings.subject = 'my subject.';
//     scope.sended = false;

//     elm = angular.element('<ms-content-item-contact-form content-item-data="contentItemData"></ms-content-item-contact-form>');

//     $compile(elm)(scope);
//     scope.$digest();

//   }));


//   it("creates a contact form", inject(function ($compile, $rootScope) {

//     var inputs = elm.find('input');
//     var textareas = elm.find('textarea');
//     var buttons = elm.find('button');

//     expect(inputs.eq(0).val()).toBe('Darren Schwarz');
//     expect(inputs.eq(1).val()).toBe('darrenschwarz@gmail.com');
//     expect(textareas.eq(0).val()).toBe('No Comment.');
//     expect(buttons.eq(0).text()).toBe('send');
//   }));
// });

// describe('msContentItemContactFormEdit directive', function () {

//   var elm, scope, $compile, $httpBackend, $window;

//   beforeEach(module('msConfig'));
//   beforeEach(module('msContentItemContactForm'));
//   beforeEach(module('modules/msContentItemContactForm/contactForm.html'));

//   beforeEach(inject(function (_$compile_, $rootScope, $injector, _$window_) {
//     $httpBackend = $injector.get('$httpBackend');
//     $window = _$window_;
//     scope = $rootScope;
//     $compile = _$compile_;

//     $window.sessionStorage = { // mocking sessionStorage
//       getItem: function (key) {
//         return this[key];
//       }
//     };

//     $window.sessionStorage.modulesDirectory = 'modules'; // comment: this is hre because other constants are accessed this way. Not sure this work across all browsers though, needs review

//     //$httpBackend.expectGET('webConfig.xml').respond(200, '');
//     $httpBackend.expectGET('config.json').respond(200, '');
//     $httpBackend.expectGET('/snapshots/server/jsredirects.json').respond(200, '');

//     scope.contentItemData = scope.contentItemData || { 'settings': {} };
//     scope.contentItemData.settings.buttonText = 'send';
//     scope.sended = false;

//     elm = angular.element('<ms-content-item-contact-form-edit content-item-data="contentItemData"></ms-content-item-contact-form-edit>');

//     $compile(elm)(scope);
//     scope.$digest();

//   }));

//   it('creates a contact form with button text set correctly', function () {
//     //compileDirective('TestButtonText');
//     var buttons = elm.find('button');

//     expect(buttons.eq(0).text()).toBe('send');
//   });
// });

// describe('msContentItemContactFormSettings directive', function () {

//   beforeEach(module('msContentItemContactForm'));

//   var settingsHtml = '<div> From: <input type="text" ng-model="settings.fromEmail"/><br/> To: <input type="text" ng-model="settings.toEmail"/><br/> Subject of the message: ' +
//     '<input type="text" ng-model="settings.subject"/><br/> Button text: <input type="text" ng-model="settings.buttonText"/><br/> </div>';

//   var scope, elm, $compile;

//   //Load scope
//   beforeEach(inject(function ($rootScope, _$compile_) {
//     scope = $rootScope.$new();
//     $compile = _$compile_;
//   }));

//   var compileDirective = function (buttonText, fromEmail, toEmail, subject, tpl) {
//     scope.settings = scope.settings || {};
//     scope.settings.buttonText = buttonText;
//     scope.settings.fromEmail = fromEmail;
//     scope.settings.toEmail = toEmail;
//     scope.settings.subject = subject;

//     if (!tpl) tpl = settingsHtml;
//     tpl = '<form name="form">' + tpl + '</tpl>';
//     var form = $compile(tpl)(scope);
//     elm = form.children();
//     scope.$digest();
//   };

//   it('creates a contact form settings form', function () {
//     compileDirective('TestButtonText', 'from@me.com', 'to@me.com', 'TestSubjectText');

//     //console.log removed.

//     expect(elm.children().eq(0).val()).toContain('from@me.com');
//     expect(elm.children().eq(2).val()).toContain('to@me.com');
//     expect(elm.children().eq(4).val()).toContain('TestSubjectText');
//     expect(elm.children().eq(6).val()).toContain('TestButtonText');
//   });

// });
