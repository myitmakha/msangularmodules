'use strict';

angular.module('msContentItemContactForm')
  .directive('msContentItemContactFormEdit', ['Config', '$timeout', '$compile', 'Helper', 'MessagingManager', function (Config, $timeout, $compile, Helper, MessagingManager) {
    return {
      restrict: 'A,E',
      replace: true,
      scope: {
        contentItemData: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemContactForm/contactForm.html',
      link: function (scope, element) {

        scope.formData = [];

        scope.defaultFormItems = [
          {
            id: Helper.guid(),
            type: 'text',
            label: 'Name',
            placeholder: 'Enter your name',
            required: false
          },
          {
            id: Helper.guid(),
            type: 'text',
            label: 'Email',
            placeholder: 'Enter your email address',
            required: false
          },
          {
            id: Helper.guid(),
            type: 'textarea',
            label: 'Message',
            placeholder: 'Enter your message',
            required: false
          }
        ];

        scope.contentItemData.settings.toEmail =  scope.contentItemData.settings.toEmail || 'mail@yourwebsite.com';
        scope.contentItemData.settings.subject =  scope.contentItemData.settings.subject || 'You have a new contact form message';
        scope.contentItemData.settings.buttonText = scope.contentItemData.settings.buttonText || 'Send';
        scope.contentItemData.settings.formItems = scope.contentItemData.settings.formItems || scope.defaultFormItems;


        angular.forEach( scope.contentItemData.settings.formItems, function(item){
          if(item.id === undefined){
            item.id = Helper.guid();
          }
        });

        var newEmptyObeject = function(){
          var emptyObject = {};
          scope.formData.push(emptyObject);
        };

        angular.forEach( scope.contentItemData.settings.formItems, function(){
          newEmptyObeject();
        });

        scope.$on('newFieldAdded', function() {
          newEmptyObeject();
        });


        scope.submitForm = function () {
        };

      }
    };
  }]);
