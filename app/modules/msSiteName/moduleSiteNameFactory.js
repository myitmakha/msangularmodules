'use strict';

angular.module('msSiteName')
  .factory('msSiteName', ['$q', 'PageBuilderManager', function ($q, PageBuilderManager) {


    var checkSiteName = function(){
      var deferred = $q.defer();

      PageBuilderManager.getWebsiteData().then(
        function (data) {
          var websiteName = data.settings.siteName,
              accountId = data.settings.accountId || '123456';

          if( !websiteName || websiteName === undefined || websiteName === accountId ){
            deferred.resolve(true);
          } else {
            deferred.resolve(false);
          }
        }
      );
      return deferred.promise;
    };

    var siteNameChecker = {
      checkSiteName:checkSiteName
    };

    return siteNameChecker;
  }]);
