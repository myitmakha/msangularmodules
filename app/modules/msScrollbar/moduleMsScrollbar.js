'use strict';
angular.module('msScrollbar', [])

  .directive('msScrollbar', [function ($timeout) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {

        scope.options = scope.$eval(attrs.options) || {};

        //console.log removed.

        element.perfectScrollbar({
          includePadding: true,
          suppressScrollY: scope.options.stopScrollY || false,
          suppressScrollX: scope.options.stopScrollX || false,
          useBothWheelAxes: true
        });

        scope.$on('updateScrollbar', function () {
          element.perfectScrollbar('update');
        });

      }
    };
  }]);
