'use strict';

angular.module('msContentItemBlog')
  .directive('msContentItemBlog', ['$compile','Config', function ($compile,Config) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '=',
        contentItemMetaData: '='
      },
      template:'<div></div>',
      link: function (scope, element) {

        var getDirective = function(urlParameters){
          var param1 = urlParameters.param1;

          var directive = 'ms-content-item-blog-post-list';

          switch(param1){
            case 'post':
              scope.$emit('maincontentItem',{context:'blog', type:'post',id:urlParameters.param2});
              directive = 'ms-content-item-blog-post';
              break;
            default:
              //scope.$emit('maincontentItem',{context:'blog', type:'blog'});
              directive = 'ms-content-item-blog-post-list';
              break;
          }
          return '<div><div ng-hide="!contentItemMetaData.loading" style="position: absolute;margin-left: 50%;margin-top: 10%;">Loading...</div><div '+directive+' content-item-data="contentItemData" content-item-meta-data="contentItemMetaData"></div></div>';
        };


        var loadDirective = function(){
          $compile(getDirective((scope.contentItemMetaData||{}).urlParams))(scope, function (compiled) {
            element.html(compiled);
          });
        };

        loadDirective();
      }
    };
  }]);