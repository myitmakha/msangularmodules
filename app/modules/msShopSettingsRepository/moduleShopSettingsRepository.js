'use strict';

angular.module('msShopSettingsRepository').factory('shopSettingsRepository', ['ShopRestangular', function (ShopRestangular) {

  var shopSettingsRepository = {};

  shopSettingsRepository.UpdateShopSetttings = function (postData) {
    return ShopRestangular.all('settings/shop').customPUT(postData).then(
      function (response) {
        return response.data;
      },
      function (error) {
        //console.log removed.
        return error;
      }
    );
  };

  shopSettingsRepository.GetShopSettings = function () {
    return ShopRestangular.one('settings/shop').get().then(
      function (response) {
        return response.data;
      },
      function (error) {
        return error;
      }
    );
  };

  return shopSettingsRepository;

}]);
