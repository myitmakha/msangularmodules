'use strict';

angular.module('msScript')
  .directive('msScript', ['$compile', function ($compile) {
    return {
      replace:true,
      template: '<div></div>',
      restrict: 'E',
      link: function (scope, element, attrs) {
        var scriptSource = attrs.src;
        element.html('<script src="'+scriptSource+'"></script>');
      }
    };
  }]);