'use strict';

angular.module('msImageItem')
  .directive('msImageItem', ['$compile', '$sce', '$rootScope', 'MediaLibraryManager', 'PageBuilderManager', '$timeout', function ($compile, $sce, $rootScope, MediaLibraryManager, PageBuilderManager, $timeout) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        isEditable: '=',
        msImageItem: '=',
        multiple: '='
      },
      link: function (scope, element) {
        scope.editableId = MediaLibraryManager.randomString(); // This is use by the media library manager for unique hook to each ms-upload directive.
        scope.images = scope.$parent.images || {};
        scope.uploadImages = [];

        scope.removeImage = function () {
          scope.images[scope.msImageItem].value = '';
          scope.images[scope.msImageItem].themeImage = '';
          scope.images[scope.msImageItem].isRemove = true;

          scope.uploadImages = [];
          element.find('img').attr('ng-src', '');
          element.find('img').attr('src', '');

          scope.$parent.images = scope.images;
          scope.$parent.$root.$broadcast('markChanges', scope.msImageItem + ' has been removed.');
        };

        var compileToView = function () {
          if(scope.images[scope.msImageItem] === undefined || (scope.images[scope.msImageItem].value === '' && scope.images[scope.msImageItem].isRemove === false) ){
            scope.images[scope.msImageItem] = {value: '', themeImage: element.find('img').attr('src'), isRemove: false};
          }

          scope.displayImage = scope.images[scope.msImageItem].value;
          if(scope.displayImage === '' && !scope.images[scope.msImageItem].remove){
            scope.displayImage = scope.images[scope.msImageItem].themeImage;
          }

          element.find('img').attr('ng-src', scope.displayImage);
          element.find('img').attr('src', scope.displayImage);

          var html = element.html();
          if (scope.isEditable && scope.isEditable !== undefined) {
            element.addClass('ms-tools');
            //html += '<a class="delete icon-delete ms-button danger" style="position: absolute;top: -10px;z-index: 1;left: -8px;font-size: 0.7rem;" ng-click="removeImage()" ng-if="displayImage"></a>';
            if (!element.find('ms-media-library').attr('mode')) {
              scope.uploadImages.push({'url': '', default: 0 });
              html += '<ms-media-library button-text="Add a logo" mode="\'hover\'" editable-id="editableId" uploaded-files="uploadImages" multiple-upload=multiple title="name"></ms-media-library>';
            }
          }
          $compile(html)(scope, function (html) {
            element.html(html);
          });
        };

        scope.$watch('uploadImages', function () {
          if (scope.images[scope.msImageItem]) {
            if (scope.uploadImages.length > 0) {
              if (scope.images[scope.msImageItem].value !== scope.uploadImages[0].url) {
                scope.images[scope.msImageItem].value = scope.uploadImages[0].url;
                scope.images[scope.msImageItem].isRemove = false;

                scope.$parent.images = scope.images;
                scope.$parent.$root.$broadcast('markChanges', scope.msImageItem + ' has been changed.');
              }
            }
          }
          compileToView();
        });
      }
    };
  }]);
