'use strict';

angular.module('msContentItemHtml')
  .directive('msContentItemHtmlEdit', ['$compile', function ($compile) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {
        var compileToView = function (ele) {

          var html = '<div class="ms-no-settings">' +
                       'Embed means you can insert media from other websites.' +
                     '</div>';

          if (scope.contentItemData.settings.htmlCode) {
            html = scope.contentItemData.settings.htmlCode;
          }

          $compile(html)(scope, function (html) {
            element.html(html);
          });
        };
        compileToView(scope.contentItemData.settings.htmlCode);
        scope.$watch('contentItemData.settings.htmlCode', function (nV, oV) {
          compileToView(scope.contentItemData.settings.htmlCode);
        });
      }
    };
  }]);
