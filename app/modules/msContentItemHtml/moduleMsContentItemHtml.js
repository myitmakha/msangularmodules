'use strict';

angular.module('msContentItemHtml')
  .directive('msContentItemHtml', ['$compile', function ($compile) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {

        var html = '<div class="ms-no-settings">Add your embed code in the widget settings</div>';

        if (scope.contentItemData.settings.htmlCode) {
          html = scope.contentItemData.settings.htmlCode;
        }

        $compile(html)(scope, function (html) {
          element.html(html);
        });
      }
    };
  }]);