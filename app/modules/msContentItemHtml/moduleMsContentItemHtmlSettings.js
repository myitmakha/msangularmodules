'use strict';

angular.module('msContentItemHtml')
  .directive('msContentItemHtmlSettings', ['$compile', '$rootScope', function ($compile, $rootScope) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        settings: '='
      },
      template: '<div>' +
      '<div class="row-fix">' +
      '<div class="col-fix14">' +
      '<label>Embed Code</label>' +
      '</div>' +
      '</div>' +
      '<div class="row-fix">' +
      '<div class="col-fix14">' +
      '<textarea rows="10" cols="100"  ng-model="settings.htmlCode"/>' +
      '</div>' +
      '</div>' +
      '</div>',
      link: function (scope) {
        scope.$watch('settings', function (oldValue, newValue) {
          if (oldValue !== newValue) {
            $rootScope.$broadcast('markChanges', 'html settings has been changed.');
          }
        }, true);
      }
    };
  }]);
