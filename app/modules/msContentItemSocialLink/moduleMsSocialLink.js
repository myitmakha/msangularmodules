'use strict';

angular.module('msContentItemSocialLink')
  .directive('msContentItemSocialLink', ['$compile', '$window', 'SiteRepository', 'Config', function ($compile, $window, SiteRepository, Config) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {
        scope.openSettings = scope.$parent.actions.displaySettings;

        scope.socialLinks = scope.socialLinks || [];

        SiteRepository.getSiteSettings().then(
          function (settings) {
            scope.contentItemData.settings.socialLinks = settings.socialLinks;
          }
        );

        scope.contentItemData.settings.selectedStyle.id = scope.contentItemData.settings.selectedStyle.name.replace(' ', '-').toLowerCase();
        scope.contentItemData.settings.selectedColor.id = scope.contentItemData.settings.selectedColor.name.toLowerCase();

        var compileToView = function () {
          scope.socialLinks = [];

          angular.forEach(scope.contentItemData.settings.socialLinks, function (socialLink) {
            var url = '';
            if (socialLink.url !== '') {
              if (socialLink.name === 'facebook') {
                url = '//www.facebook.com/' + socialLink.url;
              }
              if (socialLink.name === 'twitter') {
                url = '//www.twitter.com/' + socialLink.url;
              }
              if (socialLink.name === 'pinterest') {
                url = '//www.pinterest.com/' + socialLink.url;
              }
              if (socialLink.name === 'googleplus') {
                url = '//plus.google.com/' + socialLink.url + '/posts';
              }
              if (socialLink.name === 'instagram') {
                url = '//www.instagram.com/' + socialLink.url;
              }
              if (socialLink.name === 'linkedin') {
                url = '//linkedin.com/' + socialLink.url;
              }
              if (socialLink.name === 'tumblr') {
                url = '//' + socialLink.url + '.tumblr.com/';
              }
              if (socialLink.name === 'flickr') {
                url = '//flickr.com/photos/' + socialLink.url;
              }
              if (socialLink.name === 'email') {
                url = 'mailto:' + socialLink.url;
              }
              scope.socialLinks.push({name: socialLink.name, url: url});
            }
          });
          var html = '<div class="ms-no-settings ms-tools"><button class="ms-button primary" ng-click="openSettings()">Add your Social Links</button>';

          scope.svgUrl = function( link, mask ){
            if(mask){
              return Config.GetEndPoint('modulesDirectory') + '/msContentItemSocialLink/sprites.svg#'+link+'-mask';
            } else{
              return Config.GetEndPoint('modulesDirectory') + '/msContentItemSocialLink/sprites.svg#'+link;
            }

          };

          if (scope.socialLinks.length > 0) {
            html = '<ul class="{{\'icon-shape-\' + contentItemData.settings.selectedStyle.shape}} {{\'icon-color-\' + contentItemData.settings.selectedColor.id}}" ng-class="{\'icon-style-mask\': contentItemData.settings.selectedStyle.mask}">' +
            '<li ng-repeat="(name, setting) in socialLinks" class="social-icon {{setting.name}}">' +
            '<span ng-click="open(setting.url)" >' +
            '<svg role="img" class="ms-svg-icon" viewBox="0 0 64 64">' +
            '<use class="ms-use-icon standard" xlink:href="{{svgUrl(setting.name)}}"></use>' +
            '<use class="ms-use-icon mask" xlink:href="{{svgUrl(setting.name, true)}}"></use>' +
            '</svg>' +
            '</span>' +
            '</li>' +
            '</ul>';
          }

          $compile(html)(scope, function (ele) {
            element.html(ele);
          });

        };

        scope.open = function (link) {
          $window.open(link);
        };

        compileToView();
      }
    };
  }]);
