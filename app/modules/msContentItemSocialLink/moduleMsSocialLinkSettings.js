'use strict';

angular.module('msContentItemSocialLink')
  .directive('msContentItemSocialLinkSettings', ['$compile', '$sce', '$timeout', 'SiteRepository', '$rootScope', 'Config', function ($compile, $sce, $timeout, SiteRepository, $rootScope, Config) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        settings: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemSocialLink/SocialLinkSettings.html',

      link: function (scope, element) {
        scope.settings = scope.settings || {};

        scope.socialIconStyles = [
          {id: 'standard', 'name': 'Standard', 'shape': 'standard', 'mask': false},
          {id: 'square', 'name': 'Square', 'shape': 'square', 'mask': false},
          {id: 'square-mask', 'name': 'Square Mask', 'shape': 'square', 'mask': true},
          {id: 'circle', 'name': 'Circle', 'shape': 'circle', 'mask': false},
          {id: 'circle-mask', 'name': 'Circle Mask', 'shape': 'circle', 'mask': true}
        ];

        scope.socialIconColors = [
          {id: 'dark', 'name': 'Dark'},
          {id: 'light', 'name': 'Light'},
          {id: 'standard', 'name': 'Standard'}
        ];

        var compileToView = function () {
          SiteRepository.getSiteSettings().then(
            function (settings) {
              scope.settings = settings;
              scope.settings.selectedStyle = scope.settings.selectedStyle || scope.socialIconStyles[0];
              scope.settings.selectedColor = scope.settings.selectedColor || scope.socialIconColors[0];
            }
          );
        };

        compileToView();


        scope.$watch('settings.selectedStyle', function (newValue, oldValue) {
          scope.settings.selectedStyle = newValue;
        });

        scope.$watch('settings', function (newValue, oldValue) {
          if (oldValue !== newValue) {
            $rootScope.$broadcast('markChanges', 'social link settings has been changed.');

            SiteRepository.updateSiteSettings(scope.settings).then(
              function (response) {
                var a = response;
              }
            );
          }
        }, true);
      }
    };
  }]);
