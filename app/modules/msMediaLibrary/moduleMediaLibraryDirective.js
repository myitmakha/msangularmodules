'use strict';

angular.module('msMediaLibrary')
  .directive('msMediaLibrary', ['$compile', '$rootScope', 'MediaLibraryManager', function ($compile, $rootScope, MediaLibraryManager) {
    return {
      restrict: 'E',
      scope: {
        title: '=',
        uploadedFiles: '=',
        editableId: '=',
        multipleUpload: '=',
        mode: '=',
        buttonText: '@buttonText',
        fileType: '@fileType'
      },

      link: function (scope, element) {

        scope.buttonText = scope.buttonText || 'Add an image';
        scope.fileType = scope.fileType || 'files';

        scope.$watch('uploadedFiles', function (newValue, oldValue) {
          if (newValue !== oldValue) {
            $rootScope.$broadcast('modalClose');
          }
        }, true);

        element.addClass('ms-tools clearfix');

        var html = '';

        if( scope.mode === 'dialog' ) {
          html = '<div class="align-center upload-box dropZone" ng-class="{loading : fileLoadingClass}">' +
          '<div ms-upload="uploadedFiles" ms-button-text="{{buttonText}}" ms-upload-style="{{mode}}" ms-title="{{title}}"  ms-enable-multiple=multipleUpload ms-file-type="{{fileType}}"></div>' +
          '</div>';
        } else if( scope.mode === 'hover' ) {
          element.addClass('media-wrap');

          element.parent().css({
            'position': 'relative'
          });

          html = '<div ms-upload="uploadedFiles" ng-class="{loading : fileLoadingClass}" ms-button-text="{{buttonText}}" ms-upload-style="{{mode}}" ms-title="{{title}}"  ms-enable-multiple=multipleUpload class="ms-media-library {{mode}}" ms-file-type="{{fileType}}"></div>';
        } else {
          html = '<div ms-upload="uploadedFiles" ng-class="{loading : fileLoadingClass}" ms-button-text="{{buttonText}}" ms-upload-style="{{mode}}" ms-title="{{title}}"  ms-enable-multiple=multipleUpload class="ms-media-library {{mode}}" ms-file-type="{{fileType}}"></div>';
        }

        $compile(html)(scope, function (html) {
          element.html(html);
        });

      }
    };
  }]);
