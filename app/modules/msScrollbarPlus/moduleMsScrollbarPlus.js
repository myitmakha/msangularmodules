'use strict';
angular.module('msScrollbarPlus')

  .directive('msScrollbarPlus', ['$parse', function ($parse) {

    var sbOptions = [
      'autoScrollSize', 'autoUpdate', 'disableBodyScroll', 'duration', 'ignoreMobile', 'ignoreOverlay', 'scrollStep', 'showArrows', 'stepScrolling', 'type', 'scrollx', 'scrolly', 'onDestroy', 'onInit', 'onUpdate'
    ];

    return {
      restrict: 'A',
      link: function (scope, element) {


        //var options = {};
        //
        //for (var i=0, l=sbOptions.length; i<l; i++) {
        //  var opt = sbOptions[i];
        //  if (attrs[opt] !== undefined) {
        //    options[opt] = $parse(attrs[opt])();
        //  }
        //}

        element.scrollbar({
          onScroll : function(){
            scope.$parent.$broadcast('scrollbarScolling');
          },
          autoUpdate : true
        });

      }
    };
  }]);
