'use strict';

angular.module('msChangesTrackingManager')
  .directive('msChangesNotification', ['$compile', function ($compile) {
    return {
      restrict: 'A',
      replace: true,
      scope: false,
      link: function (scope, element, attrs) {
        var html = '<div class="notification" ng-show="changesMessage"><h2>{{changesMessage}}</h2></div>';

        $compile(html)(scope, function (html) {
          element.html(html);
        });
      }
    };
  }]);