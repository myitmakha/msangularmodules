'use strict';

angular.module('msChangesTrackingManager')
  .factory('msChangesTracking', ['$rootScope', '$timeout', '$window', function ($rootScope, $timeout, $window) {

    $rootScope.changes = $rootScope.changes || [];

    $rootScope.$on('markChanges', function (event, value) {
      if ($rootScope.displayChangesTrackingMessage) {
        $rootScope.changesMessage = $rootScope.defaultChangesMessage;
      }
      if ($rootScope.changes.indexOf(value) === -1) {
        $rootScope.changes.push(value);
      }
    });

    $rootScope.$on('unMarkChanges', function (event, value) {
      if ($rootScope.changes.indexOf(value) !== -1) {
        $rootScope.changes.splice(value);
      }
    });

    $rootScope.$on('saveChanges', function (event, value) {
      $rootScope.changes = [];
      $rootScope.changesMessage = value;

      $timeout(function () {
        $rootScope.changesMessage = '';
      }, $rootScope.changesTimer);
    });

    $rootScope.$watch('changes', function () {
      if ($rootScope.changes.length > 0) {
        //console.log removed.
        $window.onbeforeunload = function () {
          return $rootScope.leavingPageMessage;
        };
      }
      else {
        $window.onbeforeunload = function () {
          return;
        };
      }
    }, true);

    //Factories always needs to return an object
    return {};
  }]);
