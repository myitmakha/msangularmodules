'use strict';

angular.module('msBlogRepository').factory('BlogRepository', ['BaseBlogRestangular', 'BlogRestangular', '$q', function (BaseBlogRestangular, BlogRestangular, $q) {

  var createBlog = function (blogDetail) {
    var deferred = $q.defer();
    BaseBlogRestangular.all('').post(blogDetail).then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var convertBlogDates = function(blog){
    if(!blog){
      return;
    }
    blog.Created=new Date(blog.Created);
    blog.LastModified=new Date(blog.LastModified);
  };

  var convertBlogPostDates = function(blogPost){
    if(!blogPost){
      return;
    }
    blogPost.Created=new Date(blogPost.Created);
    blogPost.LastModified=new Date(blogPost.LastModified);
  };

  var getBlog = function () {
    var deferred = $q.defer();
    BlogRestangular.one('').get().then(
      function (response) {
        var blog = response.data;
        convertBlogDates(blog);
        deferred.resolve(blog);
      }
    );
    return deferred.promise;
  };

  var updateBlog = function (blogDetail) {
    var deferred = $q.defer();
    BlogRestangular.all('').customPUT(blogDetail).then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  //####### POST ##########

  var createPost = function (postDetail) {
    var deferred = $q.defer();
    BlogRestangular.all('posts').post(postDetail).then(
      function (response) {
        postsDataPromise = undefined;
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getPostRestangular = function (postId) {
    var deferred = $q.defer();
    BlogRestangular.one('posts', postId).get().then(
      function (response) {
        var post = response.data;
        convertBlogPostDates(post);
        deferred.resolve(post);
      }
    );
    return deferred.promise;
  };

  var getPost = function (postId) {
    return getAllPosts().then(function(posts){
      for(var i=0;posts[i];i++){
        if(posts[i].PostId===postId){
          return posts[i];
        }
      }
      return getPostRestangular(postId);
    });
  };


  var postsDataPromise;

  var getAllPosts = function () {
    var deferred = $q.defer();
    if(!postsDataPromise){
      postsDataPromise = BlogRestangular.one('posts', '').get();
    }
    postsDataPromise.then(
      function (response) {
        var posts =[];
        if(response && response.data && response.data.ResourceList){
          posts = response.data.ResourceList;
          for(var i=0;posts[i];i++){
            convertBlogPostDates(posts[i]);
          }
        }
        deferred.resolve(posts);
      }
    );
    return deferred.promise;
  };

  var updatePost = function (postId, postDetail) {
    var deferred = $q.defer();
    BlogRestangular.all('posts/' + postId).customPUT(postDetail).then(
      function (response) {
        postsDataPromise = undefined;
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var removePost = function(postId){
    var deferred = $q.defer();
    BlogRestangular.one('posts').customDELETE(postId).then(
      function (response) {
        postsDataPromise = undefined;
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var blogRepository = {
    createBlog: createBlog,
    getBlog: getBlog,
    updateBlog: updateBlog,

    createPost: createPost,
    getPost: getPost,
    getAllPosts: getAllPosts,
    updatePost: updatePost,
    removePost: removePost
  };

  return blogRepository;
}]);
