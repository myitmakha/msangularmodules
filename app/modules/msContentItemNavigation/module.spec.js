//'use strict';
//
//describe('msContentItemNavigation directive', function () {
//  // load the controller's module
//  beforeEach(module('msContentItemNavigation'));
//
//  var pageHtml = '<div ms-content-item-navigation></div>';
//  var scope, elm, $compile;
//
//  //Load scope
//  beforeEach(inject(function ($rootScope, _$compile_) {
//    scope = $rootScope.$new();
//    $compile = _$compile_;
//  }));
//
//  //Mock page repository
//  var mockPageRepository = function (pages) {
//    inject(function (PageRepository, $q) {
//      var defered = $q.defer();
//      defered.resolve(pages);
//      spyOn(PageRepository, 'getPages').andReturn(defered.promise);
//    });
//  };
//
//  var compileDirective = function (pages, tpl) {
//    mockPageRepository(pages);
//    if (!tpl) tpl = pageHtml;
//    tpl = '<form name="form">' + tpl + '</form>';
//    var form = $compile(tpl)(scope);
//    elm = form.children().eq(0);
//    scope.$digest();
//  };
//
//  it('the navigation renders no link', function () {
//    compileDirective([]);
//    var links = elm.find('a');
//    expect(links.length).toBe(0);
//  });
//
//  it('the navigation renders one link', function () {
//    compileDirective([{name:'Home',url:'/home'}]);
//    var links = elm.find('a');
//    expect(links.length).toBe(1);
//    expect(links.text()).toBe('Home');
//    expect(links.attr('href')).toBe('/#/home');
//  });
//
//  it('the navigation renders multiple links', function () {
//    compileDirective([{name:'Home',url:'/home'},{name:'About',url:'/about'},{name:'contactUs',url:'/contactus'}]);
//    var links = elm.find('a');
//    expect(links.length).toBe(3);
//    expect(links.eq(0).text()).toBe('Home');
//    expect(links.eq(0).attr('href')).toBe('/#/home');
//    expect(links.eq(1).text()).toBe('About');
//    expect(links.eq(1).attr('href')).toBe('/#/about');
//    expect(links.eq(2).text()).toBe('contactUs');
//    expect(links.eq(2).attr('href')).toBe('/#/contactus');
//  });
//
//});
