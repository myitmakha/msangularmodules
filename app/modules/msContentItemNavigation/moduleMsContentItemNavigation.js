/**
 * Created by Albert on 15/07/2014.
 */
'use strict';

angular.module('msContentItemNavigation')
  .directive('msContentItemNavigation', ['SiteRepository', 'Config', '$timeout', '$rootScope', 'MenuRepository', 'PageBuilderManager', 'Helper', '$q', function (SiteRepository, Config, $timeout, $rootScope, MenuRepository, PageBuilderManager, Helper, $q) {
    return {
      restrict: 'A',
      replace: true,
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemNavigation/msContentItemNavigation.tpl.html',
      link: function (scope, element) {

        var getData = function(){
          $q.all([
            MenuRepository.getMenu(),
            PageBuilderManager.getPages()
          ]).then(function(data) {

            scope.menu = data[0];
            scope.pages = data[1];

            shopUrl();
            getPageData(scope.menu, scope.pages);

          });
        };
        
        var shopUrl = function(){
          var shopFound = false;
          angular.forEach(scope.pages, function (page) {
            if(shopFound){ return; }
            if(page.type === 'shop'){
              shopFound = true;
              scope.shopUrl = page.url + '/';
            } else {
              scope.shopUrl = undefined;
            }
          });
        };

        var getPageData = function ( menuList, pages ) {
          angular.forEach(menuList, function (menuItem) {

            menuItem.id = Helper.guid();

            if(menuItem.type === 'Page'){
              angular.forEach(pages, function (page) {
                if(page.id === menuItem.pageId){
                  menuItem.name = page.name;
                  menuItem.url = page.url;
                }
              });
            }else if(menuItem.type === 'Category'){

              if(scope.shopUrl === undefined){
                menuItem.hideInMenu = true;
              }

              var lastItem = menuItem.categories.length - 1;
              menuItem.name = menuItem.categories[lastItem];
              menuItem.url = menuItem.categories.join('/');
            }

            if(menuItem.hasOwnProperty('subMenuItems')){
              getPageData(menuItem.subMenuItems, pages);
            }
          });
        };

        var menuOpen = element.find('.mobile-menu'),
          menuClose = element.find('.mobile-menu-close');

        getData();

        menuOpen.on('click', function(){
          element.addClass('open');
          $('body').addClass('menu-open');
          $('body').css({
            'overflow': 'hidden'
          });
        });

        menuClose.on('click', function(){
          element.removeClass('open');
          $('body').removeClass('menu-open');
          $('body').removeAttr('style');
        });

        $rootScope.$on('menuChanged', function () {
          getData();
        });

        var nav = element.find('ul.nav'),
            links = nav.find('a');

        scope.linkClicked = function(){
          element.removeClass('open');
          $('body').removeClass('menu-open');
          $('body').removeAttr('style');
        };

      }
    };
  }]);