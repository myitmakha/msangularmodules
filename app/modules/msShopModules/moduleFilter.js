'use strict';

angular.module('msShopModules')
  .directive('msFilter', ['$timeout', 'HelperService', 'Config', function($timeout, HelperService, Config) {
      return {
        restrict: 'A',
        scope: {
          searchQuery: '=msFilterSearchQuery',
          response: '=msFilterResponse',
          staticFilters: '=msStaticFilters'
        },
        templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msShopModules/filter.html',
        link: function(scope, elm) {
         // if(!scope.model) return; // do nothing if no model
          scope.filtered = {};
          if (scope.searchQuery === undefined) {
            scope.searchQuery = {
              Facets : {},
              PageNum : 0,
              PageSize : 10,
              Query : ''
            };
          } else if (scope.searchQuery.Facets !== undefined){
            scope.filtered = scope.searchQuery.Facets;
          }

          scope.filterCount = 0;

          scope.setTagitVisibility = function() {
            scope.tagitVisibilityClass = Object.keys(scope.filtered).length > 0;
          };
          scope.setTagitVisibility();


          scope.removeActiveFilters = function(facetList) {
            angular.forEach(scope.filtered, function(value, key){
              angular.forEach(value.Values, function(filter){
                if (facetList !== undefined && facetList[key] !== undefined && facetList[key][filter] !== undefined) {
                  delete facetList[key][filter];
                }
              });
            });
            return facetList;
          };

          scope.$watch('response.FacetList', function(newValue) {
            if (newValue !== undefined) {
              scope.filters = scope.removeActiveFilters(scope.response.FacetList);
            }
          });

          scope.doSearch = function() {
            scope.searchQuery.PageNum = 0;
            scope.setTagitVisibility();
          };

          scope.$watch('searchQuery.Query', HelperService.debounce(function() {
            scope.searchQuery.Query = this.last;
          }, 500, false));


          scope.addItem = function (filterGroup, filter) {
            scope.filterCount++;
            if (scope.filtered[filterGroup] === undefined) {
              scope.filtered[filterGroup] = {Values:[]};
            }
            scope.filtered[filterGroup].Values.push(filter);
            scope.filterGroupSelect = 0;
            scope.filterSelect = 0;
            scope.searchQuery.Facets = scope.filtered;
            scope.doSearch();
          };


          scope.deleteItem = function (filterGroup, filter) {
            scope.filterCount--;
            scope.filtered[filterGroup].Values.splice(filter, 1);
            scope.searchQuery.Facets = scope.filtered;
            scope.doSearch();
          };


          var selectValue = elm.find('select.select-value');
          scope.triggerSelectValue = function () {
            $timeout(
              function(){
                //move option back up
                selectValue.find('option.default').insertBefore(selectValue.find('option:first-child'));
                selectValue[0].selectedIndex = 0;
                //refocus
                if (document.createEvent) {
                  var e = document.createEvent('MouseEvents');
                  e.initMouseEvent('mousedown', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                  selectValue[0].dispatchEvent(e);
                } else if (elm.fireEvent) {
                  selectValue[0].fireEvent('onmousedown');
                }
              }
            );
          };


        }
      };
    }]);