'use strict';

angular.module('msShopModules')
  .directive('msAutocomplete', ['$timeout', function($timeout) {
    return {
      restrict: 'A',
      scope: {
        autoCompleteItems: '=autoCompleteItems',
        model: '=model'
      },
      link : function(scope, elm) {
        if(!scope.model){
          return; // do nothing if no model
        }
        scope.autoCompleteItems = (scope.autoCompleteItems) ? scope.autoCompleteItems : [];
        $(elm).parent().addClass('ui-front');
        $(elm).autocomplete({
          source: scope.autoCompleteItems, // for autocomplete
          autoFocus: true
        }).bind('focus', function(){ $(this).autocomplete('search'); } );


        var fieldsAvailable = scope.autoCompleteItems, fieldsUsed = [];

        scope.$watchCollection('model', function(newFields) {
          fieldsAvailable = scope.autoCompleteItems;
          //when model change, update autoCompleteItems for a filtered autocomplete
          fieldsUsed = [];
          angular.forEach(newFields, function(value, key){
            fieldsUsed[key] = value.name;
          });

          fieldsAvailable  =
            fieldsAvailable.filter(
              function(x) {
                return fieldsUsed.indexOf(x) < 0;
              }
            );

          $(elm).autocomplete('option', { source: fieldsAvailable });
        });

      }
    };
  }]);
