'use strict';

angular.module('msShopModules').factory('ShopModel', function() {
  var Shop = function(){
    var self = this;
    self.owner = '';
    self.categories = ['Food', 'Fruit', 'Baby', 'Clothes', 'Jeans', 'Book'];
    self.tags = ['Food', 'Fruit', 'Baby', 'Clothes', 'Jeans', 'Book', '3D', 'Adsense', 'advertising', 'AJAX', 'animation', 'Apps', 'Art', 'ASCII', 'Avatars', 'Backgrounds', 'Best Of', 'Best Practices', 'blogging', 'Books', 'branding', 'Browsers', 'brushes', 'Business', 'Business cards', 'buttons', 'Calendars', 'Calligraphy', 'cartoons', 'carts', 'CG', 'charts', 'Cheat Sheets', 'Christmas', 'Clients', 'CMS', 'CMYK', 'coding', 'colors', 'Communication', 'Community', 'Conferences', 'Content', 'Contests', 'contracts', 'Copywriting', 'corporate', 'covers', 'Creativity', 'CSS', 'data visualization', 'Design', 'development', 'diagrams', 'Discussions', 'domains', 'Downloads', 'drawing', 'dreamweaver', 'E-Commerce', 'eBooks', 'editors', 'Email', 'Emotional Design', 'Errors', 'Events', 'FAQ', 'favicons', 'Flash', 'Fonts', 'Forms', 'Freebies', 'Free Fonts', 'gadgets', 'galleries', 'Global Web Design', 'Google', 'Graphics', 'grids', 'guidelines', 'halloween', 'Icons', 'ideas', 'illustrations', 'Illustrator', 'Infographics', 'innovation', 'Inspiration', 'Internet Explorer', 'Interviews', 'layouts', 'legacy', 'logo design', 'Mac', 'magazines', 'Marketing', 'movies', 'Music', 'Navigation', 'Open Source', 'Opinion Column', 'optimization', 'paintings', 'PDF', 'Performance', 'Photography', 'Photoshop', 'Portfolios', 'Posters', 'principles', 'print', 'Process', 'productivity', 'PSD', 'psychology', 'retro', 'reviews', 'SEO', 'Showcases', 'signs', 'Skills', 'Smashing Book', 'Smashing Daily', 'Smashing Library', 'Smashing Magazine', 'Social', 'software', 'Studies', 'Techniques', 'Templates', 'testing', 'Textures', 'The Lost Files', 'Themes', 'The Smashing Newsletter', 'time management', 'time savers', 'Tools', 'Trends', 'Tutorials', 'Twitter', 'Typography', 'Useful', 'User Interaction', 'vectors', 'Videos', 'vintage', 'visualizations', 'Wallpapers', 'Web', 'web 2.0', 'Web Design', 'weblogs', 'Wireframing', 'WordPress', 'Workflow'];
    self.attributes = ['Colour', 'Size', 'Material'];
    self.fields = ['Author'];
    self.productTypes = ['Food', 'Fruit', 'Book'];
    self.collection = {'Author': ['Bob', 'Jimi'], 'RAM': ['1mb', 'green', 'blue'], 'Colour': ['red', 'green', 'blue'], 'Material': ['plastic', 'metal', 'fiber'], 'Size': ['small', 'medium', 'large', '1','2','3','4','5','6','7','8','9','10']};
  };

  return {
    create: function(value){
      if(value === undefined){
        return new Shop();
      }
      else{
        var newObj = new Shop();

        newObj.owner = value.owner;
        newObj.categories = value.categories;
        newObj.attributes = value.attributes;
        newObj.fields = value.fields;
        newObj.productTypes = value.productTypes;
        newObj.collection = value.collection;

        return newObj;
      }
    }
  };
});