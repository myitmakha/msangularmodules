'use strict';

angular.module('msShopModules').factory('ProductService', ['ShopRestangular', '$q', function (ShopRestangular, $q) {

  var productService = {};

  var productPromise;
  var retries = 0;
  productService.getAll = function (ignoreCache) {
    if(!productPromise||ignoreCache){
      var deferred = $q.defer();
      ShopRestangular.one('products', '').get().then(function(response){
        if(response&&response.data&&response.data.ResourceList){
          return response.data.ResourceList;
        }
        return [];
      },function(error){
        retries++;
        //If we get an error we retry (maximum 5 times)
        if(retries<=5){
          return productService.getAll(true);
        }
      }).then(function(products){
        deferred.resolve(products);
      });
      productPromise = deferred.promise;
    }
    return productPromise;
  };

  productService.get = function (id) {
    var deferred = $q.defer();
    ShopRestangular.one('products', id).get().then(
      function (response) {
        var product = response.data;
        deferred.resolve(product);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  productService.save = function (postData) {
    productPromise = undefined;
    return ShopRestangular.all('products').post(postData);
  };

  productService.update = function (id, postData) {
    productPromise = undefined;
    return ShopRestangular.all('products/' + id).customPUT(postData);
  };

  productService.remove = function (id) {
    productPromise = undefined;
    return ShopRestangular.all('products/' + id).remove();
  };

  return productService;
}]);
