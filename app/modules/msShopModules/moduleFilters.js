'use strict';

angular.module('msShopModules')
  .filter('removeInvalidChars', function() {
    return function(input) {
      return input.replace(/[^a-zA-Z\s]/g, ' ');
    };
  })
  //keep objects in order
  .filter('fixedOrder', function() {
    return function(input) {
      if (!input) {
        return [];
      }
      return Object.keys(input);
    };
  })
  //add your new filter logic here
  .filter('addAnotherFilterHere', function() {
    return function(input) {
      return input + 's';
    };
  }
);