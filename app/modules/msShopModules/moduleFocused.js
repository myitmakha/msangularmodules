'use strict';

angular.module('msShopModules')
  .directive('msFocus', [function() {
    var FOCUS_CLASS = 'ms-focused';
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attrs, ctrl) {
        ctrl.$focused = false;
        element.bind('focus', function(evt) {
          element.addClass(FOCUS_CLASS);
          scope.$apply(function() {ctrl.$focused = true;});
          if (attrs.msFocus) {
            scope.$eval(attrs.msFocus);
          }
        }).bind('blur', function(evt) {
            element.removeClass(FOCUS_CLASS);
            scope.$apply(function() {ctrl.$focused = false;});
            if (attrs.msFocusBlur) {
              scope.$eval(attrs.msFocusBlur);
            }
          });
      }
    };
  }]);