'use strict';

angular.module('msShopModules')
  .directive('msDraggable', ['$timeout', function($timeout) {
      return {
        restrict: 'A',
        transclude: true,
        scope: {
          ngModel: '=msDraggable'
        },
        replace:  true,
        template: '<ul ng-transclude></ul>',
        link: function(scope, elm, attrs) {
          // set variables to be used
          var startIndex, newIndex, toMove, original;
          $(elm).sortable({
            delay: 150,
            revert: true,
            scroll: false,
            placeholder: 'sortable-placeholder',
            cancel: '.ui-state-disabled',
            forcePlaceholderSize: true,
            forceHelperSize: true,
            containment: 'parent',
            axis: 'y',
            tolerance: 'pointer',
            start: function(event, ui) {
              // on start we define where the item is dragged from
              startIndex = $(ui.item).index();
              //dont want li's to float when dragging
              $(this).addClass('no-float');
            },
            update: function(event, ui) {
              //clean up class
              $(this).removeClass('no-float');
              // get the new index of the dragged item
              newIndex = $(ui.item).index();
              // get the dragged item from model
              toMove = scope.ngModel[startIndex];
              // remove the dragged item from model
              scope.ngModel.splice(startIndex,1);
              // add the dragged item to the correct position in the model object
              scope.ngModel.splice(newIndex,0,toMove);
              // restore original dom order, so angular does not get confused
              $(this).children('li').eq(newIndex).insertBefore($(this).children('li').eq(startIndex));
              // notify angular of the change, which will update dom order correctly! (mission!)
              scope.$apply();
            }
          })
            .disableSelection()
            .sortable('disable');

          // Listen to fileuploadstop event
          scope.$watch('ngModel', function(newValue, oldValue){
            if ( newValue !== oldValue && scope.ngModel.length > 1) {
              $(elm).sortable('enable');
            }
          }, true);


        }
      };
    }]);