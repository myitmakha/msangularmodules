'use strict';

angular.module('msShopModules').factory('OrderService', ['ShopRestangular', '$q', function (ShopRestangular, $q) {
  var createOrder = function (basket) {
    var orderBasket = {};
    for (var i = 0; basket[i]; i++) {
      orderBasket[basket[i].id] = basket[i].quantity;
    }
    var deferred = $q.defer();
    ShopRestangular.all('orders').post(orderBasket).then(
      function (response) {
        deferred.resolve(response.data);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getAllOrder = function () {
    var deferred = $q.defer();
    ShopRestangular.one('orders', '').get().then(
      function (response) {
        var orders = response.data;
        deferred.resolve(orders);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getOrderById = function (id) {
    var deferred = $q.defer();
    ShopRestangular.one('orders', id).get().then(
      function (response) {
        var order = response.data;
        deferred.resolve(order);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var updateOrderStatus = function (orderId, status) {
    var deferred = $q.defer();
    var postData = {Status: status};
    ShopRestangular.all('orders/' + orderId + '/status').customPUT(postData).then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  return {
    GetAllOrder: getAllOrder,
    GetOrderById: getOrderById,
    createOrder: createOrder,
    UpdateOrderStatus: updateOrderStatus
  };
}]);
