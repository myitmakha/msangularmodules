'use strict';

//function Product(json){
//  angular.extend(this, json);
//}
//
//Product.create = function(value) {
//  if (value === undefined) {
//    return new Product({
//      name: '',
//      description: '',
//      images: [],
//      attributes: [],
//      variants: [],
//      categories: [],
//      tags: [],
//      fields: [],
//      published: false,
//      productSettings: {
//        taxSettings: {
//          taxExempt: false
//        },
//        additionalShipping: 0
//      }
//    });
//  }
//  else {
//    return new Product({
//      id: value.id,
//      name: value.name,
//      description: value.description,
//      images: value.images,
//      attributes: value.attributes.ResourceList,
//      variants: value.variants.ResourceList,
//      categories: value.categories,
//      tags: value.tags,
//      fields: value.fields,
//      published: value.published,
//      productSettings: value.productSettings
//    });
//  }
//};

angular.module('msShopModules').factory('ProductModel', function() {
  return {
    create: function(value) {
      if (value === undefined) {
        return {
          name: '',
          description: '',
          images: [],
          attributes: [],
          variants: [],
          categories: [],
          tags: [],
          fields: [],
          published: false,
          productSettings: {
            taxSettings: {
              taxExempt: false
            },
            additionalShipping: 0
          }
        };
      }
      else {
        return {
          id: value.id,
          name: value.name,
          description: value.description,
          images: value.images,
          attributes: value.attributes.ResourceList,
          variants: value.variants.ResourceList,
          categories: value.categories,
          tags: value.tags,
          fields: value.fields,
          published: value.published,
          productSettings: value.productSettings
        };
      }
    }
  };
});




//  return {
//    create: function (value) {
//      if (value === undefined) {
//        this.name = '';
//        this.description = '';
//        this.images = [];
//        this.attributes = [];
//        this.variants = [];
//        this.categories = [];
//        this.tags = [];
//        this.fields = [];
//        this.published = false;
//        this.productSettings = {
//          taxSettings: {
//            taxExempt: false
//          }
//        };
//        this.productSettings.additionalShipping = 0;
//      }
//      else {
//        this.id = value.id;
//        this..name = value.name;
//        this.description = value.description;
//        this.images = value.images;
//        this.attributes = value.attributes.ResourceList;
//        this.variants = value.variants.ResourceList;
//        this.categories = value.categories;
//        this.tags = value.tags;
//        this.fields = value.fields;
//        this.published = value.published;
//        this.productSettings = value.productSettings;
//      }
//    }
//  };



//
//  var Product = function(){
//    var self = this;
////    var initialSettings = atts || {};
////    //initial settings if passed in
////    for(var setting in initialSettings){
////      if(initialSettings.hasOwnProperty(setting)) {
////        self[setting] = initialSettings[setting];
////      }
////    }
//    self.name = '';
//    self.description = '';
//    self.images = [];
//    self.attributes = [];
//    self.variants = [];
//    self.categories = [];
//    self.tags = [];
//    self.fields = [];
//    self.published  = false;
//    self.productSettings = {
//      taxSettings : {
//        taxExempt : false
//      }
//    };
//
//    self.productSettings.additionalShipping = 0;
//    //self._embedded = [];
//    //self._links = [];
//  };
//
//  return {
//    create: function(value){
//      if(value === undefined){
//        return new Product();
//      }
//      else{
//        var newObj = new Product();
//        newObj.id = value.id;
//        newObj.name = value.name;
//        newObj.description = value.description;
//        newObj.images = value.images;
//        newObj.attributes = value.attributes.ResourceList;
//        newObj.variants = value.variants.ResourceList;
//        newObj.categories = value.categories;
//        newObj.tags = value.tags;
//        newObj.fields = value.fields;
//        newObj.published  = value.published;
//        newObj.productSettings = value.productSettings;
//        //newObj._embedded = value._embedded;
//        //newObj._links = value._links;
//
//        return newObj;
//      }
//    }
//  };
//

//});

