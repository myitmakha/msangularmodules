'use strict';

angular.module('msShopModules')
  .directive('msMaxHeight', ['$timeout', 'HelperService', function($timeout, HelperService) {
    return {
      restrict: 'A',
      scope: {
        maxHeightModel: '=msMaxHeightModel'
      },
      transclude: true,
      replace:  true,
      template: '<div ng-transclude class="max-height" style="max-height: {{msMaxHeight}}px; -webkit-transition: max-height .3s; transition: max-height .3s;"></div>',
      link: function(scope, elm, attrs) {
        if (!attrs.msMaxHeight){
          return;
        }

        var elems = attrs.msMaxHeight.split(' '),
          height = 0,
          windowElHeight = 0,
          firstElHeight = 0,
          secondElHeight = 0,
          windowEl,
          firstEl,
          secondEl,
          maxHeightOffset = (attrs.msMaxHeightOffset) ? attrs.msMaxHeightOffset : 0,
          win = $(window);

        function setHeights(){
          windowEl = (windowEl === undefined) ? $('.'+elems[0]) : windowEl;
          firstEl = (firstEl === undefined) ? $('.'+elems[1]) : firstEl;
          secondEl = (secondEl === undefined) ? $('.'+elems[2]) : secondEl;

          windowElHeight = (windowEl !== undefined) ? windowEl.outerHeight(true) : 0;
          firstElHeight = (firstEl !== undefined) ? firstEl.offset().top + firstEl.outerHeight(true) : 0 ;
          secondElHeight = (secondEl !== undefined) ? secondEl.outerHeight(true) : 0;
          height = windowElHeight - (firstElHeight + secondElHeight) - maxHeightOffset;

//          //console.log removed.
//          //console.log removed.
//          //console.log removed.
//          //console.log removed.

          scope.msMaxHeight = (height) ? height : 100;
        }



        win.on('resize', HelperService.debounce(function(){
          setHeights();
        }, 500, false));

        scope.$watch('msMaxHeightModel', function(newValue, oldValue) {
          $timeout(
            function(){
              setHeights();
            }, 1000
          );
        });

        if (attrs.msMaxHeightOverflowYHidden) {
          $('.'+attrs.msMaxHeightOverflowYHidden).css('overflow-y','hidden');
        }

        scope.$on('$destroy', function() {
          $('.'+attrs.msMaxHeightOverflowYHidden).attr('style','');
          win.off('resize');
        });

      }
    };
  }]);