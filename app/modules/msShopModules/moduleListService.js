'use strict';

angular.module('msShopModules').factory('listService', ['Restangular', function(Restangular) {

  var listService = {};

  listService.search = function(searchQuery, type){
    if(type === 'products'){
      return Restangular.one('search/products').get(searchQuery);
    }
    else{
      return Restangular.one(type).get(searchQuery);
    }
  };

  listService.remove = function(id, type){
    return Restangular.one(type, id).remove();
  };

  return listService;
}]);