
/*extend the jquery tagit plugin*//*
$.widget( "custom.tagit", $.ui.tagit, {
  reAssignAutocomplete: function(autocompleteChoices) {
    //console.log removed.
    var that = this;
    this.options.autocomplete.source = function(search, showChoices) {
      var filter = search.term.toLowerCase();
      var choices = $.grep(autocompleteChoices, function(element) {
        // Only match autocomplete options that begin with the search term.
        // (Case insensitive.)
        return (element.toLowerCase().indexOf(filter) === 0);
      });
      if (!this.options.allowDuplicates) {
        choices = this._subtractArray(choices, this.assignedTags());
      }
      showChoices(choices);
    };

    // Bind autocomplete.source callback functions to this context.
    if ($.isFunction(this.options.autocomplete.source)) {
      this.options.autocomplete.source = $.proxy(this.options.autocomplete.source, this);
    }
    // Autocomplete.
    if (this.options.availableTags || this.options.tagSource || this.options.autocomplete.source) {
      var autocompleteOptions = {
        select: function(event, ui) {
          that.createTag(ui.item.value);
          // Preventing the tag input to be updated with the chosen value.
          return false;
        }
      };
      $.extend(autocompleteOptions, this.options.autocomplete);

      // tagSource is deprecated, but takes precedence here since autocomplete.source is set by default,
      // while tagSource is left null by default.
      autocompleteOptions.source = this.options.tagSource || autocompleteOptions.source;

      this.tagInput.autocomplete(autocompleteOptions).bind('autocompleteopen.tagit', function(event, ui) {
        that.tagInput.data('autocomplete-open', true);
      }).bind('autocompleteclose.tagit', function(event, ui) {
          that.tagInput.data('autocomplete-open', false)
        });

      this.tagInput.autocomplete('widget').addClass('tagit-autocomplete');
    }
  }
});*/