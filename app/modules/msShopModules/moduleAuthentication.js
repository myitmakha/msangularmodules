'use strict';

angular.module('msShopModules').factory('$authShop', ['Restangular', '$window', function(Restangular, $window/*, $location*/){
  var auth = {};

  auth.isLoggedIn = function(){
    if($window.sessionStorage.token === undefined || $window.sessionStorage.token === '' || $window.sessionStorage.token === 'null'){
      //$location.path('/');
      //auth.setToken('9bdd0891-853b-4fce-b68f-4fb3d94414e8');
      return false;
    }
    else{
      // TODO: need to check here, is the token valid or not via api
      if(Restangular.defaultHeaders.Authorization === undefined){
        //auth.setShopId('9bdd0891-853b-4fce-b68f-4fb3d94414e8');
        setDefaultHeaders();
      }
      return true;
    }
  };

  auth.setToken = function(token){
    $window.sessionStorage.token = token;
    setDefaultHeaders();
    return true;
  };

  auth.setIdToken = function(idToken){
    $window.sessionStorage.idToken = idToken;
    setDefaultHeaders();
    return true;
  };

  auth.setShopId = function(shopId){
    $window.sessionStorage.shopId = shopId;
    setDefaultHeaders();
    return true;
  };

  var setDefaultHeaders = function(){
    //Restangular.setBaseUrl(config.apiEndpoint + 'shops/'+$window.sessionStorage.shopId+'/');
    Restangular.setDefaultHeaders({ 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + $window.sessionStorage.token,'Authorization-Id': 'Bearer ' + $window.sessionStorage.idToken });
  };

  return auth;
}]
);