'use strict';

angular.module('msShopModules')
  .directive('msAddshadow', function () {
    return{
      restrict: 'A',
      scope: {
        msAddshadow: '@',
      },
      link: function (scope, element, attrs) {
        var scroll = $('.' + scope.msAddshadow).scrollTop();

        $('.' + scope.msAddshadow).on('scroll', function() {
          //console.log removed.
          if ( scroll > 5 ) {
            element.addClass('shadow');
          }else{
            element.removeClass('shadow');
          }
        });
      }
    };
  });