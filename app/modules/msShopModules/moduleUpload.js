'use strict';

angular.module('msShopModules')
  .directive('msUploadDuplicated', ['$timeout', 'Config', '$window', function($timeout, Config, $window) {
      return {
        restrict: 'A',
        scope: {
          files: '=msUpload',
          title: '@msTitle',
          enableMultiple: '=msEnableMultiple'
        },
        templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msShopModules/upload.html',
        link: function(scope, elm) {
          //console.log removed.
         // if(!scope.model) return; // do nothing if no model
          var header = {'Authorization': 'Bearer ' + $window.sessionStorage.token};

          function setWidthsHeights(items) {
            scope.height = (items <= 3) ? '100%' : '50%';

            switch(items)
            {
              case 1:
                scope.cols = 24;
                break;
              case 3:
                scope.cols = 8;
                break;
              default:
                scope.cols = 12;
            }
          }

          // Listen to fileuploaddone event
          scope.$on('fileuploaddone', function(e ,data){
            data.headers = header;
            var uploadedImages = data._response.result;
            var i=0;
            while(uploadedImages[i]){
              scope.files.push({url:uploadedImages[i].Url});
              i++;
            }
          });

          //Override the url where is going to upload the picture
          scope.$on('fileuploadadd', function(e, data){
            data.headers = header;
            data.url = Config.GetEndPoint('siteApiEndpoint') + 'shops/' + data.scope.$parent.$root.shopId + '/upload';
            scope.$parent.fileLoadingClass = ' loading';
            scope.fileLoadingError = false;
          });
          //always runs
          scope.$on('fileuploadstop', function(){
            scope.$parent.fileLoadingClass = '';
          });
          //runs on error
          scope.$on('fileuploadfail', function(){
            //error message
            scope.$parent.fileLoadingError = true;
          });
          //runs on file drag over

          var timer;
          scope.$on('fileuploaddragover', function(){
            $timeout(function(){
              scope.$parent.fileDragOverClass = 'file-drag-over';
            });
            $timeout.cancel(timer);
            timer = $timeout(function(){
              scope.$parent.fileDragOverClass = false;
            }, 100);
          });
          //runs on file drop over
          scope.$on('fileuploaddrop', function(){
            scope.$parent.fileDragOverClass = false;
          });


          scope.deleteItem = function(id){
            scope.files.splice(id, 1);
          };

          var insert, scrollWindow;
          scope.setActive = function(id){
            if (id !== 0) {
              //get scroll window
              scrollWindow = elm.find('.grid');
              //get model clicked, ready to insert at top of array
              insert = scope.files[id];
              //remove it
              scope.files.splice(id, 1);
              //scroll to top
              scrollWindow.animate({ scrollTop: '0' });
              //add it back to start of array
              $timeout(function(){
                scope.files.unshift(insert);
              }, 300);
            }
          };


          //runs on files.length
          scope.$watch('files.length', function(newValue, oldValue){
            if ( newValue !== oldValue ) {
              setWidthsHeights(newValue);
            }
          });


        }
      };
    }]);