'use strict';

angular.module('msShopModules')
  .factory('UnauthorizedInterceptor', function($q, $location) {
    return {

      responseError: function (rejection) {
        //console.log removed. // Contains the data about the error.

        if(rejection.status === 401) {
          $location.path('/login');
          return $q.reject(rejection);
        } else {
          return $q.reject(rejection);
        }
      }
    };
  });
