'use strict';

angular.module('msShopModules')
  .directive('msExpander', function () {
    return{
      restrict: 'A',
      link: function (scope, element, attrs) {
        
        var parentWrap,
            parentOriginalHeight,
            index,
            expanderHeight,
            oldTopPos,
            currentTopPos,
            expander;

        element.on('click', 'a.edit', function() {

          parentWrap = $(this).parents('li');
          expander = parentWrap.find('.expander');
          parentOriginalHeight = parentWrap.outerHeight();
          index = parentWrap.index();
          expanderHeight = expander.outerHeight();
          oldTopPos = element.find('li.expanded').offset();
          currentTopPos = parentWrap.offset();

          function expanderSwitch(){
            if( parentWrap.hasClass('expanded') ){
              return false;
            } else {
              element.find('li.expanded').removeClass('expanded');
              element.find('.expander').not( ':eq('+index+')' ).hide();
              expander.show();
              parentWrap.addClass('expanded');
            }
          }

          function expanderSlide(){
            if( parentWrap.hasClass('expanded') ){
              return false;
            } else {
              element.find('li.expanded').removeClass('expanded');
              element.find('.expander').not( ':eq('+index+')' ).slideUp();
              expander.slideDown();
              parentWrap.addClass('expanded');
            }
          }

          if( element.find('li.expanded').length === 0 ){
            expanderSlide();
          } else {
            if( oldTopPos.top === currentTopPos.top ){
              expanderSwitch();
            } else {
              expanderSlide();
            }
          }
        }).on('click', 'a.close', function() {
          expander.slideUp();
          parentWrap.removeClass('expanded');
        
        });
      }
    };
  });