'use strict';

angular.module('msShopModules').factory('ShopService', ['MenuRepository','MenuManager','ShopRestangular', '$q','$locale', function(MenuRepository,MenuManager,ShopRestangular, $q, $locale) {

  var getCategories = function(){
    var deferred = $q.defer();
    ShopRestangular.one('categories').get().then(
      function (response) {
        var categories = response.data;
        deferred.resolve(categories);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getCategoryHierarchies = function(){
    var deferred = $q.defer();
    ShopRestangular.one('categoryhierarchies').get().then(
      function (response) {
        var categoryHierarchies = response.data;
        deferred.resolve(categoryHierarchies.categoryHierarchies);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var updateCategoryHierarchies = function(postData){
    var deferred = $q.defer();
    ShopRestangular.all('categoryhierarchies').customPUT({'categoryHierarchies':postData}).then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    /*MenuRepository.getMenu().then(function(menu){
      //console.log removed.
      MenuManager.includeSubCategoryHierarchiesOnMenu(menu, postData);
    });*/
    return deferred.promise;
  };

  

  var setAppCurrencyFromShopSettings = function(){
    getShop().then(function (shop) {
      var symbol;
      if (shop.Currency && shop.Currency.Symbol) {
        symbol = shop.Currency.Symbol;
      }
      var currency = {symbol: symbol || '£'};
      setAppCurrency(currency);
    });
  };

  var setAppCurrency = function(currency){
    $locale.NUMBER_FORMATS.CURRENCY_SYM=currency.symbol||$locale.NUMBER_FORMATS.CURRENCY_SYM;
    $locale.NUMBER_FORMATS.DECIMAL_SEP=currency.decimal||$locale.NUMBER_FORMATS.DECIMAL_SEP;
    $locale.NUMBER_FORMATS.GROUP_SEP=currency.GROUP_SEP||$locale.NUMBER_FORMATS.GROUP_SEP;
  };

  var shopPromise;
  var getShop = function(){
    if(!shopPromise){
      var deferred = $q.defer();
      ShopRestangular.one('').get().then(
        function (response) {
          var shop = response.data;
          deferred.resolve(shop);
        },
        function (error) {
          deferred.resolve(error);
        }
      );
      shopPromise = deferred.promise;
    }
    return shopPromise;
  };

  var getShopCheckoutInfo = function(){
    var deferred = $q.defer();
    ShopRestangular.one('checkoutinfo').get().then(
      function (response) {
        var shopCheckoutInfo = response.data;
        deferred.resolve(shopCheckoutInfo);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var updateShopAddress = function(postData){
    var deferred = $q.defer();
    ShopRestangular.all('address').customPUT(postData).then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var updateShopLocalSettings = function(TimezoneCode, CurrencyCode, UnitCode){
    var deferred = $q.defer();
    ShopRestangular.all('').customPUT({TimezoneCode:TimezoneCode,CurrencyCode:CurrencyCode,UnitCode:UnitCode}).then(
      function (response) {
        shopPromise=undefined;
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;

  };

  var updateShopPaymentGateways = function(postData){
    var deferred = $q.defer();
    ShopRestangular.all('paymentgateways').customPUT(postData).then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var updateShopShippingSettings = function(postData){
    var deferred = $q.defer();
    ShopRestangular.all('shipping').customPUT(postData).then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  return {
    GetCategories: getCategories,
    GetShop: getShop,
    GetShopCheckoutInfo: getShopCheckoutInfo,
    UpdateShopAddress: updateShopAddress,
    UpdateShopPaymentGateways: updateShopPaymentGateways,
    UpdateShopShippingSettings: updateShopShippingSettings,
    SetCurrency:setAppCurrency,
    SetAppCurrencyFromShopSettings: setAppCurrencyFromShopSettings,
    updateShopLocalSettings:updateShopLocalSettings,
    getCategoryHierarchies:getCategoryHierarchies,
    updateCategoryHierarchies:updateCategoryHierarchies
  };
}]);
