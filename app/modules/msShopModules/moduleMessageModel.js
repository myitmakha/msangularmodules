'use strict';

angular.module('msShopModules').factory('MessageModel', function() {
  var Message;
  Message = function () {
    var self = this;
    self.title = '';
    self.message = '';
    self.exception = '';
  };

  return {
    create: function(value){
      if (value !== undefined) {
        var newObj = new Message();
        newObj.title = value.title;
        newObj.message = value.message;
        newObj.exception = value.exception;

        return newObj;
      }
      else {
        return new Message();
      }
    }
  };
});
