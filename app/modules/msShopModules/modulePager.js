'use strict';

angular.module('msShopModules')
  .directive('msPager', function(Config) {
    return {
      restrict: 'A',
      scope: {
        searchQuery: '=msSearchQuery',
        response: '=msPagerResponse'
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msShopModules/pager.html',
      link: function(scope) {
        // if(!scope.model) return; // do nothing if no model
        if (scope.searchQuery === undefined) {
          scope.searchQuery = {
            Facets : {},
            PageNum : 0,
            PageSize : 10,
            Query : ''
          };
        }

        var pageNumbers = [],
            totalPages;

        scope.$watch('response', function(newValue) {
          if (newValue !== undefined && newValue.TotalResults) {
            totalPages = Math.ceil(newValue.TotalResults / scope.searchQuery.PageSize);
            scope.totalPages = (newValue.TotalResults > 1) ? totalPages : 0;

            pageNumbers = [];

            //pager if less than 5 pages
            if (scope.searchQuery.PageNum+1 <= 3) { //if less than 5 pages
              for (var i=0; i < 5; i++){
                pageNumbers.push(i+1);
              }
            } else if (scope.searchQuery.PageNum+1 < totalPages - 2) { //if we in middle of pager
              for (var j = scope.searchQuery.PageNum - 2; j < scope.searchQuery.PageNum + 3; j++){
                pageNumbers.push(j+1);
              }
            } else if (scope.searchQuery.PageNum+1 >= totalPages - 3) { //if we at end of pager
              for (var k = totalPages - 5; k < totalPages; k++){
                pageNumbers.push(k+1);
              }
            }

            scope.pageNumbers = pageNumbers;

          } else {
            scope.pageNumbers = 0;
            scope.totalPages = 0;
          }
        },true);

        scope.first = function () {
          scope.searchQuery.PageNum = 0;
        };
        scope.back = function () {
          scope.searchQuery.PageNum--;
        };
        scope.next = function () {
          scope.searchQuery.PageNum++;
        };
        scope.last = function () {
          scope.searchQuery.PageNum = scope.totalPages - 1;
        };
        scope.goTo = function (goTo) {
          scope.searchQuery.PageNum = goTo -1;
        };
      }
    };
  });