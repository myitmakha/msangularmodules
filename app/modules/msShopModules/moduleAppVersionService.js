'use strict';

angular.module('msShopModules').factory('AppRestangular', function(Restangular) {
  return Restangular.withConfig(function(RestangularConfigurer) {
    RestangularConfigurer.setBaseUrl('/');
  });
});

angular.module('msShopModules').factory('ApiRestangular', ['Restangular', 'Config', function(Restangular, Config) {
  return Restangular.withConfig(function(RestangularConfigurer) {
    RestangularConfigurer.setBaseUrl(Config.GetEndPoint('siteApiEndpoint'));
  });
}]);

angular.module('msShopModules').factory('AppVersionService', ['AppRestangular', 'ApiRestangular', 'HelperService', function(AppRestangular, ApiRestangular, helperService) {
  var VersionService = {};

  VersionService.getAppVersion = function(callBack){
    AppRestangular.one('version.txt').get().then(
      function(response) {
        callBack(response.data);
      },
      function(error) {
        callBack(helperService.getMessage(error.data));
      }
    );
  };

  VersionService.getApiVersion = function(callBack){
    ApiRestangular.one('version').get().then(
      function(response) {
        callBack(response.data);
      },
      function(error) {
        callBack(helperService.getMessage(error.data));
      }
    );
  };
  return VersionService;
}]);