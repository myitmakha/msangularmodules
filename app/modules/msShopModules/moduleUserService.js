'use strict';

angular.module('msShopModules').factory('UserService', ['Restangular', 'HelperService', '$window', function(restangular, helperService, $window) {
    var userService = {};

    $window.sessionStorage.token = undefined;

    userService.register = function(user) {
      return restangular.all('account/register').post(user, {});
    };

    userService.login = function(user) {
      var login = 'grant_type=password&username=' + user.UserName + '&password=' + user.Password;
      return restangular.all('token').post(login, { 'Content-Type': 'application/x-www-form-urlencoded' });
    };

    return userService;
  }]);