'use strict';

angular.module('msShopModules')
  .directive('msAddItem', ['$timeout', 'Config', function($timeout, Config) {
      return {
        restrict: 'A',
        scope: {
          model: '=model',
          autoCompleteItems: '=autoCompleteItems'
        },
        templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msShopModules/addItem.html',
        link: function(scope, elm, attrs) {
          $timeout(
            function(){
              if(!scope.model){
                return; // do nothing if no model
              }
            });
          scope.inputAdd = '';
          scope.buttonText = attrs.buttonText;
          scope.label = attrs.label;
          scope.showError = false;

          $timeout(
            //annoying little hack to add placeholder, cant seem to pass it correctly
            function(){
              elm.find('input.ui-autocomplete-input').attr('placeholder',attrs.placeholder);
            },500);

          var textInput = elm.find('input'),
              item,
              tagsAvailable = [];

          scope.setErrorFalse = function() {
            $timeout(
              function(){
                if (!textInput.hasClass('ms-focused')) {
                  scope.showError = false;
                }
              },1500);
          };

          scope.setError = function() {
            scope.showError = false;
            if (textInput.val().trim().length === 0) {
              scope.showError = 'You must enter a name';
            }
            angular.forEach(scope.model, function(value){
              if (textInput.val().trim().toLowerCase() === value.name.trim().toLowerCase()) {
                scope.showError = 'You\'ve already added \'' + textInput.val().trim() + '\'';
                return;
              }
            });
          };
          scope.addItem = function() {
            $timeout(
              function(){
                //refocus
                $(textInput).trigger('focus');
              });
            scope.setError();
            if (scope.showError) {
              return false;
            }
            /*
             @ToDo
             something like this needs to be done...
             field = getFieldByName(textInput.val())
             tagsAvailable = field.tagsAvailable;
             --OR--
             fields = getFieldsHTTP
             for (var i=0, fields<i; i++) {
             if (fields[i].name = textInput.val()) {
             tagsAvailable = fields[i].tagsAvailable;
             return
             }
             }
             */
            item = {
              name: textInput.val(),
              options: []
            };

            $timeout(
              function(){
                //remove text
                $(textInput).val('');
              },100);

            scope.model.push(item);
          };
        }
      };
    }]);