'use strict';

angular.module('msShopModules').factory('UserModel', function() {
  var User = function() {
    var self = this;
    self.UserName = '';
    self.Password = '';
    self.PasswordConfirm  = '';
  };

  return {
    create: function(user){
      if(user === undefined){
        return new User();
      }
      else{
        var thisUser = new User();

        //TODO: do we need this here??
        thisUser.UserName = user.name;
        return thisUser;
      }
    }
  };
});