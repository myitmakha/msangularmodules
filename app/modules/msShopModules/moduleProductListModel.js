'use strict';

angular.module('msShopModules').factory('ProductListModel', function() {
  var ProductList;
  ProductList = function () {
    var self = this;
    self.FacetList = {};
    self.ProductList = [];
    self.SearchQuery = {};
    self.Total = '';
  };

  return {
    create: function(value){
      if (value !== undefined) {
        var thisProductList = new ProductList();
        thisProductList.FacetList = value.FacetList;
        thisProductList.ResourceList = value.ResourceList;
        thisProductList.SearchQuery = value.SearchQuery;
        thisProductList.TotalResults = value.TotalResults;

        return thisProductList;
      }
      else {
        return new ProductList();
      }
    }
  };
});
