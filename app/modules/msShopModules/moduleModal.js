'use strict';

angular.module('msShopModules')
  .directive('msModal', function () {
    return{
      restrict: 'A',
      scope: {
        msAddshadow: '@'
      },
      link: function (scope, element, attrs) {

        var modal = element.find('.modal');
        var openButton = element.find('.modal-open');
        var closeButton = element.find('.modal-close');

        openButton.on('click', function() {
          modal.before('<div class="modal-overlay"></div>');
          modal.addClass('show');
        });

        closeButton.on('click', function() {
          $('.modal-overlay').remove();
          modal.removeClass('show');
        });
      }
    };
  });