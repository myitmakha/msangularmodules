'use strict';

angular.module('msShopModules')
  .directive('msAviary', ['$timeout', 'HelperService', '$window', '$http', function($timeout, HelperService, $window, $http) {
      return {
        restrict: 'A',
        scope: {
          images: '=msAviaryImages'
        },
        template: '<li data-ng-repeat="(key, img) in images">  <img ng-src="{{img.url}}"" id="img-{{key}}"" data-ng-click="launchEditor($index)"" width="300" /> </li>',
        link: function(scope) {

          var header = {'Authorization': 'Bearer ' + $window.sessionStorage.token, 'Content-Type': 'image/jpeg'},
            img,
            featherEditor;

          featherEditor = new Aviary.Feather({
            apiKey: '166143ab5f52e45a',
            apiVersion: 3,
            theme: 'light', // Check out our new 'light' and 'dark' themes!
            tools: 'crop,brightness,contrast,orientation',
            cropPresets: ['Original', ['Square', '1:1'], '3:2', '5:3', '4:3', '5:4', '6:4', '7:5', '10:8', '16:9'],
            appendTo: '',
           /* headers: {'Authorization': 'Bearer ' + $window.sessionStorage.token},*/
            onSave: function(imageID, newURL) {
              //console.log removed.
              //console.log removed.
              img = document.getElementById(imageID);
              img.src = newURL;
              $http({method: 'POST', url: 'http://192.168.1.160/api/upload', data: newURL, headers: header}).
                success(function(data, status, headers, config) {
              }).
                error(function(data, status, headers, config) {
              });
            },
            onError: function(errorObj) {
              alert(errorObj.message);
            }
          });

          scope.launchEditor = function (index) {
            featherEditor.launch({
              image: 'img-' + index,
              url: scope.images[index].url
            });
            return false;
          };




        }
      };
    }]);