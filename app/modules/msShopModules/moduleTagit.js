'use strict';

angular.module('msShopModules')
  .directive('msTagit', ['$timeout', function($timeout) {
    return {
      restrict: 'A',
      scope: {
        tagsAvailable: '=tagsAvailable'
      },
      require: '?ngModel', // get a hold of NgModelController
      link : function(scope, element, attrs, ngModel) {
        if(!ngModel){
          return; // do nothing if no ng-model
        }

        $(element).parent().addClass('ui-front');
        $timeout(function(){
          $(element).tagit({
            availableTags: scope.tagsAvailable, // use tagsAvailable for autocomplete
            itemName: 'item',
            showAutocompleteOnFocus: true,
            placeholderText: 'Type here...',
            allowSpaces: true,
            onTagExists: function(event, ui) {

              ui.existingTag.addClass('tag-used').attr('data-used-tag', 'This tag exists');

              $timeout(function() {
                ui.existingTag.removeClass('tag-used');
              }, 1500);

              return false;

            },
            afterTagAdded: function(event, ui) {
              if (!ui.duringInitialization){
                scope.$apply(write);
              }
            },
            afterTagRemoved: function(event, ui) {
              if (!ui.duringInitialization){
                scope.$apply(write);
              }
            }
          }).next('.tagit').addClass(attrs.ngClass);
          // Write correct data to the model
          write();
        },150, false);

        // Write data to the model
        function write() {
          var val = element.val();
          if(val !== undefined && val.length) {
            val = val.split(',');
          }
          ngModel.$setViewValue(val);
        }
/*
        scope.$watch('tagsAvailable', function(newValue, oldValue) {
          if (newValue !== undefined && newValue !== oldValue) {
            $(element).tagit("reAssignAutocomplete", newValue);
          }
        });
*/
      }
    };
  }]);


