'use strict';

angular.module('msShopModules').factory('ListModel', function() {
  var List;
  List = function () {
    var self = this;
    self.FacetList = {};
    self.ResourceList = [];
    self.SearchQuery = {};
    self.TotalResults = '';
  };

  return {
    create: function(value){
      if (value !== undefined) {
        var thisList = new List();
        thisList.FacetList = (value.FacetList) ? value.FacetList : {};
        thisList.ResourceList = (value.ResourceList) ? value.ResourceList : [];
        thisList.SearchQuery = (value.SearchQuery) ? value.SearchQuery : {};
        thisList.TotalResults = (value.TotalResults) ? value.TotalResults : '';

        return thisList;
      } else {
        return new List();
      }
    }
  };
});
