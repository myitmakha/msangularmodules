'use strict';

angular.module('msShopModules').factory('HelperService', ['$timeout', '$rootScope', 'MessageModel',  function($timeout, $rootScope, MessageModel) {
  var helperService = {};

  helperService.combineArr = function (arg) {
    var r = [], max = arg.length - 1;

    function helper(arr, i) {
      for (var j = 0, l = arg[i].length; j < l; j++) {
        var a = arr.slice(0); // clone arr
        a.push(arg[i][j]);
        if (i === max){
          r.push(a);
        } else{
          helper(a, i + 1);
        }
      }
    }
    helper([], 0);
    return r;
  };

  helperService.appendUrl = function(value){
    return value; //config.apiEndpoint + value;
  };

  helperService.getMessage = function(data) {
    var message = MessageModel.create();

    if(data !== undefined && data.ModelState !== undefined) {
      message.title = data.Message;
      message.message = data.ModelState[''].join(', ');
      message.exception = data.ExceptionMessage;
    }
    return message; //{ errorMessage: data.Message, modelState: modelStateString,  exceptionMessage: data.ExceptionMessage };
  };

  helperService.setMesasge = function(title, message, exception){
    var newMessage = MessageModel.create();
    newMessage.title = title;
    newMessage.message = message;
    newMessage.exception = exception;
    return newMessage;
  };

  helperService.getAvailableTags = function (scope, name) {
    return scope.collection[name];
  };

  helperService.getTitle = function(obj){
    var titles = [];
    for (var k=0; k < obj.length; k++) {
      if (obj[k].options !== undefined && obj[k].options.length) {
        titles[k] = obj[k].name;
      }
    }
    return titles;
  };

  helperService.debounce = function(func, wait, immediate) {
    var timeout, args, context, timestamp, result;

    var later = function() {
      var last = new Date().getTime() - timestamp;

      if (last < wait) {
        timeout = $timeout(later, wait - last);
      } else {
        timeout = null;
        if (!immediate) {
          result = func.apply(context, args);
          context = args = null;
        }
      }
    };

    return function() {
      context = this;
      args = arguments;
      timestamp = new Date().getTime();
      var callNow = immediate && !timeout;
      if (!timeout) {
        timeout = $timeout(later, wait);
      }
      if (callNow) {
        result = func.apply(context, args);
        context = args = null;
      }

      return result;
    };
  };

  helperService.getModelMessages = function(error){
    $rootScope.rootMessage = error;
    //format error here
    return error;
  };

  return helperService;
}]);