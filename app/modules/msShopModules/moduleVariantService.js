'use strict';

angular.module('msShopModules').factory('VariantService', ['ShopRestangular', '$q', function(ShopRestangular, $q) {
  //var variantService = {};

  var generate = function(attributes){
    var postData = attributes;

    var deferred = $q.defer();

    ShopRestangular.all('variants').post( postData, {}).then(
      function(response){
        deferred.resolve(response);
      },
      function(error){
        deferred.resolve(error);
      }
    );

    return deferred.promise;
  };

  //variantService.regenerate = function(productId, attributes, variants){
  //  var postData = attributes;
  //  postData.ProductId = productId;
  //  postData.Variants = [];
  //  postData.Variants = variants;
  //  return ShopRestangular.all('variants').post(postData, {});
  //};

  return {
    Generate: generate
  };
}]);