'use strict';

angular.module('msShopModules').directive('msVersionDuplicated', ['$http', 'AppVersionService', function($http, AppVersionService){
  return {
    restrict: 'A',
    replace: true,
    scope: false,
    template: '<span>We\'re running </br> API Version: {{ version.api.build }} </br>  APP Version: {{ version.app.build }} </span>',
    link : function(scope) {
      var version = {};
      AppVersionService.getAppVersion(function(response){
        version.app = { build :  response };
      });

      AppVersionService.getApiVersion(function(resposne){
          version.api = resposne;
          scope.version = version;
        });
    }
  };
}]);