'use strict';

angular.module('msShopModules').factory('OrderModel', function() {

  var Order = function(){
    var self = this;
    self.id = '';
    self.customerName  = '';
    self.customerEmail = '';
    self.customerPhone = '';

    self.billing = {};
    self.billing.name = '';
    self.billing.company = '';
    self.billing.phone = '';
    self.billing.address = {};
    self.billing.address.addressLine1 = '';
    self.billing.address.addressLine2 = '';
    self.billing.address.city = '';
    self.billing.address.country = '';
    self.billing.address.postCode = '';

    self.shipping = {};
    self.shipping.name = '';
    self.shipping.company = '';
    self.shipping.phone = '';
    self.shipping.address = {};
    self.shipping.address.addressLine1 = '';
    self.shipping.address.addressLine2 = '';
    self.shipping.address.city = '';
    self.shipping.address.country = '';
    self.shipping.address.postCode = '';

    self.shippingMethod = '';
    self.billingMethod = '';
    self.orderItems = [];
    self.notes = [];
    self.status = 'New';
    self.tAndC = '';
    self.tax = '';
    self.total = '';

    self.created = '';
    self.modified  = '';
    self.modifiedBy = '';
  };

  return {
    create: function(value){
      if(value === undefined){
        return new Order();
      }
      else{
        var newObj = new Order();

        newObj.id = value.id;
        newObj.customerName  = value.customerName;
        newObj.customerEmail = value.customerEmail;
        newObj.customerPhone = value.phone;

        newObj.billing = {};
        newObj.billing.name = value.billing.name;
        newObj.billing.company = value.billing.company;
        newObj.billing.phone = value.billing.phone;
        newObj.billing.address = {};
        newObj.billing.address.addressLine1 = value.billing.address.addressLine1;
        newObj.billing.address.addressLine2 = value.billing.address.addressLine2;
        newObj.billing.address.city = value.billing.address.city;
        newObj.billing.address.country = value.billing.address.country;
        newObj.billing.address.postCode = value.billing.address.postCode;

        newObj.shipping = {};
        newObj.shipping.name = value.shipping.name;
        newObj.shipping.company = value.shipping.company;
        newObj.shipping.phone = value.shipping.phone;
        newObj.shipping.address = {};
        newObj.shipping.address.addressLine1 = value.shipping.address.addressLine1;
        newObj.shipping.address.addressLine2 = value.shipping.address.addressLine2;
        newObj.shipping.address.city = value.shipping.address.city;
        newObj.shipping.address.country = value.shipping.address.country;
        newObj.shipping.address.postCode = value.shipping.address.postCode;

        newObj.shippingMethod = value.shippingMethod;
        newObj.billingMethod = value.billingMethod;
        newObj.orderItems = value.orderItems;
        newObj.notes = value.orderNotes;
        newObj.status = value.status;
        newObj.tAndC = value.termsAndCondition;
        newObj.tax = '';
        newObj.total = value.totalAmount;

        newObj.created = '';
        newObj.modified  = '';
        newObj.modifiedBy = '';



        
        return newObj;
      }
    }
  };


});

