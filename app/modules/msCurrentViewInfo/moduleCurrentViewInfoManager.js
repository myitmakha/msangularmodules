'use strict';

angular.module('msCurrentViewInfo')
  .factory('msCurrentViewInfoManager', [ '$rootScope', function ($rootScope) {

    var everything={};

    var pageLoaded = function(loaded){
      $rootScope.loaded = loaded;
    };

    var reset = function(){
      everything.value={};
    };

    var getEverything = function(){
      return everything;
    };

    var setEverything = function(value){
      everything.value=value;
    };

    var add = function(name, value){
      everything.value[name]=value;
    };

    var splice = function(name, value, index, remove){
      if(everything.value[name]){
        everything.value[name].splice( index, remove, value );
      } else {
        everything.value[name] = [value];
      }
    };

    reset();

    var currentViewInfo ={
      getEverything:getEverything,
      setEverything:setEverything,
      add:add,
      splice:splice,
      reset:reset,
      pageLoaded:pageLoaded
    };

    return currentViewInfo;
  }]);