/**
 * Created with IntelliJ IDEA.
 * User: Ian
 * Date: 03/09/14
 * Time: 10:17
 * This file's name has an underscore because the IDE was being a dick and wanted to treat the version without an underscore as a typescript file.
 */

'use strict';

angular.module('msStorage').factory('Storage', ['$window', function ($window) {

  function Storage(name, store) {
    this.name = name;
    this.store = store;
  }

  Storage.prototype.get = function () {
    var item = this.store.getValue(name);
    if (item) {
      return JSON.parse(item);
    }
  };

  Storage.prototype.set = function (value) {
    if (value) {
      this.store.setItem(name, JSON.stringify(value));
    }
    else {
      this.store.removeItem(name);
    }
  };

  return {
    create: function (name) {
      return new Storage(name, $window.sessionStorage);
    }
  };

}]);