'use strict';

angular.module('msConfig').factory('Config', ['$http', '$window', '$location','ConfigConstants', function ($http, $window, $location, ConfigConstants) {

  angular.forEach(ConfigConstants, function(value, key){
    //console.log removed.
    $window.sessionStorage.setItem(key, value);
  });

  var getEndPoint = function (name) {
    var endPoint = ConfigConstants[name];
    if (endPoint === null || endPoint === undefined) {
      endPoint = $window.sessionStorage.getItem(name);
    }
    return endPoint;
  };

  var getSessionStoredValue = function (name) {
    return $window.sessionStorage.getItem(name);
  };

  var config = {
    GetEndPoint: getEndPoint,
    GetSessionStoredValue: getSessionStoredValue
  };
  return config;
}]);