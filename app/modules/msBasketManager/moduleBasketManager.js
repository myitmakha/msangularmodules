'use strict';

angular.module('msBasketManager').factory('BasketManager', ['$q', 'BasketRepository', function ($q, BasketRepository) {

  var newItem = function(id,name,imgUrl,quantity, price, attributes){
    return {id:id,name:name,imgUrl:imgUrl,quantity:quantity,price:price, attributes:attributes};
  };

  var validateItem = function(id){
    var deferred = $q.defer();
    BasketRepository.getItems().then(function(items){
      var index = BasketRepository.getItemIndexById(id);
      if((!index && index!==0)||!items[index]){
        return;
      }
      var item = items[index];
      //Quantity can't be below 0
      if(item.quantity<0){
        item.quantity=0;
      }
      //If an item quantity goes below 0 we should remove the item from the list
      if(item.quantity===0){
        return deleteItem(id);
      }
    }).then(function(){
      deferred.resolve();
    });
    return deferred.promise;
  };

  var updateQuantity = function(id,quantity){
    var deferred = $q.defer();
    BasketRepository.getItems().then(function(items){
      var existingItem = BasketRepository.getItemById(id);
      if(existingItem){
        existingItem.quantity = quantity;
        return validateItem(id);
      }else{
        return false;
      }
    }).then(function(result){
      BasketRepository.saveItems();
      deferred.resolve(result);
    });
    return deferred.promise;
  };

  var incrementQuantity = function(id,quantity){
    var deferred = $q.defer();
    BasketRepository.getItems().then(function(items){
      var existingItem = BasketRepository.getItemById(id);
      if(existingItem){
        existingItem.quantity += quantity;
        return validateItem(id);
      }
    }).then(function(){
      BasketRepository.saveItems();
      deferred.resolve();
    });
    return deferred.promise;
  };

  var addItem = function(id,name,imgUrl,quantity, price, attributes){
    //console.log removed.
    var deferred = $q.defer();
    BasketRepository.getItems().then(function(items){
      var existingItem = BasketRepository.getItemById(id);
      if(existingItem){
        existingItem.quantity += quantity;
      }else{
        items.push(newItem(id,name,imgUrl,quantity, price, attributes));
      }
      return validateItem(id);
    }).then(function(){
      BasketRepository.saveItems();
      deferred.resolve();
    });
    return deferred.promise;
  };

  var getItemById = function(id,name,imgUrl,quantity, price, attributes){
    return BasketRepository.getItemById(id);
  };

  var listItems = function(){
    return BasketRepository.getItems();
  };

  var emptyBasket = function(){
    return BasketRepository.reset();
  };

  var deleteItem = function(id){
    var deferred = $q.defer();
    BasketRepository.getItems().then(function(items){
      var index = BasketRepository.getItemIndexById(id);
      if(index||index===0){
        items[index].quantity=0;
        items.splice(index,1);
      }
    }).then(function(){
      BasketRepository.saveItems();
      deferred.resolve();
    });
    return deferred.promise;
  };

  var basketManager = {
    updateQuantity: updateQuantity,
    incrementQuantity: incrementQuantity,
    addItem: addItem,
    getItemById: getItemById,
    listItems: listItems,
    deleteItem: deleteItem,
    emptyBasket: emptyBasket,
  };

  return basketManager;
}]);