'use strict';

describe('msBasketManager', function () {

 // load the controller's module
 beforeEach(module('msBasketManager'));

 var BasketManager, rootScope, $http;

 // Initialize the controller and a mock scope
 beforeEach(inject(function (_BasketManager_, $rootScope, _$http_) {
   BasketManager = _BasketManager_;
   rootScope = $rootScope;
   $http=_$http_;
   BasketManager.emptyBasket();
 }));

 it('should get an empty basket on init', function () {
   BasketManager.listItems().then(function (list) {
     expect(list).toEqual([]);
     expect(list.length).toEqual(0);
   });
   rootScope.$digest();
 });

 it('should add an item', function () {
   var id = 123;
   var name = 'Red shoe';
   var imgUrl = 'a.jpg';
   var quantity = 2;
   var price = 1.2;
   BasketManager.addItem(id,name,imgUrl,quantity,price).then(function () {
     return BasketManager.listItems();
   }).then(function (list) {
     expect(list[0].id).toEqual(id);
     expect(list[0].quantity).toEqual(quantity);
     expect(list.length).toEqual(1);
   });
   rootScope.$digest();
 });

 it('should remove an item', function () {
   var id = 123;
   var name = 'Red shoe';
   var imgUrl = 'a.jpg';
   var quantity = 2;
   var price = 1.2;
   BasketManager.addItem(id,name,imgUrl,quantity,price).then(function () {
     return BasketManager.listItems();
   }).then(function (list) {
     expect(list.length).toEqual(1);
     return BasketManager.deleteItem(id);
   }).then(function () {
     return BasketManager.listItems();
   }).then(function (list) {
     expect(list.length).toEqual(0);
   });
   rootScope.$digest();
 });

 it('should remove an item if we set 0 quantity', function () {
   var id = 123;
   var name = 'Red shoe';
   var imgUrl = 'a.jpg';
   var quantity = 2;
   var price = 1.2;
   BasketManager.addItem(id,name,imgUrl,quantity,price).then(function () {
     return BasketManager.listItems();
   }).then(function (list) {
     expect(list.length).toEqual(1);
     return BasketManager.incrementQuantity(id,-quantity);
   }).then(function () {
     return BasketManager.listItems();
   }).then(function (emptyList) {
	 expect(emptyList.length).toEqual(0);
   });
   rootScope.$digest();
 });

 it('should add the quantity', function () {
   var id = 123;
   var name = 'Red shoe';
   var imgUrl = 'a.jpg';
   var quantity = 2;
   var price = 1.2;
   BasketManager.addItem(id,name,imgUrl,quantity,price).then(function () {
     return BasketManager.listItems();
   }).then(function (list) {
     expect(list[0].quantity).toEqual(quantity);
     return BasketManager.incrementQuantity(id,quantity);
   }).then(function () {
     return BasketManager.listItems();
   }).then(function (list) {
	 expect(list[0].quantity).toEqual(2*quantity);
   });
   rootScope.$digest();
 });

 it('should update the quantity', function () {
   var id = 123;
   var name = 'Red shoe';
   var imgUrl = 'a.jpg';
   var quantity = 2;
   var price = 1.2;
   BasketManager.addItem(id,name,imgUrl,quantity,price).then(function () {
     return BasketManager.listItems();
   }).then(function (list) {
     expect(list[0].quantity).toEqual(quantity);
     return BasketManager.updateQuantity(id,quantity);
   }).then(function () {
     return BasketManager.listItems();
   }).then(function (list) {
	 expect(list[0].quantity).toEqual(quantity);
   });
   rootScope.$digest();
 });

 it('should update of a not valid id will not create any item', function () {
   var id = 123;
   var name = 'Red shoe';
   var imgUrl = 'a.jpg';
   var quantity = 2;
   var price = 1.2;
   BasketManager.updateQuantity(id,quantity).then(function (result) {
     expect(result).toEqual(false);
     return BasketManager.listItems();
   }).then(function (list) {
     expect(list.length).toEqual(0);
   });
   rootScope.$digest();
 });
});
