'use strict';

angular.module('msShippingSettingsRepository').factory('shippingSettingsRepository', ['ShopRestangular', function (ShopRestangular) {

  var shippingSettingsRepository = {};

  shippingSettingsRepository.getShippingMethods = function () {
    return ShopRestangular.one('settings/shipping').get();
  };

  shippingSettingsRepository.saveShippingMethods = function (postData) {
    return ShopRestangular.all('settings/shipping').customPUT(postData, {});
  };

  return shippingSettingsRepository;
}]);
/**
 * Created by Phil on 30/07/14.
 */
