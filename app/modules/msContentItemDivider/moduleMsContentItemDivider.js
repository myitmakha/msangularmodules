'use strict';
angular.module('msContentItemDivider')
  .directive('msContentItemDivider', [function () {
    return {
      restrict: 'A',
      replace: true,
      template:'<hr>',
      link: function (scope) {
      }
    };
  }]);