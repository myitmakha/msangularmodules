'use strict';

angular.module('msThemeRepository').factory('ThemeRepository', ['ThemeRestangular', 'PageBuilderManager', '$q', function (ThemeRestangular, PageBuilderManager, $q) {

  var getTheme = function (themeId) {
    var deferred = $q.defer();
    ThemeRestangular.one('', themeId).get().then(
      function (response) {
        var theme = response.data;
        deferred.resolve(theme);
      }
    );
    return deferred.promise;
  };

  var getCurrentTheme = function () {
    var deferred = $q.defer();
    PageBuilderManager.getWebsiteData().then(function (siteData) {
      var themeId = siteData.defaultTheme;
      var i = 0;
      while (siteData.themes[i]) {
        if (siteData.themes[i].id === themeId) {
          deferred.resolve(siteData.themes[i]);
          return;
        }
        i++;
      }
      deferred.resolve();
    });
    return deferred.promise;
  };

  var themesDataPromise;

  var getThemes = function () {
    if(!themesDataPromise){
      var deferred = $q.defer();
      ThemeRestangular.one('').get().then(
        function (response) {
          var themes = response.data;
          deferred.resolve(themes);
        }
      );
      themesDataPromise = deferred.promise;
    }
    return themesDataPromise;
  };

  var getThemeSettings = function (themeName) {
    var deferred = $q.defer();
    deferred.resolve({
      bgColor: '#555',
      fontColor: 'red',
      fontSize: 15,
      defaultFontFamily: 'Arial',
      logoImage: 'http://uk.mrsite.com/assets/img/site-logo.png'
    });
    return deferred.promise;
  };

  var themeRepository = {
    getTheme: getTheme,
    getThemes: getThemes,
    getThemeSettings: getThemeSettings,
    getCurrentTheme: getCurrentTheme
  };

  return themeRepository;
}]);