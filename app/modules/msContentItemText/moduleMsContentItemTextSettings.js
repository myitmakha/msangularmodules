/**
 * Created by Albert on 15/07/2014.
 */
'use strict';
angular.module('msContentItemText')
  .directive('msContentItemTextSettings1', ['$compile', '$sce', function ($compile, $sce) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        settings: '='
      },
      template: '<div>Font color: <input type="text" ng-model="settings.fontcolor"/>Background color: <input type="text" ng-model="settings.backgroundcolor"/>Line height: <input type="range"step="0.1" min="0.1" max="5" ng-model="settings.lineHeight"/></div>',
      link: function (scope) {
        scope.settings = scope.settings || {};
        scope.settings.fontcolor = scope.settings.fontcolor || '';
        scope.settings.backgroundcolor = scope.settings.backgroundcolor || '';
        scope.settings.lineHeight = scope.settings.lineHeight || '';
      }
    };
  }]);