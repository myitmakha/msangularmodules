/**
 * Created by Albert on 15/07/2014.
 */
'use strict';
angular.module('msContentItemText')
  .directive('msContentItemText', ['$sce', function ($sce) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      template: '<div style="color:{{contentItemData.settings.fontcolor}};background-color:{{contentItemData.settings.backgroundcolor}};line-height:{{contentItemData.settings.lineHeight}}" ng-bind-html="trustedHTML"></div>',
      link: function (scope) {
        var text = scope.contentItemData.value;
        if (scope.contentItemData.value.indexOf('<p>') !== 0) {
          text = '<p>' + text + '</p>';
        }
        scope.trustedHTML = $sce.trustAsHtml(text);

      }
    };
  }]);