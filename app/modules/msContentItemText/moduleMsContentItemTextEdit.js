'use strict';
angular.module('msContentItemText')
  .directive('msContentItemTextEdit', ['$rootScope', function ($rootScope) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      template: '<div style="color:{{contentItemData.settings.fontcolor}};background-color:{{contentItemData.settings.backgroundcolor}};line-height:{{contentItemData.settings.lineHeight}}"><div ck-inline editor-content="contentItemData.value" contenteditable="true"></div></div>',
      link: function (scope) {
        scope.$watch('contentItemData.value', function (oldValue, newValue) {
          if (oldValue !== newValue) {
            $rootScope.$broadcast('markChanges', 'text has been changed.');
          }
        });
      }
    };
  }]);
