'use strict';

angular.module('msHopscotch')
  .directive('msHopscotch', ['$timeout', '$rootScope', function ($timeout, $rootScope) {
    return{
      restrict: 'A',
      link: function (scope, element, attrs) {
        element.on('click', function(){
          //console.log removed.
          $rootScope.$broadcast('modalClose');

          var tour = {
            id: 'hello-hopscotch',
            steps: [
              {
                title: 'Start here!',
                content: 'To add some text, click on the ‘Text’ tile here, hold your mouse down and drag the tile onto your web page. To add different tile elements to your page, repeat the process with the other tiles.',
                target: 'widget-text',
                placement: 'right',
                nextOnTargetMouseDown: true,
                highlight: true,
                fixedElement: true,
                zindex: 100000,
                showNextButton: false,
                showCloseButton: false,
                delay: 1000
              },
              {
                title: 'Drop the tile here!',
                content: 'To add the text to your page, drop the \'Text\' tile here',
                target: 'ms-content-items-layout',
                placement: 'bottom',
                smoothScroll: false,
                nextOnTargetMouseOver: true,
                zindex: 100000,
                showNextButton: false,
                showCloseButton: false
              },
              {
                title: 'Next Step!',
                content: 'To edit your text, click in the text box, and start typing It’s that easy!',
                target: 'ms-content-items-layout',
                placement: 'bottom',
                smoothScroll: false,
                zindex: 100000,
                highlight: true,
                showNextButton: true,
                showCloseButton: false
              },
              {
                title: 'And finally!',
                content: 'To tweak your site design, click on the ‘Design’ toggle here. You can play with all the different design elements that appear to get your site design how you want it. Scroll along to see all the available options.',
                target: 'edit-switch',
                placement: 'top',
                nextOnTargetClick: true,
                zindex: 100000,
                fixedElement: true,
                showNextButton: false,
                highlight: true,
                showCloseButton: false
              }
            ]
          };

          /* jshint ignore:start */
          hopscotch.endTour(tour);
          hopscotch.startTour(tour);
          /* jshint ignore:end */

        });
      }
    };
  }]);