'use strict';

angular.module('msContentItemVideo')
  .directive('msContentItemVideo', ['$compile', 'moduleMsContentItemVideoManager', 'Config', function ($compile, moduleMsContentItemVideoManager, Config) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {

        var html = '<div class="ms-no-settings">Add your embed code in the widget settings</div>';
        var videoSource = '';
        var video = moduleMsContentItemVideoManager.getVideoCodeFromUrl(scope.contentItemData.settings.url);
        if (video) {
          if (video.type === 'youtube') {
            videoSource = '//www.youtube.com/embed/' + video.id;
            html = '<div class="video-wrapper"><iframe width="560px" height="315px" src="' + videoSource + '"  frameborder="0" allowfullscreen></iframe></div>';
          }
          else if (video.type === 'vimeo') {
            videoSource = '//player.vimeo.com/video/' + video.id;
            html = '<div class="video-wrapper"><iframe src="' + videoSource + '" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
          }
        }

        $compile(html)(scope, function (html) {
          element.html(html);
        });
      }
    };
  }]);