'use strict';
angular.module('msContentItemVideo')
  .factory('moduleMsContentItemVideoManager', [ function () {
    var getVideoCodeFromUrl = function (pastedData) {
      var success = false;
      var media = {};
      if (pastedData) {
        if (pastedData.match('//(www.)?youtube|youtu.be')) {
          var youtubeId = '';
          if (pastedData.match('embed')) {
            youtubeId = pastedData.split(/embed\//)[1].split('"')[0];
          }
          else {
            youtubeId = pastedData.split(/v\/|v=|youtu.be\//)[1].split(/[?&]/)[0];
          }
          media.type = 'youtube';
          media.id = youtubeId;
          success = true;
        }
        else if (pastedData.match('//(player.)?vimeo\\.com')) {
          var vimeoId = '';
          if (pastedData.match('video')) {
            vimeoId = pastedData.split(/video\/|\/\/vimeo\.com\//)[1].split(/[?&]/)[0];
          } else {
            var parts = pastedData.split(/\/\/vimeo\.com\//)[1].split(/[/]/);
            vimeoId = parts[parts.length - 1];
          }

          media.type = 'vimeo';
          media.id = vimeoId;
          success = true;
        }

        if (success) {
          return media;
        }
        else {
          return('No valid media id detected');
        }
      }
      return false;
    };

    var videoManager = {
      getVideoCodeFromUrl: getVideoCodeFromUrl
    };
    return videoManager;
  }]);