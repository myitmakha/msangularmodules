'use strict';

angular.module('msContentItemVideo')
  .directive('msContentItemVideoEdit', ['$compile', 'moduleMsContentItemVideoManager', 'Config', function ($compile, moduleMsContentItemVideoManager, Config) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {

        scope.contentItemData.settings.height = scope.contentItemData.settings.height || 315;
        scope.url = scope.url || '';

        var compileToView = function (video) {

          var html = '<div class="ms-no-settings ms-tools">' +
            'Video' +
            '<input type="text" ng-model="url" placeholder="Enter YouTube or Vimeo link here" />' +
            '<button ng-click="ok()" class="ms-button primary" >OK</button>' +
            '</div>';

          var videoSource = '';
          if (video) {
            if (video.type === 'youtube') {
              videoSource = '//www.youtube.com/embed/' + video.id;
              html = '<div class="video-wrapper"><iframe width="560px" height="315px" src="' + videoSource + '"  frameborder="0" allowfullscreen></iframe></div>';
            }
            else if (video.type === 'vimeo') {
              videoSource = '//player.vimeo.com/video/' + video.id;
              html = '<div class="video-wrapper"><iframe src="' + videoSource + '" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
            }
          }

          $compile(html)(scope, function (html) {
            element.html(html);
          });
        };

        compileToView(scope.contentItemData.settings.url);

        scope.ok = function(){
          scope.contentItemData.settings.url = scope.url;
        };

        scope.$watch('contentItemData.settings', function () {
          var video = moduleMsContentItemVideoManager.getVideoCodeFromUrl(scope.contentItemData.settings.url);
          compileToView(video);
        }, true);
      }
    };
  }]);