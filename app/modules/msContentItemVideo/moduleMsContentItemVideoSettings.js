'use strict';

angular.module('msContentItemVideo')
  .directive('msContentItemVideoSettings', ['$compile', '$rootScope', function ($compile, $rootScope) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        settings: '='
      },
      template: '<div>' +
        '<div class="row-fix">' +
        '<div class="col-fix14">' +
        '<label>Youtube or Vimeo video url</label>' +
        '<input type="text" value="{{settings.url}}" ng-model="settings.url" />' +
        '</div>' +
        '</div>' +
        '</div>',
      link: function (scope, element) {

        $('.videoHeightSlider', element).slider({
          min: 0,
          max: 1000,
          range: 'min',
          value: scope.settings.height,
          slide: function (event, ui) {
            scope.settings.height = ui.value;
            scope.$apply();
          }
        });

        scope.$watch('settings', function (oldValue, newValue) {
          if (oldValue !== newValue) {
            $rootScope.$broadcast('markChanges', 'video settings has been changed.');
          }
        }, true);
      }
    };
  }]);
