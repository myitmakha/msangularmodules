'use strict';

angular.module('msHeaderItemLogo')
  .directive('msHeaderItemLogo', ['$compile', '$sce', '$rootScope', 'MediaLibraryManager', 'PageBuilderManager', function ($compile, $sce, $rootScope, MediaLibraryManager, PageBuilderManager) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        isEditable: '='
      },
      link: function (scope, element) {
        scope.logoImage = scope.logoImage || '';

        PageBuilderManager.getLogoImage().then(
          function (logoImage) {
            scope.logoImage = logoImage;
            var thisImage = scope.logoImage || 'https://s3-eu-west-1.amazonaws.com/irhtestbucket/uploads/ender.jpg';
            scope.images = [];
            scope.images.push({url: thisImage, default: 0});
          }
        );

        scope.editableId = MediaLibraryManager.randomString(); // This is use by the media library manager for unique hook to each ms-upload directive.

        var compileToView = function () {
          var html = '';
          if (scope.isEditable) {
            scope.modalTitle = 'Header logo';
            scope.multiple = true;
            html = '<span class="hidden">Ender</span><img src=' + scope.images[0].url + '><ms-media-library mode="\'hover\'" editable-id="editableId" uploaded-files="images" multiple-upload=multiple title="modalTitle"></ms-media-library>';
          } else {
            html = '<span class="hidden">Ender</span><img src=' + scope.images[0].url + '>';
          }
          $compile(html)(scope, function (html) {
            element.html(html);
          });
        };

        scope.$watch('images', function () {
          try {
            scope.images[0].url = scope.image[0].url.replace(/'/g, "''"); //jshint ignore:line
          } catch (err) {
            ////console.log removed.
          }
          if (scope.images[0].url !== scope.logoImage) {
            PageBuilderManager.saveLogo(scope.images[0].url).then(function () {
              //console.log removed.
            });
          }
          compileToView();
        });
      }
    };
  }]);