'use strict';

angular.module('msTaxSettingsRepository').factory('taxSettingsRepository', ['ShopRestangular', function (ShopRestangular) {

  var taxSettingsRepository = {};

  taxSettingsRepository.getTaxSettings = function () {
    ShopRestangular.one('settings/tax').get().then(
      function (response) {
        return response.data;

      },
      function (error) {
        return error;
      }
    );
  };

  taxSettingsRepository.saveTaxSettings = function (postData) {
    ShopRestangular.all('settings/tax').customPUT(postData).then(
      function (response) {
        return response.data;
      },
      function (error) {
        return error;
      }
    );
  };

  return taxSettingsRepository;
}]);
