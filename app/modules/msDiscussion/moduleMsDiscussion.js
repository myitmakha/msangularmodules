'use strict';

angular.module('msDiscussion')
  .directive('msDiscussion', ['$compile','DiscussionRepository','Config','$timeout', function ($compile, DiscussionRepository,Config,$timeout) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        discussionId: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msDiscussion/msDiscussion.html',
      link: function (scope, element) {

        scope.svgUrl = function( link ){
          return Config.GetEndPoint('modulesDirectory') + '/msContentItemBlog/sprites.svg#'+link;
        };

        var timer;
        var displayMessage = function(message){
          if(timer){
            clearTimeout(timer);
          }
          scope.message = message;
          timer = $timeout(function(){scope.message='';}, 5000);
        };

        scope.addComment = function(){
          var comment = {Title:'New Comment',Content:scope.comment,Author:'Anonymous',Images:[]};
          DiscussionRepository.addNewComment(scope.discussionId, comment).then(function(result){
            scope.comment='';
            displayMessage('Comment added.');
            loadComments(scope.discussionId);
          });
        };

        var loadComments = function(discussionId){
          DiscussionRepository.getAllComments(discussionId).then(function(comments){
            scope.comments=comments;
          });
        };
        
        scope.paging={};

        scope.$watch('discussionId',function(discussionId){
          if(discussionId){
            loadComments(discussionId);
          }
        });
      }
    };
  }]);