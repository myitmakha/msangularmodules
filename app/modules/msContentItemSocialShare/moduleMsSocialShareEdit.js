'use strict';

angular.module('msContentItemSocialShare')
  .directive('msContentItemSocialShareEdit', ['$compile', '$timeout', '$sce', function ($compile, $timeout, $sce) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {

        //console.log removed.

        //TODO: this should from api
        var socialLinks = [
          {name: 'facebook', selected: true},
          {name: 'twitter', selected: true},
          {name: 'pinterest', selected: true},
          {name: 'linkedin', selected: true}
        ];

        if (!scope.contentItemData.settings || scope.contentItemData.settings.length === 0) {
          scope.contentItemData.settings = socialLinks;
        }

        var facebookLink = '<a class="addthis_button_facebook at300b" title="Facebook" href="#"><span class="at16nc at300bs at15nc at15t_facebook at16t_facebook"><span class="at_a11y">Share on facebook</span></span></a>';
        var twitterLink = '<a class="addthis_button_twitter at300b" title="Tweet" href="#"><span class="at16nc at300bs at15nc at15t_twitter at16t_twitter"><span class="at_a11y">Share on twitter</span></span></a>';
        var pinterest = '<a class="addthis_button_pinterest at300b"><span class="at_PinItButton"></span></a>';
        var linkedin = '<a class="addthis_button_linkedin at300b" href="http://www.addthis.com/bookmark.php?v=300&amp;winname=addthis&amp;pub=ra-53f1bcdd17460bb8&amp;source=tbx-300&amp;lng=en-US&amp;s=linkedin&amp;url=http%3A%2F%2F127.0.0.1%3A9000%2F%23%2FpageBuilder%2Fhome&amp;title=MrSite&amp;ate=AT-ra-53f1bcdd17460bb8/-/-/53f2117ce3042404/3&amp;frommenu=1&amp;uid=53f2117c65916aa6&amp;ct=1&amp;uct=1&amp;tt=0&amp;captcha_provider=nucaptcha" target="_blank" title="LinkedIn"><span class="at16nc at300bs at15nc at15t_linkedin at16t_linkedin"><span class="at_a11y">Share on linkedin</span></span></a>';

        var selectedSocialLinks = '';
        var getSelectedSocialLinks = function () {
          var availableSocialLinks = '';
          angular.forEach(scope.contentItemData.settings, function (socialLink) {
            if (socialLink.name === 'facebook' && (socialLink.selected === true || socialLink.selected === 'true')) {
              availableSocialLinks += facebookLink;
            }
            if (socialLink.name === 'twitter' && (socialLink.selected === true || socialLink.selected === 'true')) {
              availableSocialLinks += twitterLink;
            }
            if (socialLink.name === 'pinterest' && (socialLink.selected === true || socialLink.selected === 'true')) {
              availableSocialLinks += pinterest;
            }
            if (socialLink.name === 'linkedin' && (socialLink.selected === true || socialLink.selected === 'true')) {
              availableSocialLinks += linkedin;
            }
          });

          selectedSocialLinks = '<div class="addthis_toolbox">' + availableSocialLinks + '<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-53f1bcdd17460bb8"></script></div>';
        };


        var compileToView = function () {
          getSelectedSocialLinks();
          $compile(selectedSocialLinks)(scope, function (ele) {
            element.html(ele);
          });
        };

        scope.$watch('contentItemData.settings', compileToView, true);
      }
    };
  }]);