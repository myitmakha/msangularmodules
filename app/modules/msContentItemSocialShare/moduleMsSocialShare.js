'use strict';

angular.module('msContentItemSocialShare')
  .directive('msContentItemSocialShare', ['$compile', function ($compile) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {
        var availableSocialLinks = '';
        angular.forEach(scope.contentItemData.settings, function (socialLink) {
          if (socialLink.selected === true || socialLink.selected === 'true') {
            availableSocialLinks += '<a class="addthis_button_' + socialLink.name + '"></a>';
          }
        });

        var selectedSocialLinks = '<div class="addthis_toolbox">' + availableSocialLinks + '</div><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-53f1bcdd17460bb8"></script>';

        $compile(selectedSocialLinks)(scope, function (ele) {
          element.html(ele);
        });
      }
    };
  }]);