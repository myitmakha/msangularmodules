'use strict';

angular.module('msContentItemSocialShare')
  .directive('msContentItemSocialShareSettings', ['$compile', '$sce', '$timeout', function ($compile, $sce, $timeout) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        settings: '='
      },
      link: function (scope, element) {
        //TODO: this should from api
        var socialLinks = [
          {name: 'facebook', selected: true},
          {name: 'twitter', selected: true},
          {name: 'pinterest', selected: true},
          {name: 'linkedin', selected: true}
        ];

        if (!scope.settings || !scope.settings.length) {
          scope.settings = socialLinks;
        }

        var html = '<label for="{{setting.name}}" ng-repeat="setting in settings">{{setting.name}}<input type="text" id="{{setting.name}}" value="{{setting.name}}" ng-model="setting.selected"></label>';

        $compile(html)(scope, function (html) {
          element.html(html);
        });
      }
    };
  }]);