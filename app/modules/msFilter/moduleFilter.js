'use strict';

angular.module('msFilter')
  .filter('removeInvalidChars', function () {
    return function (input) {
      return input.replace(/[^a-zA-Z\s]/g, ' ');
    };
  })
  .filter('htmlToPlainText', function() {
    return function(text) {
      return String(text).replace(/<[^>]+>/gm, '');
    };
  })
  //keep objects in order
  .filter('fixedOrder', function () {
    return function (input) {
      if (!input) {
        return [];
      }
      return Object.keys(input);
    };
  })
  .filter('cleanUrl', function () {
    return function (input) {
      if (input) {
        return input.toLowerCase()
          .replace(/\s+/g, '-')
          .replace(/[^a-z0-9-]/ig, '');
      }
    };
  })
  .filter('unsafe', ['$sce', function ($sce) {
    return function (val) {
      return $sce.trustAsHtml(val);
    };
  }])
  //add your new filter logic here
  .filter('addAnotherFilterHere', function () {
    return function (input) {
      return input + 's';
    };
  })
  .filter('safeUrl', ['$sce', function($sce) {
    return function(url) {
      return $sce.trustAsResourceUrl(url);
    };
  }]);
