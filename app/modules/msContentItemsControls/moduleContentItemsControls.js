'use strict';

angular.module('msContentItemsControls')
  .directive('msContentItemControls', ['LayoutService', '$compile', function (LayoutService, $compile) {
    return {
      scope: {
        actions: '='
      },
      restrict: 'A',
      link: function (scope, element, attrs) {

        var controls = scope.actions.controls || [],
          elementControls = angular.element('<div class="element-controls ms-tools"></div>');

        scope.deleteElem=function() {
          var myId = element.attr('data-id');
          LayoutService.deleteElement(myId);
        };

        controls.forEach(function (className) {
          var addelem = angular.element('<div></div>');

          addelem.attr('class', 'element-control has-tooltip ' + className);

          if (className === 'delete icon-delete') {

            addelem.attr('ng-click', 'deleteElem()');
            addelem.attr('ms-tooltip', '');
            addelem.attr('tooltip-content', 'Delete');

          } else if (className === 'handle icon-drag') {

            addelem.attr('ms-tooltip', '');
            addelem.attr('tooltip-content', 'Click and drag to move');

          } else if (className === 'settings icon-settings' && scope.actions.displaySettings) {

            addelem.attr('ng-click', 'actions.displaySettings()');
            addelem.attr('ms-tooltip', '');
            addelem.attr('tooltip-content', 'Settings');

          } else {
            return;
          }
          elementControls.append(addelem);
        });


        $compile(elementControls)(scope, function (html) {
          element.append(html);
        });



      }
    };
  }]);