'use strict';

angular.module('msContentItemDownloadableFile')
  .directive('msContentItemDownloadableFileEdit', ['$compile', function ($compile) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {
        var compileToView = function () {

          var html = '<div class="ms-no-settings">Click settings to add file.</div>';
         // var html = '<ms-media-library mode="\'dialog\'" editable-id="editableId" uploaded-files="files" button-text="Add a file" title=modalTitle multiple-upload=false file-type="documents"></ms-media-library>';

          if (scope.contentItemData.settings.displayName) {
            html = '<div><a ng-href="{{contentItemData.settings.url}}" target="_blank" class="button">' + scope.contentItemData.settings.displayName + '</a></div>';
          }

          $compile(html)(scope, function (html) {
            element.html(html);
          });
        };

        compileToView();

        scope.$watch('contentItemData.settings', function (nV, oV) {
          compileToView();
        }, true);
      }
    };
  }]);
