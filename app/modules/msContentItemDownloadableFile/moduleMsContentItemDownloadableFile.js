'use strict';

angular.module('msContentItemDownloadableFile')
  .directive('msContentItemDownloadableFile', ['$compile', function ($compile) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {

        var html = '';

        if (scope.contentItemData.settings.displayName) {
          html = '<div><a ng-href="{{contentItemData.settings.url}}" target="_blank" class="button">' + scope.contentItemData.settings.displayName + '</a></div>';
        }

        $compile(html)(scope, function (html) {
          element.html(html);
        });
      }
    };
  }]);