'use strict';

angular.module('msContentItemDownloadableFile')
  .directive('msContentItemDownloadableFileSettings', ['$compile', '$rootScope', 'MediaLibraryManager', function ($compile, $rootScope, MediaLibraryManager) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        settings: '='
      },
      template: '<div>' +
                  '<div class="row-fix">' +
                    '<div class="col-fix14">' +
                      '<label>{{settings.url}}</label>' +
                    '</div>' +
                  '</div>' +
                  '<div class="row-fix">' +
                    '<div class="col-fix14">' +
                      '<div class="image-wrap">' +
                        '<ms-media-library mode="\'dialog\'" editable-id="editableId" uploaded-files="files" button-text="Add a file" title=modalTitle multiple-upload=false file-type="documents"></ms-media-library>' +
                      '</div>' +
                    '</div>' +
                  '</div>' +
                  '<div class="row-fix">' +
                    '<div class="col-fix14">' +
                      '<label>Display name</label>' +
                    '</div>' +
                  '</div>' +
                  '<div class="row-fix">' +
                    '<div class="col-fix14">' +
                      '<input type="text" ng-model="settings.displayName"' +
                    '</div>' +
                  '</div>' +
                '</div>',
      link: function (scope) {

        scope.settings.url = scope.settings.url || '';
        scope.files = [];
        scope.files.push({url: scope.settings.url, default: 0});
        scope.editableId = MediaLibraryManager.randomString(); // This is use by the media library manager for unique hook to each ms-upload directive.
        scope.modalTitle = 'Upload file';

        scope.$watch('files', function (oldValue, newValue) {
          scope.settings.url = scope.files[0].url;
        });

        scope.$watch('settings', function (oldValue, newValue) {
          if (oldValue !== newValue) {
            $rootScope.$broadcast('markChanges', 'downloadable file settings has been changed.');
          }
        }, true);
      }
    };
  }]);
