'use strict';

angular.module('msContentItemsToolbar')
  .factory('ResizeAreaManager', ['$timeout', '$rootScope', function ($timeout, $rootScope) {

    var active = false;

    var createAllResizables = function (containerSelector, callback) {
      var $container = $(containerSelector);
      var $containerOfResizables = $('<div>');
      $containerOfResizables.addClass('container-resizables');
      $('body').append($containerOfResizables);
      $container.find('.ms-row').each(function (i, row) {
        var $row = $(row);
        var rowOffset = $row.offset();
        var $cols = $row.children().children('.ms-col');
        var numCols = $cols.length;
        $cols.each(function (i, col) {
          if (numCols > i + 1) {
            var $col = $(col);
            var colOffset = $col.offset();
            var width = 5;
            createResizableDiv($containerOfResizables, colOffset.left + $col.outerWidth() - width / 2, colOffset.top, width, $row.outerHeight() - (colOffset.top - rowOffset.top), $col, callback);
          }
        });
      });
    };

    var createResizableDiv = function ($divContainer, x, y, w, h, $col, callback) {
      var $el = $('<div class="resizeHandle">');
      var id = $col.attr('data-id');
      var colSize = parseInt($col.attr('data-size'));
      var colLeft = $col.offset().left;
      var colWidth = $col.outerWidth();
      var draggedCol = colSize;
      var verticalMargin = 20;
      var gridSize = colWidth/colSize;
      $el.css({
        position: 'absolute',
        top: y+verticalMargin,
        left: x,
        width: w,
        height: h-verticalMargin*2
        //,opacity:1
      });
      $el.attr('data-id', id);
      $divContainer.append($el);
      $el.draggable({
          grid:[gridSize,gridSize],
          revert: function () {
            return !resized && false;
          },
          axis: 'x',
          start: function (e, ui) {
            active=true;
            ui.helper.append('<div class="draggableLayout" style="z-index:-99;position:fixed;top:0px;left:0px;width:100%;height:100%"></div>');
          },
          stop: function (e, ui) {
            active=false;
            ui.helper.find('.draggableLayout').remove();
            recalculate(undefined, callback);
          },
          drag: function (e, ui) {
            //console.log removed.
            var id = $(this).attr('data-id');
            var x = e.clientX;

            var col = ((x - (colLeft + colWidth)) / colWidth + 1) * colSize;
            col = Math.round(col);
            if (col !== draggedCol) {
              draggedCol = col;
              resized = true;
              callback(id, col, parseInt($col.attr('data-size')));
            }
          }
        }
      );
    };

    var deleteAllDropables = function () {
      $('.container-resizables').remove();
    };

    var globalContainerSelector, globalCallback, resized;
    var start = function (containerSelector, callback) {
      containerSelector = containerSelector || globalContainerSelector || $('.container');
      callback = callback || globalCallback || function () {
        //console.log removed.
      };
      globalContainerSelector = containerSelector;
      globalCallback = callback;
      resized = false;
      $timeout(function () {
        createAllResizables(containerSelector, callback);
      });
      startWatch();

    };

    var recalculate = function () {
      stop();
      start();
    };

    var stop = function () {
      deleteAllDropables();
      endtWatch();
    };

    var interval;
    var startWatch = function(){
      var previousHeight=$(document).outerHeight();
      var previousWidth=$(document).outerWidth();
      var modified=false;
      if(!interval){
        interval = setInterval(function(){
          if(active){
            return;
          }
          var height = $(document).outerHeight();
          var width = $(document).outerWidth();
          if(previousHeight!==height || previousWidth!==width){
            previousHeight=height;
            modified=true;
            //}else if(modified){
            recalculate();
            modified=false;
          }
        },500);
      }
    };
    var endtWatch = function(){
      clearInterval(interval);
      interval = undefined;
    };

    $rootScope.$on('recalculateWidth', function () {
      recalculate();
    });

    var droppableAreaManager = {
      start: start,
      stop: stop,
      recalculate: recalculate
    };
    return droppableAreaManager;
  }]);