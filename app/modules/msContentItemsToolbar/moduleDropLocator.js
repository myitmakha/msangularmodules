'use strict';

angular.module('msContentItemsToolbar')
  .factory('DropLocator', [function () {
    var EPOS = {
      TOP: {value: 0, name: 'TOP'},
      BOTTOM: {value: 1, name: 'BOTTOM'},
      LEFT: {value: 2, name: 'LEFT'},
      RIGHT: {value: 3, name: 'RIGHT'}
    };
    var EPLACE = {
      IN: {value: 0, name: 'IN'},
      OUT: {value: 1, name: 'OUT'},
      BORDER: {value: 2, name: 'BORDER'}
    };

    var whereIsFromBox = function (box, point, border) {
      var pos;
      var place;
      var diff = {x: point.x - box.x, y: point.y - box.y};
      if (diff.x < 0 || diff.y < 0 || diff.x > box.w || diff.y > box.h) {
        place = EPLACE.OUT;
      } else if (diff.x < border || diff.y < border || diff.x > box.w - border || diff.y > box.h - border) {
        place = EPLACE.BORDER;
      } else {
        place = EPLACE.IN;
      }
      if (diff.x < box.w / 2) {
        pos = EPOS.LEFT;
      }
      if (diff.x >= box.w / 2) {
        pos = EPOS.RIGHT;
      }
      if (diff.y < border) {
        pos = EPOS.TOP;
      }
      if (diff.y > box.h - border) {
        pos = EPOS.BOTTOM;
      }
      return {pos: pos, place: place};
    };

    var whichColumn = function ($row, position) {
      var $columns = $('[class*="ms-col-"]', $row);
      var columnId = -3;
      var found = false;
      var relativePostion = 'left';
      var $column;
      $columns.each(function (i, el) {
        if (found) {
          return;
        }
        var $el = $(el);
        var offset = $el.offset();
        var minX = offset.left;
        var maxX = offset.left + $el.width();
        if (minX < position.x) {
          if (maxX > position.x) {
            columnId = $el.attr('data-id').split('-')[1];
            if ($el.is(':last-child') && (maxX + minX) / 2 < position.x) {
              relativePostion = 'right';
            } else if ((maxX + minX) / 2 < position.x) {
              columnId = parseInt($el.attr('data-id').split('-')[1]) + 1;
            }
            $column = $el;
            found = true;
          } else {
            columnId = -1;
          }
        } else {
          columnId = -2;
        }
      });
      return {$column: $column, columnId: columnId, relativePostion: relativePostion};
    };

    var whichRow = function (position) {
      var $rows = $('.ms-row');
      var rowId = -3;
      var found = false;
      var relativePosition;
      var $row;
      $rows.each(function (i, el) {
        if (found) {
          return;
        }

        var $el = $(el);
        var offset = $el.offset();

        var pos = whereIsFromBox({x: offset.left, y: offset.top, w: $el.outerWidth(), h: $el.outerHeight()}, position, 10);


        if (pos !== EPOS.OUT) {
          found = true;
          $row = $el;
          rowId = $el.attr('data-id');
          relativePosition = pos;
        }
      });
      return {$row: $row, rowId: rowId * 1, relativePosition: relativePosition};
    };

    var boxes;

    var createBoxes = function () {
      if (!boxes) {

      }
    };

    var whereShouldDrop = function (position) {
      //var pos = whereIsFromBox({x:100,y:100,w:300,h:300},position,50);
      ////console.log removed.
      var row = whichRow(position);
      var result = {};

      var firstRow = $('.ms-row').first(),
        firstRowData = Number(firstRow.attr('data-id')),
        lastRow = $('.ms-row').last(),
        lastRowData = Number(lastRow.attr('data-id'));

      if (row.rowId === -1) { //Add a new row to the bottom
        return {row: lastRowData, col: 0, position: 'bottom', type: 'row'};
      }

      if (row.rowId === -2) { //Add a new row to the top
        return {row: firstRowData, col: 0, position: 'top', type: 'row'};
      }

      if (row.positionY === 1) {
        return {row: row.rowId, col: 0, position: 'bottom', type: 'row'};
      } else if (row.positionY === -1 && row.rowId === firstRowData) {
        return {row: row.rowId, col: 0, position: 'top', type: 'row'};
      } else if (row.positionY === -1) {
        return {row: (row.rowId) - 1, col: 0, position: 'bottom', type: 'row'};
      }

      //Here is if we are on a row
      if (row.relativePosition !== 0) {
        //Add to the left of the row.rowId
        return {row: row.rowId, col: 0, position: 'left', type: 'col'};
      } else if (row.relativePosition === 1) {
        //Add to the right of the row.rowId
        return {row: row.rowId, col: 12, position: 'right', type: 'col'};
      }
      //Now we have to between which column we should add
      var column = whichColumn(row.$row, position);
      return {row: row.rowId * 1, col: column.columnId * 1, position: column.relativePostion, type: 'col'};
    };


    var dropLocator = {whereShouldDrop: whereShouldDrop};
    return dropLocator;
  }]);