'use strict';

angular.module('msContentItemsToolbar')
  .directive('contentItemsToolbarDirective', ['LayoutService', '$compile', '$timeout', 'DroppableAreaManager', 'ResizeAreaManager', function (LayoutService, $compile, $timeout, DroppableAreaManager, ResizeAreaManager) {
    return {
      restrict: 'A',
      scope: {},
      link: function (scope, element, attrs) {

        var getGlobalSettingsDroppableArea = function(){
          var settings={};
          settings.margins={};
          settings.margins.left = $('.ms-sidebar').outerWidth();
          settings.margins.top = $('.ms-topbar').outerHeight();
          //console.log removed.
          return settings;
        };

        var makeContentItemsSortable = function () {
          //This timeout should disappear
          $timeout(
            function () {
              $('.ms-block').draggable({
                  revert: true,
                  handle: '.handle',
                  opacity: 0.7,
                  scrollSensitivity: 50,
                  //helper: 'clone',
                  //containment: '.ms-grid',
                  start: function (e, ui) {
                    ui.helper.css({'zIndex': 999999});
                    ui.helper.append('<div class="draggableLayout" style="z-index:-99;position:fixed;top:0px;left:0px;width:100%;height:100%"></div>');
                    DroppableAreaManager.start($('.ms-content-items-layout-directive'), function ($dropped, $draggedTo) {
                      var droppedId = $dropped.attr('data-id');
                      var draggedToId = $draggedTo.attr('data-id');
                      var draggedToPosition = $draggedTo.attr('data-position');
                      if(draggedToPosition === 'nrl' || draggedToPosition === 'nrr'){ // Adding nested row
                        LayoutService.moveToNestedRow(droppedId, draggedToId, draggedToPosition === 'nrr');
                      }else {
                        LayoutService.moveElement(droppedId, draggedToId, draggedToPosition === 'r' || draggedToPosition === 'b');
                      }
                      makeContentItemsSortable();
                    },getGlobalSettingsDroppableArea());
                  },
                  stop: function (e, ui) {
                    ui.helper.css({'zIndex': 0});
                    ui.helper.find('.draggableLayout').remove();
                    DroppableAreaManager.stop();
                  }
                }
              );
            }, 1000
          );
        };

        var widgetsInitialized = false;

        var makeWidgetsDraggable = function () {
          if(widgetsInitialized){
            return;
          }
          widgetsInitialized=true;
          LayoutService.getAvailableWidgets().forEach(function (widgets) {
            var wrapWidget = angular.element('<div class="draggable"></div>'),
              wrapWidgetInner = angular.element('<div class="widgetContent"></div>'),
              widget = angular.element('<div class="label">' + widgets.name + '</div>');

            wrapWidget.attr('class', 'widget ' + widgets.type + ' icon-widget-' + widgets.type);
            wrapWidget.attr('id', 'widget-' + widgets.type);
            wrapWidget.attr('data-type', widgets.type);

            wrapWidgetInner.append(widget);
            wrapWidget.append(wrapWidgetInner);
            element.append(wrapWidget);


            $timeout(function () {
              wrapWidget.draggable({
                opacity: 1,
                helper: 'clone',
                appendTo: '.widgets',
                drag:function(e,ui){
                  var initialTop = ui.helper.parent().offset().top;
                  var height = ui.helper.outerHeight();
                  ui.position.top = e.pageY-initialTop-height/2;
                  return DroppableAreaManager.isStarted();
                },
                start: function (e,ui) {
                  var settingsDroppableArea = {
                    scrollToView:true,
                    darkenSoroundings:true,
                    margins:getGlobalSettingsDroppableArea().margins
                  };
                  DroppableAreaManager.start($('.ms-content-items-layout-directive'), function ($dropped, $draggedTo) {
                    var draggedToId = $draggedTo.attr('data-id');
                    var draggedToPosition = $draggedTo.attr('data-position');
                    if(draggedToPosition === 'nrl' || draggedToPosition === 'nrr'){ // Adding nested row
                      LayoutService.addNestedRow(draggedToId, draggedToPosition === 'nrr', {type: $dropped.attr('data-type')});
                    }else {
                      LayoutService.addElement(draggedToId, draggedToPosition === 'r' || draggedToPosition === 'b', {type: $dropped.attr('data-type')});
                    }
                    makeContentItemsSortable();
                  },settingsDroppableArea);
                },
                stop: DroppableAreaManager.stop
              });
            }, 100);

          });
        };

        var resizeElement = function(id, colSize, colCurrentSize){
          ////console.log removed.


          LayoutService.resizeElement(id, colSize);
          ////console.log removed.
        };

        scope.$on('page.loaded',function(){
          ////console.log removed.

        });

        var init = function () {
          var containerSelector = '.ms-content-items-layout-directive';
          var $container = $(containerSelector);

          $container.attr('id', 'ms-content-items-layout');

          if ($container.length === 0) {
            $timeout(init, 100);
            return;
          }
          //console.log removed.
          $timeout(function(){ResizeAreaManager.start(containerSelector,resizeElement);}, 2000);

          makeWidgetsDraggable();
          makeContentItemsSortable();
        };

        init();
        scope.$on('$routeChangeSuccess',function(){
          //Everytime we change the page we recalculate all the draggable stuff
          $timeout(init,1000);
        });
      }
    };
  }]);