'use strict';

angular.module('msContentItemsToolbar')
  .factory('DroppableAreaManager', ['$timeout', function ($timeout) {
    var POS_TO_DROP = {
      TOP_________: 't',
      BOTTOM______: 'b',
      LEFT________: 'l',
      RIGHT_______: 'r',
      NESTED_LEFT_: 'nrl',
      NESTED_RIGHT: 'nrr'
    };
    var REL_ELEM = {
      CHILD_: 'c',
      PARENT: 'p',
      SELF__: 's'
    };
    var INNER_AREA = {
      HALF__: 'half',
      MARGIN: 'm'
    };
    var OUTER_AREA = {
      FILL__: 'fill',
      IGNORE: 'ignore'
    };
    var MIN_MARGIN = {
      NONE: {val: 0},
      DEFAULT: {val: 20}
    };

    var $dropBar = $('<div class="dropIndicator" style="position:absolute">');

    var hideWhereYouWantToDraw = function ($el, ui) {
      if($el) {
        $el.css('opacity', 0.1);
      }
      $('.droppableArea').css('opacity', 0.1);
      //$('.activeDropArea').css('background-color','');
      $('.activeDropArea').removeClass('activeDropArea');
      $dropBar.hide();
    };

    var drawWhereYouWantToDraw = function ($el, $dragging, $container) {
      $el.css('opacity', 0.8);
      var id = $el.attr('data-id');
      if (id && id === $dragging.attr('data-id')) {
        return;
      }

      var defaultSettings = {color: 'blue', zIndex: 1};
      var settings = angular.extend({}, defaultSettings, $el.data('settings'));

      var position = $el.attr('data-position');
      var $target;

      if (id === '') {

        if (position === 't') {
          $target = $('.ms-row:first');
        } else {
          $target = $('.ms-row:last');
        }
      } else if(!id){
        //In case that there is no elements on the page we use the container
        $target = $('.ms-content-items-layout-directive');
      }else {
        $target = $('[data-id=' + id + ']:first');
      }
      if ($target.length === 1) {
        var $parent = $target.parents('[data-id]:first');
        if(position[0]!=='n') {
          $parent.addClass('activeDropArea');
          //$parent.css('background-color', 'red');
        }else{ //If it's a nested row then we will indicate on the element itself
          $target.addClass('activeDropArea');
          //$target.css('background-color', 'red');
        }
        var offset = $target.offset();
        var x = offset.left;
        var y = offset.top;
        var w = $target.outerWidth();
        var h = $target.outerHeight();
        var thickness = 5;
        $dropBar.css({
          //background: settings.color,
          zIndex: settings.zIndex
        });
        var lastChar = position.charAt(position.length - 1);
        $dropBar.removeClass('top');
        $dropBar.removeClass('bottom');
        $dropBar.removeClass('left');
        $dropBar.removeClass('right');
        if (lastChar === 't') {
          $dropBar.addClass('top');
          $dropBar.css({
            left: x,
            top: y - thickness / 2,
            width: w,
            height: thickness
          });
        } else if (lastChar === 'b') {
          $dropBar.addClass('bottom');
          $dropBar.css({
            left: x,
            top: y + h - thickness / 2,
            width: w,
            height: thickness
          });
        } else if (lastChar === 'l') {
          $dropBar.addClass('left');
          $dropBar.css({
            left: x - thickness / 2,
            top: y,
            width: thickness,
            height: h
          });
        } else if (lastChar === 'r') {
          $dropBar.addClass('right');
          $dropBar.css({
            left: x + w - thickness / 2,
            top: y,
            width: thickness,
            height: h
          });
        }
        $dropBar.show();
      } else {
        //console.log removed.
      }
    };

    var getRandomColor = function () {
      var letters = '0123456789ABCDEF'.split('');
      var color = '#';
      for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
    };

    var deleteAllDropables = function () {
      hideWhereYouWantToDraw();
      $('.container-droppables').remove();
    };

    var getValuesForElements = function ($elements) {
      var result = {count: 0, bottom: 0, right: 0, left: 999999, top: 99999, isHorizontal: true};
      if (!$elements) {
        result.left = 0;
        result.top = 0;
        return result;
      }
      result.count = $elements.length;
      $elements.each(function (i, el) {
        var $el = $(el);
        var offset = $el.offset();
        var w = $el.outerWidth();
        var h = $el.outerHeight();
        var id = $el.attr('data-id');
        result.isHorizontal = (offset.left - result.left > offset.top - result.top);
        result.top = Math.min(result.top, offset.top);
        result.bottom = Math.max(result.bottom, offset.top + h);
        result.left = Math.min(result.left, offset.left);
        result.right = Math.max(result.right, offset.left + w);
        result.lastId = id;
        if (!result.firstId) {
          result.firstId = id;
        }
      });
      return result;
    };

    var drawDroppableDiv = function (area, position, margin, id, positionid, $droppables, dropCallback) {
      var divPos;
      var settings = {};
      if (positionid.length > 1) {
        settings.color = 'red';
      }

      if (position === POS_TO_DROP.TOP_________) {
        if (area.height < margin) {
          return;
        }
        divPos = [area.left, area.top, area.width, margin];
        area.top += margin;
        area.height -= margin;
      } else if (position === POS_TO_DROP.BOTTOM______) {
        if (area.height < margin) {
          //return;
          margin = area.height;
        }
        divPos = [area.left, area.top + area.height - margin, area.width, margin];
        area.height -= margin;
      } else if (position === POS_TO_DROP.LEFT________) {
        if (area.width < margin) {
          return;
        }
        divPos = [area.left, area.top, margin, area.height];
        area.left += margin;
        area.width -= margin;
      } else if (position === POS_TO_DROP.RIGHT_______) {
        if (area.width < margin) {
          return;
        }
        divPos = [area.left + area.width - margin, area.top, margin, area.height];
        area.width -= margin;
      }
      if (divPos) {
        createDroppableDiv($droppables, divPos[0], divPos[1], divPos[2], divPos[3], id, positionid, dropCallback, settings);
      }
    };

    var getHowWeNeedToCreateTheDroppableAreasOnThisElement = function ($element, family) {
      var settings = {whereToDraw: []};
      //The shape of the array is:
      // 0 - Where to drop it: t (top), b (bottom), l (left), r (right), nrl (nested row left), nrr (nested row right)
      // 1 - Relative element to which will add the droppable area: c (child element, if it's a list of elements depending if you are adding before or after is going to add to the first or last element of the array), p (parent of the element), otherwise is adding it to the element itself
      // 2 - How to manage the areas within the element: half (will halve the area available on the element and put the droppable area there), otherwise (will add the margin)
      // 3 - If you want to fill up all the space available in case there is a gap between the parent and the element (for instance: the bottom of the columns of different heights). fill (will fill up the space), otherwise no
      // 4 - Minimum Margin: integer (this will make sure that if the theoretical margin for that is unexistent you can specify minimum margin)
      if ($element.hasClass('ms-level-1')) {
        if ($element.hasClass('ms-row')) {
          settings.$childElements = $element.find('.ms-col.ms-level-1');
          settings.whereToDraw = [
            [POS_TO_DROP.TOP_________, REL_ELEM.SELF__, INNER_AREA.MARGIN, OUTER_AREA.FILL__],
            [POS_TO_DROP.BOTTOM______, REL_ELEM.SELF__, INNER_AREA.MARGIN, OUTER_AREA.FILL__],
            [POS_TO_DROP.LEFT________, REL_ELEM.CHILD_, INNER_AREA.MARGIN, OUTER_AREA.FILL__],
            [POS_TO_DROP.RIGHT_______, REL_ELEM.CHILD_, INNER_AREA.MARGIN, OUTER_AREA.FILL__]
          ];
        } else if ($element.hasClass('ms-col')) {
          settings.$childElements = $element.find('.ms-block.ms-level-1,.ms-row.ms-level-2');
          settings.whereToDraw = [
            [POS_TO_DROP.TOP_________, REL_ELEM.CHILD_, INNER_AREA.MARGIN, OUTER_AREA.FILL__, MIN_MARGIN.NONE],
            [POS_TO_DROP.BOTTOM______, REL_ELEM.CHILD_, INNER_AREA.MARGIN, OUTER_AREA.FILL__, MIN_MARGIN.NONE]
          ];
        } else if ($element.hasClass('ms-block')) {
          settings.whereToDraw = [
            [POS_TO_DROP.TOP_________],
            [POS_TO_DROP.BOTTOM______],
          ];
          if (family.$siblings.length > 1) {
            settings.whereToDraw.push([POS_TO_DROP.LEFT________, REL_ELEM.PARENT, INNER_AREA.MARGIN, OUTER_AREA.FILL__]);
            settings.whereToDraw.push([POS_TO_DROP.RIGHT_______, REL_ELEM.PARENT, INNER_AREA.MARGIN, OUTER_AREA.FILL__]);
            settings.whereToDraw.push([POS_TO_DROP.NESTED_LEFT_, REL_ELEM.SELF__, INNER_AREA.HALF__, OUTER_AREA.FILL__]);
            settings.whereToDraw.push([POS_TO_DROP.NESTED_RIGHT, REL_ELEM.SELF__, INNER_AREA.HALF__, OUTER_AREA.FILL__]);
          } else {
            settings.whereToDraw.push([POS_TO_DROP.LEFT________, REL_ELEM.PARENT, INNER_AREA.HALF__, OUTER_AREA.FILL__]);
            settings.whereToDraw.push([POS_TO_DROP.RIGHT_______, REL_ELEM.PARENT, INNER_AREA.HALF__, OUTER_AREA.FILL__]);
          }
        }
      } else if ($element.hasClass('ms-level-2')) {
        if ($element.hasClass('ms-row')) {
          settings.$childElements = $element.find('.ms-col.ms-level-2');
          settings.whereToDraw = [
            [POS_TO_DROP.TOP_________, REL_ELEM.SELF__, INNER_AREA.MARGIN, OUTER_AREA.FILL__],
            [POS_TO_DROP.BOTTOM______, REL_ELEM.SELF__, INNER_AREA.MARGIN, OUTER_AREA.FILL__]
          ];
        } else if ($element.hasClass('ms-col')) {
          settings.$childElements = $element.find('.ms-block.ms-level-2,.ms-row.ms-level-3');
          settings.whereToDraw = [
            [POS_TO_DROP.TOP_________, REL_ELEM.CHILD_, INNER_AREA.MARGIN, OUTER_AREA.FILL__, MIN_MARGIN.NONE],
            [POS_TO_DROP.BOTTOM______, REL_ELEM.CHILD_, INNER_AREA.MARGIN, OUTER_AREA.FILL__, MIN_MARGIN.NONE]
          ];
        } else if ($element.hasClass('ms-block')) {
          settings.whereToDraw = [
            [POS_TO_DROP.TOP_________],
            [POS_TO_DROP.BOTTOM______],
          ];
          settings.whereToDraw.push([POS_TO_DROP.LEFT________, REL_ELEM.PARENT, INNER_AREA.HALF__, OUTER_AREA.FILL__]);
          settings.whereToDraw.push([POS_TO_DROP.RIGHT_______, REL_ELEM.PARENT, INNER_AREA.HALF__, OUTER_AREA.FILL__]);
        }
      } else if ($element.hasClass('ms-content-items-layout-directive')) {
        settings.$childElements = $element.find('.ms-row.ms-level-1');
        if (settings.$childElements.length === 0) {
          settings.whereToDraw = [
            [POS_TO_DROP.TOP_________, REL_ELEM.SELF__, INNER_AREA.MARGIN, OUTER_AREA.FILL__],
            [POS_TO_DROP.BOTTOM______, REL_ELEM.SELF__, INNER_AREA.MARGIN, OUTER_AREA.FILL__],
          ];
        }
      }
      return settings;
    };

    var callTheChildrenToCreateDroppables = function (settings, childValues, $element, area, $droppables, dropCallback) {

      if (settings.$childElements) {
        settings.$childElements.each(function (i, el) {
          var $el = $(el);
          var id = $el.attr('data-id');
          var offset = $el.offset();
          var subArea = angular.copy(area);
          if (!$element.hasClass('ms-content-items-layout-directive')) {
            subArea.left = Math.floor(Math.max(area.left, offset.left));
            subArea.width = Math.floor(Math.min(area.left + area.width, offset.left + $el.outerWidth()) - subArea.left);
            subArea.top = Math.floor(Math.max(area.top, offset.top));
          }
          if (childValues.firstId !== id) {
            subArea.top = Math.floor(Math.max(area.top, offset.top));
          }
          if (childValues.lastId !== id && !$el.hasClass('ms-col')) {
            subArea.height = Math.floor(Math.min(area.top + area.height, offset.top + $el.outerHeight()) - subArea.top);
          } else {
            subArea.height = Math.floor(area.height - (subArea.top - area.top));
          }
          ////console.log removed.
          createDroppable($(el), {$siblings: settings.$childElements, $parent: $element}, subArea, $droppables, dropCallback);
        });
      }
    };

    var getTargetIdAndSizeOfTheDroppableAreaForThisDraggableArea = function (whereToDraw, $element, childValues, currentValues, area) {
      var whereToDrawPosition = whereToDraw[0];
      whereToDrawPosition = whereToDrawPosition.charAt(whereToDrawPosition.length - 1);
      var drawOnChild = whereToDraw[1] === REL_ELEM.CHILD_;
      var drawOnParent = whereToDraw[1] === REL_ELEM.PARENT;
      var halfSpace = whereToDraw[2] === INNER_AREA.HALF__;
      var fillAllTheGap = whereToDraw[3] === OUTER_AREA.FILL__;
      var minMargin = (whereToDraw[4] || MIN_MARGIN.DEFAULT).val;
      var isBefore = true;
      var margin = minMargin;
      var referenceValues = childValues;
      if (childValues.count === 0) {
        referenceValues = currentValues;
      }
      referenceValues = angular.copy(referenceValues);
      if (halfSpace) {
        var gapInPixelsY = (referenceValues.top + referenceValues.bottom) % 2; //If is odd number
        var gapInPixelsX = (referenceValues.left + referenceValues.right) % 2; //If is odd number

        referenceValues.top = (referenceValues.top + referenceValues.bottom) / 2;
        referenceValues.bottom = referenceValues.top - gapInPixelsY;
        referenceValues.left = (referenceValues.left + referenceValues.right) / 2;
        referenceValues.right = referenceValues.left - gapInPixelsX;
        //console.log removed.
        //console.log removed.
      }
      if (whereToDrawPosition === 't') {
        margin = referenceValues.top - area.top;
      } else if (whereToDrawPosition === 'b') {
        isBefore = false;
        margin = area.top + area.height - referenceValues.bottom;
      } else if (whereToDrawPosition === 'l') {
        margin = referenceValues.left - area.left;
      } else if (whereToDrawPosition === 'r') {
        isBefore = false;
        margin = area.left + area.width - referenceValues.right;
      }
      if (fillAllTheGap || halfSpace) {
        margin = Math.max(margin, minMargin);
      } else {
        margin = minMargin;
      }
      margin = Math.floor(margin);
      var id;
      if (drawOnChild) {
        if (isBefore) {
          id = childValues.firstId;
        } else {
          id = childValues.lastId;
        }
      } else {
        if (isBefore) {
          id = currentValues.firstId;
        } else {
          id = currentValues.lastId;
        }
      }
      if (drawOnParent) {
        id = $element.parents('[data-id]:first').attr('data-id');
      }
      return {
        id: id,
        margin: margin,
        whereToDrawPosition: whereToDrawPosition
      };
    };

    var createDroppable = function ($element, family, area, $droppables, dropCallback) {
      var settings = getHowWeNeedToCreateTheDroppableAreasOnThisElement($element, family);

      var childValues = getValuesForElements(settings.$childElements);
      var currentValues = getValuesForElements($element);
      ////console.log removed.

      for (var i = 0; settings.whereToDraw[i]; i++) {
        var targetAndSize = getTargetIdAndSizeOfTheDroppableAreaForThisDraggableArea(settings.whereToDraw[i], $element, childValues, currentValues, area);
        ////console.log removed.
        if (targetAndSize.margin > 0) {
          drawDroppableDiv(area, targetAndSize.whereToDrawPosition, targetAndSize.margin, targetAndSize.id, settings.whereToDraw[i][0], $droppables, dropCallback);
        }
      }

      callTheChildrenToCreateDroppables(settings, childValues, $element, area, $droppables, dropCallback);
    };

    var createAllDropables = function ($container, margins, dropCallback) {
      margins=margins||{};
      margins.top=margins.top||0;
      margins.left=margins.left||0;
      margins.right=margins.right||0;
      margins.bottom=margins.bottom||0;
      var $containerOfDroppables = $('<div>');
      $containerOfDroppables.addClass('container-droppables');
      $('body').append($containerOfDroppables);
      hideWhereYouWantToDraw();
      $containerOfDroppables.append($dropBar);
      var $document = $(document);
      var activeArea = {
        top: margins.top,
        left: margins.left,


        width: $document.outerWidth()-margins.left-margins.right,
        height: $document.outerHeight()-margins.top-margins.bottom
      };
      var $emptyJQueryObject = $('.hereweneedtoreturnanempyjqueryobject.this.will.do.i.guess');
      createDroppable($container, {siblings: $emptyJQueryObject, parent: $emptyJQueryObject}, activeArea, $containerOfDroppables, dropCallback);
    };

    var createDroppableDiv = function ($container, x, y, w, h, id, position, dropCallback, settings) {
      var defaultSettings = {zIndex: 1};
      settings = angular.extend({}, defaultSettings, settings);
      var $el = $('<div class="droppableArea">');
      $el.css({
        position: 'absolute',
        top: y,
        left: x,
        width: w,
        height: h,
        zIndex: settings.zIndex,
        //background: getRandomColor(),
        opacity: 0.1
      });
      $el.attr('data-id', id);
      $el.attr('data-position', position);
      $el.data('settings', settings);
      $container.append($el);
      $el.droppable({
          tolerance: 'pointer',
          over: function (event, ui) {
            var $self = $(this);
            $timeout(function () {
              drawWhereYouWantToDraw($self, $(ui.helper));
            }); //To be able to assure that over is going to execute after
          },
          out: function (event, ui) {
            hideWhereYouWantToDraw($(this), ui);
          },
          drop: function (e, ui) {
            var $dropped = $(ui.helper);
            var $draggedTo = $(this);
            dropCallback($dropped, $draggedTo, e);
            stop();
          }
        }
      );
    };

    var scrollToView = function($container){
      var middleScreen = $(window).height()/2+$('body').scrollTop();
      var containerTop=$container.offset().top;
      if(containerTop>middleScreen){
        $('body, html').animate({ scrollTop: containerTop-20 });
      }
    };

    var darkenSoroundings = function($container){
      var $overlay = $('<div class="ms-overlay"></div>');
      $overlay.css({
        'position':'absolute',
        'background':'black',
        'opacity':'0.5'
      });
      //This is just too cheeky....
      var $containerWidth=$container.find('.ms-row-inner:first');
      if($containerWidth.length===0){
        $containerWidth=$container;
      }
      var margin = 10;
      var top = $container.offset().top-margin;
      var left = $containerWidth.offset().left-margin;
      var height = $container.outerHeight()+margin*2;
      var width = $containerWidth.outerWidth()+margin*2;
      var bottom = top+height;
      var right = left+width;
      var $topOverlay = $overlay.clone();
      var $bottomOverlay = $overlay.clone();
      var $leftOverlay = $overlay.clone();
      var $rightOverlay = $overlay.clone();
      var totalHeight = $(document).outerHeight();
      var totalWidth = $(document).outerWidth();
      //console.log removed.
      $topOverlay.css({
        'top':'0px',
        'left':'0px',
        'width':'100%',
        'height':top+'px'
      });
      $bottomOverlay.css({
        'top':bottom+'px',
        'left':'0px',
        'width':'100%',
        'height':(totalHeight-bottom)+'px'
      });
      $leftOverlay.css({
        'top':top+'px',
        'left':'0px',
        'width':left+'px',
        'height':height+'px'
      });
      $rightOverlay.css({
        'top':top+'px',
        'left':right+'px',
        'width':(totalWidth-right)+'px',
        'height':height+'px'
      });
      $('.container-droppables').append($topOverlay);
      $('.container-droppables').append($bottomOverlay);
      $('.container-droppables').append($leftOverlay);
      $('.container-droppables').append($rightOverlay);
    };

    var status;
    var start = function ($container, callback, settings) {
      status='started';
      settings=settings||{};
      createAllDropables($container, settings.margins, callback);
      if(settings.scrollToView){
        scrollToView($container);
      }
      if(settings.darkenSoroundings){
        darkenSoroundings($container);
      }
      $(document).on('keydown.droppableArea',function(evt) {
        evt = evt || window.event;
        if (evt.keyCode === 27) {
          stop();
        }
      });
    };

    var isStarted = function(){
      return status==='started';
    };

    var stop = function () {
      status='stopped';
      $(document).off('keydown.droppableArea');
      deleteAllDropables();
    };

    var droppableAreaManager = {
      start: start,
      stop: stop,
      isStarted:isStarted
    };
    return droppableAreaManager;
  }]);
