'use strict';

angular.module('msCheckoutSettingsRepository').factory('checkoutSettingsRepository', ['$rootScope', 'ShopRestangular', function ($rootScope, ShopRestangular) {

  var checkoutSettingsRepository = {};

  checkoutSettingsRepository.getAllPayments = function () {
    ShopRestangular.all('settings/checkout').getList().then(
      function (response) {
        //HelperService.setModelMessages({'title':'test', 'message': 'test', 'status': 'success'});
        return response.data;

      },
      function (error) {
        //HelperService.getModelMessages(error.data);
        return error;
      }
    );
  };

  checkoutSettingsRepository.savePayments = function (postData) {
    ShopRestangular.all('settings/checkout').customPUT(postData).then(
      function (response) {
        //HelperService.getModelMessages('saved');
        return response.data;
      },
      function (error) {
        //HelperService.getModelMessages(error.data);
        return error;
      }
    );
  };

  checkoutSettingsRepository.activatePayment = function (postData) {
    ShopRestangular.all('settings/payment/activate').customPUT(postData).then(
      function (response) {
        $rootScope.$broadcast('activatePaymentSuccess', postData);
        //HelperService.getModelMessages('saved');
        return response.data;
      },
      function (error) {
        $rootScope.$broadcast('activatePaymentFail', postData);
        //HelperService.getModelMessages(error.data);
        return error;
      }
    );
  };

  return checkoutSettingsRepository;
}]);
