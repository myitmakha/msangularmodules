'use strict';

angular.module('msCreatedBy')
  .directive('msCreatedBy', ['$compile', function ($compile) {
    return {
      restrict: 'E',
      link: function (scope, element) {
        scope.settings = scope.$parent.settings || {};

        var compileToView = function(){
          var html = '<div class="created-by"><a href="http://www.mrsite.com/uk" target="_blank">Created at Mr Site</a></div>';
          if (scope.settings.hideCreatedBy === true) {
            html = '';
          }

          $compile(html)(scope, function (html) {
            element.html(html);
          });
        };

        scope.$watch('settings.hideCreatedBy', function () {
          compileToView();
        });
      }
    };
  }]);
