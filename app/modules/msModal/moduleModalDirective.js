'use strict';

angular.module('msModal')
  .directive('msModal', ['$timeout', function ($timeout) {
    return{
      restrict: 'A',
      link: function (scope, element, attrs) {
        var active = false;
        $timeout(function () { //need this timeout see http://blog.brunoscopelliti.com/run-a-directive-after-the-dom-has-finished-rendering

          var modalClass = (attrs.msModalClass) ? attrs.msModalClass : '',
            modalContent = angular.element('#' + attrs.msModal),
            modal = angular.element('#modal'),
            modalOverlay = angular.element('#modal-overlay'),
            modalWrapperId = attrs.msModal + '-modal-wrapper';

          if (attrs.msModal === '') {
            throw new Error('You must supply the id of the modal content, e.g. ms-modal="my-modal-content"');
          }

          if (modalContent.length === 0) {
            throw new Error('Could not find your modal content');
          }

          var modalBottom = modal.offset().top + modal.outerHeight();
          var newHeight = 'auto';

          var modalHeight = function () {
            modalContent.removeAttr('style');
            $timeout(function () {
              var windowBottom = $(window).outerHeight();
              if (modalBottom >= windowBottom) {
                newHeight = modal.outerHeight() - (modalBottom - windowBottom ) - 40;
              }
              modalContent.css({
                'height': newHeight
              });
            }, 10);
          };

          var modalPosition = function () {
            modal.css({
              top: ($(window).outerHeight() - modal.outerHeight()) / 4,
              left: ($(window).outerWidth() - modal.outerWidth()) / 2
            });
          };

          var tabHeight = function () {
            if ($('.tabs-wrap', modal).length > 0) {
              $('.tab-content', modal).removeAttr('style');

              $timeout(function () {
                $('.tab-content', modal).css({
                  'height': modal.outerHeight() - $('.nav-tabs', modal).outerHeight()
                });
              }, 2);
            }
          };

          element.on('click', function () {
            modalContent.clone().appendTo( modal );
            modal.addClass(modalClass);
            modalOverlay.addClass(modalClass);
            $timeout(function () {
              tabHeight();
              modalHeight();
              modalPosition();
            }, 1);
            modal.addClass('show').fadeIn();
            modalOverlay.addClass('show-overlay').fadeIn();
            modal.find('input').eq(0).trigger('focus');
            active = true;
          });

          var close = function () {
            active = false;
            modal.removeClass('show').fadeOut();
            modalOverlay.removeClass('show-overlay').fadeOut();
            $timeout(function () {
              modalOverlay.removeClass(modalClass);
              modal.empty();
              modal.removeClass(modalClass);
            }, 500);


            modalContent.removeAttr('style');
          };

          var closeSignUpForm = function(){
            close();
            modalContent.remove();
            //console.log removed.
          };

          modal.on('click', '.modal-close', function () {
            close();
          });

          scope.$on('modalClose', function () {
            close();
          });

          scope.$on('closeSignUpForm', function () {
            closeSignUpForm();
          });

          scope.$on('tabChanged', function () {
            if (active) {
              if ($('.tabs-wrap', modal).length > 0) {
                modalHeight();
                tabHeight();
              }
            }
          });

          modalOverlay.on('click', function () {
            close();
          });

          $(window).on('resize', function () {
            if (active) {
              modalHeight();
              modalPosition();
              if ($('.tabs-wrap', modal).length > 0) {
                tabHeight();
              }
            }
          });
        }, 0);
      }
    };
  }]);