//!!!!!!!!!!!!!!!!!!!!! NOT TESTING THIS BIT BECAUSE PROBABLY WE NEED TO REFACTOR IT TO USE RESTANGULAR NOT $HTTP
//'use strict';
//
//describe('msTemplateRepository', function () {
//
//  // load the controller's module
//  beforeEach(module('msTemplateRepository'));
//
//  var TemplateRepository, rootScope, httpBackend, $http;
//
//  // Initialize the controller and a mock scope
//  beforeEach(inject(function (_TemplateRepository_, $rootScope, _$http_) {
//    TemplateRepository = _TemplateRepository_;
//    rootScope = $rootScope;
//    $http=_$http_;
//  }));
//
//  beforeEach(inject(function ($httpBackend) {
//    httpBackend = $httpBackend;
//    $httpBackend.expectGET('http://dev.sitebuilderapi.web/sites/server/')
//      .respond(200, {SiteData: {siteId: 123, pages: [
//        {name: 'Home', url: '/home'},
//        {name: 'About', url: '/about'}
//      ]}});
//    $httpBackend.expectGET('http://themes.dev.clientsites.web/123/staging/main.css').respond(200, 'globalStyles');
//    $httpBackend.expectGET('http://themes.dev.clientsites.web/123/staging/Home.html').respond(200, 'homeHtml');
//    spyOn($http,'get').andCallFake(function(uri){
//      if(uri==='home'){
//        //Error!
//        return 'HomeTemplate';
//      }
//      if(uri==='page'){
//        return 'PageTemplate';
//      }
//      if(uri==='http://themes.dev.clientsites.web/123/staging/main.css'){
//        return 'css';
//      }
//    });
//  }));
//
//
//  it('should get the styles and html msTemplateRepository from Home page', function () {
//    TemplateRepository.getTemplate({name: 'Home'}).then(function (template) {
//      expect(template.html).toEqual('homeHtml');
//      expect(template.styles).toEqual('globalStyles');
//    });
//    httpBackend.flush();
//    rootScope.$digest();
//  });
//});
