/**
 * Created by Albert on 15/07/2014.
 */
'use strict';
angular.module('msTemplateRepository')
  .factory('TemplateRepository', ['Config', '$window', '$http', '$q', 'PageBuilderManager', 'SiteRestangular', function (Config, $window, $http, $q, PageBuilderManager, SiteRestangular) {

    //Every minute is going to be a diferent number, this way we will update the theme after a minute if customer refresh
    var versioningEveryMinute = Math.floor(new Date().getTime() / 1000 / 60);


    var defaultTemplateError = {
      html: '<div>We could not load the requested page</div>',
      styles: '',
      importLess: ''
    };

    var getExternalResource = function (uri) {
      return $http.get(uri + '?v=' + versioningEveryMinute, {cache: true});
    };

    var retrieveTemplate = function (htmlUri, defaultUri, stylesPromise, deferred) {
      getExternalResource(htmlUri).success(function (html) {
        stylesPromise.success(function (styles) {
          deferred.resolve({html: html, styles: styles});
        }).error(function () {
          //console.log removed.
          deferred.resolve({html: html, styles: defaultTemplateError.styles});
        });

      }).error(function () {
        if (!defaultUri || defaultUri === '') {
          //console.log removed.
          deferred.resolve({html: defaultTemplateError.html, styles: defaultTemplateError.styles});
          return;
        }
        //console.log removed.
        retrieveTemplate(defaultUri, '', stylesPromise, deferred);
      });
    };

    var getBaseUrl = function (themeId) {
      var deferred = $q.defer();
      PageBuilderManager.getSiteId().then(function (siteId) {
        var baseUrl = Config.GetEndPoint('themesEndpoint') + siteId + '/' + themeId + '/';
        deferred.resolve(baseUrl);
      });
      return deferred.promise;
    };

    var getTemplate = function (themeId, pageName, ishomepage) {
      var deferred = $q.defer();
      getBaseUrl(themeId).then(function (baseUrl) {
        //This is to avoid the loading flick if the specific page doesn't exists
        var htmlUri = baseUrl + 'page.html';
        var htmlDefaultUri = '';
        //var htmlUri = baseUrl + pageName + '.html';
        //var htmlDefaultUri = baseUrl + 'page.html';
        if(ishomepage){
          htmlUri = baseUrl + 'home.html';
          htmlDefaultUri = baseUrl + 'page.html';
        }

        var stylesUri = baseUrl + 'main.css';
        var stylesPromise = getExternalResource(stylesUri);
        retrieveTemplate(htmlUri, htmlDefaultUri, stylesPromise, deferred);
      });
      return deferred.promise;
    };

    var getTemplateCssLanguage = function (themeId, extension) {
      var deferred = $q.defer();
      getBaseUrl(themeId).then(function (baseUrl) {
        var mainLessUri = baseUrl + 'main.' + extension;
        var importLessUri = baseUrl + 'imports.' + extension;
        var importLessPromise = getExternalResource(importLessUri);
        getExternalResource(mainLessUri).success(function (mainStyles) {
          importLessPromise.success(function (importStyles) {
            deferred.resolve({mainStyles: mainStyles, importStyles: importStyles});
          }).error(function () {
            //console.log removed.
            deferred.resolve({mainStyles: mainStyles, importStyles: defaultTemplateError.importLess});
          });
        }).error(function () {
          //console.log removed.

          importLessPromise.success(function (importStyles) {
            deferred.resolve({mainStyles: defaultTemplateError.styles, importStyles: importStyles});
          }).error(function () {
            //console.log removed.
            deferred.resolve({mainStyles: defaultTemplateError.styles, importStyles: defaultTemplateError.importLess});
          });
        });
      });
      return deferred.promise;
    };

    var getTemplateLess = function (themeId) {
      return getTemplateCssLanguage(themeId, 'less');
    };

    var getTemplateSass = function (themeId) {
      return getTemplateCssLanguage(themeId, 'scss');
    };


    var templateSettings = [
      {name: 'mainBgColor', variable: 'main-bg-color', type: 'color', value: '#e1e1e1'},
      {name: 'mainTextColor', variable: 'main-text-color', type: 'color', value: '#333'},
      {name: 'containerBgColor', variable: 'container-bg-color', type: 'color', value: '#fff'},
      {name: 'fontSize', variable: 'fontSize', type: 'slider', value: '12'},
      {name: 'logoImage', variable: 'logoImage', type: 'image', value: 'http://uk.mrsite.com/assets/img/site-logo.png'},
      {name: 'backgroundImage', variable: 'backgroundImage', type: 'image', value: 'http://www.lichtjagd.de/media/com_twojtoolbox/castell-de-bellver.jpg'},
      {name: 'defaultFontFamily', variable: 'defaultFontFamily', type: 'font-family', value: 'Arial'}
    ];

    var templateSettingsPromise;

    var getTemplateSettings = function (themeId) {
      var deferred = $q.defer();
      if (!themeId) {
        return deferred.promise;
      }
      if (!templateSettingsPromise) {
        templateSettingsPromise = SiteRestangular.one('themes/' + themeId + '/settings').get();
      }
      //TODO: For now we are just genereting them but we should take them from the API
      //console.log removed.
      templateSettingsPromise.then(function (data) {
        if (!(data.data.Settings instanceof Array)) {
          data.data.Settings = JSON.parse(data.data.Settings);
        }
        var settings = data.data.Settings;

        angular.forEach(settings, function (setting) {
          setting.variable = setting.variable || setting.Variable;
          setting.name = setting.name || setting.Name;
          if(typeof setting.value==='undefined'){
            setting.value = setting.Value;
          }
          setting.type = setting.type || setting.Type || 'color';
        });
        deferred.resolve({
          settings:settings,
          styles:data.data.Styles,
          compiler:data.data.Compiler
        });
      });
      return deferred.promise;
    };

    var setTemplateSettings = function (themeId, settings) {
      var deferred = $q.defer();
      if (!themeId) {
        return deferred.promise;
      }
      //console.log removed.
      SiteRestangular.all('themes/' + themeId + '/settings').customPUT(settings).then(function (data) {
        angular.forEach(settings, function (setting) {
          setting.Variable = setting.variable;
          setting.Name = setting.name;
          setting.Value = setting.value;
          setting.Type = setting.type;
        });
        //console.log removed.
        //console.log removed.
        deferred.resolve(data);
      });
      return deferred.promise;
    };

    //public methods
    var templateRepository = {
      getTemplate: getTemplate,
      getTemplateLess: getTemplateLess,
      getTemplateSass: getTemplateSass,
      getTemplateSettings: getTemplateSettings,
      setTemplateSettings: setTemplateSettings
    };

    return templateRepository;
  }]);
