'use strict';

angular.module('msNavigationAdmin')
  .directive('msNavigationAdmin', ['$timeout', '$route', '$location', 'Config','$rootScope','msNavigationAdminHelper', 'ngDialog' , 'PageBuilderManager', 'MenuManager', function ($timeout,$route,$location,Config,$rootScope,msNavigationAdminHelper, ngDialog, PageBuilderManager, MenuManager) {
    return{
      restrict: 'A',
      template:'<div>'+
      '<ul class="pages" ms-scrollbar-plus>'+
      '<li class="page" ng-class="{open:item.openSettings}" data-pageid="{{item.pageId}}" ng-repeat="item in items"><span style="width: 13.5em;">'+
      '<a ng-href="/#/pageBuilder{{item.url}}" ng-if=" item.name.length > 28 ">{{item.name | limitTo : 28}}&hellip;</a>'+
      '<a ng-href="/#/pageBuilder{{item.url}}" ng-if=" item.name.length < 28 ">{{item.name}}</a>'+
      '</span><div class="actions">'+
      '<i class="icon-delete" ng-dialog="presentation/pagebuilder/delete-page.modal.tpl.html" ng-dialog-scope="this" ng-dialog-data="{{item}}" ng-dialog-class="delete-page"></i>' +
      '<i ng-click="openSettings(item)" class="icon-settings"></i>'+
      '<div ms-page-settings class="page-settings" page="item.page"></div></li>'+
      '</ul>'+
      '</div>',
      link: function (scope, element, attrs) {

        PageBuilderManager.getPages().then(function (pages) {
          scope.items = pages;
        });

        var resetAllSettings = function(){
          angular.forEach(scope.items,function(item){
            item.openSettings=false;
          });
        };

        scope.deletePage = function (page) {

          MenuManager.removeFromMenu(page);

          PageBuilderManager.getPages().then(function (pages) {
            if (pages.length <= 1) {
              //console.log removed.
            } else {
              PageBuilderManager.deletePage(page).then(function () {
                $rootScope.$broadcast('modalClose');
                PageBuilderManager.getPages().then(function (pages) {
                  var foundPage = false;
                  angular.forEach(pages, function (page) {
                    if (page.ishomepage) {
                      $location.path('pageBuilder' + page.url);
                      foundPage = true;
                    }
                  });
                  if (!foundPage) {
                    //Go to the page
                    $location.path('pageBuilder' + pages[0].url);
                  }
                });
              });
            }
          });

          $route.reload();
        };

        scope.openSettings = function(item){

          var previous = item.openSettings;
          resetAllSettings();
          item.openSettings = !previous;

          ngDialog.open({
            'template': Config.GetSessionStoredValue('modulesDirectory') + '/msNavigationAdmin/pageSettings.modal.tpl.html',
            'scope': scope,
            'className': 'page-settings tabs',
            'controller': ['$scope', '$filter', function($scope, $filter) {

              $scope.settingsPage = item;
              scope.settingsPage = item;

              $scope.localSettings = {name:$scope.settingsPage.name};

              $scope.$watch('localSettings.name', _.debounce(function (name) {
                $scope.settingsPage.name = name;
                $scope.settingsPage.url = '/' + $filter('cleanUrl')(name);
                $scope.$apply();
              }, 500));

            }]
          });

        };

        scope.$watch('settingsPage.ishomepage', function (ishomepage) {
          if (ishomepage) {
            PageBuilderManager.getPages().then(function (pages) {
              angular.forEach(pages, function (page) {
                if (page !== scope.settingsPage) {
                  page.ishomepage = false;
                }
              });
            });
          }
        });

      }
    };
  }]);