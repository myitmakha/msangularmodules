'use strict';

angular.module('msNavigationAdmin')
  .factory('msNavigationAdminHelper', ['$q', 'Helper', '$rootScope', function ($q, Helper, $rootScope) {

    var factory = function(settings){

      var $container = settings.$container;
      var pages = settings.pages;
      var menuList = settings.menuList;
      var sortableItems = settings.sortableItems;
      var maxNumberLevels = settings.maxNumberLevels;
      var levelIndentation = settings.levelIndentation||48;
      var onStart = settings.onStart||function(){};


      if (typeof maxNumberLevels === 'undefined') {
        maxNumberLevels = 2;
      }
      maxNumberLevels=Math.max(1,maxNumberLevels);
      var previousDepth = -1;
      var firstElementLeft;

      var initSortable = function () {
        sortableItems.splice(0,sortableItems.length);
        populateArrayWithMenus(sortableItems, menuList, pages, 0);

        $container.sortable({
          sort: updatePlaceholderPositionIfNecessary,
          beforeStop: updateDepthOnSortedItem,
          stop: function (e, ui) {
            parseUI($container, ui);
          },
          start: function (e, ui) {
            onStart();
            var $firstElementOnTheList = ui.item.parents('ul:first').children('li:first');
            firstElementLeft = $firstElementOnTheList.offset().left;
            previousDepth = -1;
            joinChildrenRecursive(ui.item);
            $container.sortable('refresh');
          },
          containment: $container,
          placeholder: 'ui-state-highlight page',
          handle: '.actions .icon-reorder'
        });
        $container.disableSelection();
      };

      var updateDepthOnSortedItem = function (e, ui) {
        var depth = ui.placeholder.data('depth');
        ui.item.removeClass('depth-0');
        ui.item.removeClass('depth-1');
        ui.item.addClass('depth-' + depth);
        ui.item.data('depth', depth);
      };

      var updatePlaceholderPositionIfNecessary = function (e,ui) {
        var depth = getDepthByDragPosition(ui);

        if (depth !== previousDepth) {
          ui.placeholder.removeClass('depth-' + previousDepth);
          ui.placeholder.addClass('depth-' + depth);
          ui.placeholder.data('depth', depth);
          previousDepth=depth;
        }
      };

      var joinChildrenRecursive = function ($li) {
        var currentDepth = $li.data('depth');
        var $nextNext;
        for (var $next = $li.next(); $next.length === 1; $next = $nextNext) {
          $nextNext = $next.next();
          if (!$next.data('pageid')) {
            continue;
          }
          var nextDepth = $next.data('depth');
          if (nextDepth <= currentDepth) {
            return;
          }
          joinChildrenRecursive($next);
          var $ulchildren = $li.find('ul.children');
          if ($ulchildren.length === 0) {
            $ulchildren = $('<ul></ul>').addClass('children');
            $li.append($ulchildren);
          }
          $ulchildren.append($next);
        }
      };

      var parseUiRecursive = function (subMenuItemList, listOfUiElements, depth) {
        while (listOfUiElements[0]) {
          var element = listOfUiElements[0];
          if (element.depth < depth) {
            return;
          }
          if (element.depth > depth) {
            var lastItem = subMenuItemList[subMenuItemList.length - 1];
            parseUiRecursive(lastItem.subMenuItems, listOfUiElements, depth + 1);
            continue;
          }
          subMenuItemList.push({pageId: element.pageId, hideOnMenu:!!element.hideOnMenu, subMenuItems: []});
          listOfUiElements.shift();
        }
      };

      var parseUI = function ($list) {
        menuList.splice(0, menuList.length);
        var listOfUiElements = [];

        //console.log removed.

        angular.forEach($list.find('li.page'), function (el) {
          var $el = $(el);
          var $parent = $el.parents('li.page:first');
          if($parent.length===1){
            $el.data('depth',Math.min(maxNumberLevels-1,$parent.data('depth')+1));
          }
          listOfUiElements.push({
            depth: $el.data('depth'),
            pageId: $el.data('pageid'),
            hideOnMenu: $el.data('hideonmenu')
          });
        });

        //console.log removed.

        parseUiRecursive(menuList, listOfUiElements, 0);
        $rootScope.$apply();
        //console.log removed.
      };

      var updateMenu = function(){
        parseUI($container);
      };

      var getPageItemByPageId = function (pages, id) {
        if (!id) {
          return;
        }
        for (var i = 0; pages[i]; i++) {
          if (pages[i].id === id) {
            return pages[i];
          }
        }
      };

      var populateArrayWithMenus = function (list, menuList, pages, depth) {
        if (!menuList) {
          return;
        }
        var i = 0;
        while (menuList[i]) {
          var menu = menuList[i];
          var page = getPageItemByPageId(pages, menu.pageId);
          list.push({page: page, hideOnMenu:!!menu.hideOnMenu, pageId: menu.pageId, depth: depth});
          populateArrayWithMenus(list, menu.subMenuItems, pages, depth + 1);
          i++;
        }
      };

      var getMaxMinValues = function (ui) {

        var minDepth = 0;
        var next = ui.placeholder.next();
        if (next[0] === ui.item[0]) {
          next = next.next();
        }
        if (next.length === 1) {
          minDepth = next.data('depth');
        }

        var previous = ui.placeholder.prev();
        var maxDepth = 0;
        if (previous[0] === ui.item[0]) {
          previous = previous.prev();
        }
        if (previous.length === 1) {
          maxDepth = previous.data('depth') + 1;
        }

        maxDepth = Math.min(maxNumberLevels-1, maxDepth);
        //console.log removed.
        return {max: maxDepth, min: minDepth};
      };

      var getDepthByDragPosition = function (ui) {
        var maxMin = getMaxMinValues(ui);
        var leftCurrent = ui.item.offset().left;
        var realDepth = Math.floor((leftCurrent - firstElementLeft + levelIndentation/2) / levelIndentation);
        var cappedDepth = Math.max(maxMin.min, Math.min(maxMin.max, realDepth));
        return cappedDepth;
      };


      var msNavigationAdminHelper = {
        initSortable:initSortable,
        updateMenu:updateMenu
      };

      return msNavigationAdminHelper;
    };
    return {
      create: function(settings){
        return factory(settings);
      }
    };
  }]);