'use strict';

angular.module('msMetaData')

  .directive('metaDataDirective', ['$compile','MetaDataManager','MetaDataRepository', 'Config','$q', function ($compile,MetaDataManager,MetaDataRepository, Config,$q) {
    return {
      scope: {
        scope: '@',
        type: '@',
        typeId: '@'
      },
      restrict: 'AE',
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msMetaData/msMetaData.tpl.html',
      link: function postLink(scope, element, attrs) {


        var save = function(){
          if(!settings.scope||!settings.type){
            return;
          }
          var properties = [];
          for (var key in scope.properties) {
            if (scope.properties.hasOwnProperty(key)) {
              var obj = scope.properties[key];

              obj.Name=key;
              properties.push(obj);
            }
          }
          MetaDataRepository.updateMetaDataProperties(settings.scope,settings.type,settings.typeId,properties).then(function(result){
          });
        };

        scope.onChange = _.debounce(save,1000);


        scope.properties=[];
        var settings = {};

        var loadMetaData = function(){
          if(!scope.scope||!scope.type||!scope.typeId){
            return;
          }
          if(settings.scope===scope.scope&&settings.type===scope.type&&settings.typeId===scope.typeId){
            return;
          }

          settings.scope=MetaDataManager.getScope(scope.scope);
          settings.type=scope.type;
          settings.typeId=scope.typeId;

          MetaDataRepository.getMetaDataByScopeTypeAndTypeId(settings.scope,settings.type,settings.typeId).then(function(result){
            angular.forEach(result.MetaProperties,function(obj){
              if(obj&&obj.Name){
                if(scope.properties[obj.Name]){
                  scope.properties[obj.Name].Value=obj.Value;
                }else{
                  scope.properties[obj.Name]={Value:obj.Value};
                }
              }
            });
          });
        };

        scope.$watch('scope',loadMetaData);
        scope.$watch('type',loadMetaData);
        scope.$watch('typeId',loadMetaData);
      }
    };
  }])

  .factory('MetaDataManager',['Config','MetaDataRepository',function(Config,MetaDataRepository){
    var getScope = function(scope){
      switch(scope){
        case 'shop':
          return 'shop-'+Config.GetSessionStoredValue('shopId');
        case 'site':
          return 'site-'+Config.GetSessionStoredValue('siteId');
        case 'blog':
          return 'blog-'+Config.GetSessionStoredValue('blogId');
      }
      return scope;
    };

    var injectMetaDataInPage = function(scope,type,typeId, defaultValue,defaultValueDescription, siteName){
      if(!typeId){
        if(defaultValueDescription){
          angular.element('head').find('meta[name=description]').attr('content',defaultValueDescription);
        }
        if(defaultValue){
          angular.element('title').html(defaultValue + ' | ' + siteName);
        }else{
          angular.element('title').html(type + ' | ' + siteName);
        }
        return;
      }
      var realScope = getScope(scope);
      MetaDataRepository.getMetaDataByScopeTypeAndTypeId(realScope,type,typeId).then(function(result){
        angular.forEach(result.MetaProperties,function(obj){
          if(obj&&obj.Name&&obj.Value){
            if(obj.Name==='PageTitle'){
              angular.element('title').html(obj.Value + ' | ' + siteName);
            }else if(obj.Name==='PageDescription'){
              angular.element('head').find('meta[name=description]').attr('content',obj.Value);
            }
          }
        });
      },function(error){
        if(typeId){
          injectMetaDataInPage(scope,type,undefined,defaultValue,defaultValueDescription, siteName);
        }
      });
    };

    return {
      getScope:getScope,
      injectMetaDataInPage:injectMetaDataInPage
    };
  }]);
