'use strict';

angular.module('msEditableItemCheckbox')
  .directive('editableItemCheckboxDirective', ['$compile', '$http', function ($compile, $http) {
    return {
      scope: {
        editable: '='
      },
      template: '<span class="label">{{editable.name}}</span><input ng-model="editable.value" type="checkbox" />',
      restrict: 'A',
      link: function (scope, element, attrs) {
        //Initialize editable
        if(scope.editable.value==='true' || scope.editable.value==='True' || scope.editable.value==='yes' || scope.editable.value===true){
          scope.editable.value = true;
        }else{
          scope.editable.value = false;
        }
      }
    };
  }]);