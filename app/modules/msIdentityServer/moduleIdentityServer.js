'use strict';

angular.module('msIdentityServer').factory('IdentityServer', ['$window', '$location', 'OAuthClient', 'Config', function ($window, $location, oAuthclient, Config) {

    var getClaimsFromToken = function (idToken) {
      var jws = new KJUR.jws.JWS('');
      jws.parseJWS(idToken);
      var json = JSON.parse(jws.parsedJWS.payloadS);
      var claims = {};
      if (json.site) {
        claims.site = json.site;
      }
      if (json.shop) {
        claims.shop = json.shop;
      }
      return claims;
    };

    var getURL = function (client, cssUrl, username, password) {
      var scope = 'openid read write shop';
      var responseType = 'id_token token';
      oAuthclient.setUrl(Config.GetEndPoint('idServerEndpoint') + '/core/connect/authorize');
      var req = oAuthclient.createImplicitFlowRequest(
        client,
        'default',
        $location.protocol() + '://' + $location.host() + ':' + $location.port(),
        scope,
        responseType);
      var url = req.url;
      if (cssUrl) {
        url += '&css=' + encodeURIComponent(cssUrl);
      }
      if (username && password) {
        url += '&username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password);
      }
      return url;
    };

    var redirectToLoginServerWithoutIFrame = function () {
      $window.location.href = getURL('implicitclient');
    };

    var closeWindow = function () {
      $('#diviframeLogin').remove();
      $(window).off('message.idServer');
    };

    var popupIframe = function (url, hidden, callback, callbackError) {
      var iframe = '<iframe id="iframeLogin" style="display:none;position: absolute;width: 30%;height: 90%;top: 5%;left: 35%;" src="' + url + '" />';
      var $iframe = $('<div id="diviframeLogin">' + iframe + '</div>');

      $('body').append($iframe);

      $(window).on('message.idServer', function (e) {
        //console.log removed.
        var data = e.originalEvent.data;
        if (data.type === 'loginsuccess') {
          var tokens = {scope: e.originalEvent.data.scope, accessToken: e.originalEvent.data.accessToken, idToken: e.originalEvent.data.identityToken};
          validateTokens(tokens, callback, function () {
            //console.log removed.
          });
        }
        if (data.type === 'cancel') {
          closeWindow();
        }
        if (data.type === 'loginerror') {
          callbackError(data);
        }
      });

      $('#iframeLogin').load(function () {
        if (!hidden) {
          $('#iframeLogin').fadeIn('fast');
        }
      });


    };

    var redirectToLoginServer = function (cssUrl, callback, callbackError) {
      var url = getURL('implicittrustedclient', cssUrl);
      popupIframe(url, false, callback, callbackError);
    };

    var loginServer = function (callbackSuccess, callbackError, username, password) {
      var url = getURL('implicittrustedclient', undefined, username, password);
      popupIframe(url, true, callbackSuccess, function (data) {
        closeWindow();
        callbackError(data);
      });
    };

    var redirectToLogout = function (callback) {
      var iframe = '<iframe id="iframeLogout" style="" src="' + Config.GetEndPoint('idServerEndpoint') + '/core/logout" />';
      var $iframe = $('<div id="diviframeLogin">' + iframe + '</div>');
      $('body').append($iframe);

      $('#iframeLogout').load(function () {
        $('#diviframeLogin').remove();
        if (callback) {
          callback();
        }
      });
    };

    var redirectToLoginServerTrusted = function () {
      $window.location.href = getURL('implicittrustedclient');
    };

    var getTokensFromURI = function () {
      var hash = $location.path().substring(1);
      return oAuthclient.parseResult(hash);
    };

    var validateTokens = function (data, callback, callbackError) {
      if (data.scope.indexOf('read') >= 0 && data.scope.indexOf('write') >= 0) {
        var claims = getClaimsFromToken(data.idToken);
        callback(data.accessToken, data.idToken, claims);
      } else {
        callbackError();
      }
    };

    var ValidateTokens = function (callback, callbackError) {
      var response = getTokensFromURI();
      if (response && response.scope) {
        var idTokenArray = 'id_token';
        var accessTokenArray = 'access_token';
        validateTokens({scope: response.scope, accessToken: response[accessTokenArray], idToken: response[idTokenArray]}, callback, callbackError);
      }
    };

    var redirectToIdentityServer = function (client) {
      $window.location.href = getURL2(client);
    };

    var getURL2 = function (client) {
      var scope = 'openid profile read write email mr_site';
      var responseType = 'id_token token';
      oAuthclient.setUrl(Config.GetEndPoint('idServerEndpoint') + '/core/connect/switch');
      var req = oAuthclient.createImplicitFlowRequest(
        client,
        'default',
        $location.protocol() + '://' + $location.host() + ':' + $location.port() + '/#/authorize',
        scope,
        responseType);

      var url = req.url;
      return url;
    };

    var identityServer = {
      redirectToIdentityServer: redirectToIdentityServer,
      getClaimsFromToken: getClaimsFromToken,
      redirectToLoginServerWithoutIFrame: redirectToLoginServerWithoutIFrame,
      closeWindow: closeWindow,
      redirectToLoginServer: redirectToLoginServer,
      loginServer: loginServer,
      redirectToLogout: redirectToLogout,
      redirectToLoginServerTrusted: redirectToLoginServerTrusted,
      ValidateTokens: ValidateTokens
    };
    return identityServer;
  }]
);
