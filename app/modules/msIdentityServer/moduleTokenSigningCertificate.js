'use strict';

angular.module('msIdentityServer').factory('TokenSigningCertificate', ['$http', 'Config', function ($http, Config) {

  var url = Config.GetEndPoint('idServerEndpoint') + '/core/.well-known/jwks';

  return $http.get(url).then(function (response) {
    //console.log removed.
    return response.data.keys[0].x5c[0];
  });

}]);
