'use strict';

angular.module('msIdentityServer').factory('ImplicitFlow', ['IdServerRestangular','QueryStringParametersManager','$q', function (IdServerRestangular, QueryStringParametersManager,$q) {

    var extractAccessTokenAndSiteId = function () {
      var deferred = $q.defer();
      var params = QueryStringParametersManager.getParametersOnHash();

      if (!params) {
        return;
      }
      var accessToken = params.access_token;  // jshint ignore:line
      if (!accessToken) {
        deferred.resolve({success:false});
        return deferred.promise;
      }

      var idToken = params.id_token;  // jshint ignore:line
      if (!idToken) {
        deferred.resolve({success:false});
        return deferred.promise;
      }


      IdServerRestangular.setDefaultHeaders({ 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + accessToken});
      var request = IdServerRestangular.one('core/connect/userinfo').get();

      request.then(function (result) {
        //console.log removed.
        var siteId;
        if (Array.isArray(result.data.siteid)) {

        } else {
          siteId = result.data.siteid;
        }

        deferred.resolve({success:true, siteId:siteId, accessToken:accessToken, idToken:idToken, sub:result.data.sub});
        //console.log removed.
      });

      return deferred.promise;
    };

    var implicitFlow = {
      extractAccessTokenAndSiteId: extractAccessTokenAndSiteId
    };
    return implicitFlow;
  }]
);

angular.module('msIdentityServer')
  .factory('QueryStringParametersManager', [function () {

    var getParametersOnHash = function () {
      var hash = location.hash;
      var indexLastHash = hash.lastIndexOf('#');
      if (indexLastHash === hash.indexOf('#')) {
        return;
      }
      var rawParams = hash.substring(indexLastHash + 1);
      var rawParamsArray = rawParams.split('&');
      var result = {};
      for (var i = 0; rawParamsArray[i]; i++) {
        var rawParam = rawParamsArray[i];
        var rawParamArray = rawParam.split('=');
        result[rawParamArray[0]] = rawParamArray[1];
      }
      return result;
    };

    var setsRepository = {
      getParametersOnHash: getParametersOnHash
    };

    return setsRepository;

  }]);
