'use strict';

angular.module('msIdentityServer').factory('OAuthClient', [function () {

    var setUrl = function (url) {
      oAuthClient.url = url;
    };

    /*
     var generateAndPersistRandomNumber = function (key) {
     var number = (Date.now() + Math.random()) * Math.random();
     number = number.toString().replace('.', '');
     Storage.
     //console.log removed.
     //console.log removed.
     store.set(number);
     return number;
     };
     */

    var createImplicitFlowRequest = function (clientid, tenant, callback, scope, responseType) {
      responseType = responseType || 'token';

      /*
       var state = generateAndPersistRandomNumber('state');
       var nonce = generateAndPersistRandomNumber('nonce');
       */


      var state = (Date.now() + Math.random()) * Math.random();
      state = state.toString().replace('.', '');
      //Storage.$default({state: state});

      var nonce = (Date.now() + Math.random()) * Math.random();
      nonce = nonce.toString().replace('.', '');
      //Storage.$default({nonce: nonce});


      var url =
        this.url + '?' +
        'client_id=' + encodeURIComponent(clientid) + '&' +
        'tenant=' + encodeURIComponent(tenant) + '&' +
        'redirect_uri=' + encodeURIComponent(callback) + '&' +
        'response_type=' + encodeURIComponent(responseType) + '&' +
        'scope=' + encodeURIComponent(scope) + '&' +
        'state=' + encodeURIComponent(state) + '&' +
        'nonce=' + encodeURIComponent(nonce);

      return {
        url: url,
        state: state,
        nonce: nonce
      };
    };

    var parseResult = function (queryString) {
      var params = {},
      //queryString = location.hash.substring(1),
        regex = /([^&=]+)=([^&]*)/g,
        m;

      while (!!(m = regex.exec(queryString))) {
        params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
      }

      return params;

    };

    var oAuthClient = {
      setUrl: setUrl,
      createImplicitFlowRequest: createImplicitFlowRequest,
      parseResult: parseResult
    };

    return oAuthClient;
  }]
);