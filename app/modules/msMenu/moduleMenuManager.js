'use strict';

angular.module('msMenu')
  .factory('MenuManager', ['$q', 'Helper', '$rootScope', 'MenuRepository', function ($q, Helper, $rootScope, MenuRepository) {


    var recursiveDelete = function( menu, page ){
      angular.forEach( menu, function( v, i ) {
        if(page.type === 'Page' || page.type === 'default' || page.type === 'custom') {
          if (v.pageId === page.id) {
            menu.splice(i, 1);
          }
          if(v.subMenuItems){
            recursiveDelete( v.subMenuItems, page );
          }
        }
      });
    };

    var addToMenu = function(page){
      MenuRepository.getMenu().then(function (menu) {
        menu.push({ 'type': 'Page', 'pageId': page.id, 'subMenuItems': [], 'hideOnMenu': false });

        MenuRepository.saveMenu(menu).then( function(){
          $rootScope.$broadcast('menuChanged');
        });
      });

    };

    var removeFromMenu = function(page){
      MenuRepository.getMenu().then(function (menu) {
        recursiveDelete( menu, page );
        MenuRepository.saveMenu(menu).then( function(){
          $rootScope.$broadcast('menuChanged');
        });
      });
    };

    return {
      addToMenu: addToMenu,
      removeFromMenu: removeFromMenu
    };
  }]);
