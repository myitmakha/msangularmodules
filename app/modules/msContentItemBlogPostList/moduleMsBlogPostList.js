'use strict';

angular.module('msContentItemBlogPostList')
  .directive('msContentItemBlogPostList', ['$compile','BlogRepository','Config', 'DiscussionRepository', function ($compile,BlogRepository,Config, DiscussionRepository) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '=',
        contentItemMetaData: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemBlogPostList/msContentItemBlogPostList.tpl.html',

      link: function (scope, element) {

        scope.paging = {};

        scope.selectBlogPost = function(blogPost){
          //console.log removed.
          scope.contentItemMetaData.setUrlParamsByPageType('blog','post',(blogPost||{}).PostId);
        };

        scope.svgUrl = function( link ){
          return Config.GetEndPoint('modulesDirectory') + '/msContentItemBlog/sprites.svg#'+link;
        };

        scope.customSearch = function(blogPost){
          if(blogPost){
            if(!scope.searchText||scope.searchText===''){
              return true;
            }
            var searchText = scope.searchText.toLowerCase();
            var searchRegex = new RegExp(searchText, 'i');
            if(blogPost.Title&&blogPost.Title.search(searchRegex)>=0){
              return true;
            }
            if(blogPost.Content&&blogPost.Content.search(searchRegex)>=0){
              return true;
            }
            if(blogPost.Author&&blogPost.Author.search(searchRegex)>=0){
              return true;
            }
          }
          return false;
        };

        scope.paging={};

        var loadBlogPosts = function(){
          scope.contentItemMetaData.loading=true;
          BlogRepository.getAllPosts().then(function (blogPosts) {
            scope.contentItemMetaData.loading=false;

            scope.blogPosts=[];
            angular.forEach(blogPosts, function (blogPost) {
              blogPost.pendingCommentsCount = 0;
              DiscussionRepository.getAllComments(blogPost.DiscussionId).then(
                function (comments) {
                  blogPost.totalComments = comments.length;
                }
              );
              scope.blogPosts.push(blogPost);
            });

          });
        };
        loadBlogPosts();
      }
    };
  }]);
