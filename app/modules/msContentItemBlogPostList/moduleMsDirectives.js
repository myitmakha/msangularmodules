'use strict';

angular.module('msContentItemBlogPostList')
  .directive('msBlogPostSearch', ['Config', function (Config) {
    return {
      restrict: 'AE',
      replace: true,
      scope: {
        searchText: '=',
        category: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemBlogPostList/msBlogPostSearch.tpl.html',
      link: function (scope, element, attrs) {

        scope.svgUrl = function( link ){
          return Config.GetEndPoint('modulesDirectory') + '/msContentItemBlog/sprites.svg#'+link;
        };

      }
    };
  }]);

