'use strict';

angular.module('msAuthentication').factory('$auth', ['SiteRestangular', 'ShopRestangular', 'BlogRestangular','DiscussionRestangular', '$window', 'Config', '$location', 'IdentityServer','$authSiteExtension','$rootScope','$q','IdServerRestangular', function (SiteRestangular, ShopRestangular, BlogRestangular,DiscussionRestangular, $window, Config, $location, IdentityServer,$authSiteExtension,$rootScope, $q,IdServerRestangular) {

    var isCurrentlyLoggedIn = false;
    var currentToken;

    var isLoggedIn = function () {
      if ($window.sessionStorage.token === undefined || $window.sessionStorage.token.trim() === '') {
        //$location.path('/login');
        if (isCurrentlyLoggedIn) {
          isCurrentlyLoggedIn=false;
          logout();
          authChange();
        }
      }
      else {
        if (!isCurrentlyLoggedIn || currentToken!==$window.sessionStorage.token) {
          currentToken=$window.sessionStorage.token;
          isCurrentlyLoggedIn=true;
          authChange();
        }
      }
      return isCurrentlyLoggedIn;
    };

    var getUserInfo = function(accessToken){
      if(!accessToken){
        $rootScope.userInfo = {};
        return;
      }
      IdServerRestangular.setDefaultHeaders({ 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + accessToken});
      var request = IdServerRestangular.one('core/connect/userinfo').get();

      request.then(function (result) {
        var siteId;
        if (Array.isArray(result.data.siteid)) {

        } else {
          siteId = result.data.siteid;
        }
        $rootScope.userInfo = {
          siteId:siteId,
          email:result.data.email
        };
      });
    };

    var redirectIdServer = function (data) {
      IdentityServer.redirectToLoginServer(data, loginSuccess, loginError);
    };

    var redirectToIdServer = function (data) {
        data=data||{};
        var clientId = data.clientId;
        var tenant = data.tenant;
        var provider = data.provider;
        var scopes = data.scopes||'read+write+openid+mrsite+email';
        var redirectUri = data.redirectUri;

        if(redirectUri && redirectUri.indexOf('http')!==0){
          var host = $location.protocol() + '://'+ $location.host();
          if($location.port() && $location.port()!==80){
            host += ':'+  $location.port();
          }
          redirectUri=host+redirectUri;
        }

        var url =  Config.GetEndPoint('idServerEndpoint') + '/core/connect/switch?client_id='+clientId+'&scope='+scopes+'&response_type=id_token+token&nonce=123213123&redirect_uri='+redirectUri;
        var acrValues='';
        if(tenant){
          acrValues+='tenant:'+tenant+'+';
        }
        if(provider){
          acrValues+='idp:'+provider+'+';
        }
        if(acrValues!==''){
          url += '&acr_values='+acrValues;
        }
        $window.location.href = url;
      };

    var redirectToIdServerFromClientSite = function(provider){
      
      var data = {
        clientId:'mrsite-'+$window.sessionStorage.siteId,
        tenant:'mrsite-'+$window.sessionStorage.siteId,
        redirectUri:'/%23!%2FimplicitCallback%23',
        provider:provider
      };
      redirectToIdServer(data);
    };

    var redirectToAccountCentre = function (accountId, isBuyNow) {
      var path = Config.GetEndPoint('accountCentreEndpoint') + 'NewAccount/ManagePackages.aspx';
      if(isBuyNow === true) {
        path = Config.GetEndPoint('accountCentreEndpoint') + 'NewAccount/AddNewPackage.aspx?Type=Upgrade&AccountId=' + accountId;
      }
      $window.location.href = path;
    };

    var redirectTo = 'dash';

    var setRedirectTo = function (location) {
      redirectTo = location;
    };

    var login = function (userName, password) {
      IdentityServer.loginServer(loginSuccess, loginError, userName, password);
    };

    var logout = function () {
      //$window.sessionStorage.siteId = '';
      //$window.sessionStorage.shopId = '';
      $window.sessionStorage.token = '';
      $window.sessionStorage.idToken = '';

      //SiteRestangular.setDefaultHeaders({});
      //ShopRestangular.setDefaultHeaders({});
      //IdentityServer.closeWindow();
      authChange();
    };

    var loginSuccess = function (accessToken, idToken, claims) {
      IdentityServer.closeWindow();
      setClaim('token',accessToken);
      setClaim('idToken',idToken);
      setClaim('siteId',claims.site);
      setClaim('sub',claims.sub);

      authChange().then(function(){
        $location.url('/' + redirectTo);
      });
    };

    var setClaim = function (claimName, claimValue) {
      if(!claimName){
        return;
      }
      if(!claimValue){
        delete $window.sessionStorage[claimName];
      }else{
        $window.sessionStorage[claimName] = claimValue;
      }
      //var data={};
      //data[claimName]=claimValue;
      //$rootScope.$broadcast(claimName+'Change',data);
    };

    var authChange = function(){
      var defered = $q.defer();
      $rootScope.$broadcast('auth:tokenChange',{
        accessToken:$window.sessionStorage.token,
        idToken:$window.sessionStorage.idToken,
        headers:getHeaders()
      });
      getUserInfo($window.sessionStorage.token);

      $rootScope.$broadcast('auth:siteIdChange',{siteId:$window.sessionStorage.siteId});

      $authSiteExtension.extractExtensionsFromSite().then(function(extensions){
        setClaim('shopId',extensions.shopid);
        setClaim('blogId',extensions.blogid);
        $rootScope.$broadcast('auth:shopIdChange',{shopId:$window.sessionStorage.shopId});
        $rootScope.$broadcast('auth:siteIdChange',{siteId:$window.sessionStorage.siteId});
        $rootScope.$broadcast('auth:blogIdChange',{blogId:$window.sessionStorage.blogId});
        defered.resolve();
      });

      return defered.promise;
    };

    var loginError = function (data) {
    };


    var getHeaders = function () {
      var hearders = {};
      hearders['Content-Type'] = 'application/json; charset=utf-8';
      if($window.sessionStorage.token){
        hearders.Authorization = 'Bearer ' + $window.sessionStorage.token;
      }
      if($window.sessionStorage.idToken){
        hearders['Authorization-Id'] = 'Bearer ' + $window.sessionStorage.idToken;
      }
      return hearders;
    };

    var setTokensAndSiteId = function (accessToken, idToken, siteId, sub) {
      loginSuccess(accessToken, idToken, {site: siteId, sub: sub});
    };

    var authentication = {
      setRedirectTo: setRedirectTo,
      isLoggedIn: isLoggedIn,
      login: login,
      logout: logout,
      setTokensAndSiteId: setTokensAndSiteId,
      redirectToAccountCentre: redirectToAccountCentre,
      redirectToIdServerFromClientSite:redirectToIdServerFromClientSite,
      redirectToIdServer:redirectToIdServer
    };

    return authentication;
  }]
)

.factory('$authSiteExtension', ['$q','SiteRepository', function ($q,SiteRepository) {

    var extractExtensionsFromSite = function(siteId){
      var defered = $q.defer();
      SiteRepository.getSite(siteId).then(function(site){
        defered.resolve(site.extensions);
      });
      return defered.promise;
    };

    var authSiteExtension = {
      extractExtensionsFromSite: extractExtensionsFromSite
    };

    return authSiteExtension;
  }]
);