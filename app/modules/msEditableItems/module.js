/**
 * Created by Albert on 15/07/2014.
 */

'use strict';
angular.module('msEditableItems', ['msEditableItemColor', 'msEditableItemFontFamily', 'msEditableItemSlider', 'msEditableItemImage', 'msEditableItemDropdownTakeaway', 'msEditableItemDropdown', 'msEditableItemCheckbox']);