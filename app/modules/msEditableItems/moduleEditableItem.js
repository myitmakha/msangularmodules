'use strict';

angular.module('msEditableItems')
  .directive('editableItemDirective', ['$compile', function ($compile) {
    return {
      scope: {
        editable: '='
      },
      restrict: 'A',
      link: function (scope, element, attrs) {
        var doesThisClassExistsOnPage = function(selector){
          if(!selector){
            return true;
          }
          return $(selector).length>0;
        };
        var renderContentItem = function () {
          if(scope.editable.Settings && !doesThisClassExistsOnPage(scope.editable.Settings.showWhenExists)){
            scope.editable.hidden = true;
            return;
          }
          scope.editable.hidden = false;
          var msDirectiveName = 'editable-item-' + scope.editable.type + '-directive';
          var html = '<div ' + msDirectiveName + ' editable="editable" class="editable editable-{{editable.type}}">The editable {{editable.type}} is not defined</div>';
          var ele = angular.element(html);
          $compile(ele)(scope, function (cloned) {
            element.html(cloned);
          });
        };
        renderContentItem();
      }
    };
  }]);