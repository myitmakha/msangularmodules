'use strict';

angular.module('msContentItemSlideShow')
  .directive('msContentItemSlideShowEdit', ['$compile', '$timeout', 'Config', 'MediaLibraryManager', function ($compile, $timeout, Config, MediaLibraryManager) {
    return {
      restrict: 'A,E',
      replace: true,
      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {

        scope.editableId = MediaLibraryManager.randomString(); // This is use by the media library manager for unique hook to each ms-upload directive.

        scope.openSettings = scope.$parent.actions.displaySettings;
        scope.contentItemData.settings.images = scope.contentItemData.settings.images || [];

        var compileToView = function () {
          var slideShow = {'id': 'slideshow', 'name': 'Slideshow'};
          scope.contentItemData.settings.selectedType = scope.contentItemData.settings.selectedType || slideShow;
          scope.contentItemData.settings.height = scope.contentItemData.settings.height || 300;
          scope.contentItemData.settings.interval = scope.contentItemData.settings.interval || 5000;
          scope.contentItemData.settings.itemsPerRow = scope.contentItemData.settings.itemsPerRow || 3;
          scope.contentItemData.settings.showCaptionsInGrid = scope.contentItemData.settings.showCaptionsInGrid || false;
          scope.contentItemData.settings.cropImages = scope.contentItemData.settings.cropImages || false;


          var slides = scope.slides = [];

          scope.addSlide = function (image) {
            slides.push({
              image: image.url,
              caption: image.caption
            });
          };

          scope.getGalleryTemplate = function () {
            if (scope.contentItemData.settings.images.length > 0) {
              return Config.GetEndPoint('modulesDirectory') + '/msContentItemSlideShow/gallery.' + scope.contentItemData.settings.selectedType.id + '.tpl.html';
            } else {
              return Config.GetEndPoint('modulesDirectory') + '/msContentItemSlideShow/gallery.noimages.tpl.html';
            }
          };

          if (scope.contentItemData.settings.selectedType.id === 'slideshow') {

            var showControls = function () {
              $timeout(
                function () {
                  $('.carousel-control, .carousel-indicators').hide();
                }
              );
            };

            if (!scope.contentItemData.settings.showControls) {
              showControls();
            }

          } else if (scope.contentItemData.settings.selectedType.id === 'grid') {
            scope.itemsPerRowClass = 'items-per-row-' + scope.contentItemData.settings.itemsPerRow;
          }


          if (scope.contentItemData.settings.images.length > 0) {
            scope.contentItemData.settings.images.forEach(function (image) {
              scope.addSlide(image);
            });
          }
          else {
            $timeout(function () {
              if (!scope.contentItemData.settings.opened) {
                angular.element('#open-settings').trigger('click');
                scope.contentItemData.settings.opened = true;
              }
            }, 1);
          }
        };

        scope.$watch('contentItemData.settings', function () {
          compileToView();
        }, true);

      },
      template: '<div ng-include="getGalleryTemplate()"></div>'
    };
  }]);
