'use strict';

angular.module('msContentItemSlideShow')
  .directive('msImageSettings', ['$compile', '$sce',  function ($compile, $sce) {
    return {
      restrict: 'A,E',
      replace: true,
      scope: {
        settings: '='
      },
      link: function (scope, element) {
        scope.backupSettings = angular.copy(scope.settings);
        scope.settings.caption = scope.settings.caption || '';

        var topBar = element.parent();
        var sideBar = element.find('.image');
        var settingsPanel = $('body').find('.imageSettingsPlaceHolder');
        var settingsOverlay = $('body').find('.settingsOverlay');

        var closePopUp = function() {
          settingsPanel.removeClass('active image-settings');
          settingsOverlay.removeClass('active').removeAttr('style');
          element.find('.ms-block').removeClass('active');
          settingsPanel.removeAttr('style');
          $('body').removeClass('ov-hidden');
          $('.image-settings','.image-wrap').remove();
          $('.images, .upload-box').show();
        };

        scope.ok = function () {
          settingsPanel.removeClass('active image-settings');
          closePopUp();
        };

        settingsOverlay.on('click', function () {
          scope.ok();
        });

        scope.displaySettings = function (){

          var actionButtons = '<div class="buttons">' +
            '<div class="ms-buttons">' +
            '<button ng-click="ok()" class="ms-button info">Done</button>' +
            '</div>' +
            '</div>';

          var htmlEle = '<div class="image-settings">' +
            '<h2 class="title">Image settings</h2>' +
            '<div class="wrapper">' +
            '<div class="settings-mask">' +
            '<div class="row-fix"><div class="col-fix13"><label>Caption</label><input type="text" value="settings.caption" ng-model="settings.caption" /></div></div>' +
            '</div>' +
            '</div>' + actionButtons +
            '</div>' +
            '</div>';

          var ele = angular.element(htmlEle);

          $('body').find('.ms-block').removeClass('active');

          $compile(ele)(scope, function (cloned) {

            $('.image-wrap.slide-show').append(cloned);
            $('body').addClass('ov-hidden');
            $('.images, .upload-box').hide();

          });

        };

        var compileToView = function(){
          var elementControls = angular.element('<span ng-click="displaySettings()" class="settings icon-settings control"></span>');
          elementControls.attr('ms-tooltip', '');
          elementControls.attr('tooltip-content', 'Settings');

          $compile(elementControls)(scope, function (html) {
            element.append(html);
          });
        };

        scope.$watch('settings', function(){
          compileToView();
        });
      }
    };
  }]);
