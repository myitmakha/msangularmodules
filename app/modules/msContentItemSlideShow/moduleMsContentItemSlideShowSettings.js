'use strict';

angular.module('msContentItemSlideShow')
  .directive('msContentItemSlideShowSettings', ['$compile', '$sce', 'MediaLibraryManager', 'Config', '$rootScope', '$window', '$timeout', function ($compile, $sce, MediaLibraryManager, Config, $rootScope, $window, $timeout) {
    return {
      restrict: 'A,E',
      replace: true,
      scope: {
        settings: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemSlideShow/slideShowSettings.html',

      link: function (scope, element) {

        scope.settings.selectedType = scope.settings.selectedType || scope.galleryTypes[0];

        scope.settings.images = scope.settings.images || [];

        scope.settings.showCaptionsInGrid = scope.settings.showCaptionsInGrid || false;

        scope.settings.cropImages = scope.settings.cropImages || false;

        scope.images = [];

        scope.deleteImage = function (idx) {
          scope.settings.images.splice(idx, 1);
        };

        scope.galleryTypes = [
          {id: 'slideshow', 'name': 'Slideshow'},
          {id: 'grid', 'name': 'Grid'}
        ];

        scope.editableId = MediaLibraryManager.randomString(); // This is use by the media library manager for unique hook to each ms-upload directive.

        var reorderImages = function (newOrderList) {
          var newImagesArray = [];
          angular.forEach(newOrderList, function (newOrder) {
            angular.forEach(scope.settings.images, function (image) {
              if (image.$$hashKey === newOrder) {
                newImagesArray.push(image);
              }
            });
          });

          scope.settings.images = [];
          scope.settings.images = newImagesArray;
        };

        $('.image-wrap', element).sortable({
          items: '.image',
          tolerance: 'pointer',
          distance: 1,
          stop: function (event, ui) {
            var newOrder = $(this).sortable('toArray', 'id');
            reorderImages(newOrder);
          }
        });

        $('#height', element).slider({
          min: 175,
          max: 900,
          range: 'min',
          value: scope.settings.height,
          slide: function (event, ui) {
            scope.settings.height = ui.value;
            scope.$apply();
          }
        });

        $('#itemsPerRow', element).slider({
          min: 1,
          max: 10,
          range: 'min',
          value: scope.settings.itemsPerRow,
          slide: function (event, ui) {
            scope.settings.itemsPerRow = ui.value;
            scope.$apply();
          }
        });

        $('#interval', element).slider({
          min: 10,
          max: 10000,
          step: 10,
          range: 'min',
          value: scope.settings.interval,
          slide: function (event, ui) {
            scope.settings.interval = ui.value;
            scope.$apply();
          }
        });

        scope.$watch('images', function () {
          if (scope.images.length > 0) {
            scope.settings.images.push({url: scope.images[0].url, caption: scope.images[0].caption});
          }
        });

        scope.$watch('settings.selectedType', function (newValue, oldValue) {
          scope.settings.selectedType = newValue;
        });

        scope.$watch('settings', function (oldValue, newValue) {
          if (oldValue !== newValue) {
            $rootScope.$broadcast('markChanges', 'slide show image settings has been changed.');
          }
        }, true);
      }
    };
  }]);
