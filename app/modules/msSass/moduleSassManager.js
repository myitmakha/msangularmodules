'use strict';

angular.module('msSass')
  .factory('SassManager', ['$q', function ($q) {
    var editablesToSettings = function (editables) {
      var deferred = $q.defer();
      var editablesSass = '';
      for (var i = 0; i < editables.length; i++) {
        var editable = editables[i];
        //if (editable.type === 'image') {
        //  editablesSass += '$' + editable.variable + ':\'' + editable.value + '\';';
        //} else {
        editablesSass += '$' + editable.variable + ':' + editable.value + ';';
        //}
      }
      deferred.resolve(editablesSass);
      return deferred.promise;
    };

    var compile2Css = function (sass) {
      var deferred = $q.defer();
      var css = Sass.compile(sass);
      deferred.resolve(css);
      return deferred.promise;
    };

    var storedScss;
    var storeScss = function(scss){
      //storedScss = {scss:'@mixin clearfix {}@mixin resetMargin {}@mixin resetPadding {}@function getValueMinMax($value, $min, $max) {@return (($value*($max - $min))/100)+$min;}@mixin transition($transition-property, $transition-time, $method) {-webkit-transition: $transition-property $transition-time $method;-moz-transition: $transition-property $transition-time $method;-ms-transition: $transition-property $transition-time $method;-o-transition: $transition-property $transition-time $method;transition: $transition-property $transition-time $method;}@mixin background-opacity($color, $opacity: 0.3) {background: $color;background: rgba($color, $opacity);}@function strip-unit($num) {@return $num / ($num * 0 + 1);}@mixin rem-fallback($property, $values...) {$max: length($values);$pxValues: "";$remValues: "";@for $i from 1 through $max {$value: strip-unit(nth($values, $i));$pxValues: #{$pxValues + $value*16}px;@if $i < $max {$pxValues: #{$pxValues + " "};}}@for $i from 1 through $max {$value: strip-unit(nth($values, $i));$remValues: #{$remValues + $value}rem;@if $i < $max {$remValues: #{$remValues + " "};}}#{$property}: $pxValues;#{$property}: $remValues;}$cols: 12;$row-padding: 1em 0;$block-padding: 1em;.ms-grid{> .ms-row{margin-left: -$block-padding;margin-right: -$block-padding;}[class*=ms-col] {}[class*=ms-col] .ms-block {padding-top: $block-padding;padding-bottom: $block-padding;padding-left: $block-padding;padding-right: $block-padding;}/*NOCOMPILE*/@mixin column($x, $cols:$cols) {width: (100% / $cols) * $x;@for $y from 1 through $x {.ms-col-#{$y} {width: (100% / $x) * $y;}}}@for $i from 1 through $cols {.ms-col-#{$i} {@include column($i);}}/*ENDNOCOMPILE*/}#ms-body{font-family: $main-font , Helvetica, Arial, sans-serif;color: $main-font-color;.intro{header{.inner{h1{width: getValueMinMax($logo-size, 50, 800)+px;}.navigation-wrapper{@include background-opacity( $navigation-background-color, getValueMinMax($navigation-background-opacity, 0, 1) );nav{.mobile-menu{svg{fill: $navigation-color;}span{color: $navigation-color;}}.mobile-menu-close{svg{fill: $navigation-color;}span{color: $navigation-color;}}> ul{> li{&.close-menu{color: $navigation-color;font-family: $navigation-font, $main-font;}a{color: $navigation-color;font-family: $navigation-font, $main-font;@include rem-fallback( font-size, getValueMinMax( $navigation-size, 0.5, 1.5) );}ul.subnav{li{a{color: $navigation-color;font-family: $navigation-font, $main-font;&:hover{color: $navigation-highlight-color;}}}}&.active,&:hover{a{color: $navigation-highlight-color;}}}}}}}.slogan{@include rem-fallback( font-size, getValueMinMax($slogan-font-size, 1, 5) );h2{font-family: $slogan-sub-font;}h1,h3,h4,h5,h6,p,pre,address,div{color: $slogan-font-color;font-family: $slogan-font, $main-font;}}}.banner{background-color: $banner-background-color;background-image: url( $banner-image );}}.content{background: $page-background;.inner{background: $content-background;}.ms-block{&.text-block{h1,h2,h3,h4,h5,h6{color: $heading-font-color;font-family: $heading-font, $main-font;@include rem-fallback( font-size, getValueMinMax( $heading-font-size, 1.5, 5 ) );}p{line-height: getValueMinMax($font-line-height, 1.4, 3)+em;@include rem-fallback( font-size, getValueMinMax($main-font-size, 1, 3) );}}a{color: $link-color;&:hover{color: $link-highlight-color;}}&.shop-category-block{h1{color:$heading-font-color;}h3{color:$heading-font-color;}input,textarea{border: 1px solid $link-color;color: $link-color;outline-color: $link-color;}.button{color: $link-color;border: 1px solid $link-color;&.addToCart{input,textarea{border: 1px solid $link-color;color: $link-color;outline-color: $link-highlight-color;}button{color: $link-color;border: 1px solid $link-color;&:hover{background-color: $link-highlight-color;border: 1px solid $link-highlight-color;}}&:hover{border: 0px solid $link-highlight-color;}}&:hover{background-color: $link-highlight-color;border: 1px solid $link-highlight-color;}}aside{> div .title{svg{color: $heading-font-color;}}.mini-cart{.button{border: 1px solid $link-color;background-color: $link-color;&:hover{background-color: $link-highlight-color;border: 1px solid $link-highlight-color;}}}.categories{div{&:hover{color: $link-highlight-color;}}}}.shop-content{.category{li{.image{height: getValueMinMax($product-size, 100, 300)+px;}}}}}&.contact-form-block{.form-item{label{color: $form-color;}input,textarea{background: $form-input-color;border: 1px solid $form-input-color;color: darken( $form-input-color, 20%);&::-webkit-input-placeholder {@if (lightness($form-input-color) > 50) {color: darken( $form-input-color, 30% );} @else {color: lighten( $form-input-color, 30% );}}&:-moz-placeholder {@if (lightness($form-input-color) > 50) {color: darken( $form-input-color, 30% );} @else {color: lighten( $form-input-color, 30% );}}&::-moz-placeholder {@if (lightness($form-input-color) > 50) {color: darken( $form-input-color, 30% );} @else {color: lighten( $form-input-color, 30% );}}&:-ms-input-placeholder {@if (lightness($form-input-color) > 50) {color: darken( $form-input-color, 30% );} @else {color: lighten( $form-input-color, 30% );}}&:focus{border: 1px solid $form-color;}}button{background: $form-color;color: lighten($form-color, 40%);&:hover{background: darken($form-color, 10%);}}}}&.social-link-block{ul{li{&.facebook{background-image: url( $theme_url+"assets/img/facebook-icon.png");}&.twitter{background-image: url( $theme_url+"assets/img/twitter-icon.png");}&.pinterest{background-image: url( $theme_url+"assets/img/pinterest-icon.png");}&.googleplus{background-image: url( $theme_url+"assets/img/googleplus-icon.png");}&.instagram{background-image: url( $theme_url+"assets/img/instagram-icon.png");}}}}}}footer{background-color: $footer-background-color;.info{.copyright{color: $copyright-color;a{color: $copyright-color;}}}nav{> ul{> li{a{color: $footer-navigation-color;font-family: $navigation-font, $main-font;}}}}}}'};
      //storedScss = {scss:'@mixin clearfix {}@mixin resetMargin {}@mixin resetPadding {}@function getValueMinMax($value, $min, $max) {@return (($value*($max - $min))/100)+$min;}@mixin transition($transition-property, $transition-time, $method) {}@mixin background-opacity($color, $opacity: 0.3) {}@function strip-unit($num) {@return $num / ($num * 0 + 1);}@mixin rem-fallback($property, $values...) {$max: length($values);$pxValues: "";$remValues: "";@for $i from 1 through $max {$value: strip-unit(nth($values, $i));$pxValues: #{$pxValues + $value*16}px;@if $i < $max {$pxValues: #{$pxValues + " "};}}@for $i from 1 through $max {$value: strip-unit(nth($values, $i));$remValues: #{$remValues + $value}rem;@if $i < $max {$remValues: #{$remValues + " "};}}#{$property}: $pxValues;#{$property}: $remValues;}$cols: 12;$row-padding: 1em 0;$block-padding: 1em;.ms-grid{[class*=ms-col] {}[class*=ms-col] .ms-block {}}#ms-body{.intro{header{.inner{}.slogan{h1,h3,h4,h5,h6,p,pre,address,div{color: $slogan-font-color;}}}}/*NOCOMPILE*/.content{.ms-block{&.contact-form-block{.form-item{input,textarea{&::-webkit-input-placeholder {@if (lightness($form-input-color) > 50) {} @else {}}&:-moz-placeholder {@if (lightness($form-input-color) > 50) {} @else {}}&::-moz-placeholder {@if (lightness($form-input-color) > 50) {} @else {}}&:-ms-input-placeholder {@if (lightness($form-input-color) > 50) {} @else {}}}}}}}/*ENDNOCOMPILE*/}'};
      //storedScss = {scss: removeLinesAndSpaces(removeComments(scss))};
      storedScss = {scss: scss};
    };
    var compressScssFailed = function(variable){
      //We rollback the compression
      storedScss[variable] = storedScss.scss;
    };


    //Characters valid for ....
    var cvfSelectors = '[A-Z]|[a-z]|:|\\s|[0-9]|-|\\.|@media|\\(|\\)|\\[|\\]|\\*|\\&|#|>|\\,';
    var cvfSAndL = '\\r\\n|\\n|\\r|\\t|\\s\\s|^\\s'; //Space and line
    var cvfNoVariables = '[A-Z]|[a-z]|:|\\s|[0-9]|-|%|\\.|_|\\(|\\)|\\!|\\,|\\||\\\'|\\"|\\+|\\/|#|@include';
    var cvfComments = '\\/\\*([^*]|[\\r\\n]|(\\*+([^*\\/]|[\\r\\n])))*\\*+\\/';

    var removeEmptyRules = function(scss){
      var output=scss;
      var regexToRemoveEmptyRules = new RegExp('^('+cvfSelectors+')*([A-Z]|[a-z]|\\*)('+cvfSelectors+')*\\{('+cvfSAndL+')*\\}('+cvfSAndL+')', 'gm');
      output=output.replace(regexToRemoveEmptyRules,''); //Lets remove all the empty rules
      output=output.replace(regexToRemoveEmptyRules,''); //Lets remove all the empty rules
      output=output.replace(regexToRemoveEmptyRules,''); //Lets remove all the empty rules
      output=output.replace(regexToRemoveEmptyRules,''); //Lets remove all the empty rules

      return output;
    };
    var removeComments = function(scss){
      var output=scss;

      output=output.replace(new RegExp('\\/\\/.*('+cvfSAndL+')*', 'gm'),'\r\n'); //Remove comments //
      
      //Remove code in between nocompile tags
      while(output.indexOf('/*NOCOMPILE*/')>=0){
        var begining = output.indexOf('/*NOCOMPILE*/');
        var end = output.indexOf('/*ENDNOCOMPILE*/');
        if(end>=0){
          end+=16;
        }else{
          end=13;
        }
        output = output.substr(0,begining)+output.substr(end);
      }


      output=output.replace(new RegExp(cvfComments, 'gm'),''); //Remove comments /**/

      return output;
    };
    var removeLinesAndSpaces = function(scss){
      var output=scss;
            
      output=output.replace(new RegExp('('+cvfSAndL+')', 'gm'),''); //Remove spaces and lines
      
      return output;
    };
    var removeEmptyLines = function(scss){
      var output=scss;
            
      output=removeLinesAndSpaces(output);
      output=output.replace(new RegExp(';', 'gm'),';\r\n');
      output=output.replace(new RegExp('\\{', 'gm'),'{\r\n');
      output=output.replace(new RegExp('\\}', 'gm'),'}\r\n');
      output=output.replace(new RegExp('#\\{\\r\\n.*\\}(\\r\\n.*;)?', 'gm'),function(match){return match.replace(new RegExp('('+cvfSAndL+')', 'gm'),'');}); //Lets put the #{x} as one liners

      return output;
    };
    var removeStaticLines = function(scss){
      var output=scss;
      
      output=output.replace(new RegExp('^('+cvfNoVariables+')*;('+cvfSAndL+')', 'gm'),''); //Lets remove all the lines that doesn't have variables($) or mixins(@)

      return output;
    };

    var removeAllStaticConentFromScss = function(scss){
      var output;
      output = scss;
      //output = "@mixin clearfix {}@mixin resetMargin {}@mixin resetPadding {}@function getValueMinMax($value, $min, $max) {@return (($value*($max - $min))/100)+$min;}@mixin transition($transition-property, $transition-time, $method) {-webkit-transition: $transition-property $transition-time $method;-moz-transition: $transition-property $transition-time $method;-ms-transition: $transition-property $transition-time $method;-o-transition: $transition-property $transition-time $method;transition: $transition-property $transition-time $method;}@mixin background-opacity($color, $opacity: 0.3) {background: $color;background: rgba($color, $opacity);}@function strip-unit($num) {@return $num / ($num * 0 + 1);}@mixin rem-fallback($property, $values...) {$max: length($values);$pxValues: '';$remValues: '';@for $i from 1 through $max {$value: strip-unit(nth($values, $i));$pxValues: #{$pxValues + $value*16}px;@if $i < $max {$pxValues: #{$pxValues + ' '};}}@for $i from 1 through $max {$value: strip-unit(nth($values, $i));$remValues: #{$remValues + $value}rem;@if $i < $max {$remValues: #{$remValues + ' '};}}#{$property}: $pxValues;#{$property}: $remValues;}$cols: 12;$row-padding: 1em 0;$block-padding: 1em;.ms-grid{> .ms-row{margin-left: -$block-padding;margin-right: -$block-padding;}[class*=ms-col] {}[class*=ms-col] .ms-block {padding-top: $block-padding;padding-bottom: $block-padding;padding-left: $block-padding;padding-right: $block-padding;}/*NOCOMPILE*/@mixin column($x, $cols:$cols) {width: (100% / $cols) * $x;@for $y from 1 through $x {.ms-col-#{$y}{width: (100% / $x) * $y;}}}@for $i from 1 through $cols {.ms-col-#{$i}{@include column($i);}}/*ENDNOCOMPILE*/}#ms-body{font-family: $main-font , Helvetica, Arial, sans-serif;color: $main-font-color;.intro{header{.inner{h1{width: getValueMinMax($logo-size, 50, 800)+px;}.navigation-wrapper{@include background-opacity( $navigation-background-color, getValueMinMax($navigation-background-opacity, 0, 1) );nav{.mobile-menu{svg{fill: $navigation-color;}span{color: $navigation-color;}}.mobile-menu-close{svg{fill: $navigation-color;}span{color: $navigation-color;}}> ul{> li{&.close-menu{color: $navigation-color;font-family: $navigation-font, $main-font;}a{color: $navigation-color;font-family: $navigation-font, $main-font;@include rem-fallback( font-size, getValueMinMax( $navigation-size, 0.5, 1.5) );}ul.subnav{li{a{color: $navigation-color;font-family: $navigation-font, $main-font;&:hover{color: $navigation-highlight-color;}}}}&.active,&:hover{a{color: $navigation-highlight-color;}}}}}}}.slogan{@include rem-fallback( font-size, getValueMinMax($slogan-font-size, 1, 5) );h2{font-family: $slogan-sub-font;}h1,h3,h4,h5,h6,p,pre,address,div{color: $slogan-font-color;font-family: $slogan-font, $main-font;}}}.banner{background-color: $banner-background-color;background-image: url( $banner-image );}}.content{background: $page-background;.inner{background: $content-background;}.ms-block{&.text-block{h1,h2,h3,h4,h5,h6{color: $heading-font-color;font-family: $heading-font, $main-font;@include rem-fallback( font-size, getValueMinMax( $heading-font-size, 1.5, 5 ) );}p{line-height: getValueMinMax($font-line-height, 1.4, 3)+em;@include rem-fallback( font-size, getValueMinMax($main-font-size, 1, 3) );}}a{color: $link-color;&:hover{color: $link-highlight-color;}}&.shop-category-block{h1{color:$heading-font-color;}h3{color:$heading-font-color;}input,textarea{border: 1px solid $link-color;color: $link-color;outline-color: $link-color;}.button{color: $link-color;border: 1px solid $link-color;&.addToCart{input,textarea{border: 1px solid $link-color;color: $link-color;outline-color: $link-highlight-color;}button{color: $link-color;border: 1px solid $link-color;&:hover{background-color: $link-highlight-color;border: 1px solid $link-highlight-color;}}&:hover{border: 0px solid $link-highlight-color;}}&:hover{background-color: $link-highlight-color;border: 1px solid $link-highlight-color;}}aside{> div .title{svg{color: $heading-font-color;}}.mini-cart{.button{border: 1px solid $link-color;background-color: $link-color;&:hover{background-color: $link-highlight-color;border: 1px solid $link-highlight-color;}}}.categories{div{&:hover{color: $link-highlight-color;}}}}.shop-content{.category{li{.image{height: getValueMinMax($product-size, 100, 300)+px;}}}}}&.contact-form-block{.form-item{label{color: $form-color;}input,textarea{background: $form-input-color;border: 1px solid $form-input-color;color: darken( $form-input-color, 20%);&::-webkit-input-placeholder {@if (lightness($form-input-color) > 50) {color: darken( $form-input-color, 30% );}@else {color: lighten( $form-input-color, 30% );}}&:-moz-placeholder {@if (lightness($form-input-color) > 50) {color: darken( $form-input-color, 30% );}@else {color: lighten( $form-input-color, 30% );}}&::-moz-placeholder {@if (lightness($form-input-color) > 50) {color: darken( $form-input-color, 30% );}@else {color: lighten( $form-input-color, 30% );}}&:-ms-input-placeholder {@if (lightness($form-input-color) > 50) {color: darken( $form-input-color, 30% );}@else {color: lighten( $form-input-color, 30% );}}&:focus{border: 1px solid $form-color;}}button{background: $form-color;color: lighten($form-color, 40%);&:hover{background: darken($form-color, 10%);}}}}&.social-link-block{ul{li{&.facebook{background-image: url( $theme_url+'assets/img/facebook-icon.png');}&.twitter{background-image: url( $theme_url+'assets/img/twitter-icon.png');}&.pinterest{background-image: url( $theme_url+'assets/img/pinterest-icon.png');}&.googleplus{background-image: url( $theme_url+'assets/img/googleplus-icon.png');}&.instagram{background-image: url( $theme_url+'assets/img/instagram-icon.png');}}}}}}footer{background-color: $footer-background-color;.info{.copyright{color: $copyright-color;a{color: $copyright-color;}}}nav{> ul{> li{a{color: $footer-navigation-color;font-family: $navigation-font, $main-font;}}}}}}";
                  
      output = removeComments(output);
      output = removeEmptyLines(output);
      output = removeStaticLines(output);
      output = removeEmptyRules(output);

      return output;
    };

    var simplifyScss = function(variable){
      if(!storedScss){
        return '';
      }

      var simplified = 'simplified';
      var simplifiedUncompressed = 'simplifiedUncompressed';
      if(!storedScss[simplified]){
        storedScss[simplifiedUncompressed] = removeAllStaticConentFromScss(storedScss.scss);
        storedScss[simplified] = removeLinesAndSpaces(storedScss[simplifiedUncompressed]);
      }

      variable=variable||'scss';
      if(storedScss[variable]){
        return storedScss[variable];
      }

      var output;

      output = storedScss[simplifiedUncompressed];

      output=output.replace(new RegExp('^('+cvfNoVariables+'|\\$)*;('+cvfSAndL+')', 'gm'),function(match){if(match[0]==='$'||match[1]==='$'||match.indexOf(variable)>=0){return match;}return ''; }); //Lets remove all the lines that doesn't have variables($) or mixins(@)

      output = removeEmptyRules(output);
      output = removeEmptyRules(output);//Lets remove twice to make sure we remove more nested rules
      output = removeLinesAndSpaces(output);

      storedScss[variable] = output;
      return storedScss[variable];
    };

    return {
      editablesToSettings: editablesToSettings,
      compile2Css: compile2Css,
      simplifyScss:simplifyScss,
      storeScss:storeScss,
      compressScssFailed:compressScssFailed
    };
  }]);