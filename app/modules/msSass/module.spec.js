'use strict';

describe('SassManager Factory', function () {

  var scope, SassManager;

  var editables = [
    {name: 'bgColor', type: 'color', variable: 'bg-color', value: 'red'},
    {name: 'fontFamily', type: 'fontFamily', variable: 'font-family', value: 'Commic Sans'},
    {name: 'fontSize', type: 'slider', variable: 'font-size', value: '16'},
    {name: 'logo', type: 'image', variable: 'logo-image', value: 'url(\'http://logo.com/logo.png\')'}
  ];

  var sassSettings = '$bg-color:red;$font-family:Commic Sans;$font-size:16;$logo-image:url(\'http://logo.com/logo.png\');';

  var sassCss = 'body{background-color:$bg-color;}';

  beforeEach(module('msSass'));

  beforeEach(inject(function ($rootScope, _$compile_, _$q_, _SassManager_) {
    scope = $rootScope.$new();
    SassManager = _SassManager_;
  }));

  it('should create the sass from editables', function () {
    SassManager.editablesToSettings(editables).then(function (sassConverted) {
      expect(sassConverted).toEqual(sassSettings);
    });
    scope.$apply();
  });

  it('should compile sass to css', function () {
    SassManager.compile2Css(sassSettings + sassCss).then(function (sassSettings) {
      //console.log removed.
      var expecting = expect(sassSettings);
      expecting.toContain('background-color: red;');
    });
    scope.$apply();
  });
});
