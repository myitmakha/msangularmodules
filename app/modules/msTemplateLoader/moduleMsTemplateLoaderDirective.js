'use strict';

angular.module('msTemplateLoader')
  .directive('msTemplateLoaderDirective', ['$document', '$compile', '$timeout', 'TemplateLoaderDirectiveHelpers', 'TemplateRepository', 'LessManager', 'SassManager', 'msChangesTracking', 'PageBuilderManager','$rootScope','MetaDataManager', function ($document, $compile, $timeout, TemplateLoaderDirectiveHelpers, TemplateRepository, LessManager, SassManager, msChangesTracking, PageBuilderManager,$rootScope,MetaDataManager) {
    return {
      restrict: 'AEC',
      replace: false,
      scope: {
        page: '=',
        themeId: '=',
        editMode: '=',
        images: '=',
        settings: '=',
        working:'='
      },
      link: function (scope, element, attrs) {

        //Creating all the helpers for the template to read from
        var themeScope = scope.$new(true); //We create a isolated scope

        themeScope.settings = scope.settings;

        var pageType='page';
        scope.$on('maincontentItem',function(a,b,c){
          /*var title=b.context;
           if(b.context!=b.type){
           title+=' - '+ b.type;
           }
           angular.element('title').html(title);*/
          pageType=b.type;
          if (!scope.editMode ) {
            MetaDataManager.injectMetaDataInPage(b.context,b.type,b.id,scope.page.name,(scope.page.seo||{}).metaDescription, scope.settings.siteName);
          }
        });

        scope.$watch('images', function () {
          themeScope.images = scope.images;
        }, true);

        scope.$watch('settings', function () {
          themeScope.settings = scope.settings;
          MetaDataManager.injectMetaDataInPage('site','page',scope.page.id, scope.page.name,(scope.page.seo||{}).metaDescription, scope.settings.siteName);
        }, true);

        scope.$watch('editMode', function () {
          themeScope.editMode = scope.editMode;
        });

        scope.$watch('page', function () {
          themeScope.page = scope.page;
          if(scope.page){
            $timeout(function(){
              if(pageType!=='page'){
                return;
              }
              //scope.page.seo.metaTitle

              MetaDataManager.injectMetaDataInPage('site','page',scope.page.id, scope.page.name,(scope.page.seo||{}).metaDescription, scope.settings.siteName);
            },300);

          }
        });



        var loadSiteData = function (themeId) {
          return PageBuilderManager.getWebsiteData().then(
            function (data) {
              return TemplateLoaderDirectiveHelpers
                .getHelpersForTheTemplate(data, themeId)
                .then(function (helper) {
                  themeScope.helper = helper;
                });
            }
          );
        };

        if(scope.editMode){
          var $cssStylesPrev = angular.element('#cssStylesPrev');
          if ($cssStylesPrev.length === 0) {
            angular.element('head').append(angular.element('<style id="cssStylesPrev"></style>'));
            $cssStylesPrev = angular.element('#cssStylesPrev');
          }
        }

        var $cssStyles = angular.element('#cssStyles');
        if ($cssStyles.length === 0) {
          angular.element('head').append(angular.element('<style id="cssStyles"></style>'));
          $cssStyles = angular.element('#cssStyles');
        }

        //This is to avoid backing up the styles when we are updating them constantly
        var backupStyles = _.debounce(function () {
          $cssStylesPrev.html($cssStyles.html());
        }, 200);

        var injectCss = function (css,firstTime) {
          if(scope.editMode){
            backupStyles();
          }
          $cssStyles.html(css);
        };

        var getElementToInjectHead = function ($el, identifiers) {
          identifiers = identifiers || [];
          var headEl = {
            tagName: $el.prop('tagName'),
            html: $el.html(),
            attributes: {},
            ids: identifiers
          };
          for (var i = 0, attrs = $el[0].attributes, n = attrs.length; i < n; i++) {
            headEl.attributes[attrs[i].nodeName] = attrs[i].value;
          }
          var iid = 0;
          while (identifiers[iid]) {
            if (headEl.attributes[identifiers[iid]]) {
              headEl.id = identifiers[iid];
              break;
            }
            iid++;
          }
          return headEl;
        };

        var getHeadTags = function (metaTags, linkTags) {
          var headEls = [];
          if(metaTags){
            metaTags.forEach(function (element) {
              var $element = $(element);
              headEls.push(getElementToInjectHead($element, ['name', 'http-equiv', 'charset']));
            });
          }
          if(linkTags){
            linkTags.forEach(function (element) {
              var $element = $(element);
              headEls.push(getElementToInjectHead($element, ['href']));
            });
          }
          if (scope.page.seo && scope.page.seo.metaTitle) {
            var $element = $('<title>' + scope.page.seo.metaTitle + '</title>');
            //headEls.push(getElementToInjectHead($element));
          }
          if (scope.page.seo && scope.page.seo.metaDescription) {
            var $elementDesc = $('<meta name="description" content="' + scope.page.seo.metaDescription + '"></meta>');
            //headEls.push(getElementToInjectHead($elementDesc, ['name']));
          }
          return headEls;
        };

        var addTemplateToPage = function (template) {

          if (!scope.editMode) {
            injectIntoHtmlHead(template);
          }

          $compile(template.html)(themeScope, function (templateHtml) {
            var ele = angular.element('<div id="ms-html"><div id="ms-body"></div></div>');
            angular.element('#ms-body', ele).append(templateHtml);
            //Delay the injection of the html so we give time to the browser to compile the css
            $timeout(function(){element.html(ele);});
          });
        };

        var injectTagsIntoHtmlHead = function (tags) {
          var $head = angular.element('head');

          angular.forEach(tags, function (headEl) {
            var jqueryQuery = headEl.tagName;
            if(jqueryQuery === 'TITLE') {
              var $title = $head.find(jqueryQuery);
              $title.html(headEl.html);
            } else if (headEl.id) {
              if (headEl.id === 'charset') {
                jqueryQuery += '[' + headEl.id + ']';
              } else {
                if(!headEl.attributes[headEl.id]){
                  headEl.attributes[headEl.id]='emptyId';
                }
                var html = '<div>'+headEl.attributes[headEl.id]+'</div>';

                $compile(html)(themeScope, function (templateHtml) {
                  $timeout(function(){
                    ////console.log removed.
                    var parsedValue=templateHtml[0].innerHTML;
                    //jqueryQuery += '[' + headEl.id + '="' + headEl.attributes[headEl.id] + '"]';
                    jqueryQuery += '[' + headEl.id + '="' + parsedValue + '"]';

                    //headEl.attributes['idhead'] = parsedValue;



                    var $el = $head.find(jqueryQuery);
                    //var $el2 = $head.find(jqueryQuery);
                    var found = $el.length === 1;
                    if (!found) {
                      //Lets create the element
                      $el = $('<' + headEl.tagName + '>');
                    }
                    //Lets update the attributes
                    angular.forEach(headEl.attributes, function (value, id) {
                      if(headEl.id===id){
                        value=parsedValue;
                      }
                      $el.attr(id, value);
                    });
                    $el.html(headEl.html);
                    if (!found) {
                      //Lets append the element
                      $head.append($el);
                    }

                  });
                });

                ////console.log removed.
              }
            }

          });
        };

        var injectIntoHtmlHead = function (template) {

          injectCss(template.styles);

          var tags = getHeadTags(template.metaTags, template.linkTags);

          injectTagsIntoHtmlHead(tags);

        };

        var loadTemplateToPage = function (themeId, page) {

          if(page.editableOnPageBuilder===false){
            $('body').addClass('noneditable-page');
          }else{
            $('body').removeClass('noneditable-page');
          }

          var themePromise = TemplateRepository.getTemplate(themeId, page.name, page.ishomepage);

          themePromise.then(function (template) {
            try {
              template.metaTags = template.html.match(/<meta [^>]*>/g);
              template.linkTags = template.html.match(/<link [^>]*>/g);
              template.html = TemplateLoaderDirectiveHelpers.stripHTMLConflictingMarkup(template.html);

              addTemplateToPage(template);
            }
            catch (err) {
              console.log('There was a problem in the theme html or in the content blocks...');
              /*RemoveLogging:skip*/
              console.log(err);
              /*RemoveLogging:skip*/
            }
          });
        };

        var init = function(){
          scope.$watch('page.name', function () {
            if (typeof scope.page !== 'undefined') {
              loadTemplateToPage(scope.themeId, scope.page);
            }
          });
        };

        if (scope.editMode) {
          scope.$watch('themeId', function () {
            if (typeof scope.themeId !== 'undefined') {
              TemplateRepository.getTemplateSettings(scope.themeId).then(function (editables) {
                scope.editables = editables.settings;
                scope.themeStyles = TemplateLoaderDirectiveHelpers.cssRelativeToThePageBuilderPreview(editables.styles);
                scope.themeCompiler = editables.compiler;
                if (scope.editMode) {
                  loadStyles(scope.themeStyles,scope.themeCompiler);
                }
              });
            }
          });
        }

        var templateLoaded = false;
        var firstTime = true;

        var loadStyles = function(styles, compiler){
          if (templateLoaded) {
            return;
          }
          scope.working=true;
          templateLoaded = true;
          if(compiler==='less'){
            scope.$watch('editables', function (editables) {
              if (!editables) {
                return;
              }
              $rootScope.$broadcast('editableChanged');
              LessManager.editablesToSettings(editables).then(function (editablesSettings) {
                var defaultSettingsLess = '@theme_url:\'' + themeScope.helper.theme.url + '\';\n@cdn_url:\'' + themeScope.helper.cdn.url + '\';';
                var lessCss = defaultSettingsLess + editablesSettings + styles;
                LessManager.compile2Css(lessCss).then(function (css) {
                  if (css.err) {
                    console.log(css.err);
                    /*RemoveLogging:skip*/
                    console.log(lessCss);
                    /*RemoveLogging:skip*/
                  }
                  injectCss(css.css);
                  scope.working=false;
                });
              });
            }, true);
          }else if(compiler==='scss'){
            SassManager.storeScss(styles);
            scope.$watch('editables', function (editables) {
              if (!editables) {
                return;
              }
              $rootScope.$broadcast('editableChanged');
              SassManager.editablesToSettings(editables).then(function (editablesSettings) {
                var defaultSettingsSass = '$theme_url:\'' + themeScope.helper.theme.url + '\';\n$cdn_url:\'' + themeScope.helper.cdn.url + '\';';
                var sassSimplification = 'simplified';
                var sassStylesSimplified=SassManager.simplifyScss();
                var sass = defaultSettingsSass + editablesSettings + sassStylesSimplified;
                if(firstTime){
                  sass = defaultSettingsSass + editablesSettings + styles;
                }
                //var startTime = new Date;
                SassManager.compile2Css(sass).then(function (css) {
                  if (css.message) {
                    SassManager.compressScssFailed(sassSimplification);
                    console.log(css);
                    /*RemoveLogging:skip*/
                    console.log(sass);
                    /*RemoveLogging:skip*/
                  }
                  injectCss(css,firstTime);
                  firstTime=false;
                  scope.working=false;
                });
              });
            }, true);
          }else{
            scope.working=false;
            console.log('Unknown compiler', compiler);
            /*RemoveLogging:skip*/
          }
        };

        scope.$watch('themeId', function (themeId) {
          if(!themeId){
            return;
          }
          loadSiteData(themeId).then(init);
        });
      }
    };
  }]);
