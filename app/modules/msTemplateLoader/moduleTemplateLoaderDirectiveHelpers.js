/**
 * Created by Albert on 15/07/2014.
 */
'use strict';

angular.module('msContentItemsLayout')
  .factory('TemplateLoaderDirectiveHelpers', ['$q', 'Config', function ($q,Config) {
    var stripHTMLConflictingMarkup = function (html) {
      return html.replace(/<[\/]{0,1}(body|BODY|html|HTML|meta|META|link|LINK|title|TITLE)[^><]*>/g, '');
    };
    var cssRelativeToThePageBuilderPreview = function (css) {
      css = css.replace(new RegExp('body ', 'g'), '#ms-body ');
      css = css.replace(new RegExp('html ', 'g'), '#ms-html ');
      return css;
    };

    var getHelpersForTheTemplate = function(site, themeId){
      var promise = $q.defer();
      //console.log removed.
      var helper={};
      helper.theme ={};
      helper.theme.url = Config.GetEndPoint('themesEndpoint') + site.siteId + '/' + themeId + '/';
      helper.cdn = {};
      helper.cdn.url = Config.GetEndPoint('themesEndpoint');
      helper.site={};
      helper.site.name=site.settings.siteName;
      helper.menu=site.menu;
      helper.date=new Date();
      promise.resolve(helper);
      return promise.promise;
    };

    //Defining public methods
    var genericPageHelper = {
      stripHTMLConflictingMarkup: stripHTMLConflictingMarkup,
      cssRelativeToThePageBuilderPreview: cssRelativeToThePageBuilderPreview,
      getHelpersForTheTemplate:getHelpersForTheTemplate
    };
    return genericPageHelper;
  }]);