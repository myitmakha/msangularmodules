//'use strict';
//
//describe('msTemplateLodar directive', function () {
//
//  var scope, elm, $compile, $q, TemplateRepository;
//
//  var pages = [{name:'Home'},{name:'About'},{name:'Shop'}];
//  var pageHtml = '<div id="thisIsAVeryObscureIdSoItIsUnique">Hi i am the html template of the theme that needs to be loaded.<div>';
//  var styles = 'body{background-color:red;}';
//
//  beforeEach(module('msTemplateLoader'));
//
//  beforeEach(inject(function($rootScope,_$compile_, _$q_, _TemplateRepository_) {
//    scope = $rootScope.$new();
//    $compile=_$compile_;
//    $q=_$q_;
//    TemplateRepository = _TemplateRepository_;
//  }));
//
//  var compileDirective = function(pageId,tpl) {
//    scope.page = pages[pageId];
//    if (!tpl) tpl = '<div id="ms-preview" ms-template-loader-directive page="page"></div>';
//    tpl = '<body id="body">' + tpl + '</body>';
//    var body = $compile(tpl)(scope);
//    scope.$digest();
//    elm = body.children().eq(0);
//  };
//
//  var mockAllObjects = function(pageHtml, styles) {
//    var template = {html:pageHtml,styles:styles};
//    var deferred = $q.defer();
//    deferred.resolve(template);
//    spyOn(TemplateRepository,'getTemplate').andReturn(deferred.promise);
//  };
//
//  describe('initialisation on home page', function() {
//    beforeEach(function() {
//      mockAllObjects(pageHtml,styles);
//    });
//
//    beforeEach(function() {
//      compileDirective(0); //Loading home page
//    });
//
//    it('should append the styles', function() {
//      var cssSyles = angular.element('#cssStyles');
//      expect(cssSyles.length).toBe(1);
//      expect(cssSyles.html()).toBe(styles);
//    });
//
//    it('should append the ms-html', function() {
//      var msHtml = elm;
//      expect(msHtml.attr('id')).toBe('ms-html');
//    });
//
//    it('should append the ms-body', function() {
//      var msBody = elm.children().eq(0);
//      expect(msBody.attr('id')).toBe('ms-body');
//    });
//
//    it('should append the text inside the body', function() {
//      var msBody = elm.find('#ms-body');
//      var msDirective = msBody.find('#thisIsAVeryObscureIdSoItIsUnique');
//      expect(msDirective.text()).toBe('Hi i am the html template of the theme that needs to be loaded.');
//    });
//  });
//});
