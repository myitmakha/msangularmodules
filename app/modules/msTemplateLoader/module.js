'use strict';

angular.module('msTemplateLoader', ['msContentItemsLayout', 'msContentItems', 'msImageItem', 'msHeaderItemText', 'msHeaderItemLogo', 'msTemplateRepository', 'msConfig', 'msLess', 'msSass', 'msChangesTrackingManager', 'msScript', 'msCopyright', 'msAddPage']);
