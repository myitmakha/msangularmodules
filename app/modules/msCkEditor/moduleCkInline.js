'use strict';

angular.module('msCkEditor')
  .directive('ckInline', ['$compile', '$sce', function ($compile, $sce) {
    return {
      scope: {
        editorContent: '='
      },
      restrict: 'AE',
      link: function postLink(scope, element, attrs) {
        var config={};
        if(typeof attrs.ckplaceholder !== 'undefined'){
          config.placeholder = attrs.ckplaceholder;
        }
        var ck = CKEDITOR.inline(element[0],config);
        var unregister = scope.$watch('editorContent',function(content){
          if(content){
            setTimeout(function(){ck.setData(content);});
          }
        });
        ck.on('change', function () {
          scope.$apply(function () {
            scope.editorContent = ck.getData();
            if(scope.editorContent){
              unregister();
            }
          });
        });
        
      }
    };
  }]);