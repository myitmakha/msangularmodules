'use strict';

angular.module('msAddAttributes')
  .directive('msAddAttributes', [ 'Config', 'VariantService', function  (Config, VariantService) {
    return {
      restrict: 'EA',
      scope: {
        attributes: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msAddAttributes/addAttributes.tpl.html',
      link: function (scope) {
        scope.enableAttributes = true;
        scope.attributeLimit = 5;

        scope.attributes = scope.attributes || [
          {
            name: '',
            options: ['']
          }
        ];

        scope.addItem = function () {
          scope.attributes.push({
            name: '',
            options: ['']
          });
        };

        scope.deleteItem = function (index) {
          scope.attributes.splice(index, 1);
        };

        scope.regenerateVariants = function() {
          VariantService.Generate(scope.attributes).then(
          function (response) {
          },
          function (error) {
          });
        };

        scope.$watch('attributes', function(){
          //console.log removed.
        });
      }
    };
  }]);
