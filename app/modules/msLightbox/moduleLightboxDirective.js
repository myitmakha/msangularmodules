'use strict';

angular.module('msLightbox')
  .directive('msLightbox', ['Config', function (Config) {
    return{
      restrict: 'A',
      link: function (scope, element, attrs) {
        var checkforCss = function () {
          var lightboxCss = Config.GetEndPoint('modulesDirectory') + '/msLightbox/jquery.fancybox.css';
          if (!$('link[href="' + lightboxCss + '"]').length) {
            $('head').append('<link href="' + lightboxCss + '" type="text/css" rel="stylesheet" />');
          }
        };
        checkforCss();

        element.addClass('fancybox');

        $('.fancybox').fancybox({
          helpers: {
            overlay: {
              locked: false
            }
          }
        });
      }
    };
  }]);