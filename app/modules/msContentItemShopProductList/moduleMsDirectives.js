'use strict';

angular.module('msContentItemShopProductList')
  .directive('msShopSearch', ['Config', function (Config) {
    return {
      restrict: 'AE',
      replace: true,
      scope: {
        searchText: '=',
        category: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemShopProductList/msShopSearch.tpl.html',
      link: function (scope, element, attrs) {

        element.on('click', function () {
          var win = $(window).width();
          if (win <= 640) {
            element.toggleClass('open');
            $('aside .mini-cart, aside .categories').toggleClass('hide');
          }
        });
      }
    };
  }]);

angular.module('msContentItemShopProductList')
  .directive('msShopCategoryList', ['Config', '$timeout', function (Config, $timeout) {
    return {
      restrict: 'AE',
      replace: true,
      scope: {
        categories: '=',
        callback: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemShopProductList/msShopCategoryList.tpl.html',
      link: function (scope, element, attrs) {

        scope.svgUrl = function( link ){
          return Config.GetEndPoint('modulesDirectory') + '/msContentItemShop/sprites.svg#'+link;
        };

        //console.log removed.

        $timeout(
          function() {
            element.find('.expander-control').click(function(){

              //console.log removed.

              //Expand or collapse this panel
              $(this).next().slideToggle('fast');
              $(this).parent().toggleClass('active');

              //Hide the other panels
              element.find('ul.sub-categories').not($(this).next()).slideUp('fast');
              element.find('.category-list > ul > li').not($(this).parent()).removeClass('active');

            });
          }, 100
        );




        var currentCategory = '';

        scope.selectCategory = function (category, subcategory) {

          currentCategory = category;

          if (scope.callback) {
            scope.callback(category, subcategory);
          }

          if ($(window).width() <= 640) {
            $('aside .mini-cart, aside .search, aside .categories').removeClass('hide');
            $('aside .search, aside .categories').removeClass('open');
          }

        };



        element.on('click', function () {
          var win = $(window).width();
          if (win <= 640) {
            element.toggleClass('open');
            $('aside .mini-cart, aside .search').toggleClass('hide');
          }

        });
      }
    };
  }]);
