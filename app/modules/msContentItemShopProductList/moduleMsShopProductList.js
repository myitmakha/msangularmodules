'use strict';

angular.module('msContentItemShopProductList')
  .directive('msContentItemShopProductList', ['$compile','ProductService','ShopService','Config', function ($compile,ProductService,ShopService,Config) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '=',
        contentItemMetaData: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemShopProductList/msContentItemShopProductList.tpl.html',

      link: function (scope, element) {

        scope.paging = {};

        scope.selectProduct = function(product){
          scope.contentItemMetaData.setUrlParamsByPageType('shop','products',(product||{}).ProductId);
        };

        scope.selectCategory = function(category, subcategory){
          var subpage = 'categories';
          if(category==='All'){
            category=undefined;
            subpage=undefined;
          }
          //console.log removed.
          //console.log removed.
          //Phil here you can set the subcategory and the page is going to redirect properly.

          scope.contentItemMetaData.setUrlParamsByPageType('shop',subpage,category,subcategory);
        };

        var loadValuesFromUrl = function(){
          if(scope.contentItemMetaData.urlParams.param2){
            var categoryFilters = [scope.contentItemMetaData.urlParams.param2];
            if(scope.contentItemMetaData.urlParams.param3){
              categoryFilters.push(scope.contentItemMetaData.urlParams.param3);
            }
            if(scope.contentItemMetaData.urlParams.param4){
              categoryFilters.push(scope.contentItemMetaData.urlParams.param4);
            }
            scope.categoryFilters=categoryFilters;
          }
        };

        //var calculateCategories = function(){
        //  scope.categories = ['All'];
        //
        //  var categoriesAdded = {};
        //  var categoryFound = false||!scope.categoryFilters;
        //  var categoryFilter;
        //  if(scope.categoryFilters&&scope.categoryFilters.length>0){
        //    categoryFilter=scope.categoryFilters[scope.categoryFilters.length-1];
        //  }
        //  for (var ip = 0; scope.products[ip]; ip++) {
        //    var product = scope.products[ip];
        //    for (var ic = 0; product && product.Categories[ic]; ic++) {
        //      var category = product.Categories[ic];
        //      if (!categoriesAdded[category]) {
        //        categoriesAdded[category] = category;
        //        scope.categories.push(category);
        //        if(category===categoryFilter){
        //          categoryFound=true;
        //        }
        //      }
        //    }
        //  }
        //  delete scope.error;
        //  if(!categoryFound){
        //    scope.error='Category not found';
        //  }
        //};

        scope.customSearch = function (item) {
          //console.log removed.
          if (scope.categoryFilters && typeof(scope.categoryFilters)==='string' && scope.categoryFilters.length > 0 && item.Categories.indexOf(scope.categoryFilters) === -1) {
            return false;
          }
          if (scope.categoryFilters && typeof(scope.categoryFilters)!=='string' && scope.categoryFilters.length > 0) {
            for(var i=0;scope.categoryFilters[i];i++){
              if(item.Categories.indexOf(scope.categoryFilters[i]) === -1){
                return false;
              }
            }
          }
          if (!scope.searchText || scope.searchText.length === 0) {
            return true;
          }
          var searchText = scope.searchText.toLowerCase();

          if (item.Name.toLowerCase().indexOf(searchText) >= 0) {
            return true;
          }
          if (item.Description&&item.Description.toLowerCase().indexOf(searchText) >= 0) {
            return true;
          }
          return false;
        };

        var loadProducts = function(){
          scope.contentItemMetaData.loading=true;
          ProductService.getAll().then(function (products) {
            scope.contentItemMetaData.loading=false;
            scope.products = products;
            //console.log removed.
            //calculateCategories();
          });
        };

        var getCategories = function () {
          ShopService.getCategoryHierarchies().then(function (hierarchies) {
            hierarchies.unshift({'name':'All', children: [ ]});
            scope.categories = hierarchies;
          });
        };


        loadValuesFromUrl();
        loadProducts();
        getCategories();
      }
    };
  }]);
