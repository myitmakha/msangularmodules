'use strict';

angular.module('msSiteRepository').factory('SiteRepository', ['SiteRestangular','BaseSiteRestangular', '$q', function (SiteRestangular,BaseSiteRestangular, $q) {

  var websiteDataPromise;

  var getSite = function () {
//    if (websiteDataPromise) {
//      return websiteDataPromise;
//    }
    var deferred = $q.defer();
    websiteDataPromise = deferred.promise;
    var promise = SiteRestangular.one('').get();
    promise.then(function (response) {
      var siteData = response.data.SiteData;
      siteData.images = siteData.images || {};
      // updated this to false, so default theme image will show if user didn't change anything on images
      siteData.images.headerLogo = siteData.images.headerLogo || {value: '', themeImage: '', isRemove: false};
      siteData.images.footerLogo = siteData.images.footerLogo || {value: '', themeImage: '', isRemove: false};
      deferred.resolve(siteData);
    });

    return websiteDataPromise;
  };

  var getSiteId = function () {
    var deferred = $q.defer();
    getSite().then(function (websiteData) {
      deferred.resolve(websiteData.siteId);
      return websiteDataPromise;
    });
    return deferred.promise;
  };

  var createSite = function (postData) {
    var deferred = $q.defer();
    BaseSiteRestangular.all('').post(postData).then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var updateSite = function (postData) {
    var deferred = $q.defer();

    postData.siteData = undefined;
    postData.SiteData = undefined;
    //postData={SiteData:postData};

    SiteRestangular.all('').customPUT(postData).then(
      function (response) {
        deferred.resolve(true);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getSiteSettings = function () {
    var deferred = $q.defer();
    SiteRestangular.one('').get().then(
      function (response) {
        deferred.resolve(response.data.SiteData.settings);
      }
    );
    return deferred.promise;
  };

  var setDefaultSocialSettings = function () {
    var deferred = $q.defer();
    var socialLinks = [];
    socialLinks.push({'name': 'facebook', 'url': ''});
    socialLinks.push({'name': 'twitter', 'url': ''});
    socialLinks.push({'name': 'pinterest', 'url': ''});
    socialLinks.push({'name': 'googleplus', 'url': ''});
    socialLinks.push({'name': 'instagram', 'url': ''});
    socialLinks.push({'name': 'linkedin', 'url': ''});
    socialLinks.push({'name': 'tumblr', 'url': ''});
    socialLinks.push({'name': 'flickr', 'url': ''});
    socialLinks.push({'name': 'email', 'url': ''});

    getSiteSettings().then(
      function (settings) {
        settings.socialLinks = socialLinks;

        updateSiteSettings(settings).then(
          function (response) {
            deferred.resolve(settings);
          }
        );
      }
    );
    return deferred.promise;
  };

  var updateSiteSettings = function (settings) {
    var deferred = $q.defer();
    SiteRestangular.all('settings').customPUT(settings).then(
      function (response) {
        deferred.resolve(response.data);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var installTheme = function (themeId) {
    var deferred = $q.defer();
    SiteRestangular.all('themes').post('"' + themeId + '"').then(
      function (response) {
        var data = response.data.trim();
        data = data.replace(/(^"|"$)/g, '');
        deferred.resolve(data);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getMyThemeById = function (id) {
    var deferred = $q.defer();
    SiteRestangular.one('themes/' + id).get().then(
      function (response) {
        deferred.resolve(response.data);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getMyThemes = function () {
    var deferred = $q.defer();
    SiteRestangular.one('themes').get().then(
      function (response) {
        deferred.resolve(response.data.Themes);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var setThemeToLive = function (id) {
    var deferred = $q.defer();
    getMyThemeById(id).then(
      function (data) {
        var theme = data.Theme;
        theme.IsLive = true;
        SiteRestangular.all('themes/' + id).customPUT(theme).then(
          function (response) {
            deferred.resolve(true);
          },
          function (error) {
            deferred.resolve(error);
          }
        );
      }
    );

    return deferred.promise;
  };

  var deleteMyThemeById = function (id) {
    //console.log removed.
    var deferred = $q.defer();
    SiteRestangular.all('themes/' + id).remove().then(
      function (response) {
        deferred.resolve(response.data);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var saveContentSnapShot = function (postData) {
    var deferred = $q.defer();
    SiteRestangular.all('contentsnapshots').post(postData).then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };


  var getThemeUpdates = function () {
    var deferred = $q.defer();
    SiteRestangular.all('themes').get('updates').then(
      function (response) {
        var themeUpdates = response.data;
        var listThemes = [];
        for (var i = 0; themeUpdates[i]; i++) {
          listThemes.push(themeUpdates[i]);
        }
        deferred.resolve(listThemes);
      }
    );
    return deferred.promise;
  };


  var updateTheme = function (themeId) {
    var deferred = $q.defer();
    SiteRestangular.all('themes/' + themeId).one('update').customPUT().then(
      function (response) {
        deferred.resolve(response.data);
      }
    );
    return deferred.promise;
  };


  var createNewPage = function (name, type) {
    var deferred = $q.defer();
    var postData = {'name': name, 'type': type};
    SiteRestangular.all('pages').post(postData).then(
      function (response) {
        deferred.resolve(response.data);
      }
    );
    return deferred.promise;
  };

  var siteRepository = {
    createNewPage: createNewPage,
    getSite: getSite,
    getSiteId: getSiteId,
    createSite: createSite,
    updateSite: updateSite,
    getSiteSettings: getSiteSettings,
    setDefaultSocialSettings: setDefaultSocialSettings,
    updateSiteSettings: updateSiteSettings,
    getMyThemes: getMyThemes,
    installTheme: installTheme,
    setThemeToLive: setThemeToLive,
    deleteMyThemeById: deleteMyThemeById,
    saveContentSnapShot: saveContentSnapShot,
    getThemeUpdates: getThemeUpdates,
    updateTheme: updateTheme
  };
  return siteRepository;
}]);
