//'use strict';
//
//describe('msSiteRepository', function () {
//  var scope, SiteRepository, SiteRestangular;
//  beforeEach(module('msSiteRepository'));
//
//  var loadDependencies = function (siteData, status) {
//    inject(function (_SiteRestangular_, $rootScope) {
//      scope = $rootScope.$new();
//      spyOn(_SiteRestangular_, 'one').andReturn(getRestangularResponse(siteData, status));
//      spyOn(_SiteRestangular_, 'all').andReturn(postRestangularResponse);
//      SiteRestangular = _SiteRestangular_;
//    });
//    inject(function (_SiteRepository_) {
//      SiteRepository = _SiteRepository_;
//    });
//  };
//
//  var getRestangularResponse = function (siteData, status) {
//    var $q = {};
//    inject(function (_$q_) {
//      $q = _$q_;
//    });
//    var deferred = $q.defer();
//    deferred.resolve({data: {SiteData: siteData}});
//    return {
//      get: function () {
//        return deferred.promise;
//      }
//    };
//  };
//
//  var postRestangularResponse = function(status){
//    var $q = {};
//    inject(function (_$q_) {
//      $q = _$q_;
//    });
//    var deferred = $q.defer();
//    deferred.resolve({status: 200});
//    return {
//      post: function(){
//       return deferred.promise;
//      }
//    };
//  };
//
//  describe('getSite', function () {
//
//    it('should return the site', function () {
//      loadDependencies({name: 'home'});
//      SiteRepository.getSite().then(function (data) {
//        expect(data).toEqual({name: 'home'});
//        expect(SiteRestangular.one).toHaveBeenCalledWith('');
//      });
//      scope.$apply();
//    });
//
//    it('should return empty site if does not exists', function () {
//      loadDependencies({});
//      SiteRepository.getSite().then(function (data) {
//        expect(data).toEqual({});
//        expect(SiteRestangular.one).toHaveBeenCalledWith('');
//      });
//      scope.$apply();
//    });
//  });
//
//  describe('createSite', function(){
//    it('should return 200 upon site creation', function(){
//      loadDependencies
//    });
//  });
//});
