'use strict';

angular.module('msBasketRepository').factory('BasketRepository', ['$q', '$window', function ($q, $window) {

  var basket;

  var reset = function(){
    basket = {items:[]};
  };

  var getItems = function(){
    var deferred = $q.defer();
    deferred.resolve(basket.items);
    return deferred.promise;
  };

  var getItemById = function(id){
    return basket.items[getItemIndexById(id)];
  };

  var getItemIndexById = function(id){
    for(var i=0;basket.items[i];i++){
      if(basket.items[i].id===id){
        return i;
      }
    }
    return;
  };

  var saveItems = function(){
    $window.sessionStorage.basket = JSON.stringify(basket);
  };

  var retrieveItems = function(){
    if($window.sessionStorage.basket){
      basket  = JSON.parse($window.sessionStorage.basket);
    }
  };
  reset();
  retrieveItems();

  var basketRepository = {
    getItems: getItems,
    saveItems: saveItems,
    getItemById: getItemById,
    getItemIndexById: getItemIndexById,
    reset: reset
  };

  return basketRepository;

}]);