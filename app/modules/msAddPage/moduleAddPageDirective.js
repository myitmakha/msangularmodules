'use strict';

angular.module('msAddPage')
  .directive('msAddPage', ['$compile', '$rootScope', '$filter', '$timeout', '$location', 'Config', '$auth', 'PackageRepository', 'PageBuilderManager', 'MenuManager', 'ngDialog', function ($compile, $rootScope, $filter, $timeout, $location, Config, $auth, PackageRepository, PageBuilderManager, MenuManager, ngDialog) {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        siteData: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msAddPage/addpage.tpl.html',
      link: function (scope, element) {

        scope.settings = scope.$parent.siteData.settings || {};

        scope.pageToAdd = {};

        scope.addToMenu = { add: false };

        scope.addPageModal = function(){

          ngDialog.open({
            template: 'presentation/pagebuilder/add-page.modal.tpl.html',
            scope: scope,
            className: 'add-page tabs'
          });

        };

        scope.updateUrl = function (name) {
          scope.pageToAdd.url = '/' + $filter('cleanUrl')(name);
        };

        scope.addPage = function () {
          scope.pageError = undefined;

          $rootScope.$broadcast('markChanges', scope.pageToAdd.name + ' page has been added.');
          PageBuilderManager.createNewPage(scope.pageToAdd.name, 'default').then(function (addedPage) {
            scope.siteData.pages.push(addedPage);

            if(scope.addToMenu.add === true){
              MenuManager.addToMenu(addedPage);
            }

            $location.path('pageBuilder' + addedPage.url);
          });
          scope.pageToAdd = {};
          $rootScope.$broadcast('modalClose');

        };

        scope.buyNow = function () {
          if (scope.settings.accountId) {
            $auth.redirectToAccountCentre(scope.settings.accountId, true);
          } else {
            //console.log removed.
          }
        };

        var compileToView = function () {

          scope.pages = scope.siteData.pages || [];

          var statusBar = element.find('.status-bar');

          var colourBar = function () {
            var activeBar = statusBar.find('.ui-progressbar-value'),
              value = ( scope.pageAmount.used / scope.pageAmount.limit ) * 100;

            if (value < 60) {
              activeBar.css({'background': '#a9d96c'});
            } else if (value < 70) {
              activeBar.css({'background': '#f8d347'});
            } else if (value < 80) {
              activeBar.css({'background': '#ffa760'});
            } else {
              activeBar.css({'background': '#ff6c60'});
            }
          };

          if (scope.settings) {
            scope.pageAmount = {
              used: scope.pages.length,
              limit: parseInt(scope.settings.pageLimit)
            };

            scope.isExceed = false;
            if (scope.pageAmount.used >= scope.pageAmount.limit) {
              scope.isExceed = true;
            }

            statusBar.progressbar({
              value: scope.pageAmount.used,
              max: scope.pageAmount.limit
            });
            colourBar();
          } else {
            PackageRepository.getFeature('pageLimit').then(function (pageLimit) {
              scope.pageAmount = {
                used: scope.pages.length,
                limit: parseInt(pageLimit.value)
              };

              scope.isExceed = false;
              if (scope.pageAmount.used >= scope.pageAmount.limit) {
                scope.isExceed = true;
              }

              statusBar.progressbar({
                value: scope.pageAmount.used,
                max: scope.pageAmount.limit
              });
              colourBar();
            });

            statusBar.bind('progressbarchange', function (event, ui) {
              colourBar();
            });
          }
        };

        scope.$watch('settings', function () {
          compileToView();
        });
      }
    };
  }]);
