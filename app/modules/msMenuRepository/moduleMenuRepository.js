'use strict';

angular.module('msMenuRepository').factory('MenuRepository', ['SiteRestangular', '$q', function (SiteRestangular, $q) {

  var getMenu = function () {
    var deferred = $q.defer();
    SiteRestangular.one('menu').get().then(
      function (menuData) {
        deferred.resolve(menuData.data);
      }, function (error) {
        deferred.reject(error);
      });
    return deferred.promise;
  };

  var saveMenu = function (postData, event) {
    var deferred = $q.defer();
    SiteRestangular.all('menu').customPUT(postData).then(
      function (response) {
        deferred.resolve(true);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  return {
    getMenu: getMenu,
    saveMenu: saveMenu
  };
}]);
