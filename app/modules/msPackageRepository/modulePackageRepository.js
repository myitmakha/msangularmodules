'use strict';

angular.module('msPackageRepository').factory('PackageRepository', ['PageBuilderManager', '$q', 'Config', function (PageBuilderManager, $q, Config) {

  var defaultPackage = 'Taster';

  //var packages = Config.GetEndPoint('packages');
  var packages = {
    'Taster': {
      'price': 8,
      'features': {
        'pageLimit': { value:4, 'isActive': true },
        '1 feature': { 'isActive': true },
        '2 feature': { 'isActive': true },
        '3 feature': { 'isActive': true },
        '4 feature': { 'isActive': true },
        '5 feature': { 'isActive': false },
        '6 feature': { 'isActive': false },
        '7 feature': { 'isActive': false },
        '8 feature': { 'isActive': false }
      }
    },
    'Starter': {
      'price': 16,
      'features': {
        'pageLimit': { value:20, 'isActive': true },
        '1 feature': { 'isActive': true },
        '2 feature': { 'isActive': true },
        '3 feature': { 'isActive': true },
        '4 feature': { 'isActive': true },
        '5 feature': { 'isActive': true },
        '6 feature': { 'isActive': true },
        '7 feature': { 'isActive': false },
        '8 feature': { 'isActive': false }
      }
    },
    'Pro': {
      'price': 24,
      'features': {
        'pageLimit': { value:75,'isActive': true },
        '1 feature': { 'isActive': true },
        '2 feature': { 'isActive': true },
        '3 feature': { 'isActive': true },
        '4 feature': { 'isActive': true },
        '5 feature': { 'isActive': true },
        '6 feature': { 'isActive': true },
        '7 feature': { 'isActive': true },
        '8 feature': { 'isActive': true }
      }
    }
  };

  var currentPackagePromise;

  var getPackages = function () {
    var deferred = $q.defer();
    deferred.resolve(packages);
    return deferred.promise;
  };

  var getPackage = function () {
    if(!currentPackagePromise){
      var deferred = $q.defer();
      PageBuilderManager.getWebsiteData().then(function (site) {
        var packageName = ((site||{}).settings||{}).package;
        var currentPackage = packages[packageName]||packages[defaultPackage];
        deferred.resolve(currentPackage);
      });
      currentPackagePromise = deferred.promise;
    }
    return currentPackagePromise;
  };

  var hasFeature = function (feature) {
    var deferred = $q.defer();
    getPackage().then(function(currentPackage){
      var featureValue = currentPackage&&currentPackage.features&&currentPackage.fetures[feature];
      if(!featureValue){
        console.log('Feature '+feature+' not found');
        /*RemoveLogging:skip*/
      }
      deferred.resolve(featureValue);
    });
    return deferred.promise;
  };

  var getFeature = function (feature) {
    var deferred = $q.defer();
    getPackage().then(function(currentPackage){
      deferred.resolve(((currentPackage||{}).features||[])[feature]);
    });
    return deferred.promise;
  };

  var packageRepository = {
    getPackages: getPackages,
    getPackage: getPackage,
    hasFeature: hasFeature,
    getFeature: getFeature,
  };

  return packageRepository;
}]);