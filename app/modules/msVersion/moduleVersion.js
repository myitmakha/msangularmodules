'use strict';

angular.module('msVersion').directive('msVersion', ['$http', 'VersionRepository',
  function ($http, VersionRepository) {
    return {
      restrict: 'A',
      replace: true,
      scope: false,
      template: '<span>We\'re running </br> API Version: {{ version.api.build }} </br>  APP Version: {{ version.app.build }} </span>',
      link: function (scope) {
        var version = {};
        VersionRepository.getAppVersion().then(
          function (response) {
            version.app = { build: response };
          }
        );

        VersionRepository.getApiVersion().then(
          function (response) {
            version.api = response;
            scope.version = version;
          }
        );
      }
    };
  }]);