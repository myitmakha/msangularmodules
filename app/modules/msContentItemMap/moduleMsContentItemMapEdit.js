/**
 * Created by Albert on 15/07/2014.
 */
'use strict';
angular.module('msContentItemMap')
  .directive('msContentItemMapEdit', ['$compile', 'msContentItemMapManager', '$timeout', function ($compile, msContentItemMapManager, $timeout) {
    return {
      restrict: 'A',
      replace: true,

      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {

        scope.openSettings = scope.$parent.actions.displaySettings;
        scope.contentItemData.settings = msContentItemMapManager.resetSettings(scope.contentItemData.settings);
        var refreshIframe = function () {
          var ele = msContentItemMapManager.getMapIframe(scope.contentItemData.settings);

          $compile(ele)(scope, function (cloned) {
            element.html(cloned);
          });
        };

        var debounceFunctionOfRefreshIframe = _.debounce(function () {
          refreshIframe();
          scope.$apply();
        }, 500);
        var initializing = true;

        var firstLoadTriggerInstantlyOtherwiseDebounce = function () {
          if (initializing) {
            $timeout(function () {
              initializing = false;
            });
          } else {
            debounceFunctionOfRefreshIframe();
          }
        };

        scope.$watch('contentItemData.settings.apiKey', firstLoadTriggerInstantlyOtherwiseDebounce);
        scope.$watch('contentItemData.settings.place', firstLoadTriggerInstantlyOtherwiseDebounce);
        scope.$watch('contentItemData.settings.height', firstLoadTriggerInstantlyOtherwiseDebounce);
        refreshIframe();
      }
    };
  }]);