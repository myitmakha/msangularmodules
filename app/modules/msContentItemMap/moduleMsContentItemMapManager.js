/**
 * Created by Albert on 15/07/2014.
 */
'use strict';
angular.module('msContentItemMap')
  .factory('msContentItemMapManager', [ function () {
    var resetSettings = function (settings) {
      settings = settings || {};
      settings.place = settings.place || 'mrsite';
      //settings.apiKey=settings.apiKey||'';
      settings.height = settings.height || '450';
      return settings;
    };
    var getMapIframe = function (settings) {
      settings = resetSettings(settings);
      var url = 'https://www.google.com/maps/embed/v1/place?q=' + settings.place + '&key=AIzaSyDHzR_F3yvth5q1FGwQfcqmNb0cRwwO6po'/* + settings.apiKey*/;
      return '<iframe width="100%" height="{{contentItemData.settings.height}}" frameborder="0" style="border:0" src="' + url + '"></iframe>';
    };

    var mapManager = {
      resetSettings: resetSettings,
      getMapIframe: getMapIframe
    };
    return mapManager;
  }]);