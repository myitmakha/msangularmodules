/**
 * Created by Albert on 15/07/2014.
 */
'use strict';
angular.module('msContentItemMap')
  .directive('msContentItemMapSettings', ['$compile', 'msContentItemMapManager', function ($compile, msContentItemMapManager) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        settings: '='
      },
      template: '<div>' +
        '<div class="row-fix">' +
        '<div class="col-fix14">' +
        '<label>Address</label>' +
        '<input type="text" ng-model="settings.place" />' +
        '</div>' +
        '</div>' +
        '<div class="row-fix">' +
        '<div class="col-fix14">' +
        '<label>Height</label>' +
        '<div class="slider"></div>' +
        '</div>' +
        '</div>' +
        '</div>',

      link: function (scope, element) {
        $('.slider', element).slider({
          min: 50,
          max: 800,
          range: 'min',
          value: scope.settings.height,
          slide: function (event, ui) {
            scope.settings.height = ui.value;
            scope.$apply();
          }
        });
        scope.settings = msContentItemMapManager.resetSettings(scope.settings);
      }
    };
  }]);