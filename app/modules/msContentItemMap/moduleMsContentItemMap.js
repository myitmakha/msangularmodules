/**
 * Created by Albert on 15/07/2014.
 */
'use strict';
angular.module('msContentItemMap')
  .directive('msContentItemMap', ['$compile', 'msContentItemMapManager', function ($compile, msContentItemMapManager) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '='
      },
      link: function (scope, element) {
        scope.openSettings = scope.$parent.actions.displaySettings;
        scope.contentItemData.settings = msContentItemMapManager.resetSettings(scope.contentItemData.settings);
        var ele = msContentItemMapManager.getMapIframe(scope.contentItemData.settings);
        $compile(ele)(scope, function (cloned) {
          element.html(cloned);
        });
      }
    };
  }]);