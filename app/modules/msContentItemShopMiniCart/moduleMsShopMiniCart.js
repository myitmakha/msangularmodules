'use strict';

angular.module('msContentItemShopMiniCart')
  .directive('msContentItemShopMiniBasket', ['BasketManager','Config', function (BasketManager,Config) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '=',
        contentItemMetaData: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemShopMiniCart/msShopMiniCart.tpl.html',
      link: function (scope, element, attrs) {
        var calculateTotal = function (items) {
          var total = 0;
          for (var i = 0; items && items[i]; i++) {
            var item = items[i];
            var subtotal = item.quantity * item.price;
            total += Math.round(subtotal * 100) / 100;
          }
          scope.total = Math.round(total * 100) / 100;
        };

        var calculateItems = function (items) {
          var totalItems = 0;
          for (var i = 0; items&& i < items.length; i++) {
            var item = items[i];
            totalItems += item.quantity;
          }
          scope.cartSize = totalItems;
        };

        scope.$watch('cart', function (items) {
          calculateTotal(items);
          calculateItems(items);
        }, true);

        BasketManager.listItems().then(function (cart) {
          scope.cart = cart;
        });

        scope.showCartAction = function () {
          scope.contentItemMetaData.setUrlParamsByPageType('shop','basket');
        };

        element.on('click', function () {
          var win = $(window).width();
          if (win <= 640) {
            scope.showCartAction();
            scope.$apply();
          }

        });

      }
    };
  }]);
