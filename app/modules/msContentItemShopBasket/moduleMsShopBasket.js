'use strict';

angular.module('msContentItemShopBasket')
  .directive('msContentItemShopBasket', ['$compile','OrderService','ShopService','BasketManager','Config','$window', function ($compile,OrderService,ShopService,BasketManager,Config,$window) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '=',
        contentItemMetaData: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemShopBasket/msShopBasket.tpl.html',
      link: function (scope, element, attrs) {

        //scope.showCheckoutWithPayPal = false;
        ShopService.GetShopCheckoutInfo().then(
          function (response) {
            //scope.showCheckoutWithPayPal = response.UseExpressCheckoutShortcut;
            scope.checkoutInfo = response;
            //console.log removed.
          }
        );

        scope.goToCheckout = function () {
          OrderService.createOrder(scope.cart).then(function (orderId) {
            if (orderId && orderId.status === 500) {
              return;
            }
            var checkoutEndpoint = Config.GetEndPoint('checkoutEndpoint');
            var siteId = Config.GetSessionStoredValue('siteId');
			      BasketManager.emptyBasket();
            $window.location.href = checkoutEndpoint + 'Checkout/Index/' + siteId + '/' + orderId;
          });

        };

        scope.checkoutWithPayPal = function () {
          OrderService.createOrder(scope.cart).then(function (orderId) {
            if (orderId && orderId.status === 500) {
              return;
            }
            var checkoutEndpoint = Config.GetEndPoint('checkoutEndpoint');
            var siteId = Config.GetSessionStoredValue('siteId');
			      BasketManager.emptyBasket();
            $window.location.href = checkoutEndpoint + 'Checkout/Ecs/' + siteId + '/' + orderId;
          });
        };

        scope.removeItem = function (item) {
          BasketManager.deleteItem(item.id);
        };

        scope.goToShop = function () {
          scope.contentItemMetaData.setUrlParamsByPageType('shop');
        };

        var calculateTotal = function (items) {
          var total = 0;
          for (var i = 0; items && items[i]; i++) {
            var item = items[i];
            var subtotal = item.quantity * item.price;
            total += Math.round(subtotal * 100) / 100;
          }
          scope.total = Math.round(total * 100) / 100;
        };

        scope.$watch('cart', function (items) {
          //console.log removed.
          calculateTotal(items);
        }, true);

        BasketManager.listItems().then(function (cart) {
          scope.cart = cart;
        });
      }
    };
  }]);
