'use strict';

angular.module('msPageBuilderManager').factory('PageBuilderManager', ['SiteRepository', '$q', function (SiteRepository, $q) {

  var websiteDataPromise;
  var currentPage;
  var editable = false;

  var getEditable = function () {
    var deferred = $q.defer();
    deferred.resolve(editable);
    return deferred.promise;
  };

  var setEditable = function (value) {
    var deferred = $q.defer();
    editable = value;
    deferred.resolve(editable);
    return deferred.promise;
  };

  var getWebsiteData = function (cachedSiteData) {

    var deferred = $q.defer();
    if (!websiteDataPromise) {
      if(cachedSiteData){
        var deferred2 = $q.defer();
        websiteDataPromise = deferred2.promise;
        deferred2.resolve(cachedSiteData);
      }else{
        websiteDataPromise = SiteRepository.getSite();
      }
    }
    websiteDataPromise.then(function (websiteData) {
      deferred.resolve(websiteData);
    });
    return deferred.promise;
  };

  var getSiteSettings = function () {
    var deferred = $q.defer();
    if (!websiteDataPromise) {
      websiteDataPromise = SiteRepository.getSite();
    }
    websiteDataPromise.then(function (websiteData) {
      deferred.resolve(websiteData.settings);
    });
    return deferred.promise;
  };

  var getLogoImage = function () {
    var deferred = $q.defer();
    if (!websiteDataPromise) {
      websiteDataPromise = SiteRepository.getSite();
    }
    websiteDataPromise.then(function (websiteData) {
      deferred.resolve(websiteData.logoImage);
    });
    return deferred.promise;
  };

  var getImages = function () {
    var deferred = $q.defer();
    if (!websiteDataPromise) {
      websiteDataPromise = SiteRepository.getSite();
    }
    websiteDataPromise.then(function (websiteData) {
      deferred.resolve(websiteData.images);
    });
    return deferred.promise;
  };

  var saveImages = function (images) {
    var deferred = $q.defer();
    getWebsiteData().then(function () {
      SiteRepository.updateSite({images: images}).then(function () {
        deferred.resolve('Saved');
      });
    });
    return deferred.promise;
  };

  var getCurrentPage = function () {
    var deferred = $q.defer();
    getWebsiteData().then(function () {
      deferred.resolve(currentPage);
    });
    return deferred.promise;
  };

  var getPages = function () {
    var deferred = $q.defer();
    getWebsiteData().then(function (websiteData) {

      deferred.resolve(websiteData.pages);
    });
    return deferred.promise;
  };

  var getCurrentPageContentBlocks = function () {
    var deferred = $q.defer();
    getWebsiteData().then(function (websiteData) {
      deferred.resolve(currentPage.pagecontentblocks);
    });

    return deferred.promise;
  };

  var setCurrentPage = function (currentPageId) {
    var deferred = $q.defer();
    getWebsiteData().then(function (websiteData) {
      currentPage = websiteData.pages.filter(function (page) {
        return String(page.url).substring(1).toLowerCase() === currentPageId;
      })[0];
      deferred.resolve(currentPage);
    });
    return deferred.promise;
  };

  var createNewPage = function(name, type) {
    var deferred = $q.defer();
    var page =  SiteRepository.createNewPage(name, type);
    deferred.resolve(page);
    return deferred.promise;
  };

  /* obsolete */
  var addPage = function (page) {
    var deferred = $q.defer();
    getWebsiteData().then(function (websiteData) {
      websiteData.pages.push(page);
      //TODO: Check that there is only one homepage
      saveWebsiteData().then(function () {

        deferred.resolve(page);
      });
    });

    //Save?
    return deferred.promise;
  };
  /* obsolete */

  var deletePage = function (page) {

    var deferred = $q.defer();
    getWebsiteData().then(function (websiteData) {
      //console.log removed.
      var indexPage = -1;
      angular.forEach(websiteData.pages, function (p, i) {
        if (page.id === p.id) {
          indexPage = i;
        }
      });

      if (indexPage >= 0) {
        //Actually delete the page from websiteData
        websiteData.pages.splice(indexPage, 1);
        //Check that there is still one homepage
        if (page.ishomepage && websiteData.pages[0]) {
          websiteData.pages[0].ishomepage = true;
        }
      }

      //console.log removed.
      //Save?
      saveWebsiteData().then(function () {
        saveMenu();
        deferred.resolve();
      });
    });
    return deferred.promise;
  };

  var saveWebsiteData = function () {
    var deferred = $q.defer();
    getWebsiteData().then(function (websiteData) {
      SiteRepository.updateSite({pages: websiteData.pages}).then(function () {
        deferred.resolve('Saved');
      });
    });
    return deferred.promise;
  };

  var saveMenu = function(){
    var deferred = $q.defer();
    getWebsiteData().then(function (websiteData) {
    });
    return deferred.promise;
  };

  var saveLogo = function (logoImage) {
    var deferred = $q.defer();
    getWebsiteData().then(function () {
      SiteRepository.updateSite({logoImage: logoImage}).then(function () {
        deferred.resolve('Saved');
        //websiteDataPromise=undefined;
      });
    });
    return deferred.promise;
  };

  var getCurrentTheme = function () {
    var deferred = $q.defer();
    if (!websiteDataPromise) {
      websiteDataPromise = SiteRepository.getSite();
    }
    websiteDataPromise.then(
      function (siteData) {
        var themeId = siteData.defaultTheme;
        var i = 0;
        while (siteData.themes[i]) {
          if (siteData.themes[i].id === themeId) {
            deferred.resolve(siteData.themes[i]);
            return;
          }
          i++;
        }
        deferred.resolve();
      }
    );
    return deferred.promise;
  };

  var saveContentSnapShot = function (domainname, path, pagesource) {
    var deferred = $q.defer();
    SiteRepository.saveContentSnapShot({domainname: domainname, path: path, pagesource: pagesource}).then(
      function () {deferred.resolve('Saved');}
    );
    return deferred.promise;
  };

  var getSiteId = function () {
    var deferred = $q.defer();
    if (!websiteDataPromise) {
      websiteDataPromise = SiteRepository.getSite();
    }
    websiteDataPromise.then(function (websiteData) {
      deferred.resolve(websiteData.siteId);
      return websiteDataPromise;
    });
    return deferred.promise;
  };

  var themeUpdatesDataPromise;

  var getThemeUpdates = function () {
    if (!themeUpdatesDataPromise) {
      themeUpdatesDataPromise = SiteRepository.getThemeUpdates();
    }
    return themeUpdatesDataPromise;
  };

  var updateTheme = function (themeId) {
    themeUpdatesDataPromise = undefined;
    return SiteRepository.updateTheme(themeId);
  };

  var myThemesDataPromise;
  var getMyThemes = function () {
    if (!myThemesDataPromise) {
      myThemesDataPromise = SiteRepository.getMyThemes();
    }
    return myThemesDataPromise;
  };

  var installTheme = function (themeId) {
    myThemesDataPromise = undefined;
    websiteDataPromise=undefined;
    return SiteRepository.installTheme(themeId);
  };

  var setThemeToLive = function (id) {
    myThemesDataPromise = undefined;
    return SiteRepository.setThemeToLive(id);
  };

  var deleteMyThemeById = function (id) {
    myThemesDataPromise = undefined;
    return SiteRepository.deleteMyThemeById(id);
  };

  var pageBuilderManager = {
    createNewPage: createNewPage,
    getSiteId:getSiteId,
    getWebsiteData: getWebsiteData,
    getLogoImage: getLogoImage,
    getCurrentPage: getCurrentPage,
    getCurrentPageContentBlocks: getCurrentPageContentBlocks,
    setCurrentPage: setCurrentPage,
    addPage: addPage,
    deletePage: deletePage,
    saveMenu: saveMenu,
    saveWebsiteData: saveWebsiteData,
    saveLogo: saveLogo,
    getPages: getPages,
    getEditable: getEditable,
    setEditable: setEditable,
    getCurrentTheme: getCurrentTheme,
    getImages: getImages,
    saveImages: saveImages,
    getSiteSettings: getSiteSettings,
    saveContentSnapShot: saveContentSnapShot,
    getThemeUpdates:getThemeUpdates,
    updateTheme:updateTheme,
    getMyThemes:getMyThemes,
    installTheme:installTheme,
    setThemeToLive:setThemeToLive,
    deleteMyThemeById:deleteMyThemeById
  };

  return pageBuilderManager;
}]);
