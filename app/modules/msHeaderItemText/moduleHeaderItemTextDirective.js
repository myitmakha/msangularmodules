'use strict';

angular.module('msHeaderItemText')

  .directive('msHeaderItemText', ['$compile', '$sce', '$filter', '$timeout', function ($compile, $sce, $filter, $timeout) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        msHeaderItemText: '=',
        editable: '=',
        page: '=',
        fitText: '='
      },
      link: function (scope, element, attrs) {
        var page = scope.page || scope.$parent.page || {};
        page.pageTexts = page.pageTexts || {};
        page.pageTexts[scope.msHeaderItemText] = page.pageTexts[scope.msHeaderItemText] || {value: element.html()};
        scope.text = page.pageTexts[scope.msHeaderItemText];

        if (scope.editable) {
          element.attr('contentEditable', true);
        }

        var html = '<span>'+ $filter('htmlToPlainText')(scope.text.value) +'</span>';

        $timeout(function(){

          var origSize = element.css('font-size');

          element.attr('data-original-size', origSize);

        }, 10);

        var fitText = function( elem, parent ){

          $timeout(function() {

            var minFontSize = 11;

            var reset = function(){
              elem.attr('style', '');
            };

            var getWidths = function(){
              var elWidth = elem[0].scrollWidth,
                parentWidth = elem.parents(parent).width();

              return {
                parentEl: parentWidth,
                el: elWidth
              };
            };

            var checkOverflow = function( ){
              var e = getWidths();
              //3 >= Math.abs(e.el - e.parentEl) && Math.min(e.el, e.parentEl);
              return e.el > e.parentEl;
            };

            var adjustText = function(){

              reset();

              var origSize = parseInt( elem.css('font-size')),
                e = getWidths();

              if(checkOverflow()){
                setSize( Math.floor(origSize *= ( e.parentEl / e.el )) );
              }
            };

            var setSize = function( size ){
              if( size > minFontSize ){
                elem.css( 'font-size', size );
              } else {
                elem.css( 'font-size', minFontSize );
              }
            };

            adjustText();

          }, 100);

        };

        var debounceFitText = _.debounce(function(){
          fitText(element, scope.fitText);
        }, 200);

        $timeout( function(){
          if (scope.fitText) {
            fitText(element, scope.fitText);

            $(window).on('resize', function(){
              debounceFitText();
            });
          }
        }, 200);

        $compile( html )(scope, function (html) {
          element.html(html);
        });

        if (!scope.editable) {
          scope.trustedHTML = $sce.trustAsHtml(scope.text.value);
        }

        $(element).on('blur keyup paste', function(){

          scope.$apply(function () {
            scope.text.value = element.text();
          });

          if (scope.fitText) {
            //console.log removed.
            fitText(element, scope.fitText);
          }

        });
      }
    };
  }]);
