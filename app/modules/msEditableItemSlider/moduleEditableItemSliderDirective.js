'use strict';

angular.module('msEditableItemSlider')
  .directive('editableItemSliderDirective', ['$compile', function ($compile) {
    return {
      scope: {
        editable: '='
      },
      template: '<span class="label">{{editable.name}}</span><div slider-directive ng-model="editable.value" min="editable.Settings.min" max="editable.Settings.max" step="editable.Settings.step"></div>',
      restrict: 'A',
      link: function (scope, element, attrs) {
      }
    };
  }]);

angular.module('msEditableItemSlider')
  .directive('sliderDirective', ['$compile', function ($compile) {
    return {
      require:'ngModel',
      scope: {
        ngModel: '=',
        min: '=',
        max: '=',
        step: '='
      },
      template: '<div class="slider"></div>',
      restrict: 'A',
      link: function (scope, element, attrs, ngModelCtrl) {
        var slider = $('.slider', element);
        slider.slider({
          min: (scope.min||0)*1,
          max: (scope.max||100)*1,
          step: (scope.step||1)*1,
          value: 0,
          range: 'min',
          slide: function (event, ui) {
            //scope.ngModel = ui.value;
            ngModelCtrl.$setViewValue(ui.value);
            scope.$parent.$root.$broadcast('markChanges', 'Slider changed');
            scope.$apply();
          }
        });

        var unit='';

        ngModelCtrl.$formatters.push(function(modelValue) {
          if(typeof modelValue === 'undefined'){
            return modelValue;
          }
          var myRegexp = /([0-9]+(?:\.[0-9]+)?)(.*)/;
          var matches = myRegexp.exec(modelValue);
          unit=matches[2];
          return matches[1];
        });

        ngModelCtrl.$render = function() {
          slider.slider('value',ngModelCtrl.$viewValue);
        };

        ngModelCtrl.$parsers.push(function(viewValue) {
          return viewValue + unit;
        });
      }
    };
  }]);