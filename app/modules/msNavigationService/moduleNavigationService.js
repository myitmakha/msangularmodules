'use strict';

angular.module('msNavigationService').provider('NavigationService', function () {

  var navigationManager = {},
    navigationItems = [],
    currentPathArray;

  navigationManager.addNavigationItem = function (position, navigationItem) {
    if (typeof navigationItems[position] !== 'undefined') {
      var errStr = 'Cannot add a navigation item in a position that has already been used';
      errStr += '\nPosition: ' + String(position);
      errStr += '\nCurrent navigationItem.name: ' + navigationItems[position].name;
      errStr += '\nNew navigationItem.name: ' + navigationItem.name;
      throw new Error(errStr);
    }
    navigationItems[position] = navigationItem;
  };

  navigationManager.getNavigationItems = function () {
    return navigationItems;
  };

  navigationManager.setCurrentPath = function (currentPath) {
    currentPathArray = currentPath.split('/').slice(1);
  };

  navigationManager.isActive = function (item) {
    if (item === undefined) {
      return;
    }

    var isActive;

    if (item === currentPathArray[0] || item === currentPathArray[0] + '/' + currentPathArray[1]) {
      isActive = 'active';
    }

    return isActive;
  };

  navigationManager.$get = function () {
    //_$q = $q;
    return navigationManager;
  };

  return navigationManager;

});