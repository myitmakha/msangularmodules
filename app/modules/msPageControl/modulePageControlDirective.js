'use strict';

angular.module('msPageControl')
  .directive('msPageControl', ['Config', '$rootScope', '$location', 'PageBuilderManager', 'msCurrentViewInfoManager', function (Config, $rootScope, $location, PageBuilderManager, msCurrentViewInfoManager) {
    return {
      replace:true,
      restrict: 'E',
      scope: {
        siteData: '='
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msPageControl/pageControl.tpl.html',
      link: function (scope, element, attrs) {

        scope.everything = msCurrentViewInfoManager.getEverything();

        scope.$watch('everything.value.page',function(page){
          scope.page = page;
        });

        var resetAllSettings = function(){
          angular.forEach(scope.items,function(item){
            item.openSettings=false;
          });
        };

        var listOverlay = element.find('.page-list-overlay'),
            listWrap = element.find('.page-list'),
            currentPage = element.find('.page-name');

        currentPage.on('click', function() {
          resetAllSettings();
          scope.$apply();
          listWrap.addClass('active');
          listWrap.removeClass('hidden');
          element.find('.scroll-wrapper, .pages').css({
            'max-height': ( $(window).outerHeight() - $('body').find('.ms-topbar').outerHeight() - element.siblings('.actions').outerHeight() ) - 80
          });
          listOverlay.addClass('active');

        });

        listOverlay.on('click', function(e) {

          if (!$(e.target).is('.page-list')) {
            listWrap.removeClass('active');
            listWrap.addClass('hidden');
            listOverlay.removeClass('active');
          }
        });

      }
    };
  }]);