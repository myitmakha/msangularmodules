'use strict';

angular.module('msUpload')
  .directive('msUpload', ['$timeout', '$sce', '$window', '$compile', 'Config', '$rootScope', function ($timeout, $sce, $window, $compile, Config, $rootScope) {
    return {
      restrict: 'A',
      replace: false,
      scope: {
        files: '=msUpload',
        title: '@msTitle',
        enableMultiple: '=msEnableMultiple',
        uploadStyle: '@msUploadStyle',
        buttonText: '@msButtonText',
        fileType: '@msFileType'
      },

      link: function (scope, elm) {

        //console.log removed.


        scope.fileLoadingError = false;
        scope.fileLoadingErrorMessage = 'There was a problem uploading your files.';

        var header = {'Authorization': 'Bearer ' + $window.sessionStorage.token};

        //Override the url where is going to upload the picture
        scope.$on('fileuploadadd', function (e, data) {
          data.headers = header;
          data.url = Config.GetEndPoint('mediaApiEndpoint') + 'sites/' + $window.sessionStorage.siteId + '/' + scope.fileType;

          //console.log removed.

          scope.$parent.fileLoadingClass = true;
          scope.$parent.fileLoadingError = false;
        });

        var timer;
        //always runs
        scope.$on('fileuploadstop', function () {
          scope.$parent.fileLoadingClass = false;
        });

        //runs on error
        scope.$on('fileuploadfail', function (e, data) {
          //error message

          var result = data.result;
          // this is a temporary fix until the Media Api is made to return more meaningful error codes (not just a bloody 500!!)
          if (result&&result.message === 'Error reading MIME multipart body part.') {
            scope.fileLoadingErrorMessage += '<br/> The selected image was too big.';
          }
          scope.fileLoadingError = true;

        });

        //runs on file drag over
        scope.$on('fileuploaddragover', function () {
          $timeout(function () {
            scope.$parent.fileDragOverClass = 'file-drag-over';
          });
          $timeout.cancel(timer);
          timer = $timeout(function () {
            scope.$parent.fileDragOverClass = false;
          }, 100);
        });

        //runs on file drop over
        scope.$on('fileuploaddrop', function () {
          scope.$parent.fileDragOverClass = false;
        });

        // Listen to fileuploaddone event
        scope.$on('fileuploaddone', function (e, data) {
          var uploadedImages = data._response.result;
          var i = 0;
          scope.files = [];
          while (uploadedImages[i]) {
            scope.files.push({url: uploadedImages[i].location});
            i++;
          }
          $rootScope.$broadcast('modalClose');
        });

        scope.clickUploadButton = function (e) {
          var elem = angular.element(e.currentTarget),
            elemParent = elem.parent();
          $('.image-upload', elemParent).click();
        };

        if(scope.files !== undefined){
          if(scope.files.length === 0 || !scope.files[0].url){
            $timeout(
              function(){
                elm.find('.upload-wrap').addClass('no-images');
              },200
            );
          }
        }



        // Code from https://github.com/blueimp/jQuery-File-Upload/wiki/Drop-zone-effects to initialize dropzone hovers on blueimp. Maybe a better implementation?

        $(document).bind('dragover', function (e) {
          var dropZone = $('.dropZone'),
            timeout = window.dropZoneTimeout;
          if (!timeout) {
            dropZone.addClass('in');
          } else {
            clearTimeout(timeout);
          }
          var found = false,
            node = e.target;
          do {
            if (node === dropZone[0]) {
              found = true;
              break;
            }
            node = node.parentNode;
          } while (node !== null);
          if (found) {
            dropZone.addClass('hover');
          } else {
            dropZone.removeClass('hover');
          }
          window.dropZoneTimeout = setTimeout(function () {
            window.dropZoneTimeout = null;
            dropZone.removeClass('in hover');
          }, 100);
        });

        scope.getTemplate = function () {
          if (scope.enableMultiple) {
            return 'bower_components/msUpload/moduleUpload' + scope.uploadStyle + '-multiple' + '.html';
          } else {
            return 'bower_components/msUpload/moduleUpload' + scope.uploadStyle + '-single' + '.html';
          }
        };

      },
      template: '<div class="upload-wrap" ng-include="getTemplate()"></div>'
    };
  }]);
