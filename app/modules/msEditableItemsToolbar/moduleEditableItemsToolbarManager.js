'use strict';

angular.module('msEditableItemsToolbar')
  .factory('EditableItemsToolbarManager', ['$q', '$compile', 'ThemeRepository', function ($q, $compile, ThemeRepository) {
    var getEditablesFromSettings = function () {
      var deferred = $q.defer();
      deferred.resolve([
        {name: 'bgColor', type: 'color', value: '#555'},
        {name: 'fontSize', type: 'slider', value: '12'},
        {name: 'logoImage', type: 'image', value: 'http://uk.mrsite.com/assets/img/site-logo.png'},
        {name: 'backgroundImage', type: 'image', value: 'http://www.lichtjagd.de/media/com_twojtoolbox/castell-de-bellver.jpg'},
        {name: 'defaultFontFamily', type: 'font-family', value: 'Arial'}
      ]);
      return deferred.promise;
    };
    return {
      getEditablesFromSettings: getEditablesFromSettings
    };
  }]);