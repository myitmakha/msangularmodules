'use strict';

angular.module('msEditableItemsToolbar')
  .directive('editableItemsToolbarDirective', ['LayoutService', '$compile', 'TemplateRepository', '$timeout','$rootScope', function (LayoutService, $compile, TemplateRepository, $timeout,$rootScope) {
    return {
      template: '<div ng-hide="(editablesInGroup | filter: {hidden:false}).length==0" class="editable-group group-{{group}}" ng-init="visibleItems=0" ng-repeat="(group, editablesInGroup) in groupedEditables">'+
                  '<div class="group-title">{{group}}</div>'+
                  '<div class="editable-item" ng-hide="editable.hidden" ng-class="" ng-repeat="editable in editablesInGroup" editable-item-directive editable="editable"></div>'+
                '</div>',
      restrict: 'A',
      scope: {
        themeId: '=',
        control: '='
      },
      link: function (scope, element, attrs) {
        var editablesDirty=false;
        $rootScope.$on('editableChanged',function(){
          editablesDirty=true;
        });

        scope.control = scope.control || {};

        var isItDirty = function (editables) {
          if(editablesDirty){
            editablesDirty=false;
            return true;
          }
          return false;
        };
        scope.$watch('control',function(control){
          if(!control){
            return;
          }
          control.save = function () {
            if (!isItDirty(scope.editables)) {
              //console.log removed.
              return;
            }
            TemplateRepository.setTemplateSettings(scope.themeId, scope.editables).then(function (editables) {
              //console.log removed.
            });
          };
        });

        var groupEditables = function(editables){
          //console.log removed.
          var groupedEditables = {};
          for(var i =0;editables[i];i++){
            var editable = editables[i];
            if(!editable.Group){
              editable.Group = 'Default';
            }
            var group = editable.Group;
            if(!groupedEditables[group]){
              groupedEditables[group]=[];
            }
            groupedEditables[group].push(editable);
          }
          return groupedEditables;
        };


        TemplateRepository.getTemplateSettings(scope.themeId).then(function (editables) {
          scope.editables = editables.settings;
          scope.groupedEditables = groupEditables(scope.editables);
        });
      }
    };
  }]);