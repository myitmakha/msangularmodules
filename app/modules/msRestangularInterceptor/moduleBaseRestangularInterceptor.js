'use strict';

angular.module('msRestangularInterceptor')
  .factory('BaseRestangularInterceptor', ['BaseApiRestangular',function (BaseApiRestangular) {
    var setInterceptors = function(restangular,responseSuccessInterceptor,responseErrorInterceptor){
      restangular.addResponseInterceptor(responseSuccessInterceptor);
      restangular.setErrorInterceptor(responseErrorInterceptor);
    };

    return {
      setInterceptors:setInterceptors
    };
  }]);
