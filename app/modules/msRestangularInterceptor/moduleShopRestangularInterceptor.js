'use strict';

angular.module('msRestangularInterceptor')
  .service('ShopRestangularInterceptor', ['ShopRestangular','BaseRestangularInterceptor',function (ShopRestangular,BaseRestangularInterceptor) {
    var responseSuccessInterceptor = function(data, operation, what, url, response, deferred){
      return data;
    };
    var responseErrorInterceptor = function(response, deferred, responseHandler){
      return true;
    };
    BaseRestangularInterceptor.setInterceptors(ShopRestangular, responseSuccessInterceptor,responseErrorInterceptor);

    return {};
  }]);