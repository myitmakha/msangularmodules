'use strict';

angular.module('msDiscussionRepository').factory('DiscussionRepository', ['DiscussionRestangular', '$q', function (DiscussionRestangular, $q) {

  var addNewComment = function (discussionId, comment) {
    var deferred = $q.defer();
    DiscussionRestangular.all(discussionId + '/comments').post(comment).then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var convertCommentDates = function(comment){
    if(!comment){
      return;
    }
    comment.ModeratedOn=new Date(comment.ModeratedOn);
    comment.Created=new Date(comment.Created);
    comment.LastModified=new Date(comment.LastModified);
  };

  var updateComment = function (discussionId, comment) {
    var deferred = $q.defer();
    DiscussionRestangular.all(discussionId + '/comments').customPUT(comment).then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var deleteComment = function(discussionId, commentId){
    var deferred = $q.defer();
    DiscussionRestangular.one(discussionId + '/comments').customDELETE(commentId).then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var updateCommentModeration = function (discussionId, commentId, moderation) {
    var deferred = $q.defer();
    DiscussionRestangular.all(discussionId + '/comments/' + commentId + '/moderation').customPUT(moderation).then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getAllComments = function (discussionId) {
    var deferred = $q.defer();
    DiscussionRestangular.one(discussionId, 'comments').get().then(
      function (response) {
        var comments = [];
        if (response && response.data && response.data.ResourceList) {
          comments = response.data.ResourceList;
          for(var i=0;comments[i];i++){
            convertCommentDates(comments[i]);
          }
        }
        deferred.resolve(comments);
      }
    );
    return deferred.promise;
  };

  var discussionRepository = {
    addNewComment: addNewComment,
    updateComment: updateComment,
    deleteComment: deleteComment,
    updateCommentModeration: updateCommentModeration,
    getAllComments: getAllComments

  };

  return discussionRepository;
}]);
