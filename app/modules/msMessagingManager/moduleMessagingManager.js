'use strict';

angular.module('msMessagingManager').factory('MessagingManager', ['MessagingRestangular', '$q', 'SiteRepository', function (MessagingRestangular, $q, SiteRepository) {

  var sendMessage = function (formInfo) {
    return SiteRepository.getSiteId().then(function (siteId) {
      formInfo.siteId = siteId;
      return MessagingRestangular.all('').customPOST(formInfo);
    });
  };

  var messagingManager = {
    sendMessage: sendMessage
  };

  return messagingManager;
}]);
