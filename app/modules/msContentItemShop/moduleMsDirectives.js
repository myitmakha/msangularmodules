'use strict';

angular.module('msContentItemShop')
  .filter('paginate', function(){
    return function(items, pageData){
        var arrayToReturn = [];
        if(!items){
          return arrayToReturn;
        }
        pageData=pageData||{};
        pageData.page=pageData.page||0;
        pageData.pageSize=pageData.pageSize||6;
        var from = pageData.page*pageData.pageSize;
        var to = from + pageData.pageSize;
        pageData.pages = [];
        var maxPage = Math.ceil(items.length/pageData.pageSize);
        pageData.page = Math.max(0,pageData.page);
        pageData.page = Math.min(maxPage-1,pageData.page);
        for(var j=0;j<maxPage;j++){
          pageData.pages.push(j);
        }
        for (var i=from; items[i] && i<to; i++){
          arrayToReturn.push(items[i]);
        }
        return arrayToReturn;
      };
  });


angular.module('msContentItemShop')
  .directive('msShopAddToCart', ['BasketManager', 'Config', function (BasketManager, Config) {
    return {
      restrict: 'AE',
      replace: true,
      scope: {
        product: '=',
        variant: '=',
        disabled: '=',
        showRemove: '@'
      },
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msContentItemShop/msShopAddToCart.tpl.html',
      link: function (scope, element, attrs) {

        var checkForItem = function () {
          //console.log removed.
          if (!scope.product) {
            scope.item = scope.product;
            return;
          }

          var productId = scope.product.id; //Depending on the scope we are in

          if (scope.variant && scope.variant.VariantId) {
            productId = scope.variant.VariantId;
          }
          if(productId){
            scope.item = BasketManager.getItemById(productId);
            scope.showVariantMessage = false;
          }else{
            scope.showVariantMessage = true;
          }
        };

        scope.removeFromBasket = function (item) {
          BasketManager.deleteItem(item.id);
        };

        scope.$watch('product', function () {
          checkForItem();
        });

        scope.$watch('variant', function () {
          //console.log removed.
          checkForItem();
        });

        scope.$watch('item.quantity', function (quantity, before) {
          if (quantity === 0 && before !== quantity) {
            checkForItem();
          }
        });


        scope.addToCart = function (product, quantity) {
          if (!quantity) {
            quantity = 1;
          }
          BasketManager.addItem(scope.variant.VariantId, product.Name, product.Images[0], quantity, scope.variant.Price, scope.variant.AttributeOptions).then(function () {
            checkForItem();
          });

        };
      }
    };
  }]);