'use strict';

angular.module('msContentItemShop')
  .directive('msContentItemShopCategory', ['$compile','ShopService','Config', function ($compile,ShopService,Config) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        contentItemData: '=',
        contentItemMetaData: '='
      },
      template:'<div></div>',

      link: function (scope, element) {


        var getDirective = function(urlParameters){
          var param1 = urlParameters.param1;

          var directive = 'ms-content-item-shop-product-list';
          switch(param1){
            case 'products':
              scope.$emit('maincontentItem',{context:'shop', type:'product',id:urlParameters.param2});
              directive = 'ms-content-item-shop-product';
              break;
            case 'basket':
              scope.$emit('maincontentItem',{context:'shop', type:'basket'});
              directive = 'ms-content-item-shop-basket';
              break;
            case 'categories':
              scope.$emit('maincontentItem',{context:'shop', type:'category',id:urlParameters.param2});
              directive = 'ms-content-item-shop-product-list';
              break;
            default:
              //scope.$emit('maincontentItem',{context:'shop', type:'shop'});
              directive = 'ms-content-item-shop-product-list';
              break;
          }
          return '<div><div ng-hide="!contentItemMetaData.loading" style="position: absolute;margin-left: 50%;margin-top: 10%;">Loading...</div><div '+directive+' content-item-data="contentItemData" content-item-meta-data="contentItemMetaData"></div></div>';
        };

        var checkforCss = function () {
          var shopCss = Config.GetEndPoint('modulesDirectory') + '/msContentItemShop/shop.css';
          if (!$('link[href="' + shopCss + '"]').length) {
            $('head').append('<link href="' + shopCss + '" type="text/css" rel="stylesheet" />');
          }
        };

        var setShopSettings = function () {
          ShopService.GetShop().then(function (shop) {
            var symbol;
            if (shop.Currency && shop.Currency.Symbol) {
              symbol = shop.Currency.Symbol;
            }
            var currency = {symbol: symbol || '£'};
            ShopService.SetCurrency(currency);
            loadDirective();
          });
        };

        var loadDirective = function(){
          $compile(getDirective((scope.contentItemMetaData||{}).urlParams))(scope, function (compiled) {
            element.html(compiled);
          });
        };

        checkforCss();
        setShopSettings();
      }
    };
  }]);
