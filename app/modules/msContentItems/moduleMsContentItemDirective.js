'use strict';
angular.module('msContentItems')
  .directive('msContentItem', ['PageBuilderManager','ResourceUrlManager','$compile', '$timeout', '$injector', 'msContentItemsHelper','LayoutServiceFactoryHelper', function (PageBuilderManager,ResourceUrlManager, $compile, $timeout, $injector, msContentItemsHelper,LayoutServiceFactoryHelper) {
    return {
      restrict: 'AEC',
      replace: true,
      scope: {
        contentItemData: '=',
        editable: '='
      },
      link: function (scope, element, attrs) {
        var baseMsDirectiveName = 'ms-content-item-' + scope.contentItemData.type;

        var editDirectiveName = 'msContentItem' + msContentItemsHelper.convertToCamelCase(scope.contentItemData.type) + 'EditDirective';
        var settingsDirectiveName = 'msContentItem' + msContentItemsHelper.convertToCamelCase(scope.contentItemData.type) + 'SettingsDirective';
        var settingsOverlay = $('body').find('.settingsOverlay');
        var settingsPanelFunc = function(){return $('body').find('.settingsPlaceHolder');};
        var topBar = $('body').find('.ms-topbar');
        var sideBar = $('body').find('.ms-sidebar');

        settingsOverlay.on('click', function () {
          scope.ok();
        });

        scope.level = attrs.level || '1';

        scope.contentItemData.settings = scope.contentItemData.settings || {bla: 'asd'};
        scope.contentItemMetaData = scope.contentItemMetaData || {};
        //If it is the main content item
        scope.contentItemMetaData.urlParams = ResourceUrlManager.getAllParameters();
        scope.contentItemMetaData.setUrlParams = function(pageId,param1,param2,param3,param4){
          ResourceUrlManager.setUrlParameters({
            pageId:pageId,
            param1:param1,
            param2:param2,
            param3:param3,
            param4:param4
          });
        };
        scope.contentItemMetaData.setUrlParamsByPageType = function(pageType,param1,param2,param3,param4){
          PageBuilderManager.getPages().then(function(pages){
            if(!pages){
              return;
            }
            for(var i=0;pages[i];i++){
              //console.log removed.
              if(pages[i].type===pageType){
                var url = pages[i].url;
                if(url[0]==='/'){
                  url=url.substr(1);
                }
                ResourceUrlManager.setUrlParameters({
                  pageId:url,
                  param1:param1,
                  param2:param2,
                  param3:param3,
                  param4:param4
                });
                return;
              }
            }
          });
        };
        var renderContentItem = function (editable) {
          var msDirectiveName = baseMsDirectiveName;
          if (editable) {
            if ($injector.has(editDirectiveName)) {
              msDirectiveName += '-edit';
            }
          }
          var htmlEle = '<div ms-content-item-controls actions="actions" class="ms-block {{contentItemData.type}}-block ms-level-{{level}}" data-id="{{contentItemData.id}}"><div ' + msDirectiveName + ' content-item-data="contentItemData" content-item-meta-data="contentItemMetaData"></div></div>';
          var ele = angular.element(htmlEle);
          $compile(ele)(scope, function (cloned) {
            element.html(cloned);
          });
        };

        var disableMove = baseMsDirectiveName.indexOf('-shop-category')>0;
        var disableDelete = baseMsDirectiveName.indexOf('-shop-category')>0;

        scope.actions = scope.actions || {};
        scope.actions.controls = [];
        if(!disableDelete){
          scope.actions.controls.push('delete icon-delete');
        }
        if ($injector.has(settingsDirectiveName)) {
          scope.actions.controls.push('settings icon-settings');
        }
        if(!disableMove){
          scope.actions.controls.push('handle icon-drag');
        }

        var settingsPanelPosition = function () {

          var settingsPanel = settingsPanelFunc();

          var win = $(window),
            settingButton = element.find('.element-control.settings'),
            settingButtonOffset = settingButton.offset(),

            viewport = {
              top : win.scrollTop(),
              left : win.scrollLeft()
            },

            overlap,

            panelPositionTop = settingButtonOffset.top - 20,
            panelPositionLeft = ( settingButton.offset().left - settingsPanel.outerWidth() ) - 10;

          viewport.right = viewport.left + win.width();
          viewport.bottom = viewport.top + win.height();

          settingsPanel.css({
            'top': panelPositionTop,
            'left': panelPositionLeft
          });


          var elementBottom = settingsPanel.position().top + settingsPanel.outerHeight();

          if ( elementBottom > viewport.bottom ) {
            overlap = ( elementBottom - viewport.bottom ) + 20;

            settingsPanel.css({
              'top': panelPositionTop - overlap
            });
          }

          if ( panelPositionTop < (viewport.top + topBar.outerHeight()) ) {

            overlap = ( (viewport.top + topBar.outerHeight()) - panelPositionTop ) + 20;

            settingsPanel.css({
              'top': panelPositionTop + overlap
            });
          }

          if ( panelPositionLeft < (viewport.left + sideBar.outerWidth()) ) {

            overlap = ( (viewport.left + sideBar.outerWidth()) - panelPositionLeft ) + 20;

            settingsPanel.css({
              'left': panelPositionLeft + overlap
            });
          }
        };

        scope.actions.displaySettings = function () {

          var settingsPanel = settingsPanelFunc();
          scope.backupSettings = angular.copy(scope.contentItemData.settings);
          var actionButtons = '<div class="settings-buttons">' +
            '<div class="ms-buttons">' +
            '<button ng-click="ok()" class="ms-button primary">Ok</button>' +
            '<button ng-click="cancel()" class="ms-button danger">Cancel</button>' +
            '</div>' +
            '</div>';

          //console.log removed.

          var contentItemName = LayoutServiceFactoryHelper.getWidgetByType(scope.contentItemData.type);

          var msDirectiveName = baseMsDirectiveName + '-settings';
          var htmlEle = '<div><div class="settings-title">'+ contentItemName +'<i class="icon-reorder"></i></div><div class="settings-wrapper" ms-scrollbar-plus><div class="settings-mask"><div ' + msDirectiveName + ' settings="contentItemData.settings"></div></div></div>' + actionButtons + '</div>';
          var ele = angular.element(htmlEle);

          $('body').find('.ms-block').removeClass('active');

          $compile(ele)(scope, function (cloned) {

            settingsPanel.html(cloned);
            //$('body').addClass('ov-hidden');



            var handle = settingsPanel.find('.settings-title');

            //console.log removed.

            settingsPanel.draggable({
              handle: handle
            });


            var settingsPanelTitle = settingsPanel.find('.settings-title'),
              settingsPanelButtons = settingsPanel.find('.settings-buttons'),
              settingsPanelInner = settingsPanel.find('.settings-wrapper');

            settingsPanelInner.attr(
              'style', 'max-height: ' + ( ( ( $(window).outerHeight() - ( topBar.outerHeight() ) - 40) - settingsPanelButtons.outerHeight() ) - settingsPanelTitle.outerHeight() ) + 'px !important;'
            );

            var interval = setInterval(function(){
              if(settingsPanel.outerHeight()>100){
                clearInterval(interval);
                settingsPanelPosition();

                settingsPanel.addClass('active ' + scope.contentItemData.type);
                settingsOverlay.addClass('active');
                element.find('.ms-block').addClass('active');
              }
            },50);

          });

        };

        var closePopUp = function() {
          var settingsPanel = settingsPanelFunc();
          settingsPanel.removeClass('active ' + scope.contentItemData.type);
          settingsOverlay.removeClass('active').removeAttr('style');
          element.find('.ms-block').removeClass('active');
          settingsPanel.removeAttr('style');
          $('body').removeClass('ov-hidden');
        };

        scope.ok = function () {
          var settingsPanel = settingsPanelFunc();
          settingsPanel.removeClass('active ' + scope.contentItemData.type);
          closePopUp();
          //scope.$parent.$root.$broadcast('markChanges', 'settings changed');
        };

        scope.cancel = function () {
          scope.contentItemData.settings = scope.backupSettings;
          closePopUp();
        };

        renderContentItem(scope.editable);

      }
    };
  }]);
