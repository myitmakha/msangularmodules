/**
 * Created by Albert on 15/07/2014.
 */

'use strict';
angular.module('msContentItems')
  .factory('msContentItemsHelper', [ function () {
    var capitaliseFirstLetter = function (string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    };

    var convertToCamelCase = function (str) {
      var parts = str.split('-');
      var result = '';
      for (var i = 0; i < parts.length; i++) {
        result += capitaliseFirstLetter(parts[i]);
      }
      return result;
    };

    return {
      convertToCamelCase: convertToCamelCase
    };
  }]);
