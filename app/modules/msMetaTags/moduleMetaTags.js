'use strict';

angular.module('msMetaTags')

  .directive('metaTags', ['$compile','MetaTagsManager', 'Config', function ($compile,MetaTagsManager, Config) {
    return {
      scope: {
        metaTags: '='
      },
      restrict: 'AE',
      templateUrl: Config.GetSessionStoredValue('modulesDirectory') + '/msMetaTags/msMetaTags.tpl.html',
      link: function postLink(scope, element, attrs) {

        scope.getMetaTagType = MetaTagsManager.getMetaTagType;

        scope.delete = function(index){
          scope.metaTags.splice(index, 1);
        };

        scope.add = function(){
          var metaTag = MetaTagsManager.parseMetaTag(scope.newMetaTag);
          if(!metaTag){
            scope.error=true;
          }else{
            scope.error=false;
            scope.newMetaTag='';
            scope.metaTags.push(metaTag);
          }
        };
        scope.metaTags=scope.metaTags||[];
      }
    };
  }])

  .factory('MetaTagsManager',[function(){
    var listKnownAttributes = {
      'google-site-verification': 'Google verification',
      'p:domain_verify': 'Pinterest verification'
    };

    var getMetaTagType = function(metatag){
      if(!metatag){
        return '';
      }
      if(metatag.name){
        if(listKnownAttributes[metatag.name]){
          return listKnownAttributes[metatag.name];
        }else{
          return metatag.name;
        }
      }
      if(metatag.attributes&&metatag.attributes[0]){
        return metatag.attributes[0].name;
      }
      return '';
    };

    var parseMetaTag = function(html){
      var metaTag;
      var result = html.match(/^\s*<meta(\s*([^="'\s]+)=("|')([^'"]+)("|'))*\s*\/>\s*$/);
      if(result){
        var variables = html.match(/([^="'\s]+)=("|')([^'"]+)("|')/g);
        if(variables&&variables.length>0){
          metaTag = {attributes:[]};
          for(var i=0;variables[i];i++){
            var attributes = variables[i].match(/([^="'\s]+)=(?:"|')([^'"]+)(?:"|')/);
            if(attributes&&attributes.length>2){
              if(attributes[1]==='name'){
                metaTag.name=attributes[2];
              }
              if(!metaTag.name){
                metaTag.name=attributes[1];
              }
              metaTag.attributes.push({name:attributes[1],value:attributes[2]});
            }
          }
        }
      }
      return metaTag;
    };

    return {
      getMetaTagType:getMetaTagType,
      parseMetaTag:parseMetaTag
    };

  }]);