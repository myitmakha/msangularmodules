'use strict';

angular.module('msSetsRepository').factory('SetsRepository', ['$q', 'ApiRestangular', function ($q, ApiRestangular) {

  var getTimeZones = function () {
    var deferred = $q.defer();
    ApiRestangular.one('sets/timezones').get().then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getCurrencies = function () {
    var deferred = $q.defer();
    ApiRestangular.one('sets/currencies').get().then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getUnits = function () {
    var deferred = $q.defer();
    ApiRestangular.one('sets/units').get().then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var getCountries = function () {
    var deferred = $q.defer();
    ApiRestangular.one('sets/countries').get().then(
      function (response) {
        deferred.resolve(response);
      },
      function (error) {
        deferred.resolve(error);
      }
    );
    return deferred.promise;
  };

  var setsRepository = {
    getTimeZones: getTimeZones,
    getCurrencies: getCurrencies,
    getUnits: getUnits,
    getCountries: getCountries
  };

  return setsRepository;

}]);