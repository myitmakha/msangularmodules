'use strict';

angular.module('msCopyright')
  .directive('msCopyright', ['$compile', function ($compile) {
    return {
      restrict: 'E',
      link: function (scope, element) {
        scope.domains = scope.$parent.$root.siteData.domains;
        scope.settings = scope.$parent.settings || {};
        scope.date = new Date().getFullYear();

        var compileToView = function () {

          var domainName = scope.domains[0].name;
          if (scope.domains.length > 1) {
            domainName = scope.domains[1].name;
          }

          scope.copyrightName = scope.settings.siteName || domainName;

          var html = '<div class="copyright"> &copy; ' + 'Copyright ' + scope.copyrightName + ' ' + scope.date + '<ms-created-by></ms-created-by> </div>';

          $compile(html)(scope, function (html) {
            element.html(html);
          });
        };

        scope.$watch('settings.siteName', function () {
          compileToView();
        });
      }
    };
  }]);
