'use strict';

angular.module('msEditableItemDropdown')
  .directive('editableItemDropdownDirective', ['$compile', '$http', function ($compile, $http) {
    return {
      scope: {
        editable: '='
      },
      template: '<span class="label">{{editable.name}}</span><select ng-model="editable.value" ng-options="option.value as option.name for option in options"></select>',
      restrict: 'A',
      link: function (scope, element, attrs) {
        scope.$watch('editable.Settings.options',function(options){

          if(options){
            scope.options = JSON.parse(options);
            return;
          }
          scope.options = [];
        });
      }
    };
  }]);