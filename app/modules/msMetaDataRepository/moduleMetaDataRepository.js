'use strict';

angular.module('msMetaDataRepository').factory('MetaDataRepository', ['MetaDataRestangular', '$q', function (MetaDataRestangular, $q) {

  var getMetaDataByScopeTypeAndTypeId = function (scope,type,typeId) {
    var deferred = $q.defer();
    MetaDataRestangular.one(scope+'/'+type+'/'+typeId).get().then(function(metaData){
      //console.log removed.
      deferred.resolve(metaData.data);
    },function(error){
      //console.log removed.
      deferred.reject(error);
    });
    return deferred.promise;
  };

  var updateMetaDataProperties = function (scope,type,typeId, properties) {
    var deferred = $q.defer();

    var data = {Scope:scope,Type:type,TypeId:typeId,MetaProperties:properties};
    MetaDataRestangular.all('').customPUT(data).then(function(metaData){
      //console.log removed.
      deferred.resolve(true);
    });
    return deferred.promise;
  };

  var deleteMetaDataProperties = function (scope,type,typeId, properties) {
    var deferred = $q.defer();
    var data = {Scope:scope,Type:type,TypeId:typeId,MetaProperties:properties};
    MetaDataRestangular.all('').customDELETE(data).then(function(metaData){
      //console.log removed.
      deferred.resolve(true);
    });
    return deferred.promise;
  };

  var metaDataRepository = {
    getMetaDataByScopeTypeAndTypeId: getMetaDataByScopeTypeAndTypeId,
    updateMetaDataProperties: updateMetaDataProperties,
    deleteMetaDataProperties: deleteMetaDataProperties
  };
  return metaDataRepository;
}]);
