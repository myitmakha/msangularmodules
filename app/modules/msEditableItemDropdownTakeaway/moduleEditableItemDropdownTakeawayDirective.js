'use strict';

angular.module('msEditableItemDropdownTakeaway')
  .directive('editableItemDropdownTakeawayDirective', ['$compile', '$http', function ($compile, $http) {
    return {
      scope: {
        editable: '='
      },
      template: '<span class="label">{{editable.name}}</span><select ng-model="editable.value" ng-options="option.value as option.name for option in options"></select>',
      restrict: 'A',
      link: function (scope, element, attrs) {
        //$http.get('https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyAdFbMj5fo7GNuAtXSgpPK_zj14UouGcC4');
        scope.options = [
          {
            name:'Above Header',
            value:'above'
          },
          {
            name:'Below Header',
            value:'below'
          },
          {
            name:'Left',
            value:'left'
          },
          {
            name:'Right',
            value:'right'
          }
        ];
      }
    };
  }]);