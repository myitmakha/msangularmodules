'use strict';

angular.module('msBrowserCompatibility')
  .directive('msBrowserCompatibility', ['msBrowserCompatibility', 'ngDialog', function (msBrowserCompatibility,ngDialog) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        isEditable: '='
      },
      template:'<div></div>',
      link: function (scope, element) {
        msBrowserCompatibility.isChrome().then(function(isChrome){
          if(!isChrome){
            scope.settingsModal = function() {
              ngDialog.open({
                'template': 'presentation/pagebuilder/site-settings.modal.tpl.html',
                'scope': scope,
                'className': 'settings tabs'
              });
            };
            scope.settingsModal();
          }
        });
      }
    };
  }]);