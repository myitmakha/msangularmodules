'use strict';

angular.module('msBrowserCompatibility')
  .factory('msBrowserCompatibility', ['$q', function ($q) {

    var getBrowser = function(){
      var deferred = $q.defer();
      var browser={};

      if(navigator&&navigator.userAgent){
        navigator.sayswho= (function(){
          var ua= navigator.userAgent, tem,
            M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
          if(/trident/i.test(M[1])){
            tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
          }
          if(M[1]=== 'Chrome'){
            tem= ua.match(/\bOPR\/(\d+)/);
            if(tem!== null){ return 'Opera '+tem[1];}
          }
          M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
          if((tem= ua.match(/version\/(\d+)/i))!== null){
            M.splice(1, 1, tem[1]);
            return M;
          }
        })();

        //console.log removed.

        if(navigator.sayswho && navigator.sayswho[0]){
          browser.name = navigator.sayswho[0];
          browser.version = navigator.sayswho[1];
        } else {
          browser.name = 'an unsupported browser';
          browser.version = '0';
        }
      }
      deferred.resolve(browser);
      return deferred.promise;

    };

    var isChrome = function(){
      var deferred = $q.defer();
      if(navigator&&navigator.userAgent&&navigator.userAgent.indexOf('Chrome')>0){
        deferred.resolve(true);
      }else{
        deferred.resolve(false);
      }
      return deferred.promise;

    };

    var browserCompatibility = {
      isChrome:isChrome,
      getBrowser:getBrowser
    };

    return browserCompatibility;
  }]);
