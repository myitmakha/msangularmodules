'use strict';

angular.module('msConfig')
.constant('ConfigConstants', {
  'name': '#{name}',
  'apiEndpoint': '#{apiEndpoint}',
  'shopApiEndpoint': '#{shopApiEndpoint}',
  'siteApiEndpoint': '#{siteApiEndpoint}',
  'idServerEndpoint': '#{idServerEndpoint}',
  'themesEndpoint': '#{themesEndpoint}',
  'checkoutEndpoint': '#{checkoutEndpoint}',
  'messagingEndpoint': '#{messagingEndpoint}',
  'modulesDirectory': '#{modulesDirectory}',
  'subDomains': ['www', 'rs', 'ftp']
});